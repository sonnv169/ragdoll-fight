﻿using UnityEngine;

public class AnimatorManager : MonoBehaviour
{
    private static bool IsAnim(Animator animator, string anim)
    {
        bool isResult = false;

        if (animator != null)
        {
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                AnimationClip clip = animator.runtimeAnimatorController.animationClips[i];
                if (clip.name.Equals(anim))
                    isResult = true;
            }
        }

        return isResult;
    }

    public static void Enable(Animator animator, bool isStatus)
    {
        if (animator != null)
            animator.enabled = isStatus;
    }

    public static void Play(Animator animator, string anim)
    {
        if (animator == null || !IsAnim(animator, anim))
            return;
        animator.Play(anim);
    }

    public static void Play(Animator animator, AnimationClip clip)
    {
        if (animator == null || !IsAnim(animator, clip.name))
            return;
        animator.Play(clip.name);
    }

    public static void SetBool(Animator animator, string key, bool isStatus)
    {
        if (animator == null)
            return;
        animator.SetBool(key, isStatus);
    }

    public static void SetFloat(Animator animator, string key, float value)
    {
        if (animator == null)
            return;
        animator.SetFloat(key, value);
    }

    public static void SetInt(Animator animator, string key, float value)
    {
        if (animator == null)
            return;
        animator.SetFloat(key, value);
    }

    public static AnimationClip GetClip(Animator animator, string anim)
    {
        AnimationClip clip = null;

        if (animator != null && IsAnim(animator, anim))
        {
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                if (animator.runtimeAnimatorController.animationClips[i].name.Equals(anim))
                    clip = animator.runtimeAnimatorController.animationClips[i];
            }
        }

        return clip;
    }

    public static float GetLenght(Animator animator, string anim)
    {
        float lenght = 0;

        if (animator != null && IsAnim(animator, anim))
        {
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                AnimationClip clip = animator.runtimeAnimatorController.animationClips[i];
                if (clip.name.Equals(anim))
                    lenght = clip.length;
            }
        }

        return lenght;
    }

    public static float GetSpeed(Animator animator, string anim)
    {
        float speed = 0;

        if (animator != null && IsAnim(animator, anim))
        {
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                AnimationClip clip = animator.runtimeAnimatorController.animationClips[i];
                if (clip.name.Equals(anim))
                    speed = clip.apparentSpeed;
            }
        }

        return speed;
    }
}