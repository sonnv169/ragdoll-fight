using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public enum REWARD_KIND
{
    NONE,
    REWARD_A,
    REWARD_B
}

[System.Serializable]
public enum QUEST_KIND
{
    NONE,
    QUEST_A,
    QUEST_B
}

[System.Serializable]
public class QuestSaved
{
    public QUEST_KIND kind;
    public int collected;
    public int pupose;
    public int reward;
}

[System.Serializable]
public class QuestData
{
    public List<QuestSaved> saveds;
}

[System.Serializable]
public class QuestReward
{
    public REWARD_KIND kind;

    [Tooltip("Reward at first time")] // 
    public int first;

    [Tooltip("Number bonus after reward")] // 
    public int raise;
}

[System.Serializable]
public class QuestBase
{
    public QUEST_KIND kind;
    public Sprite avatar;
    public string story;

    [Tooltip("Pupose at first time")] // 
    public int first;

    [Tooltip("Number bonus after done")] // 
    public int raise;

    public QuestReward reward;
}

[System.Serializable]
public class QuestRewardEvent
{
    public QUEST_KIND kind;
    public int value;
}

public class QuestManager : MonoBehaviour
{
    private QuestData questData;

    public List<QuestBase> bases;

    private void OnEnable()
    {
        Init();
    }

    private void Load()
    {
        if (PlayerPrefs.HasKey("quest_data"))
            questData = XmlPrefs.GetPrefs<QuestData>("quest_data");
    }

    private void Init()
    {
        if (bases.Count <= 0)
            return;
        questData = new QuestData()
        {
            saveds = new List<QuestSaved>()
        };
        for (int i = 0; i < bases.Count; i++)
        {
            QuestSaved save = new QuestSaved()
            {
                kind = bases[i].kind,
                collected = 0,
                pupose = bases[i].first,
                reward = bases[i].reward.first
            };
            questData.saveds.Add(save);
        }

        Load();
    }

    private void Save()
    {
        XmlPrefs.SetPrefs("quest_data", questData);
    }

    public void Collect(QUEST_KIND kind, int value)
    {
        for (int i = 0; i < questData.saveds.Count; i++)
        {
            if (!questData.saveds[i].kind.Equals(kind))
                return;
            questData.saveds[i].collected += value;
            Save();
        }
    }

    public bool IsDone(QUEST_KIND kind)
    {
        bool isResult = false;
        for (int i = 0; i < questData.saveds.Count; i++)
        {
            if (!questData.saveds[i].kind.Equals(kind))
                continue;
            int collected = questData.saveds[i].collected;
            int pupose = questData.saveds[i].pupose;
            if (collected < pupose)
                continue;
            isResult = true;
        }

        return isResult;
    }

    public void Reward(QUEST_KIND kind)
    {
        for (int i = 0; i < questData.saveds.Count; i++)
        {
            if (!questData.saveds[i].kind.Equals(kind) ||
                questData.saveds[i].collected < questData.saveds[i].pupose)
                continue;
            Eventon<QuestRewardEvent>.Trigger(new QuestRewardEvent()
            {
                kind = kind,
                value = questData.saveds[i].reward
            });
            questData.saveds[i].pupose += bases[i].raise;
            questData.saveds[i].reward += bases[i].reward.raise;
            Save();
        }
    }
}