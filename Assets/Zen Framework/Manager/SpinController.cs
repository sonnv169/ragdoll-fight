﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[System.Serializable]
public class BoderPosition
{
    public float xPosition;
    public float yPosition;
    public float zPosition;
}

public class SpinController : MonoBehaviour
{
    private BoderPosition boderPosition;

    private float currentUpdateTime = 0.0f;

    private int targetPoint = 0;
    private int currentPoint = 0;
    private int currentSlot = 0;
    private int minPoint = 0;

    public float updateTime = 0.01f;

    public Vector2 varianceRound;

    public Transform slotBoder;
    public Transform container;

    public Text txtCurrentPlayTurn;
    public Text txtResetTime;
    public Button btnStart;
    public Button btnMoreTurn;
    public Button btnClose;

    private void OnEnable()
    {
        LoadBoder();
        Default();

        if (btnClose != null)
            btnClose.interactable = true;
    }

    private void Update()
    {
        Display();
    }

    private void LoadBoder()
    {
        slotBoder.position = container.GetChild(0).position;
        boderPosition = new BoderPosition();
        if (!PlayerPrefs.HasKey("border_position"))
            return;
        boderPosition = XmlPrefs.GetPrefs<BoderPosition>("border_position");
        slotBoder.position =
            new Vector3(boderPosition.xPosition, boderPosition.yPosition, boderPosition.zPosition);
    }

    private void SaveBoder()
    {
        boderPosition.xPosition = slotBoder.position.x;
        boderPosition.yPosition = slotBoder.position.y;
        boderPosition.zPosition = slotBoder.position.z;
        XmlPrefs.SetPrefs("border_position", boderPosition);
    }

    private void Default()
    {
        minPoint = container.childCount;
        btnStart.interactable = false;
        btnMoreTurn.interactable = false;

        if (FortuneManager.Instance == null)
            return;
        FortuneManager fortuneManager = FortuneManager.Instance;
        FortunesData data = fortuneManager.GetData();
        if (data.turn > 0)
            btnStart.interactable = true;
        else
            btnMoreTurn.interactable = true;
    }

    private IEnumerator SpinAgain(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        Spining();
    }

    private void Spining()
    {
        if (currentPoint < targetPoint)
        {
            currentPoint += 1;
            currentUpdateTime += updateTime;
            currentSlot = 0;
            if (currentSlot < minPoint - 1)
                currentSlot += 1;
            slotBoder.position = container.GetChild(currentSlot).transform.position;
            StartCoroutine(SpinAgain(currentUpdateTime));
        }
        else
        {
            SaveBoder();
            if (FortuneManager.Instance == null)
                return;
            FortuneManager fortuneManager = FortuneManager.Instance;
            fortuneManager.Reward();
            if (fortuneManager.GetData().turn <= 0)
                return;
            btnStart.interactable = true;
        }
    }

    public void StartSpin()
    {
        if (FortuneManager.Instance == null)
            return;
        FortuneManager fortuneManager = FortuneManager.Instance;
        FortunesData data = fortuneManager.GetData();

        if (data.turn <= 0)
            return;
        currentUpdateTime = updateTime;
        currentPoint = 0;
        fortuneManager.FindReward();
        int rangePoint = (minPoint + fortuneManager.GetNowIndex()) - currentSlot;
        int randomResult = Random.Range((int) varianceRound.x, (int) varianceRound.y);
        targetPoint = (minPoint * randomResult) + rangePoint;
        btnStart.interactable = false;
        btnMoreTurn.interactable = false;
        Spining();
    }

    private void Display()
    {
        if (FortuneManager.Instance == null)
            return;
        FortuneManager fortuneManager = FortuneManager.Instance;
        FortunesData data = fortuneManager.GetData();

        if (data.turn > 0)
        {
            btnStart.interactable = true;
            btnMoreTurn.interactable = false;
        }
        else
            btnMoreTurn.interactable = true;

        if (data.second > 0)
        {
            int minute = (int) data.second / 60;
            int second = (int) data.second % 60;
            txtResetTime.text = "Reset Minute \n" + minute + ":" + second;
        }
        else
            txtResetTime.text = null;

        txtCurrentPlayTurn.text = "Play Turn \n" + data.turn;
    }

    public void ShowAdVideoReward()
    {
        if (FortuneManager.Instance != null)
            FortuneManager.Instance.Reset();
    }
}