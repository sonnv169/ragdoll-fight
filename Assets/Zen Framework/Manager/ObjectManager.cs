﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviour
{
    private Type type;
    private Transform pool;
    private Vector3 scale;
    private string name;

    public void Init(Type type, Transform pool)
    {
        this.type = type;
        this.pool = pool;
        this.scale = transform.localScale;
        this.name = gameObject.name;
    }

    public void Return()
    {
        if (pool != null)
        {
            transform.position = pool.transform.position;
            transform.SetParent(pool.transform, true);
        }

        transform.localScale = scale;
        gameObject.SetActive(false);
    }

    public Type GetTypeOf()
    {
        return type;
    }

    public string GetName()
    {
        return name;
    }
}

public class ObjectManager : MonoBehaviour
{
    private static List<ObjectPooling> objs = new List<ObjectPooling>();

    public static GameObject Init<T>(Transform pool, Transform local, string path, bool isActive = false)
    {
        GameObject prefab = Resources.Load<GameObject>(path);
        GameObject clone = null;

        if (prefab != null)
        {
            clone = Instantiate(prefab, pool);
            ObjectPooling objectPooling = clone.AddComponent<ObjectPooling>();
            objectPooling.Init(typeof(T), pool);
            objectPooling.transform.position = local.position;
            objectPooling.gameObject.SetActive(isActive);
            if (!objs.Contains(objectPooling))
                objs.Add(objectPooling);
        }

        return clone;
    }

    public static GameObject Call<T>(Transform pool, Transform local, string path, bool isActive = true)
    {
        List<ObjectPooling> readys = new List<ObjectPooling>();
        GameObject prefab = Resources.Load<GameObject>(path);
        GameObject clone = null;

        if (prefab != null)
        {
            for (int i = 0; i < objs.Count; i++)
            {
                if (objs[i] != null)
                {
                    if (objs[i].GetTypeOf() != typeof(T) ||
                        objs[i].GetName() != prefab.name ||
                        objs[i].gameObject.activeInHierarchy)
                        continue;
                    readys.Add(objs[i]);
                }
                else
                    objs.Remove(objs[i]);
            }

            if (readys.Count <= 0)
            {
                GameObject additional = Instantiate(prefab, pool);
                ObjectPooling objectPooling = additional.AddComponent<ObjectPooling>();
                objectPooling.Init(typeof(T), pool);
                objectPooling.transform.position = local.position;
                objs.Add(objectPooling);
                readys.Add(objectPooling);
            }

            readys[0].transform.position = local.position;
            readys[0].gameObject.SetActive(isActive);
            clone = readys[0].gameObject;
        }

        return clone;
    }
}