﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class FortunesRewards
{
    public Sprite sprPreview;

    [Tooltip("% rate")] //
    public float rate;

    public int value;
}

[System.Serializable]
public class FortunesData
{
    public int turn;
    public float second;
}

public class FortuneManager : MonoBehaviour
{
    private static FortuneManager mInstance;
    private FortunesData fortuneData;
    private int index;

    public int playTurn = 5;
    public float resetMinute = 5.0f;

    public FortunesRewards[] rewards;

    #region property

    public static FortuneManager Instance
    {
        get
        {
            if (mInstance == null)
                mInstance = FindObjectOfType<FortuneManager>();
            return mInstance;
        }
    }

    #endregion

    private void OnEnable()
    {
        Init();
    }

    private IEnumerator Cooldown(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        fortuneData.second -= 1;
        if (fortuneData.second <= 0)
            Reset();
        Save();
    }

    private void Load()
    {
        if (PlayerPrefs.HasKey("fortune_data"))
            fortuneData = XmlPrefs.GetPrefs<FortunesData>("fortune_data");
        if (fortuneData.second > 0)
            StartCoroutine(Cooldown(1.0f));
    }

    private void Init()
    {
        fortuneData = new FortunesData()
        {
            turn = playTurn,
            second = 0
        };
        Load();
    }

    private void Save()
    {
        XmlPrefs.SetPrefs("fortune_data", fortuneData);
    }

    public void AddTurn(int value)
    {
        fortuneData.turn += value;
        Save();
    }

    private void PayTurn(int value)
    {
        fortuneData.turn -= value;
        if (fortuneData.turn <= 0)
            fortuneData.second = 60 * resetMinute;
        Save();
    }

    public void Reset()
    {
        fortuneData.turn = playTurn;
        fortuneData.second = 0;
        Save();
        Eventon<bool>.Trigger(true);
    }

    public FortunesData GetData()
    {
        FortunesData cloneData = new FortunesData();
        if (fortuneData != null)
            cloneData = fortuneData;
        return cloneData;
    }

    public int GetNowIndex()
    {
        return index;
    }

    public float GetPercent(int index)
    {
        float percent = 0;
        if (index < rewards.Length)
            percent = rewards[index].rate;
        return percent;
    }

    public void FindReward()
    {
        List<float> rates = new List<float>();
        for (int i = 0; i < rewards.Length; i++)
        {
            float rate = Random.Range(0, GetPercent(i));
            if (rates.Contains(rate))
                continue;
            rates.Add(rate);
        }

        if (rates.Count <= 0)
            return;
        float maxRate = rates.Max();
        for (int i = 0; i < rates.Count; i++)
        {
            if (rates[i] < maxRate)
                continue;
            index = i;
            rates.Clear();
        }

        Eventon<bool>.Trigger(true);
    }

    public void Reward()
    {
#if UNITY_EDITOR
        Debug.Log("Reward: " + index);
#endif
        PayTurn(1);
        Eventon<bool>.Trigger(false);

        //reward
    }
}