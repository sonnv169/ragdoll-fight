﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum CHECKIN_KIND
{
    NORMAL,
    SPECIAL
}

[System.Serializable]
public class CheckInReward
{
    public string name;
    public CHECKIN_KIND kind;
    public Sprite sprAvatar;
    public int value;
    public int day;
}

[System.Serializable]
public class CheckInRewardData
{
    public int day;
    public TimeOfYear time;
    public List<int> checkeds = new List<int>();
}

public class CheckInRewardManager : MonoBehaviour
{
    private static CheckInRewardManager mInstance;
    private CheckInRewardData checkinData;

    public int maxDay;
    public CheckInReward[] rewards;

    #region property

    public static CheckInRewardManager Instance
    {
        get
        {
            if (mInstance == null)
                mInstance = FindObjectOfType<CheckInRewardManager>();
            return mInstance;
        }
    }

    #endregion

    private void OnEnable()
    {
        Init();
    }

    private void Load()
    {
        if (PlayerPrefs.HasKey("checkin_data"))
            checkinData = XmlPrefs.GetPrefs<CheckInRewardData>("checkin_data");
    }

    private void Init()
    {
        checkinData = new CheckInRewardData()
        {
            day = 0
        };
        checkinData.time = new TimeOfYear()
        {
            day = DateTime.Now.Day,
            month = DateTime.Now.Month,
            year = DateTime.Now.Year
        };

        Load();
        UpDay(1, true);
    }

    private void Save()
    {
        XmlPrefs.SetPrefs("checkin_data", checkinData);
    }

    private void UpDay(int value, bool isLoop)
    {
        if (checkinData == null ||
            DateTime.Now.Day <= checkinData.time.day)
            return;
        checkinData.day += value;
        if (checkinData.day >= maxDay)
        {
            if (!isLoop)
                return;
            checkinData.day = 1;
        }

        Save();
    }

    public void Reward(int index)
    {
        if (IsRewarded(index))
            return;
        checkinData.checkeds.Add(rewards[index].day);
        Save();
        Eventon<bool>.Trigger(true);
    }

    public bool IsRewarded(int index)
    {
        return checkinData != null &&
               checkinData.checkeds.Count > 0 &&
               index < checkinData.checkeds.Count &&
               checkinData.checkeds.Contains(rewards[index].day);
    }

    public int GetRewardCount()
    {
        int number = rewards.Length;
        return number;
    }

    public CheckInReward GetReward(int index)
    {
        CheckInReward reward = new CheckInReward();
        if (index < rewards.Length)
        {
            reward.kind = rewards[index].kind;
            reward.name = rewards[index].name;
            reward.sprAvatar = rewards[index].sprAvatar;
            reward.value = rewards[index].value;
            reward.day = rewards[index].day;
        }

        return rewards[index];
    }

    public CheckInRewardData GetData()
    {
        CheckInRewardData cloneData = new CheckInRewardData()
        {
            day = 0
        };
        cloneData.time = new TimeOfYear()
        {
            day = DateTime.Now.Day,
            month = DateTime.Now.Month,
            year = DateTime.Now.Year
        };
        if (checkinData != null)
        {
            cloneData = checkinData;
        }

        return cloneData;
    }
}