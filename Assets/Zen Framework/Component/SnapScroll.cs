using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SnapScroll : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public enum SCROLL_KIND
    {
        HORIZONTAL,
        VERTICAL
    }

    private enum SCROLL_STATE
    {
        NONE,
        MOVING,
        SNAPPING
    }

    private SCROLL_STATE state;
    private float moveSpeed;
    private int snap;
    private Vector2 clickPoint;
    private Vector2 dragPoint;
    private Vector2 direction;

    public SCROLL_KIND kind;
    public float speed = 1.0f;
    public float margin = 150.0f;
    public float scale = 0.5f;
    public int contents = 10;
    public Canvas canvas;
    public Transform grid;
    public GameObject content;

    private void OnEnable()
    {
        Init();
    }

    private void Update()
    {
        Move();
    }

    private void Init()
    {
        if (grid == null ||
            content == null ||
            grid.childCount >= contents)
            return;
        for (int i = 0; i < contents; i++)
        {
            GameObject obj = Instantiate(content, grid);
            obj.SetActive(true);
        }
    }

    private void Sort()
    {
        if (grid == null ||
            grid.childCount <= 0)
            return;
        float marginScale = 1.0f;
        if (canvas != null)
        {
            Vector2 resolution = canvas.GetComponent<CanvasScaler>().referenceResolution;
            switch (kind)
            {
                case SCROLL_KIND.HORIZONTAL:
                    marginScale = Screen.width / resolution.x;
                    break;
                case SCROLL_KIND.VERTICAL:
                    marginScale = Screen.height / resolution.y;
                    break;
            }
        }

        for (int i = 0; i < grid.childCount; i++)
        {
            if (i.Equals(0))
                continue;
            switch (kind)
            {
                case SCROLL_KIND.HORIZONTAL:
                    grid.GetChild(i).position = new Vector2(
                        grid.GetChild(i - 1).position.x + margin * marginScale,
                        grid.GetChild(0).position.y);
                    break;
                case SCROLL_KIND.VERTICAL:
                    grid.GetChild(i).position = new Vector2(
                        grid.GetChild(0).position.x,
                        grid.GetChild(i - 1).position.y - margin * marginScale);
                    break;
            }
        }

        List<float> distances = new List<float>();
        for (int i = 0; i < grid.childCount; i++)
        {
            float distance = Vector2.Distance(
                grid.GetChild(i).position,
                grid.position);
            distances.Add(distance);
        }

        if (distances.Count > 0)
        {
            for (int i = 0; i < distances.Count; i++)
            {
                if (!distances[i].Equals(distances.Min()))
                    continue;
                snap = i;
            }
        }

        float originScale = 0;
        switch (kind)
        {
            case SCROLL_KIND.HORIZONTAL:
                originScale = content.transform.localScale.x;
                break;
            case SCROLL_KIND.VERTICAL:
                originScale = content.transform.localScale.y;
                break;
        }

        for (int i = 0; i < grid.childCount; i++)
        {
            if (i.Equals(snap))
            {
                grid.GetChild(i).localScale = new Vector2(
                    originScale + scale,
                    originScale + scale);
            }
            else
                grid.GetChild(i).localScale = new Vector2(originScale, originScale);
        }
    }

    private void SetState(SCROLL_STATE param, float value)
    {
        state = param;
        moveSpeed = value;
    }

    private bool IsClamp()
    {
        bool isResult = false;
        if (grid != null &&
            grid.childCount > 0)
        {
            switch (kind)
            {
                case SCROLL_KIND.HORIZONTAL:
                    if (grid.GetChild(0).position.x >= grid.position.x ||
                        grid.GetChild(grid.childCount - 1).position.x <= grid.position.x)
                    {
                        isResult = true;
                    }

                    break;
                case SCROLL_KIND.VERTICAL:
                    if (grid.GetChild(0).position.y <= grid.position.y ||
                        grid.GetChild(grid.childCount - 1).position.y >= grid.position.y)
                    {
                        isResult = true;
                    }

                    break;
            }
        }

        return isResult;
    }

    private void Move()
    {
        Sort();
        if (grid == null ||
            grid.childCount <= 0 ||
            direction.Equals(Vector2.zero))
            return;
        switch (kind)
        {
            case SCROLL_KIND.HORIZONTAL:
                direction = new Vector2(direction.x, 0);
                break;
            case SCROLL_KIND.VERTICAL:
                direction = new Vector2(0, direction.y);
                break;
        }

        for (int i = 0; i < grid.childCount; i++)
        {
            grid.GetChild(i).Translate(direction * moveSpeed * Time.deltaTime);
        }

        switch (state)
        {
            case SCROLL_STATE.MOVING:

                if (moveSpeed > 0)
                    moveSpeed -= Time.deltaTime;
                else
                {
                    clickPoint = Vector2.zero;
                    direction = grid.position - grid.GetChild(snap).position;
                    if (IsClamp())
                        direction = new Vector2(direction.x * 2.0f, direction.y * 2.0f);
                    SetState(SCROLL_STATE.SNAPPING, speed / 2);
                }

                break;
            case SCROLL_STATE.SNAPPING:
                Vector2 face = grid.position - grid.GetChild(snap).position;
                float distance = Vector2.Distance(grid.GetChild(snap).position, grid.position);
                if (direction.x < 0 && face.x < 0 && distance > 0.1f ||
                    direction.x > 0 && face.x > 0 && distance > 0.1f)
                    return;
                SetState(SCROLL_STATE.NONE, 0);
                break;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (clickPoint.Equals(Vector2.zero))
        {
            clickPoint = eventData.position;
        }
        else
        {
            dragPoint = eventData.position;
            direction = dragPoint - clickPoint;
            SetState(SCROLL_STATE.MOVING, speed);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (clickPoint.Equals(Vector2.zero))
            return;
        direction = eventData.position - clickPoint;
        clickPoint = Vector2.zero;
        SetState(SCROLL_STATE.MOVING, speed);
    }

    public void Clear()
    {
        for (int i = 0; i < grid.childCount; i++)
        {
            Destroy(grid.GetChild(i));
        }
    }

    public void ReInit()
    {
        Clear();
        Init();
    }

    public void SetSnap(int value)
    {
        snap = value;
    }

    public int GetSnap()
    {
        int number = snap;
        return number;
    }
}