﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Joystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    private Vector3 inputDirection;

    [SerializeField] private Image imgJoystick;
    [SerializeField] private Image imgHandle;

    [SerializeField] [Tooltip("% Clamp handle position with joystick size")] [Range(0, 100.0f)]
    private float clampPercent = 90.0f;

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 position;
        if (imgJoystick == null ||
            imgHandle == null)
            return;
        Vector2 joystickSize = new Vector2(
            imgJoystick.rectTransform.sizeDelta.x,
            imgJoystick.rectTransform.sizeDelta.y);
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
            imgJoystick.rectTransform,
            eventData.position,
            eventData.pressEventCamera, out position))
        {
            inputDirection = new Vector3(position.x, 0, position.y);
            Vector3 anchoredDirection = inputDirection.normalized;
            imgHandle.rectTransform.anchoredPosition = new Vector3(
                anchoredDirection.x * (joystickSize.x * (100.0f - clampPercent) / 100.0f),
                anchoredDirection.z * (joystickSize.y * (100.0f - clampPercent) / 100.0f), 0);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (imgHandle == null)
            return;
        inputDirection = Vector3.zero;
        imgHandle.rectTransform.anchoredPosition = Vector3.zero;
    }

    public Vector3 GetInputDirection()
    {
        return inputDirection;
    }
}