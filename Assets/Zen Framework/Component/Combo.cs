using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Combo : MonoBehaviour
{
    private enum COMBO_STATE
    {
        READY,
        COMBO
    }

    private COMBO_STATE state;
    private Coroutine playCoroutine;
    private List<int> anims = new List<int>();
    private int combo;
    private float holdTime;

    public float comboTime = 1.0f;
    public Animator animator;
    public AnimationClip animationDefault;
    public AnimationClip[] animationCombos;

    private void Update()
    {
        CountDown();
    }

    private void CountDown()
    {
        if (!state.Equals(COMBO_STATE.COMBO))
            return;
        if (holdTime > 0)
            holdTime -= Time.deltaTime;
        else
            Reset();
    }

    private IEnumerator IPlay(float time)
    {
        yield return new WaitForSeconds(time);
        if (anims.Count > 0)
        {
            int min = anims.Min();
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                string name = animator.runtimeAnimatorController.animationClips[i].name;
                if (!name.Equals(animationCombos[min].name))
                    continue;
                holdTime = comboTime;
                animator.Play(animationCombos[min].name);
                playCoroutine = StartCoroutine(IPlay(animationCombos[min].length));
                anims.Remove(min);
            }
        }
        else
        {
            if (playCoroutine != null)
                StopCoroutine(playCoroutine);
        }
    }

    public void Input()
    {
        if (combo >= animationCombos.Length)
            return;
        anims.Add(combo);
        combo += 1;

        if (state.Equals(COMBO_STATE.COMBO) ||
            animator == null ||
            animationCombos.Length <= 0)
            return;
        state = COMBO_STATE.COMBO;
        playCoroutine = StartCoroutine(IPlay(0));
    }

    public void Cancel()
    {
        if (playCoroutine != null)
            StopCoroutine(playCoroutine);

        state = COMBO_STATE.READY;
        anims.Clear();
        holdTime = 0;
        combo = 0;
    }

    public void Reset()
    {
        Cancel();
        if (animationDefault == null)
            return;
        for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
        {
            string name = animator.runtimeAnimatorController.animationClips[i].name;
            if (!name.Equals(animationDefault.name))
                continue;
            animator.Play(animationDefault.name);
        }
    }
}