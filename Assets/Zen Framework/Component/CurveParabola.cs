﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum PARABOLA_KIND
{
    RIGHT_X,
    LEFT_X,
    UP_Y,
    DOWN_Y,
    FORWARD_Z,
    BACK_Z
}

[System.Serializable]
public class ParabolaBase
{
    public PARABOLA_KIND kind;
    public Transform transformer;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public float time;
    public float speed;
}

public class CurveParabola : MonoBehaviour
{
    private List<ParabolaBase> parabolaBases = new List<ParabolaBase>();

    [SerializeField] private AnimationCurve animationCurve;

    private void Update()
    {
        Translating();
    }

    private void Translating()
    {
        if (parabolaBases.Count <= 0)
            return;
        for (int i = 0; i < parabolaBases.Count; i++)
        {
            if (parabolaBases[i].transformer != null)
            {
                if (parabolaBases[i].transformer.position != parabolaBases[i].endPosition)
                {
                    parabolaBases[i].time += (parabolaBases[i].speed * Time.deltaTime);
                    Vector3 lerp = Vector3.Lerp(parabolaBases[i].startPosition, parabolaBases[i].endPosition,
                        parabolaBases[i].time);
                    switch (parabolaBases[i].kind)
                    {
                        case PARABOLA_KIND.RIGHT_X:
                            lerp.x += animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case PARABOLA_KIND.LEFT_X:
                            lerp.x -= animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case PARABOLA_KIND.UP_Y:
                            lerp.y += animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case PARABOLA_KIND.DOWN_Y:
                            lerp.y -= animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case PARABOLA_KIND.FORWARD_Z:
                            lerp.z += animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                        case PARABOLA_KIND.BACK_Z:
                            lerp.z -= animationCurve.Evaluate(parabolaBases[i].time);
                            break;
                    }

                    parabolaBases[i].transformer.position = lerp;
                }
                else
                    parabolaBases.Remove(parabolaBases[i]);
            }
            else
                parabolaBases.Remove(parabolaBases[i]);
        }
    }

    public void AddBase(PARABOLA_KIND kind, Transform transformer, Vector3 endPosition, float speed)
    {
        if (transformer == null)
            return;
        ParabolaBase newBase = new ParabolaBase()
        {
            kind = kind,
            transformer = transformer,
            startPosition = transformer.position,
            endPosition = endPosition,
            time = 0,
            speed = speed
        };

        parabolaBases.Add(newBase);
    }

    public void RemoveBase(Transform transformer)
    {
        for (int i = 0; i < parabolaBases.Count; i++)
        {
            if (parabolaBases[i].transformer.Equals(transformer))
                parabolaBases.Remove(parabolaBases[i]);
        }
    }

    public void ClearBases()
    {
        parabolaBases.Clear();
    }
}