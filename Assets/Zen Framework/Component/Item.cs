﻿using UnityEngine;

public interface IItemsManager
{
    void Init();
    void Load();
    void Save();
    void Select(int index);
    void Unlock(int index);
    void Upgrade(int index);
    void Buy(int index);
    void Sell(int index);
    void Destroy(int index);
    int Broken(int index);
}

[System.Serializable]
public class Clip
{
    [Tooltip("Number of clip")] //
    public float capacity;

    [Tooltip("Number of bag")] // 
    public float unit;

    [Tooltip("% up with upgrade")] //
    public float raise;
}

[System.Serializable]
public class Attack
{
    public float hit;
    public float speed;

    [Tooltip("% up with upgrade")] //
    public float raise;
}

[System.Serializable]
public class Reload
{
    public float speed;

    [Tooltip("% up with upgrade")] //
    public float raise;
}

[System.Serializable]
public class Guard
{
    public float hp;
    public float mp;
    public float def;
    public float intel;

    [Tooltip("% up with upgrade")] //
    public float raise;
}

[System.Serializable]
public class Bonus
{
    public float percent;

    [Tooltip("% up with upgrade")] //
    public float raise;
}

[System.Serializable]
public class Enduring
{
    public float now;
    public float total;
}

[System.Serializable]
public class Upgrade
{
    public float price;
}

[System.Serializable]
public class Core
{
    public string name;

    [Tooltip("Level to use")] //
    public int level;

    public int number;
}

public abstract class Item : MonoBehaviour
{
    public string name;

    [Tooltip("Price at shop")] //
    public float price;

    [Tooltip("% receive fater sell")] //
    public float sellReceive;

    [Tooltip("Weight to speed down")] //
    public float weight;

    [Tooltip("Level to use")] //
    public int level;

    public string GetName()
    {
        string cloneName = name;
        return cloneName;
    }

    public float GetSellReceive()
    {
        float number = sellReceive;
        return number;
    }

    public float GetWeight()
    {
        float number = weight;
        return number;
    }

    public int GetLevel()
    {
        int number = level;
        return number;
    }
}