using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class BagSlot
{
    public string name;
    public int level;
    public GameObject item;
}

[System.Serializable]
public class BagData
{
    public List<BagSlot> slots = new List<BagSlot>();
}

public class Bag : MonoBehaviour
{
    private static Bag mInstance;

    private BagData bagData;

    #region property

    public static Bag Instance
    {
        get
        {
            if (mInstance == null)
                mInstance = FindObjectOfType<Bag>();
            return mInstance;
        }
    }

    #endregion

    private void OnEnable()
    {
        Init();
    }

    private void Init()
    {
        bagData = new BagData();
        Load();
    }

    private void Load()
    {
        if (PlayerPrefs.HasKey("bag_data"))
            bagData = XmlPrefs.GetPrefs<BagData>("bag_data");
    }

    private void Save()
    {
        XmlPrefs.SetPrefs("bag_data", bagData);
        Load();
    }

    public bool IsContains(BagSlot slot)
    {
        return bagData != null &&
               bagData.slots.Contains(slot);
    }

    public void Storage(string text, int value, GameObject obj)
    {
        BagSlot slot = new BagSlot()
        {
            name = text,
            level = value,
            item = obj
        };

        if (bagData.slots.Contains(slot))
            return;
        bagData.slots.Add(slot);
        Save();
    }

    public void Remove(GameObject obj)
    {
        for (int i = 0; i < bagData.slots.Count; i++)
        {
            if (!bagData.slots[i].item.Equals(obj))
                continue;
            bagData.slots.Remove(bagData.slots[i]);
        }

        Save();
    }

    public BagSlot GetSlot(int index)
    {
        BagSlot slot = new BagSlot();
        if (index < bagData.slots.Count)
            slot = bagData.slots[index];
        return slot;
    }
}