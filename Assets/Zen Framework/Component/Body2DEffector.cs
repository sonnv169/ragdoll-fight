using UnityEngine;

[System.Serializable]
public enum BODY_KIND
{
    DYNAMIC,
    KINEMATIC,
    STATIC
}

[System.Serializable]
public enum REFELCT_KIND
{
    NONE,
    TOP_TO_BOTTOM,
    LEFT_TO_RIGHT,
    RIGHT_TO_LEFT,
    BOTTOM_TO_TOP,
}

[System.Serializable]
public enum VECTOR
{
    ALL,
    X,
    Y,
    Z
}

public abstract class Body2DEffector : MonoBehaviour
{
    public static void ChangeType(Rigidbody2D body2D, BODY_KIND kind)
    {
        if (body2D == null)
            return;
        switch (kind)
        {
            case BODY_KIND.DYNAMIC:
                body2D.bodyType = RigidbodyType2D.Dynamic;
                break;
            case BODY_KIND.KINEMATIC:
                body2D.bodyType = RigidbodyType2D.Kinematic;
                break;
            case BODY_KIND.STATIC:
                body2D.bodyType = RigidbodyType2D.Static;
                break;
        }
    }

    public static void ChangeGravity(Rigidbody2D body2D, float gravity)
    {
        if (body2D != null)
            body2D.gravityScale = gravity;
    }

    public static void Reflect(Rigidbody2D body2D, REFELCT_KIND refelctKind, float power)
    {
        if (body2D == null)
            return;
        switch (refelctKind)
        {
            case REFELCT_KIND.TOP_TO_BOTTOM:
                body2D.velocity = new Vector2(body2D.velocity.x, -power);
                break;
            case REFELCT_KIND.LEFT_TO_RIGHT:
                body2D.velocity = new Vector2(power, body2D.velocity.y);
                break;
            case REFELCT_KIND.RIGHT_TO_LEFT:
                body2D.velocity = new Vector2(-power, body2D.velocity.y);
                break;
            case REFELCT_KIND.BOTTOM_TO_TOP:
                body2D.velocity = new Vector2(body2D.velocity.x, power);
                break;
        }
    }

    public static void Constraints(Rigidbody2D body2D, VECTOR vector)
    {
        if (body2D == null)
            return;
        switch (vector)
        {
            case VECTOR.ALL:
                body2D.velocity = new Vector2(0, 0);
                body2D.transform.rotation = Quaternion.identity;
                break;
            case VECTOR.X:
                body2D.velocity = new Vector2(0, body2D.velocity.y);
                break;
            case VECTOR.Y:
                body2D.velocity = new Vector2(body2D.velocity.x, 0);
                break;
            case VECTOR.Z:
                body2D.transform.rotation = Quaternion.identity;
                break;
        }
    }

    public static void Velocity(Rigidbody2D body2D, Vector2 direction, float power)
    {
        if (body2D != null && body2D.bodyType != RigidbodyType2D.Static)
            body2D.velocity = direction * power;
    }

    public static void Velocity(Rigidbody2D body2D, VECTOR vector, float power)
    {
        if (body2D == null || body2D.bodyType == RigidbodyType2D.Static)
            return;
        switch (vector)
        {
            case VECTOR.ALL:
                body2D.velocity = new Vector2(power, power);
                break;
            case VECTOR.X:
                body2D.velocity = new Vector2(power, body2D.velocity.y);
                break;
            case VECTOR.Y:
                body2D.velocity = new Vector2(body2D.velocity.x, power);
                break;
        }
    }

    public static void Velocity(Rigidbody2D body2D, float powerX, float powerY)
    {
        if (body2D != null && body2D.bodyType != RigidbodyType2D.Static)
            body2D.velocity = new Vector2(powerX, powerY);
    }

    public static void ClampVelocity(Rigidbody2D body2D, float max)
    {
        if (body2D == null)
            return;
        if (body2D.velocity.x >= max)
            body2D.velocity = new Vector2(max, body2D.velocity.y);
        else if (body2D.velocity.x <= -max)
            body2D.velocity = new Vector2(-max, body2D.velocity.y);

        if (body2D.velocity.y >= max)
            body2D.velocity = new Vector2(body2D.velocity.x, max);
        else if (body2D.velocity.y <= -max)
            body2D.velocity = new Vector2(body2D.velocity.x, -max);
    }

    public static void Destroy(Rigidbody2D body2D)
    {
        if (body2D != null)
            Object.Destroy(body2D);
    }

    public static Rigidbody2D GetBody(GameObject obj)
    {
        Rigidbody2D body = null;
        if (obj != null &&
            obj.GetComponent<Rigidbody2D>())
            body = obj.GetComponent<Rigidbody2D>();
        return body;
    }
}