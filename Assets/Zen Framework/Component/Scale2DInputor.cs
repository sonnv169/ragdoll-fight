﻿using UnityEngine;

public class Scale2DInputor : MonoBehaviour
{
    private Vector2 firstTouch;
    private Vector2 secondTouch;
    private Vector2 firstDrag;
    private Vector2 secondDrag;

    [SerializeField] private Transform scalingModel;

    [Header("Editor mode")] [SerializeField]
    private Transform firstPoint;

    [SerializeField] private Transform secondPoint;

    [Header("Variable")] [SerializeField] private float scalingSpeed;
    [SerializeField] private float min;
    [SerializeField] private float max;

    private void OnEnable()
    {
        Init();
    }

    private void Update()
    {
        OnScale();
    }

    private void Init()
    {
        firstTouch = Vector2.zero;
        firstDrag = Vector2.zero;
        secondTouch = Vector2.zero;
        secondDrag = Vector2.zero;
    }

    private void ScaleUp(VECTOR vector)
    {
        Vector2 scale = scalingModel.localScale;
        switch (vector)
        {
            case VECTOR.X:
                scale.x += scalingSpeed;
                if (scale.x >= max)
                    scale.x = max;
                break;
            case VECTOR.Y:
                scale.y += scalingSpeed;
                if (scale.y >= max)
                    scale.y = max;
                break;
        }

        scalingModel.localScale = scale;
    }

    private void ScaleDown(VECTOR vector)
    {
        Vector2 scale = scalingModel.localScale;
        switch (vector)
        {
            case VECTOR.X:
                scale.x -= scalingSpeed;
                if (scale.x <= min)
                    scale.x = min;
                break;
            case VECTOR.Y:
                scale.y -= scalingSpeed;
                if (scale.y <= min)
                    scale.y = min;
                break;
        }

        scalingModel.localScale = scale;
    }

    private void Scaling()
    {
        if (firstDrag.y > firstTouch.y && secondDrag.y > secondTouch.y ||
            firstDrag.y < firstTouch.y && secondDrag.y < secondTouch.y ||
            firstDrag.x > firstTouch.x && secondDrag.x > secondTouch.x ||
            firstDrag.x < firstTouch.x && secondDrag.x < secondTouch.x)
            return;

        if (firstDrag.y > firstTouch.y)
        {
            ScaleUp(VECTOR.Y);
            if (firstDrag.x > firstTouch.x || firstDrag.x < firstTouch.x)
                ScaleUp(VECTOR.X);
        }
        else if (firstDrag.y < firstTouch.y)
        {
            ScaleDown(VECTOR.Y);
            if (firstDrag.x < firstTouch.x || firstDrag.x > firstTouch.x)
                ScaleDown(VECTOR.X);
        }
        else if (firstDrag.y.Equals(firstTouch.y))
        {
            if (firstDrag.x > firstTouch.x)
                ScaleUp(VECTOR.X);
            else if (firstDrag.x < firstTouch.x)
                ScaleDown(VECTOR.X);
        }

        firstTouch = firstDrag;
        secondTouch = secondDrag;
    }

    private void OnScale()
    {
        if (scalingModel == null || firstPoint == null || secondPoint == null)
            return;

        secondPoint.position = new Vector2(-firstPoint.position.x, -firstPoint.position.y);
        firstDrag = firstPoint.position;
        secondDrag = secondPoint.position;
        if (secondPoint.position.y > firstPoint.position.y)
        {
            firstDrag = secondPoint.position;
            secondDrag = firstPoint.position;
        }

        Scaling();

        if (Input.touchCount < 2)
            return;
        firstPoint.position = Input.touches[0].position;
    }

    public void SetSpeed(float speed)
    {
        scalingSpeed = speed;
    }

    public void SetMin(float value)
    {
        min = value;
    }

    public void SetMax(float value)
    {
        max = value;
    }

    public void SetModel(Transform model)
    {
        scalingModel = model;
    }

    public void SetFirstPoint(Transform point)
    {
        firstPoint = point;
    }

    public void SetSecondPoint(Transform point)
    {
        secondPoint = point;
    }

    public float GetSpeed()
    {
        return scalingSpeed;
    }

    public float GetMin()
    {
        return min;
    }

    public float GetMax()
    {
        return max;
    }

    public Transform GetModel()
    {
        return scalingModel;
    }

    public Transform GetFirstPoint()
    {
        return firstPoint;
    }

    public Transform GetSecondPoint()
    {
        return secondPoint;
    }
}