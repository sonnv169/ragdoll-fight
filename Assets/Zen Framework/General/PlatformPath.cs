﻿using UnityEngine;

[System.Serializable]
public enum PLATFORM
{
    RESOURCES,
    ANDROID,
    IOS,
    ELSE
}

public static class PlatformPath
{
    public static PLATFORM GetPlatform()
    {
        PLATFORM platform;
#if UNITY_EDITOR
        platform = PLATFORM.RESOURCES;
#elif UNITY_ANDROID
        platform = PLATFORM.ANDROID;
#elif UNITY_IOS
        platform = PLATFORM.IOS;
#else
        platform = PLATFORM.ELSE;
#endif
        return platform;
    }

    public static string GetPath(PLATFORM platform)
    {
        string path = null;
        switch (platform)
        {
            case PLATFORM.RESOURCES:
                path = Application.dataPath + "/Resources/";
                break;
            case PLATFORM.ANDROID:
                path = Application.persistentDataPath;
                break;
            case PLATFORM.IOS:
                path = Application.persistentDataPath + "/";
                break;
            case PLATFORM.ELSE:
                path = Application.dataPath + "/";
                break;
        }

        return path;
    }
}