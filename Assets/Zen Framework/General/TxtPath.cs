﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;

public static class TxtPath
{
    private static char[] separators = new char[]
    {
        ',',
        '\n',
        '\t'
    };

    public static List<string> Get(string fileName)
    {
        List<string> strings = new List<string>();

        if (File.Exists(Application.dataPath + "/Resources/Csv/" + fileName + ".csv"))
        {
            StreamReader reader = new StreamReader(Application.dataPath + "/Resources/Csv/" + fileName + ".csv");

            while (!reader.EndOfStream)
            {
                string[] lines = reader.ReadLine()?.Split(separators);
                if (lines != null)
                    for (int i = 0; i < lines.Length; i++)
                    {
                        strings.Add(lines[i]);
                    }
            }

            reader.Close();
        }
        else
        {
#if UNITY_EDITOR
            Debug.Log("Csv null!");
#endif
        }

        return strings;
    }

    public static void Set(string fileName, List<string> values, int row, int column)
    {
        StreamWriter writer = new StreamWriter(
            PlatformPath.GetPath(PlatformPath.GetPlatform()) + fileName + ".csv");
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < column; j++)
            {
                if (j < column - 1)
                    writer.Write(values[j] + ',');
                else
                    writer.Write(values[j]);
            }

            writer.Write('\n');
        }

        writer.Close();
    }
}