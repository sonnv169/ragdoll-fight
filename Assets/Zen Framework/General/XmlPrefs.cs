﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Xml.Serialization;

public static class XmlPrefs
{
    private static XmlSerializer xml;
    private static List<string> keys = new List<string>();

    public static void SetPrefs<T>(string key, T obj)
    {
        xml = new XmlSerializer(typeof(T));
        StringWriter sw = new StringWriter();
        xml.Serialize(sw, obj);
        PlayerPrefs.SetString(key, sw.ToString());
    }
    
    public static T GetPrefs<T>(string key) where T : class, new()
    {
        T obj = new T();
        if (PlayerPrefs.HasKey(key))
        {
            xml = new XmlSerializer(typeof(T));
            string text = PlayerPrefs.GetString(key);
            StringReader sr = new StringReader(text);
            obj = xml.Deserialize(sr) as T;
            if (!keys.Contains(key))
                keys.Add(key);
        }

        return obj;
    }

    public static void DeleteKey(string key)
    {
        if (!PlayerPrefs.HasKey(key))
            return;
        PlayerPrefs.DeleteKey(key);
    }

    public static void DeleteSavedKeys()
    {
        for (int i = 0; i < keys.Count; i++)
        {
            if (!PlayerPrefs.HasKey(keys[i]))
                continue;
            PlayerPrefs.DeleteKey(keys[i]);
        }
    }

    public static void DeleteAllKey()
    {
        PlayerPrefs.DeleteAll();
    }
}