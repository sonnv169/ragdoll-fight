﻿using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public static class XmlPath
{
    private static XmlSerializer xml;

    public static void SetByPath<T>(string path, T obj)
    {
        xml = new XmlSerializer(typeof(T));
        FileStream stream = new FileStream(Application.dataPath + path + ".xml", FileMode.Create);
        xml.Serialize(stream, obj);
        stream.Close();
    }

    public static void SetByPlatform<T>(string path, T obj)
    {
        xml = new XmlSerializer(typeof(T));
        FileStream stream = new FileStream(
            PlatformPath.GetPath(PlatformPath.GetPlatform()) + path + ".xml", FileMode.Create);
        xml.Serialize(stream, obj);
        stream.Close();
    }

    public static T GetByPath<T>(string path) where T : class, new()
    {
        xml = new XmlSerializer(typeof(T));
        T obj = new T();
        if (File.Exists(Application.dataPath + path + ".xml"))
        {
            FileStream stream = new FileStream(Application.dataPath + path + ".xml", FileMode.Open);
            obj = xml.Deserialize(stream) as T;
            stream.Close();
        }

        return obj;
    }

    public static T GetByPlatform<T>(string path) where T : class, new()
    {
        xml = new XmlSerializer(typeof(T));
        T obj = new T();
        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + path + ".xml"))
        {
            FileStream stream = new FileStream(
                PlatformPath.GetPath(PlatformPath.GetPlatform()) + path + ".xml", FileMode.Open);
            obj = xml.Deserialize(stream) as T;
            stream.Close();
        }

        return obj;
    }

    public static T GetByResources<T>(string path) where T : class, new()
    {
        xml = new XmlSerializer(typeof(T));
        T obj = new T();
        if (File.Exists(Application.dataPath + "/Resources/" + path + ".xml"))
        {
            TextAsset textAsset = Resources.Load<TextAsset>(path);
            if (textAsset != null)
            {
                TextReader textReader = new StringReader(textAsset.ToString());
                obj = xml.Deserialize(textReader) as T;
                textReader.Close();
            }
        }

        return obj;
    }
}