public static class Eventon<T>
{
    public delegate void Delegate<T>(T param);

    public static event Delegate<T> EventStatic;

    public static event Delegate<T> EventDynamic
    {
        add
        {
            EventStatic += value;
            EventStatic += param => { EventStatic -= value; };
        }
        remove { EventStatic -= value; }
    }

    public static void Trigger(T param)
    {
        if (EventStatic == null ||
            param == null)
            return;
        EventStatic(param);
    }
}