﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public enum AD_STATE
{
    NONE,
    SHOWED,
    FINISHED,
    SKIPPED,
    LOADED,
    FAILED,
    CLICKED
}

public class AdController : MonoBehaviour
{
    private static AdController mInstance;
    private static float delay = 0.1f;
    private static bool isEvent = false;

    #region property

    public static AdController Instance
    {
        get
        {
            if (mInstance == null)
                mInstance = FindObjectOfType<AdController>();
            return mInstance;
        }
    }

    #endregion

    private static IEnumerator OnEvent(AD_STATE state)
    {
        yield return new WaitForSecondsRealtime(delay);
        Eventon<AD_STATE>.Trigger(state);
    }

    public static bool IsConnecting()
    {
        return !Application.internetReachability.Equals(NetworkReachability.NotReachable);
    }

    public static void OnAdState(AD_STATE state)
    {
        if (state.Equals(AD_STATE.SHOWED))
            isEvent = false;
        else
        {
            if (isEvent)
                return;
            Instance.StartCoroutine(OnEvent(state));
            isEvent = true;
        }
    }

    public static float GetDelay()
    {
        float number = delay;
        return number;
    }
}