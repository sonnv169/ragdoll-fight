﻿using UnityEngine;

public class IronsourceController : MonoBehaviour
{
    private bool isInited;

    public string gameId = "85460dcd";

    private void OnEnable()
    {
        Config();
    }

    private void Update()
    {
        Init();
    }

    private void Init()
    {
        if (AdController.IsConnecting())
        {
            if (isInited)
                return;
            IronSource.Agent.validateIntegration();
            IronSource.Agent.init(gameId);
            IronsourceInterstitial.Load();
            isInited = true;
        }
        else
            isInited = false;
    }

    private void Config()
    {
        IronsourceReward.Init();
        IronsourceInterstitial.Init();
    }

    private void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);
    }
}

public static class IronsourceReward
{
    public static void Init()
    {
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
    }

    private static void RewardedVideoAdRewardedEvent(IronSourcePlacement ssp)
    {
        Eventon<AD_STATE>.Trigger(AD_STATE.FINISHED);
    }

    public static void Show()
    {
        if (IronSource.Agent.isRewardedVideoAvailable())
            IronSource.Agent.showRewardedVideo();
    }
}

public static class IronsourceOfferwall
{
    public static void Show()
    {
        if (IronSource.Agent.isOfferwallAvailable())
            IronSource.Agent.showOfferwall();
    }
}

public static class IronsourceInterstitial
{
    public static void Init()
    {
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
    }

    private static void InterstitialAdClosedEvent()
    {
        Load();
    }

    public static void Load()
    {
        IronSource.Agent.loadInterstitial();
    }

    public static void Show()
    {
        IronSource.Agent.showInterstitial();
    }
}

public static class IronsourceBanner
{
    public static void Load(IronSourceBannerSize size, IronSourceBannerPosition position)
    {
        IronSource.Agent.loadBanner(size, position);
    }

    public static void Destroy()
    {
        IronSource.Agent.destroyBanner();
    }
}