﻿public static class AdReward
{
    public static void Show()
    {
        AdController.OnAdState(AD_STATE.SHOWED);
#if UNITY_EDITOR
        if (AdController.IsConnecting())
            AdController.OnAdState(AD_STATE.FINISHED);
        else
            UIDisconnect.Show();

#else
        if (AdController.IsConnecting())
        {
            if (IronSource.Agent.isRewardedVideoAvailable())
                IronSource.Agent.showRewardedVideo();
              else
            {
                if (UIMenu.Instance != null)
                    UIMenu.Instance.EnableBlocker(true);
                UINoAd.Show();
            }
        }
        else
            UIDisconnect.Show();
#endif
    }
}