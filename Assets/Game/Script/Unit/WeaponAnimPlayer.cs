﻿using UnityEngine;

public class WeaponAnimPlayer : ToggleBehaviour
{
    private Animator mAnimator;
    private Weapon mWeapon;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    private Weapon Weapon
    {
        get
        {
            if (mWeapon == null && GetComponent<Weapon>())
                mWeapon = GetComponent<Weapon>();
            return mWeapon;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        //nothing
    }

    protected override void OnCyclic()
    {
        OnAutoPlay();
    }

    protected override void OnDisappear()
    {
        //nothing
    }

    private void OnAutoPlay()
    {
        if (Singleton<GameManager>.Instance == null || Animator == null || Weapon == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() == GAME_STATE.PLAY && Weapon.GetBody() != null)
            AnimatorManager.Enable(Animator, true);
        else
            AnimatorManager.Enable(Animator, false);
    }
}