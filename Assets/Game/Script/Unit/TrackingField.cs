﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct ReTrackingEvent
{
    public bool isRetracking;
}

public class TrackingField : ToggleBehaviour
{
    private Image mImage;
    private float scale;

    [SerializeField] private Image icoEnemy;
    [SerializeField] private RectTransform tracker;

    #region property

    private Image Image
    {
        get
        {
            if (mImage == null && GetComponent<Image>())
                mImage = GetComponent<Image>();
            return mImage;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Init();
        Eventon<UnitDeathEvent>.EventStatic += OnUnitDeathEvent;
        Eventon<ReTrackingEvent>.EventStatic += OnReTrackingEvent;
    }

    protected override void OnCyclic()
    {
        Tracking();
    }

    protected override void OnDisappear()
    {
        Eventon<UnitDeathEvent>.EventStatic -= OnUnitDeathEvent;
        Eventon<ReTrackingEvent>.EventStatic -= OnReTrackingEvent;
    }

    private void OnReTrackingEvent(ReTrackingEvent param)
    {
        if (!param.isRetracking)
            return;
        Init();
    }

    private void OnUnitDeathEvent(UnitDeathEvent param)
    {
        if (tracker == null || !param.unit.GetComponent<Enemy>())
            return;
        Init();
        tracker.gameObject.SetActive(false);
    }

    private void Init()
    {
        if (tracker != null)
            tracker.gameObject.SetActive(false);
        if (icoEnemy == null || Singleton<CharacterManager>.Instance == null ||
            Singleton<StageManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        StageManager stageManager = Singleton<StageManager>.Instance;
        GameEnums.ENEMY stageEnemy = stageManager.GetEnemyID(stageManager.GetWave());
        Sprite spr = characterManager.GetEnemyAvatar(stageEnemy);
        icoEnemy.sprite = spr;
        icoEnemy.SetNativeSize();
        scale = icoEnemy.transform.localScale.x;
    }

    private void Tracking()
    {
        if (tracker == null || Image == null || Singleton<GameManager>.Instance == null ||
            Singleton<CharacterManager>.Instance == null || Singleton<Player>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        Player player = Singleton<Player>.Instance;

        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        List<float> distances = new List<float>();
        Enemy enemy = null;
        for (int i = 0; i < characterManager.GetTransforms().Count; i++)
        {
            if (characterManager.GetTransforms()[i] == null ||
                !characterManager.GetTransforms()[i].GetComponent<Enemy>())
                continue;
            Enemy enemyComponent = characterManager.GetTransforms()[i].GetComponent<Enemy>();
            if (enemyComponent.GetHp() <= 0)
                continue;
            float distance = Vector2.Distance(player.transform.position,
                characterManager.GetTransforms()[i].position);
            distances.Add(distance);
        }

        if (distances.Count <= 0)
            return;
        int min = 0;
        for (int i = 0; i < distances.Count; i++)
        {
            if (distances[i] <= distances.Min() && characterManager.GetTransforms()[i] != null &&
                characterManager.GetTransforms()[i].GetComponent<Enemy>())
                min = i;
        }

        enemy = characterManager.GetTransforms()[min].GetComponent<Enemy>();
        if (enemy == null)
            return;
        float xDistance = Vector2.Distance(
            new Vector2(player.transform.position.x, 0),
            new Vector2(enemy.transform.position.x, 0));
        float yDistance = Vector2.Distance(
            new Vector2(0, player.transform.position.y),
            new Vector2(0, enemy.transform.position.y));
        if (xDistance >= GameConfig.xTrackingRange || yDistance >= GameConfig.yTrackingRange)
        {
            if (enemy.GetHp() > 0)
                tracker.gameObject.SetActive(true);
            else
                tracker.gameObject.SetActive(false);
        }
        else
            tracker.gameObject.SetActive(false);

        Vector2 direction = (new Vector2(enemy.transform.position.x, enemy.transform.position.y) -
                             new Vector2(player.transform.position.x, transform.position.y)).normalized;
        tracker.up = direction;
        tracker.anchoredPosition = new Vector2(
            direction.x * (Image.rectTransform.sizeDelta.x * (100.0f - 50.0f) / 100.0f),
            direction.y * (Image.rectTransform.sizeDelta.y * (100.0f - 50.0f) / 100.0f));

        if (icoEnemy == null)
            return;
        icoEnemy.transform.rotation = Quaternion.identity;
        if (player.transform.position.x < transform.position.x)
            icoEnemy.transform.localScale =
                new Vector3(-scale, icoEnemy.transform.localScale.y, icoEnemy.transform.localScale.z);
        else if (player.transform.position.x > transform.position.x)
            icoEnemy.transform.localScale =
                new Vector3(scale, icoEnemy.transform.localScale.y, icoEnemy.transform.localScale.z);
    }
}