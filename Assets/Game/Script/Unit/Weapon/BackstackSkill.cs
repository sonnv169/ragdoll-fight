﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackstackSkill : ToggleBehaviour
{
    private WEAPON_SKILL_STATE weaponSkillState;
    private Coroutine changeStateCoroutine;
    private Coroutine backwardCoroutine;
    private Weapon weapon;
    private Transform parent;
    private Vector3 direction;
    private Vector3 scale;
    private bool isFoward;

    [SerializeField] private float fowardDuration;
    [SerializeField] private float fowardSpeed;
    [SerializeField] private float backwardSpeed;
    [SerializeField] private GameEnums.AUDIO_CLIP clip;
    [SerializeField] private GameObject vfxSkill;

    protected override void OnAppear()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        OnUpdate();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                Reset();
                break;
        }
    }

    private void Init()
    {
        EnableVfxSkill(false);
    }

    private void EnableVfxSkill(bool isStatus)
    {
        if (vfxSkill != null)
            vfxSkill.SetActive(isStatus);
    }

    private void ClampPosition()
    {
        if (Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        Stage stage = stageManager.GetStageObject();
        if (stage == null)
            return;
        Vector3 topClamp = Vector3.zero;
        Vector3 leftClamp = Vector3.zero;
        Vector3 rightClamp = Vector3.zero;
        Vector3 botClamp = Vector3.zero;

        float clamp = GameConfig.backStackClamp;
        if (stage.GetTopGround() != null)
            topClamp = new Vector3(0, stage.GetTopGround().position.y - clamp, 0);
        if (stage.GetLeftGround() != null)
            leftClamp = new Vector3(stage.GetLeftGround().position.x + clamp, 0, 0);
        if (stage.GetRightGround() != null)
            rightClamp = new Vector3(stage.GetRightGround().position.x - clamp, 0, 0);
        if (stage.GetBotGround() != null)
            botClamp = new Vector3(0, stage.GetBotGround().position.y + clamp, 0);

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, leftClamp.x, rightClamp.x),
            Mathf.Clamp(transform.position.y, botClamp.y, topClamp.y));
    }

    private void OnUpdate()
    {
        if (weaponSkillState != WEAPON_SKILL_STATE.START)
            return;
        if (isFoward)
        {
            if (weapon == null || direction == Vector3.zero)
                return;
            transform.Translate(direction * fowardSpeed * Time.deltaTime);
            ClampPosition();
        }
        else
        {
            if (parent == null)
                return;
            float distance = Vector3.Distance(transform.position, parent.position);
            if (distance > GameConfig.backStackRange)
                transform.position =
                    Vector3.MoveTowards(transform.position, parent.position, backwardSpeed * Time.deltaTime);
            else
            {
                ChangeState(WEAPON_SKILL_STATE.STOP);
            }
        }
    }

    private IEnumerator OnDelayChangeState(WEAPON_SKILL_STATE state, float time)
    {
        yield return new WaitForSeconds(time);
        ChangeState(state);
    }

    private IEnumerator OnBackward()
    {
        yield return new WaitForSeconds(fowardDuration);
        isFoward = false;
    }

    public void Reset()
    {
        weaponSkillState = WEAPON_SKILL_STATE.NONE;
        if (backwardCoroutine != null)
            StopCoroutine(backwardCoroutine);
        if (changeStateCoroutine != null)
            StopCoroutine(changeStateCoroutine);
        weapon = null;
        parent = null;
        direction = Vector3.zero;
        scale = Vector3.zero;
        isFoward = false;
        EnableVfxSkill(false);
        if (!GetComponent<WeaponSkillFactor>())
            return;
        WeaponSkillFactor weaponSkillFactor = GetComponent<WeaponSkillFactor>();
        weaponSkillFactor.OnDelayReset();
    }

    public void ChangeState(WEAPON_SKILL_STATE state)
    {
        weaponSkillState = state;
        switch (state)
        {
            case WEAPON_SKILL_STATE.START:
                if (transform.parent != null)
                    parent = transform.parent;
                scale = transform.localScale;
                if (GetComponent<Weapon>())
                    weapon = GetComponent<Weapon>();
                if (weapon == null || weapon.GetBody() == null || Singleton<WeaponManager>.Instance == null)
                    return;
                WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
                transform.SetParent(weaponManager.transform, true);
                direction = (transform.position - weapon.GetBody().transform.position).normalized;
                backwardCoroutine = StartCoroutine(OnBackward());
                isFoward = true;
                EnableVfxSkill(true);
                break;

            case WEAPON_SKILL_STATE.STOP:
                transform.SetParent(parent, true);
                transform.position = parent.position;
                transform.up = parent.up;
                transform.localScale = scale;
                Reset();
                break;
        }

        SoundManager.Play(AUDIO_KIND.SOUND, clip, false);
    }

    public void DelayChangeState(WEAPON_SKILL_STATE state, float time)
    {
        changeStateCoroutine = StartCoroutine(OnDelayChangeState(state, time));
    }
}