﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class WeaponPath : MonoBehaviour
{
    private GameEnums.WEAPON weapon;
    private Body body;
    private int originAtk;
    private int currentAtk;

    private void OnHit(BodyPath path)
    {
        if (path.GetBody() == null || path.GetHitDuration() > 0)
            return;
        Body pathBody = path.GetBody();
        if (body.Equals(pathBody))
            return;
        float damagePercent = path.GetDamagePercent();
        if (damagePercent > 0)
        {
            int bodyDamage = 0;
            int totalDamage = 0;
            if (pathBody.GetComponent<Player>())
            {
                Player player = pathBody.GetComponent<Player>();
                if (player.IsSheild())
                    return;
                if (body != null && body.GetComponent<Enemy>())
                {
                    Enemy enemy = body.GetComponent<Enemy>();
                    bodyDamage = enemy.GetPower();
                }

                totalDamage = (int) (bodyDamage + (int) (bodyDamage * currentAtk / 100) * path.GetCriticalRate());
                player.OnHpMinus((int) (totalDamage * damagePercent / 100));

                if (path.GetIntact() <= 0)
                    UIGameplay.Instance.DisplayCriticalDamage(path.transform.position, totalDamage);
                else
                    UIGameplay.Instance.DisplayDamage(CHARACTER.ENEMY, path.transform.position, totalDamage);

                if (player.IsDrop())
                {
                    player.OnSetHit(0);
                    path.GetBody().OnDropWeapon();
                }

                Eventon<WeaponHitEvent>.Trigger(new WeaponHitEvent()
                {
                    collisionPosition = path.transform.position
                });

                player.DisplayHp();
                player.OnDropHitUp(1);
            }
            else if (pathBody.GetComponent<Enemy>())
            {
                Enemy enemy = pathBody.GetComponent<Enemy>();
                if (body != null && body.GetComponent<Player>())
                {
                    Player player = body.GetComponent<Player>();
                    bodyDamage = player.GetPower();
                }

                totalDamage = (int) (bodyDamage + (int) (bodyDamage * currentAtk / 100) * path.GetCriticalRate());
                enemy.OnHpMinus((int) (totalDamage * damagePercent / 100));

                if (path.GetIntact() <= 0)
                    UIGameplay.Instance.DisplayCriticalDamage(path.transform.position, totalDamage);
                else
                    UIGameplay.Instance.DisplayDamage(CHARACTER.PLAYER, path.transform.position, totalDamage);

                if (enemy.IsDrop())
                {
                    enemy.OnSetHit(0);
                    path.GetBody().OnDropWeapon();
                }

                Eventon<WeaponHitEvent>.Trigger(new WeaponHitEvent()
                {
                    collisionPosition = path.transform.position
                });

                enemy.DisplayHp();
                enemy.OnDropHitUp(1);
            }

            path.OnIntactMinus(1);
            path.GetBlood();
        }
        else
            path.GetBlock();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        if (!other.gameObject.GetComponent<BodyPath>())
            return;
        BodyPath path = other.gameObject.GetComponent<BodyPath>();
        if (body == null)
        {
            if (weapon == GameEnums.WEAPON.NONE || path.GetBody() == null ||
                path.GetBody().IsDroping() || path.GetBody().GetEquiptedWeapon() != null)
                return;
            path.GetBody().SetWeapon(gameObject);
        }
        else
            OnHit(path);
    }

    public void Init(GameEnums.WEAPON weapon)
    {
        this.weapon = weapon;
        originAtk = 0;
        currentAtk = 0;
        if (weapon == GameEnums.WEAPON.NONE || Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        originAtk = weaponManager.GetBonusDamage(weapon);
        currentAtk = originAtk;

        SetAtk(currentAtk);
    }

    public void InitBody(Body obj)
    {
        if (obj == null)
            return;
        int hp = 0;
        if (obj.GetComponent<Player>())
        {
            Player player = obj.GetComponent<Player>();
            hp = player.GetHp();
        }
        else if (obj.GetComponent<Enemy>())
        {
            Enemy enemy = obj.GetComponent<Enemy>();
            hp = enemy.GetHp();
        }

        if (hp <= 0)
            return;
        body = obj;
        gameObject.layer = obj.gameObject.layer;
    }

    public void ClearBody()
    {
        body = null;
        gameObject.layer = 0;
    }

    public void SetAtk(int value)
    {
        currentAtk = value;
    }

    public Body GetBody()
    {
        return body;
    }
}