﻿using System.Collections;
using UnityEngine;

public class RotateSkill : MonoBehaviour
{
    private Coroutine resetCoroutine;
    private Coroutine rotateCoroutine;
    private Animator mAnimator;
    private int rotated;

    [SerializeField] private int rotateTurn;
    [SerializeField] private GameEnums.AUDIO_CLIP clip;
    [SerializeField] private GameObject vfxSkill;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    private void OnEnable()
    {
        Init();
    }

    private void Init()
    {
        rotated = 0;
        EnableVfxSkill(false);
    }

    private void EnableVfxSkill(bool isStatus)
    {
        if (vfxSkill != null)
            vfxSkill.SetActive(isStatus);
    }

    private IEnumerator DelayReset(float time)
    {
        yield return new WaitForSeconds(time);
        Reset();
    }

    private IEnumerator OnRotate(float time)
    {
        yield return new WaitForSeconds(time);
        if (rotated < rotateTurn)
        {
            AnimatorManager.Play(Animator, "anim_rotate");
            if (rotateCoroutine != null)
                StopCoroutine(rotateCoroutine);
            rotateCoroutine = StartCoroutine(OnRotate(AnimatorManager.GetLenght(Animator, "anim_rotate")));
            rotated += 1;

            SoundManager.Play(AUDIO_KIND.SOUND, clip, false);
        }
    }

    public float Rotate()
    {
        float lenght = 0;
        AnimatorManager.Enable(Animator, true);
        EnableVfxSkill(true);
        lenght = (AnimatorManager.GetLenght(Animator, "anim_rotate") * rotateTurn) +
                 AnimatorManager.GetLenght(Animator, "anim_rotate");
        resetCoroutine = StartCoroutine(DelayReset(lenght));
        rotateCoroutine = StartCoroutine(OnRotate(AnimatorManager.GetLenght(Animator, "anim_rotate")));
        return lenght;
    }

    public void Reset()
    {
        Init();
        AnimatorManager.Enable(Animator, false);
        transform.rotation = Quaternion.identity;
        if (resetCoroutine != null)
            StopCoroutine(resetCoroutine);
        if (rotateCoroutine != null)
            StopCoroutine(rotateCoroutine);
        if (transform.parent == null)
            return;
        transform.up = transform.parent.up;
    }
}