﻿using System.Collections;
using UnityEngine;

public class StingSkill : MonoBehaviour
{
    private WeaponSkillFactor mSkillFactor;
    private Coroutine resetCoroutine;
    private Coroutine stingCoroutine;
    private Coroutine resizeCoroutine;
    private Collider2D mCollider;
    private Vector3 stingerScale;
    private Vector2 colliderOffset;
    private Vector2 colliderSize;
    private int stinged;

    [SerializeField] private float stingDuration;
    [SerializeField] private float stingCooldown;
    [SerializeField] private int stingTurn;
    [SerializeField] private Vector3 stingSize;
    [SerializeField] private GameEnums.AUDIO_CLIP clip;
    [SerializeField] private GameObject vfxSkill;

    #region property

    private WeaponSkillFactor SkillFactor
    {
        get
        {
            if (mSkillFactor == null && GetComponent<WeaponSkillFactor>())
                mSkillFactor = GetComponent<WeaponSkillFactor>();
            return mSkillFactor;
        }
    }

    private Collider2D Collider
    {
        get
        {
            if (mCollider == null && GetComponent<Collider2D>())
                mCollider = GetComponent<Collider2D>();
            return mCollider;
        }
    }

    #endregion

    private void OnEnable()
    {
        Init();
    }

    private void Init()
    {
        stinged = 0;
        EnableVfxSkill(false);
        stingerScale = transform.localScale;
        if (Collider == null)
            return;
        colliderOffset = Collider.offset;
        if (!Collider.GetComponent<CapsuleCollider2D>())
            return;
        CapsuleCollider2D capsule = GetComponent<CapsuleCollider2D>();
        colliderSize = capsule.size;
    }

    private void EnableVfxSkill(bool isStatus)
    {
        if (vfxSkill != null)
            vfxSkill.SetActive(isStatus);
    }

    private void Resize()
    {
        EnableVfxSkill(false);
        transform.localScale = stingerScale;
        if (Collider == null)
            return;
        Collider.offset = colliderOffset;
        if (!Collider.GetComponent<CapsuleCollider2D>())
            return;
        CapsuleCollider2D capsule = GetComponent<CapsuleCollider2D>();
        capsule.size = colliderSize;
    }

    private IEnumerator OnResize(float time)
    {
        yield return new WaitForSeconds(time);
        Resize();
    }

    private IEnumerator OnSting(float time)
    {
        yield return new WaitForSeconds(time);
        EnableVfxSkill(true);
        transform.localScale = new Vector3(stingerScale.x + stingSize.x, stingerScale.y + stingSize.y,
            stingerScale.z + stingSize.z);
        if (Collider.GetComponent<CapsuleCollider2D>())
        {
            CapsuleCollider2D capsule = GetComponent<CapsuleCollider2D>();
            capsule.size = new Vector2(colliderSize.x + stingSize.x, colliderSize.y + stingSize.y);
        }

        resizeCoroutine = StartCoroutine(OnResize(stingDuration));
        stinged += 1;

        if (stinged <= stingTurn)
        {
            if (stingCoroutine != null)
                StopCoroutine(stingCoroutine);
            stingCoroutine = StartCoroutine(OnSting(stingCooldown));

            SoundManager.Play(AUDIO_KIND.SOUND, clip, false);
        }
        else
        {
            if (SkillFactor != null)
            {
                float lenght = (stingCooldown * stingTurn);
                SkillFactor.SetDelayResetTime(lenght);
                Reset();
            }
        }
    }

    public void Sting()
    {
        stingCoroutine = StartCoroutine(OnSting(0));
    }

    public void Reset()
    {
        stinged = 0;
        EnableVfxSkill(false);
        transform.localScale = stingerScale;
        if (resetCoroutine != null)
            StopCoroutine(resetCoroutine);
        if (stingCoroutine != null)
            StopCoroutine(stingCoroutine);
        if (resetCoroutine != null)
            StopCoroutine(resizeCoroutine);
    }
}