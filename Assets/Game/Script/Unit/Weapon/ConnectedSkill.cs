﻿using System.Collections;
using UnityEngine;

public class ConnectedSkill : ToggleBehaviour
{
    private Coroutine stopCastCoroutine;
    private BodyPath connectedPath;
    private LineRenderer mLine;
    private Weapon weapon;
    private LayerMask castLayer;
    private bool isCasted;
    private bool isSound;

    [SerializeField] private Transform barrel;
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private LayerMask enemyLayer;
    [SerializeField] private GameEnums.AUDIO_CLIP clip;
    [SerializeField] private GameObject connectedVfx;

    #region property

    private LineRenderer Line
    {
        get
        {
            if (mLine == null && GetComponent<LineRenderer>())
                mLine = GetComponent<LineRenderer>();
            return mLine;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Reset();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        OnUpdate();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
            case GAME_STATE.WIN:
            case GAME_STATE.LOSE:
                Reset();
                break;
        }
    }

    private void OnUpdate()
    {
        if (barrel == null || Line == null)
            return;
        if (!isCasted)
        {
            Line.SetPosition(0, new Vector2(barrel.position.x, barrel.position.y));
            Line.SetPosition(1, new Vector2(barrel.position.x, barrel.position.y));
            EnableConnectedVfx(false);
        }
        else
        {
            RaycastHit2D rayHit = Physics2D.Raycast(barrel.position, barrel.up, Mathf.Infinity, castLayer);

            if (rayHit.collider)
            {
                GameObject objCollided = rayHit.collider.gameObject;
                if (objCollided.transform.parent != null &&
                    objCollided.transform.parent.GetComponent<BodyPath>())
                {
                    BodyPath bodyPath = objCollided.transform.parent.GetComponent<BodyPath>();
                    if (bodyPath.GetBody() == null || bodyPath.GetBody().GetRigidbody() == null ||
                        bodyPath.GetBody().GetHp() <= 0)
                        return;
                    connectedPath = bodyPath;
                    connectedPath.GetBody().SetPausing(true);
                    Rigidbody2D body = connectedPath.GetBody().GetRigidbody();
                    Body2DEffector.Velocity(body, 0.0f, 0.0f);
                    body.transform.position = Vector3.MoveTowards(body.transform.position, barrel.position,
                        GameConfig.hookSpeed * Time.deltaTime);
                }
            }

            if (connectedPath != null)
            {
                Line.SetPosition(0, new Vector2(barrel.position.x, barrel.position.y));
                Line.SetPosition(1,
                    new Vector2(connectedPath.transform.position.x, connectedPath.transform.position.y));
                EnableConnectedVfx(true);
                if (connectedVfx != null)
                    connectedVfx.transform.position = new Vector2(connectedPath.transform.position.x,
                        connectedPath.transform.position.y);

                if (!isSound)
                {
                    SoundManager.Play(AUDIO_KIND.SOUND, clip, false);
                    isSound = true;
                }
            }
            else
            {
                Line.SetPosition(0, new Vector2(barrel.position.x, barrel.position.y));
                Line.SetPosition(1, new Vector2(barrel.position.x, barrel.position.y));
                EnableConnectedVfx(false);
            }
        }
    }

    private void EnableConnectedVfx(bool isStatus)
    {
        if (connectedVfx == null)
            return;
        connectedVfx.SetActive(isStatus);
        if (isStatus)
            return;
        connectedVfx.transform.position = transform.position;
        isSound = false;
    }

    private IEnumerator OnStopCast(float time)
    {
        yield return new WaitForSeconds(time);
        Reset();
    }

    public void Reset()
    {
        if (connectedPath != null)
        {
            if (Singleton<GameManager>.Instance == null)
                return;
            GameManager gameManager = Singleton<GameManager>.Instance;
            if (gameManager.GetState() == GAME_STATE.PLAY)
                connectedPath.GetBody().SetPausing(false);
            connectedPath = null;
        }

        isCasted = false;
        weapon = null;
        castLayer = 0;
        if (GetComponent<WeaponSkillFactor>())
        {
            WeaponSkillFactor weaponSkillFactor = GetComponent<WeaponSkillFactor>();
            weaponSkillFactor.OnDelayReset();
        }

        if (stopCastCoroutine != null)
            StopCoroutine(stopCastCoroutine);
    }

    public void SetWeapon(Weapon weaponComponent)
    {
        if (this.weapon != null || weaponComponent.GetBody() == null)
            return;
        this.weapon = weaponComponent;
        if (weaponComponent.GetBody().GetComponent<Player>())
            castLayer = enemyLayer;
        else if (weaponComponent.GetBody().GetComponent<Enemy>())
            castLayer = playerLayer;
    }

    public void CastLine()
    {
        if (isCasted)
            return;
        isCasted = true;
        stopCastCoroutine = StartCoroutine(OnStopCast(GameConfig.jointDuration));
    }
}