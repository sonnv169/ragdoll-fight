﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class WeaponHitEvent
{
    public Vector2 collisionPosition;
}

public class Weapon : ToggleBehaviour
{
    private Rigidbody2D weaponBody;
    private Collider2D mCollider;
    private Body body;
    private Vector3 originScale;
    private int originAtk;
    private int currentAtk;

    [SerializeField] private GameEnums.WEAPON weapon;
    [SerializeField] private GameEnums.AUDIO_CLIP clip;
    [SerializeField] private WeaponPath[] weaponPaths;

    #region property

    private Collider2D Collider
    {
        get
        {
            if (mCollider == null && GetComponent<Collider2D>())
                mCollider = GetComponent<Collider2D>();
            return mCollider;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
        Eventon<BodyExploreEvent>.EventStatic += OnBodyExploreEvent;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
        Eventon<BodyExploreEvent>.EventStatic -= OnBodyExploreEvent;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                OnDropAndReturn();
                break;
        }
    }

    private void OnBodyExploreEvent(BodyExploreEvent param)
    {
        if (weapon == GameEnums.WEAPON.NONE || body == null || !body.Equals(param.body))
            return;
        OnDrop();
    }

    private void Init()
    {
        originScale = transform.localScale;
        originAtk = 0;
        currentAtk = 0;
        if (weapon == GameEnums.WEAPON.NONE || Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        originAtk = weaponManager.GetBonusDamage(weapon);
        currentAtk = originAtk;

        gameObject.layer = 0;

        if (weaponBody != null)
            Destroy(weaponBody);
        SetAtk(currentAtk);

        for (int i = 0; i < weaponPaths.Length; i++)
        {
            if (weaponPaths[i] != null)
                weaponPaths[i].Init(weapon);
        }
    }

    private void EnableCollider(bool isStatus)
    {
        if (Collider != null)
            Collider.enabled = isStatus;
    }

    private void Return()
    {
        if (weapon == GameEnums.WEAPON.NONE)
            return;
        transform.position = GameConfig.infernal;
        transform.localScale = originScale;
        gameObject.layer = 0;

        OnClear();
    }

    private void OnHit(BodyPath path)
    {
        if (path.GetBody() == null || path.GetHitDuration() > 0)
            return;
        Body pathBody = path.GetBody();
        if (body.Equals(pathBody))
            return;
        float damagePercent = path.GetDamagePercent();
        if (damagePercent > 0)
        {
            int bodyDamage = 0;
            int totalDamage = 0;
            if (pathBody.GetComponent<Player>())
            {
                Player player = pathBody.GetComponent<Player>();
                if (player.IsSheild())
                    return;
                if (body != null && body.GetComponent<Enemy>())
                {
                    Enemy enemy = body.GetComponent<Enemy>();
                    bodyDamage = enemy.GetPower();
                }

                totalDamage = (int) (bodyDamage + (int) (bodyDamage * currentAtk / 100) * path.GetCriticalRate());
                damagePercent = path.GetDamagePercent();

                player.OnHpMinus((int) (totalDamage * damagePercent / 100));

                if (path.GetIntact() <= 0)
                    UIGameplay.Instance.DisplayCriticalDamage(path.transform.position, totalDamage);
                else
                    UIGameplay.Instance.DisplayDamage(CHARACTER.ENEMY, path.transform.position, totalDamage);

                if (player.IsDrop())
                {
                    player.OnSetHit(0);
                    path.GetBody().OnDropWeapon();
                }

                player.DisplayHp();
                player.OnDropHitUp(1);
            }
            else if (pathBody.GetComponent<Enemy>())
            {
                Enemy enemy = pathBody.GetComponent<Enemy>();
                if (body != null && body.GetComponent<Player>())
                {
                    Player player = body.GetComponent<Player>();
                    bodyDamage = player.GetPower();
                }

                totalDamage = (int) (bodyDamage + (int) (bodyDamage * currentAtk / 100) * path.GetCriticalRate());
                damagePercent = path.GetDamagePercent();

                enemy.OnHpMinus((int) (totalDamage * damagePercent / 100));

                if (path.GetIntact() <= 0)
                    UIGameplay.Instance.DisplayCriticalDamage(path.transform.position, totalDamage);
                else
                    UIGameplay.Instance.DisplayDamage(CHARACTER.PLAYER, path.transform.position, totalDamage);

                if (enemy.IsDrop())
                {
                    enemy.OnSetHit(0);
                    path.GetBody().OnDropWeapon();
                }

                enemy.DisplayHp();
                enemy.OnDropHitUp(1);
            }

            Eventon<WeaponHitEvent>.Trigger(new WeaponHitEvent()
            {
                collisionPosition = path.transform.position
            });

            path.OnIntactMinus(1);
            path.GetBlood();

            string clipName = clip.ToString();
            if (weapon == GameEnums.WEAPON.NONE || weapon == GameEnums.WEAPON.w1)
            {
                int result = Random.Range(0, 2);
                clipName = "sfx_punch_" + result;
            }

            Enum.TryParse(clipName, out clip);
            SoundManager.Play(AUDIO_KIND.SOUND, clip, false);
        }
        else
        {
            path.GetBlock();

            SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_collision_block, false);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        if (!other.gameObject.GetComponent<BodyPath>())
            return;
        BodyPath path = other.gameObject.GetComponent<BodyPath>();
        if (body == null)
        {
            if (weapon == GameEnums.WEAPON.NONE ||
                path.GetBody() == null ||
                path.GetBody().IsDroping() ||
                path.GetBody().GetEquiptedWeapon() != null)
                return;
            SetBody(path.GetBody());
            path.GetBody().SetWeapon(gameObject);
        }
        else
            OnHit(path);
    }

    public void InitBody(Body obj)
    {
        if (obj == null)
            return;
        body = obj;
        gameObject.layer = obj.gameObject.layer;

        for (int i = 0; i < weaponPaths.Length; i++)
        {
            if (weaponPaths[i] != null)
                weaponPaths[i].InitBody(obj);
        }
    }

    public void SetBody(Body obj)
    {
        if (obj == null)
            return;
        int hp = 0;
        if (obj.GetComponent<Player>())
        {
            Player player = obj.GetComponent<Player>();
            hp = player.GetHp();
        }
        else if (obj.GetComponent<Enemy>())
        {
            Enemy enemy = obj.GetComponent<Enemy>();
            hp = enemy.GetHp();
        }

        if (hp <= 0)
            return;
        if (weaponBody != null)
            Destroy(weaponBody);

        InitBody(obj);
    }

    public void Flip()
    {
        if (weapon == GameEnums.WEAPON.NONE || transform.parent == null)
            return;
        switch (transform.parent.name)
        {
            case "Inside Weapon":
                transform.localScale = new Vector3(originScale.x, originScale.y, originScale.z);
                break;

            case "Outside Weapon":
                transform.localScale = new Vector3(-originScale.x, originScale.y, originScale.z);
                break;
        }
    }

    public void SetAtk(int value)
    {
        currentAtk = value;
        for (int i = 0; i < weaponPaths.Length; i++)
        {
            if (weaponPaths[i] != null)
                weaponPaths[i].SetAtk(value);
        }
    }

    public void OnDrop()
    {
        if (weapon == GameEnums.WEAPON.NONE)
            return;
        EnableCollider(true);
        Vector3 direction = Vector3.down;
        transform.localScale = originScale;
        gameObject.layer = 0;
        if (Singleton<WeaponManager>.Instance != null)
        {
            WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
            transform.SetParent(weaponManager.transform, true);
        }

        if (body != null)
            direction = transform.position - body.transform.position;

        if (weaponBody == null)
        {
            if (!GetComponent<Rigidbody2D>())
                gameObject.AddComponent<Rigidbody2D>();

            weaponBody = GetComponent<Rigidbody2D>();
            weaponBody.simulated = true;
            weaponBody.velocity = direction.normalized * GameConfig.explorePower;
        }

        SetAtk(originAtk);
        body = null;

        for (int i = 0; i < weaponPaths.Length; i++)
        {
            if (weaponPaths[i] != null)
                weaponPaths[i].ClearBody();
        }

        if (!GetComponent<WeaponSkillFactor>())
            return;
        WeaponSkillFactor weaponSkillFactor = GetComponent<WeaponSkillFactor>();
        weaponSkillFactor.Return();
    }

    public void OnDropAndReturn()
    {
        OnDrop();
        Return();
    }

    public void OnClear()
    {
        body = null;
        if (!GetComponent<ObjectPooling>())
            return;
        ObjectPooling objectPooling = GetComponent<ObjectPooling>();
        objectPooling.Return();
    }

    public GameEnums.WEAPON GetWeapon()
    {
        return weapon;
    }

    public Body GetBody()
    {
        return body;
    }

    public int GetOriginAtk()
    {
        return originAtk;
    }

    public int GetCurrentAtk()
    {
        return currentAtk;
    }

    public bool IsHasBody()
    {
        return body != null;
    }
}