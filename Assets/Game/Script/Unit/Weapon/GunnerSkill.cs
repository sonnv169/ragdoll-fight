﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Barrel
{
    public GameEnums.BULLET bullet;
    public Transform barrel;
    public GameObject shootVfx;
}

public class GunnerSkill : ToggleBehaviour
{
    private List<Bullet> returned = new List<Bullet>();
    private WeaponSkillFactor mSkillFactor;
    private Coroutine shootCoroutine;
    private Weapon mWeapon;
    private Transform aimTarget;
    private int shooted;
    private bool isCrist;

    [SerializeField] private SpriteRenderer spr;
    [SerializeField] private Barrel[] barrels;
    [SerializeField] private GameEnums.AUDIO_CLIP clip;
    [SerializeField] private float shootPower;
    [SerializeField] private float shootAgain;
    [SerializeField] [Range(0, 100.0f)] private float aimRating;
    [SerializeField] private int shootLimit;
    [SerializeField] private bool isHidenShoot;
    [SerializeField] private bool isCristShoot;

    #region property

    private WeaponSkillFactor SkillFactor
    {
        get
        {
            if (mSkillFactor == null && GetComponent<WeaponSkillFactor>())
                mSkillFactor = GetComponent<WeaponSkillFactor>();
            return mSkillFactor;
        }
    }

    private Weapon Weapon
    {
        get
        {
            if (mWeapon == null && GetComponent<Weapon>())
                mWeapon = GetComponent<Weapon>();
            return mWeapon;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Reset();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
            case GAME_STATE.WIN:
            case GAME_STATE.LOSE:
                if (shootCoroutine != null)
                    StopCoroutine(shootCoroutine);
                Reset();
                break;
        }
    }

    private void OnShoot()
    {
        if (Weapon == null || aimTarget != null)
            return;
        if (aimRating > 0)
        {
            float result = Random.Range(0, 100.0f);
            if (result > aimRating)
                return;
            aimTarget = Weapon.GetBody().OnArm();
            if (Singleton<MainCamera>.Instance == null || aimTarget == null)
                return;
            MainCamera mainCamera = Singleton<MainCamera>.Instance;
            mainCamera.SetTarget(aimTarget);
            mainCamera.OnZoomIn();
        }

        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        CHARACTER character = CHARACTER.NONE;
        float power = shootPower;
        int damage = 0;

        for (int i = 0; i < barrels.Length; i++)
        {
            if (barrels[i] == null)
                continue;
            string path = "Prefab/Object/Bullet/" + barrels[i].bullet;
            if (Resources.Load(path) == null || barrels[i].barrel == null)
                continue;
            GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, barrels[i].barrel, path);
            obj.transform.up = barrels[i].barrel.up;

            if (Weapon != null)
            {
                damage = Weapon.GetCurrentAtk();
                if (Weapon.GetBody() == null)
                    return;
                Body body = Weapon.GetBody();
                if (body.GetComponent<Player>())
                    character = CHARACTER.PLAYER;
                else if (body.GetComponent<Enemy>())
                    character = CHARACTER.ENEMY;
            }

            if (barrels[i].shootVfx != null)
                barrels[i].shootVfx.SetActive(true);

            if (!obj.GetComponent<Bullet>())
                return;
            Bullet bulletComponent = obj.GetComponent<Bullet>();
            bulletComponent.SetGunner(character, transform, power, damage);
            if (aimTarget == null)
            {
                if (isCrist)
                    bulletComponent.EnableBulletVfx(true);

                if (!obj.GetComponent<Rigidbody2D>())
                    continue;
                Rigidbody2D objBody = obj.GetComponent<Rigidbody2D>();
                Body2DEffector.Velocity(objBody, barrels[i].barrel.up, power);
            }
            else
            {
                bulletComponent.SetAimTarget(aimTarget);
                string vfxPath = "Prefab/Object/Vfx/vfx_aim";
                if (Resources.Load(vfxPath) == null)
                    return;
                GameObject vfxObj = ObjectManager.Call<GameObject>(gameManager.transform, aimTarget.transform, vfxPath);
                vfxObj.transform.SetParent(aimTarget, true);
                vfxObj.transform.position = vfxObj.transform.parent.position;
                vfxObj.transform.rotation = Quaternion.identity;
            }

            returned.Add(bulletComponent);
        }

        if (isHidenShoot && spr != null)
            spr.color = Color.clear;
        if (isCristShoot)
            isCrist = !isCrist;
        shooted += 1;

        SoundManager.Play(AUDIO_KIND.SOUND, clip, false);
    }

    private IEnumerator OnShootAgain(float time)
    {
        yield return new WaitForSeconds(time);
        if (shootCoroutine != null)
            StopCoroutine(shootCoroutine);
        if (shooted < shootLimit)
        {
            OnShoot();
            shootCoroutine = StartCoroutine(OnShootAgain(shootAgain));
        }
        else
        {
            if (aimTarget == null && !isHidenShoot)
                Reset();
        }
    }

    public void Reset()
    {
        returned.Clear();
        aimTarget = null;
        isCrist = false;
        if (SkillFactor != null)
            SkillFactor.OnDelayReset();
        if (spr != null)
            spr.color = Color.white;
        for (int i = 0; i < barrels.Length; i++)
        {
            if (barrels[i].shootVfx != null)
                barrels[i].shootVfx.SetActive(false);
        }
    }

    public void Shoot()
    {
        shooted = 0;
        if (shootLimit <= 0)
        {
            OnShoot();
            Reset();
        }
        else
        {
            OnShoot();
            if (shootCoroutine != null)
                StopCoroutine(shootCoroutine);
            shootCoroutine = StartCoroutine(OnShootAgain(shootAgain));
        }
    }

    public void OnBulletReturn(Bullet bullet)
    {
        if (returned.Count <= 0)
            return;
        returned.Remove(bullet);
        if (returned.Count > 0 || !isHidenShoot || spr == null)
            return;
        Reset();
    }

    public void OnClearAimTarget()
    {
        Reset();
        if (Singleton<MainCamera>.Instance == null)
            return;
        MainCamera mainCamera = Singleton<MainCamera>.Instance;
        mainCamera.ReturnTarget();
    }
}