﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Bullet : ToggleBehaviour
{
    private WEAPON_SKILL_STATE state;
    private CHARACTER character;
    private Rigidbody2D mRigidbody;
    private Coroutine backstackCoroutine;
    private Transform gunner;
    private Transform headTarget;
    private Transform aimTarget;

    private float speed;
    private int damage;

    [SerializeField] private GameEnums.VFX exploredVfx;
    [SerializeField] private GameEnums.AUDIO_CLIP clip;
    [SerializeField] private GameObject bulletVfx;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float fowardDuration;
    [SerializeField] private bool isBackstack;
    [SerializeField] private bool isHeadShoot;

    #region property

    private GunnerSkill GunnerSkill
    {
        get
        {
            GunnerSkill skill = null;
            if (gunner != null && gunner.GetComponent<GunnerSkill>())
                skill = gunner.GetComponent<GunnerSkill>();
            return skill;
        }
    }

    private Rigidbody2D Rigidbody
    {
        get
        {
            if (mRigidbody == null && GetComponent<Rigidbody2D>())
                mRigidbody = GetComponent<Rigidbody2D>();
            return mRigidbody;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        EnableBulletVfx(false);
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        OnUpdate();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                Init();
                if (!GetComponent<ObjectPooling>())
                    return;
                ObjectPooling objectPooling = GetComponent<ObjectPooling>();
                objectPooling.Return();
                break;
        }
    }

    private void Init()
    {
        state = WEAPON_SKILL_STATE.NONE;
        character = CHARACTER.NONE;
        damage = 0;
        speed = 0;
        gunner = null;
        headTarget = null;
        aimTarget = null;
    }

    private void ShootHead()
    {
        if (!isHeadShoot)
            return;
        switch (character)
        {
            case CHARACTER.PLAYER:
                if (Singleton<CharacterManager>.Instance == null)
                    return;
                CharacterManager characterManager = Singleton<CharacterManager>.Instance;
                List<float> distances = new List<float>();
                for (int i = 0; i < characterManager.GetTransforms().Count; i++)
                {
                    float distance = Vector3.Distance(transform.position,
                        characterManager.GetTransforms()[i].position);
                    if (!distances.Contains(distance))
                        distances.Add(distance);
                }

                int min = 0;
                for (int i = 0; i < distances.Count; i++)
                {
                    if (distances[i] > distances.Min())
                        continue;
                    min = i;
                }

                if (characterManager.GetTransforms().Count <= 0 ||
                    characterManager.GetTransforms()[min] == null ||
                    !characterManager.GetTransforms()[min].gameObject.activeInHierarchy ||
                    !characterManager.GetTransforms()[min].GetComponent<Body>())
                    return;
                Body body = characterManager.GetTransforms()[min].GetComponent<Body>();
                headTarget = body.GetHead();
                break;

            case CHARACTER.ENEMY:
                if (Singleton<Player>.Instance == null)
                    return;
                Player player = Singleton<Player>.Instance;
                if (player.gameObject.activeInHierarchy)
                    headTarget = player.GetBody().GetHead();
                break;
        }

        if (headTarget != null)
        {
            Body2DEffector.Velocity(Rigidbody, 0.0f, 0.0f);
            transform.position =
                Vector3.MoveTowards(transform.position, headTarget.position, GameConfig.hookSpeed * Time.deltaTime);
        }
    }

    private void Backstack()
    {
        if (!isBackstack || state != WEAPON_SKILL_STATE.STOP || gunner == null)
            return;
        float distance = Vector3.Distance(transform.position, gunner.position);
        if (distance > GameConfig.backStackRange)
            transform.position = Vector3.MoveTowards(
                new Vector2(transform.position.x, transform.position.y),
                new Vector2(gunner.transform.position.x, gunner.transform.position.y),
                speed * Time.deltaTime);
        else
        {
            if (GunnerSkill != null)
                GunnerSkill.OnBulletReturn(this);
            if (!GetComponent<ObjectPooling>())
                return;
            ObjectPooling objectPooling = GetComponent<ObjectPooling>();
            objectPooling.Return();
        }
    }

    private void MoveToAim()
    {
        if (aimTarget == null)
            return;
        Vector2 direction = aimTarget.position - transform.position;
        direction.Normalize();
        Body2DEffector.Velocity(Rigidbody, direction, speed);
    }

    private void OnUpdate()
    {
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);

        ShootHead();
        Backstack();
        MoveToAim();
    }

    private IEnumerator OnBackstack()
    {
        yield return new WaitForSeconds(fowardDuration);
        if (Rigidbody != null)
            Body2DEffector.Velocity(Rigidbody, 0.0f, 0.0f);
        if (isBackstack)
            state = WEAPON_SKILL_STATE.STOP;
        else
        {
            if (backstackCoroutine != null)
                StopCoroutine(backstackCoroutine);
            Return();
        }
    }

    private void SetReturnVfx()
    {
        string path = "Prefab/Object/Vfx/" + exploredVfx;
        if (Resources.Load(path) == null || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, transform, path);
        if (!obj.GetComponent<Vfx>())
            return;
        Vfx vfx = obj.GetComponent<Vfx>();
        vfx.SetDamage(character, damage);
    }

    private void Return()
    {
        SetReturnVfx();
        state = WEAPON_SKILL_STATE.STOP;
        if (Rigidbody != null)
            Body2DEffector.Velocity(Rigidbody, 0.0f, 0.0f);
        if (aimTarget != null)
        {
            if (gunner == null || !gunner.GetComponent<GunnerSkill>())
                return;
            GunnerSkill gunnerSkill = gunner.GetComponent<GunnerSkill>();
            gunnerSkill.OnClearAimTarget();
        }

        if (!isBackstack)
        {
            Init();
            if (!GetComponent<ObjectPooling>())
                return;
            ObjectPooling objectPooling = GetComponent<ObjectPooling>();
            objectPooling.Return();
        }

        if (backstackCoroutine != null)
            StopCoroutine(backstackCoroutine);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.parent != null && other.transform.parent.GetComponent<BodyPath>())
        {
            BodyPath path = other.transform.parent.GetComponent<BodyPath>();
            if (path.GetBody() == null || gunner == null || !gunner.GetComponent<Weapon>())
                return;
            Weapon weapon = gunner.GetComponent<Weapon>();
            if (path.GetBody().Equals(weapon.GetBody()))
                return;
            float damagePercent = path.GetDamagePercent();
            damage = damage + (int) (damage * path.GetCriticalRate());
            if (damagePercent > 0)
            {
                SoundManager.Play(AUDIO_KIND.SOUND, clip, false);

                if (aimTarget != null)
                {
                    Eventon<WeaponHitEvent>.Trigger(new WeaponHitEvent()
                    {
                        collisionPosition = path.transform.position
                    });
                }

                switch (character)
                {
                    case CHARACTER.PLAYER:
                        if (!path.GetBody().GetComponent<Enemy>())
                            return;
                        Enemy enemy = path.GetBody().GetComponent<Enemy>();
                        enemy.OnHpMinus((int) (damage * damagePercent / 100));

                        if (path.GetIntact() <= 0)
                            UIGameplay.Instance.DisplayCriticalDamage(path.transform.position, damage);
                        else
                            UIGameplay.Instance.DisplayDamage(CHARACTER.PLAYER, path.transform.position, damage);

                        if (enemy.IsDrop())
                        {
                            enemy.OnSetHit(0);
                            path.GetBody().OnDropWeapon();
                        }

                        enemy.DisplayHp();
                        enemy.OnDropHitUp(1);

                        break;

                    case CHARACTER.ENEMY:
                        if (!path.GetBody().GetComponent<Player>())
                            return;
                        Player player = path.GetBody().GetComponent<Player>();
                        player.OnHpMinus((int) (damage * damagePercent / 100));

                        if (path.GetIntact() <= 0)
                            UIGameplay.Instance.DisplayCriticalDamage(path.transform.position, damage);
                        else
                            UIGameplay.Instance.DisplayDamage(CHARACTER.ENEMY, path.transform.position, damage);

                        if (player.IsDrop())
                        {
                            player.OnSetHit(0);
                            path.GetBody().OnDropWeapon();
                        }

                        player.DisplayHp();
                        player.OnDropHitUp(1);

                        break;
                }

                path.OnIntactMinus(1);
                path.GetBlood();

                SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_bullet_hit, false);
            }
            else
            {
                path.GetBlock();
                SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_collision_block, false);
            }

            Return();
        }
    }

    public void EnableBulletVfx(bool isStatus)
    {
        if (bulletVfx != null)
            bulletVfx.SetActive(isStatus);
    }

    public void SetGunner(CHARACTER character, Transform gunner, float speed, int damage)
    {
        this.character = character;
        this.gunner = gunner;
        this.speed = speed;
        this.damage = damage;
        state = WEAPON_SKILL_STATE.START;
        if (fowardDuration > 0)
            backstackCoroutine = StartCoroutine(OnBackstack());
    }

    public void SetAimTarget(Transform target)
    {
        if (aimTarget == null)
            aimTarget = target;
    }
}