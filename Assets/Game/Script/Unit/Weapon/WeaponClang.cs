﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class WeaponClang : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY || !other.gameObject.GetComponent<WeaponClang>())
            return;
        GameEnums.AUDIO_CLIP clip = GameEnums.AUDIO_CLIP.None;
        int result = Random.Range(0, 3);
        string clipName = "sfx_weapon_collision_" + result;
        Enum.TryParse(clipName, out clip);
        SoundManager.Play(AUDIO_KIND.SOUND, clip, false);

        string vfxPath = "Prefab/Object/Vfx/vfx_bullet_explore";
        ObjectManager.Call<GameObject>(gameManager.transform, transform, vfxPath);
    }
}