﻿using System.Collections;
using UnityEngine;

public enum WEAPON_SKILL
{
    NONE,
    BACKSTACK,
    GUNNER,
    STINGER,
    ROTATER,
    BACKSTACK_ROTATER,
    CONNECTER
}

public enum WEAPON_SKILL_STATE
{
    NONE,
    START,
    STOP
}

public class WeaponSkillFactor : ToggleBehaviour
{
    private Coroutine resetCoroutine;
    private Weapon weapon;
    private float countDown;
    private float delay;
    private bool isStarted;

    [SerializeField] private WEAPON_SKILL weaponSkill;

    protected override void OnAppear()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        OnCountDown();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
            case GAME_STATE.WIN:
            case GAME_STATE.LOSE:
                isStarted = false;
                if (resetCoroutine != null)
                    StopCoroutine(resetCoroutine);
                break;
        }
    }

    private void Init()
    {
        isStarted = false;
        if (!GetComponent<Weapon>() || Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        weapon = GetComponent<Weapon>();
        countDown = weaponManager.GetCooldown(weapon.GetWeapon());
        delay = countDown;
    }

    private IEnumerator OnWaitForDelayReset(float time)
    {
        yield return new WaitForSeconds(time);
        OnDelayReset();
    }

    private void OnAction()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;

        float lenght = 0;
        BackstackSkill backstackSkill = null;
        StingSkill stingSkill = null;
        RotateSkill rotateSkill = null;
        ConnectedSkill connectedSkill = null;

        if (GetComponent<BackstackSkill>())
            backstackSkill = GetComponent<BackstackSkill>();
        if (GetComponent<StingSkill>())
            stingSkill = GetComponent<StingSkill>();
        if (GetComponent<RotateSkill>())
            rotateSkill = GetComponent<RotateSkill>();
        if (GetComponent<ConnectedSkill>())
            connectedSkill = GetComponent<ConnectedSkill>();

        switch (weaponSkill)
        {
            case WEAPON_SKILL.BACKSTACK:
                if (backstackSkill != null)
                    backstackSkill.ChangeState(WEAPON_SKILL_STATE.START);
                break;
            case WEAPON_SKILL.GUNNER:
                if (!GetComponent<GunnerSkill>())
                    return;
                GunnerSkill gunnerSkill = GetComponent<GunnerSkill>();
                gunnerSkill.Shoot();
                break;
            case WEAPON_SKILL.STINGER:
                if (stingSkill == null)
                    return;
                stingSkill.Sting();
                break;
            case WEAPON_SKILL.ROTATER:
                if (rotateSkill == null)
                    return;
                lenght = rotateSkill.Rotate();
                resetCoroutine = StartCoroutine(OnWaitForDelayReset(lenght));
                break;
            case WEAPON_SKILL.BACKSTACK_ROTATER:
                if (rotateSkill == null)
                    return;
                lenght = rotateSkill.Rotate();
                if (backstackSkill == null)
                    return;
                backstackSkill.DelayChangeState(WEAPON_SKILL_STATE.START, lenght);
                break;
            case WEAPON_SKILL.CONNECTER:
                if (connectedSkill == null)
                    return;
                connectedSkill.SetWeapon(weapon);
                connectedSkill.CastLine();
                break;
        }
    }

    private void OnCountDown()
    {
        if (weapon == null || weapon.GetWeapon() == GameEnums.WEAPON.NONE || !weapon.IsHasBody() ||
            UIFight.Instance != null || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        if (delay > 0)
            delay -= Time.deltaTime;
        else
        {
            if (isStarted)
                return;
            isStarted = true;
            OnAction();
        }
    }

    public void OnDelayReset()
    {
        isStarted = false;
        delay = countDown;
    }

    public void SetDelayResetTime(float time)
    {
        resetCoroutine = StartCoroutine(OnWaitForDelayReset(time));
    }

    public void Return()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        BackstackSkill backstackSkill = null;
        GunnerSkill gunnerSkill = null;
        StingSkill stingSkill = null;
        RotateSkill rotateSkill = null;
        ConnectedSkill connectedSkill = null;

        if (GetComponent<BackstackSkill>())
            backstackSkill = GetComponent<BackstackSkill>();
        if (GetComponent<GunnerSkill>())
            gunnerSkill = GetComponent<GunnerSkill>();
        if (GetComponent<StingSkill>())
            stingSkill = GetComponent<StingSkill>();
        if (GetComponent<RotateSkill>())
            rotateSkill = GetComponent<RotateSkill>();
        if (GetComponent<ConnectedSkill>())
            connectedSkill = GetComponent<ConnectedSkill>();

        switch (weaponSkill)
        {
            case WEAPON_SKILL.BACKSTACK:
                if (backstackSkill != null)
                    backstackSkill.Reset();
                break;
            case WEAPON_SKILL.GUNNER:
                OnDelayReset();
                if (gunnerSkill != null)
                    gunnerSkill.Reset();
                break;
            case WEAPON_SKILL.STINGER:
                if (stingSkill != null)
                    stingSkill.Reset();
                break;
            case WEAPON_SKILL.ROTATER:
                if (rotateSkill != null)
                    rotateSkill.Reset();
                break;
            case WEAPON_SKILL.BACKSTACK_ROTATER:
                if (backstackSkill != null)
                    backstackSkill.Reset();
                if (rotateSkill != null)
                    rotateSkill.Reset();
                break;
            case WEAPON_SKILL.CONNECTER:
                if (connectedSkill != null)
                    connectedSkill.Reset();
                break;
        }
    }
}