﻿using System.Collections;
using UnityEngine;

public class ClothPath : MonoBehaviour
{
    private SpriteRenderer mRender;

    [SerializeField] private SpriteRenderer sprite;

    #region property

    private SpriteRenderer Render
    {
        get
        {
            if (mRender == null && GetComponent<SpriteRenderer>())
                mRender = GetComponent<SpriteRenderer>();
            return mRender;
        }
    }

    #endregion

    private void OnEnable()
    {
        StartCoroutine(OnInit());
    }

    private IEnumerator OnInit()
    {
        yield return new WaitForSeconds(0.1f);
        if (Render != null && sprite != null)
            Render.sortingOrder = sprite.sortingOrder - 1;
    }
}