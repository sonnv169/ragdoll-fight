﻿using UnityEngine;

public class Stage : ToggleBehaviour
{
    [SerializeField] private Transform fisinhLocation;
    [SerializeField] private Transform topGround;
    [SerializeField] private Transform leftGround;
    [SerializeField] private Transform rightGround;
    [SerializeField] private Transform botGround;

    protected override void OnAppear()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                if (!GetComponent<ObjectPooling>())
                    return;
                ObjectPooling objectPooling = GetComponent<ObjectPooling>();
                objectPooling.Return();
                break;
        }
    }

    private void Init()
    {
        if (Singleton<MainCamera>.Instance == null)
            return;
        MainCamera mainCamera = Singleton<MainCamera>.Instance;
        mainCamera.transform.position = transform.position;
    }

    public Vector3 GetFinishLocation()
    {
        Vector3 location = Vector3.zero;
        if (fisinhLocation != null)
            location = fisinhLocation.position;
        return location;
    }

    public Transform GetTopGround()
    {
        return topGround;
    }

    public Transform GetLeftGround()
    {
        return leftGround;
    }

    public Transform GetRightGround()
    {
        return rightGround;
    }

    public Transform GetBotGround()
    {
        return botGround;
    }
}