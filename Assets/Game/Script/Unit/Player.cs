﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class BoosterEvent
{
    public GameEnums.BOOSTER booster;
    public bool isCasting;
}

public class Player : Unit
{
    private Coroutine setWeaponCoroutine;
    private Coroutine setShieldCoroutine;
    private Coroutine setDoublePowerCoroutine;
    private Coroutine disableAnimatorCoroutine;
    private Body mBody;
    private Vfx vfxShield;
    private Vfx vfxLeftDoublePower;
    private Vfx vfxRightDoublePower;
    private int dropHit;
    private bool isShield;


    #region property

    private Body Body
    {
        get
        {
            if (mBody == null && GetComponent<Body>())
                mBody = GetComponent<Body>();
            return mBody;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Eventon<GAME_STATE>.EventStatic += OnGameState;
        Eventon<UnitDeathEvent>.EventStatic += OnUnitDeathEvent;
    }

    protected override void OnCyclic()
    {
        Clamp();
        Flip();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
        Eventon<UnitDeathEvent>.EventStatic -= OnUnitDeathEvent;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
            case GAME_STATE.WIN:
                if (Body != null)
                    Body.Scale(GameConfig.faceScale);
                if (disableAnimatorCoroutine != null)
                    StopCoroutine(disableAnimatorCoroutine);
                break;
        }
    }

    private void OnUnitDeathEvent(UnitDeathEvent param)
    {
        if (param.unit != this)
            return;
        DisplayHp();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (!transform.GetChild(i).GetComponent<Cursor>())
                continue;
            Cursor cursor = transform.GetChild(i).GetComponent<Cursor>();
            cursor.Return();
        }

        if (UIGameplay.Instance != null)
        {
            UIGameplay uiGameplay = UIGameplay.Instance;
            uiGameplay.DisplayPlayerAvatar(Color.gray);
        }

        if (Body != null)
            Body.OnBodyExplore();

        Eventon<GAME_STATE>.Trigger(GAME_STATE.LOSE);
    }


    public override void Flip()
    {
        if (Singleton<GameManager>.Instance == null || Singleton<CharacterManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY || characterManager.GetTransform() <= 0 || Body == null)
            return;
        List<float> distances = new List<float>();
        for (int i = 0; i < characterManager.GetTransform(); i++)
        {
            float distance = Vector2.Distance(transform.position, characterManager.GetTransforms()[i].position);
            distances.Add(distance);
        }

        if (distances.Count <= 0)
            return;
        for (int i = 0; i < distances.Count; i++)
        {
            if (distances[i] > distances.Min() ||
                !characterManager.GetTransforms()[i].gameObject.activeInHierarchy)
                continue;
            Transform target = characterManager.GetTransforms()[i];
            if (target.position.x < transform.position.x)
                Body.Scale(-GameConfig.faceScale);
            else if (target.position.x > transform.position.x)
                Body.Scale(GameConfig.faceScale);
        }
    }

    public override void Move()
    {
        if (Singleton<StickDrager>.Instance == null || Body == null || GetHp() <= 0)
            return;
        StickDrager stickDrager = Singleton<StickDrager>.Instance;
        Body.SetVelocity(stickDrager.GetDirection(), GetSpeed());
    }

    public override void Stop()
    {
        if (Body != null)
            Body.SetFlying(false);
    }

    public override void DisplayHp()
    {
        if (UIGameplay.Instance == null || Singleton<CharacterManager>.Instance == null)
            return;
        UIGameplay uiGameplay = UIGameplay.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        int level = characterManager.GetHeroLevel();
        uiGameplay.DisplayPlayerHp(GetHp(), characterManager.GetHeroHp(level));
    }

    public override void OnDropHitUp(int hit)
    {
        dropHit += hit;
    }

    public override void OnSetHit(int hit)
    {
        dropHit = hit;
    }

    public override bool IsDrop()
    {
        return dropHit >= GetDropHit();
    }

    private void Clamp()
    {
        if (Body == null || GetHp() <= 0)
            return;
        Body.Clamp();
    }

    private IEnumerator OnSetWeapon(GameObject weapon)
    {
        yield return new WaitForSeconds(GameConfig.initTime);
        if (Body != null && gameObject.activeInHierarchy)
        {
            SetDropHit(GameConfig.dropHit);
            Body.SetWeapon(weapon);
            GameManager gameManager = Singleton<GameManager>.Instance;
            if (gameManager != null && gameManager.GetState() == GAME_STATE.PLAY)
            {
                Body.SetBodyKind(BODY_KIND.DYNAMIC);
                Body.SetJointBodyKind(BODY_KIND.DYNAMIC);
                Body.EnableClamp(true);
            }
        }
    }

    private IEnumerator EndShield()
    {
        yield return new WaitForSeconds(GameConfig.shieldDuration);
        if (vfxShield != null)
            vfxShield.PenaltyReturn();
        isShield = false;
    }

    private IEnumerator EndDoublePower()
    {
        yield return new WaitForSeconds(GameConfig.doublePowerDuration);
        if (vfxLeftDoublePower != null)
            vfxLeftDoublePower.PenaltyReturn();
        if (vfxRightDoublePower != null)
            vfxRightDoublePower.PenaltyReturn();
        if (Body != null)
        {
            Body.SetBodyWeaponPower(GetPower());
            if (Body.GetEquiptedWeapon() != null)
            {
                Weapon weapon = Body.GetEquiptedWeapon();
                int weaponPower = weapon.GetOriginAtk();
                weapon.SetAtk(weaponPower);
            }
        }
    }

    private void SetCursor()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string path = "Prefab/Object/Vfx/cursor";
        if (Body == null || Resources.Load(path) == null)
            return;
        GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, Body.GetHead(), path);
        obj.transform.SetParent(transform, true);
        obj.transform.position = new Vector3(
            Body.GetHead().position.x, Body.GetHead().position.y + GameConfig.cursorPosition.y,
            Body.GetHead().position.z);
        if (!obj.GetComponent<Cursor>())
            return;
        Cursor cursor = obj.GetComponent<Cursor>();
        cursor.Init(Color.green);
    }

    private IEnumerator OnDisableBodyAnimator(float time)
    {
        yield return new WaitForSeconds(time);
        if (Body != null && Body.GetAnimator() != null)
            Body.GetAnimator().enabled = false;
    }

    public void Init()
    {
        if (Body != null)
        {
            Body.EnableClamp(true);
            Body.Scale(GameConfig.faceScale);
            Body.SetBodyWeaponPower(GetPower());
        }

        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        OnSetHit(0);
        if (Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        int level = characterManager.GetHeroLevel();

        SetHp(characterManager.GetHeroHp(level));
        SetSpeed(GameConfig.flyingSpeed);
        SetPower(characterManager.GetHeroAtk(level));
        SetDropHit(GameConfig.dropHit);

        transform.rotation = Quaternion.identity;
        SetCursor();
        SetWeapon();

        if (setShieldCoroutine != null)
            StopCoroutine(setShieldCoroutine);
        if (setDoublePowerCoroutine != null)
            StopCoroutine(setDoublePowerCoroutine);

        if (UIGameplay.Instance != null)
        {
            UIGameplay uiGameplay = UIGameplay.Instance;
            uiGameplay.DisplayPlayerHp(GetHp(), characterManager.GetHeroHp(level));
        }

        if (Singleton<MainCamera>.Instance == null)
            return;
        MainCamera mainCamera = Singleton<MainCamera>.Instance;
        mainCamera.SetTarget(transform);
    }

    public void SetWeapon()
    {
        if (Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        GameObject weapon = weaponManager.SetWeapon(weaponManager.GetEquipted(), transform.position);
        weapon.transform.localScale = GameConfig.weaponIngameScale;
        weapon.transform.rotation = Quaternion.Euler(0, 0, 0);
        if (setWeaponCoroutine != null)
            StopCoroutine(setWeaponCoroutine);
        setWeaponCoroutine = StartCoroutine(OnSetWeapon(weapon));
    }

    public void Reborn()
    {
        if (Body == null || Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        int level = characterManager.GetHeroLevel();

        SetDropHit(GameConfig.dropHit);
        if (setShieldCoroutine != null)
            StopCoroutine(setShieldCoroutine);
        if (setDoublePowerCoroutine != null)
            StopCoroutine(setDoublePowerCoroutine);

        if (UIGameplay.Instance != null)
        {
            UIGameplay uiGameplay = UIGameplay.Instance;
            uiGameplay.DisplayPlayerAvatar(Color.white);
            uiGameplay.DisplayPlayerHp(GetHp(), characterManager.GetHeroHp(level));
        }

        if (Singleton<MainCamera>.Instance != null)
        {
            MainCamera mainCamera = Singleton<MainCamera>.Instance;
            mainCamera.SetTarget(transform);
        }

        SetHp(characterManager.GetHeroHp(level));
        SetDoublePower();
        SetShield();
        SetCursor();

        OnSetHit(0);
        if (vfxShield != null)
            vfxShield.PenaltyReturn();
        isShield = false;
        if (vfxLeftDoublePower != null)
            vfxLeftDoublePower.PenaltyReturn();
        if (vfxRightDoublePower != null)
            vfxRightDoublePower.PenaltyReturn();

        if (Body.GetAnimator() != null)
        {
            Body.EnableClamp(false);
            Body.SetBodyKind(BODY_KIND.STATIC);
            Body.SetJointBodyKind(BODY_KIND.STATIC);
            if (Body.GetAnimator() == null)
                return;
            Body.GetAnimator().enabled = true;
            AnimatorManager.Play(Body.GetAnimator(), "anim_tpose");
            float duration = AnimatorManager.GetLenght(Body.GetAnimator(), "anim_tpose");
            disableAnimatorCoroutine = StartCoroutine(OnDisableBodyAnimator(duration));
        }

        Body.EnableClamp(true);
        Body.Scale(GameConfig.faceScale);
        Body.SetBodyKind(BODY_KIND.DYNAMIC);
        Body.SetJointBodyKind(BODY_KIND.DYNAMIC);

        if (Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        transform.position = stageManager.transform.position;
    }

    public void SetHpRegen()
    {
        if (Singleton<CharacterManager>.Instance == null ||
            Singleton<GameManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        GameManager gameManager = Singleton<GameManager>.Instance;
        int level = characterManager.GetHeroLevel();
        SetHp(characterManager.GetHeroHp(level));
        UIGameplay.Instance.DisplayPlayerHp(GetHp(), characterManager.GetHeroHp(level));

        string hpRegenPath = "Prefab/Object/Vfx/vfx_hp_regen";
        ObjectManager.Call<GameObject>(gameManager.transform, transform, hpRegenPath);
    }

    public void SetShield()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string shieldPath = "Prefab/Object/Vfx/vfx_shield";
        setShieldCoroutine = StartCoroutine(EndShield());
        isShield = true;

        GameObject obj =
            ObjectManager.Call<GameObject>(gameManager.transform, transform, shieldPath);
        obj.transform.SetParent(transform, true);
        obj.transform.position = obj.transform.parent.position;

        if (!obj.GetComponent<Vfx>())
            return;
        Vfx vfxObj = obj.GetComponent<Vfx>();
        vfxShield = vfxObj;
    }

    public void SetDoublePower()
    {
        if (Singleton<GameManager>.Instance == null || Body == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string doublePowerPath = "Prefab/Object/Vfx/vfx_double_power";
        setDoublePowerCoroutine = StartCoroutine(EndDoublePower());

        if (Body.GetInsideWeapon() != null)
        {
            GameObject obj =
                ObjectManager.Call<GameObject>(gameManager.transform, transform, doublePowerPath);
            obj.transform.SetParent(Body.GetInsideWeapon().transform, true);
            obj.transform.position = obj.transform.parent.position;

            if (!obj.GetComponent<Vfx>())
                return;
            Vfx vfxObj = obj.GetComponent<Vfx>();
            vfxLeftDoublePower = vfxObj;
        }

        int bodyPower = GetPower() * 2;
        Body.SetBodyWeaponPower(bodyPower);

        if (Body.GetEquiptedWeapon() == null)
            return;
        Weapon weapon = Body.GetEquiptedWeapon();
        int weaponPower = weapon.GetOriginAtk();
        weapon.SetAtk(weaponPower * 2);
    }

    public void SetWeaponRandom()
    {
        ReturnWeapon();
        if (Body == null)
            return;
        if (Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        GameObject obj = weaponManager.SetWeapon(weaponManager.GetWeaponRandom(), GameConfig.infernal);
        obj.transform.localScale = GameConfig.weaponIngameScale;
        obj.transform.rotation = Quaternion.Euler(0, 0, 0);
        Body.SetWeapon(obj);
        SetDropHit(GameConfig.dropHit);
    }

    public void ReturnWeapon()
    {
        if (Body != null)
            Body.OnDropWeaponAndReturn();
    }

    public void ClearWeapon()
    {
        if (Body != null)
            Body.OnClearWeapon();
    }

    public Body GetBody()
    {
        return Body;
    }

    public bool IsSheild()
    {
        return isShield;
    }
}