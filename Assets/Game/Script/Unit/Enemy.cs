﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : Unit
{
    private Coroutine stopCoroutine;
    private Coroutine setWeaponCoroutine;
    private Coroutine setShieldCoroutine;
    private Coroutine setDoublePowerCoroutine;
    private Vector2 direction;
    private Body mBody;
    private float coolDown;
    private float currentCooldown;
    private float focusRate;
    private int dropHit;
    private bool isDeath;

    #region property

    private Body Body
    {
        get
        {
            if (mBody == null && GetComponent<Body>())
                mBody = GetComponent<Body>();
            return mBody;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
        Eventon<UnitDeathEvent>.EventStatic += OnUnitDeathEvent;
    }

    protected override void OnCyclic()
    {
        Clamp();
        Move();
        Flip();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
        Eventon<UnitDeathEvent>.EventStatic -= OnUnitDeathEvent;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
            case GAME_STATE.WIN:
                if (Body != null)
                    Body.Scale(GameConfig.faceScale);
                break;
        }
    }

    private void OnUnitDeathEvent(UnitDeathEvent param)
    {
        if (isDeath || param.unit != this || Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        isDeath = true;
        DisplayHp();
        stageManager.DownTarget(1);
        if (Body != null)
            Body.OnBodyExplore();
    }

    public override void Flip()
    {
        if (Singleton<GameManager>.Instance == null || Singleton<Player>.Instance == null || UIFight.Instance != null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        Player player = Singleton<Player>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY || Body == null)
            return;
        if (player.transform.position.x < transform.position.x)
            Body.Scale(-GameConfig.faceScale);
        else if (player.transform.position.x > transform.position.x)
            Body.Scale(GameConfig.faceScale);
    }

    public override void Move()
    {
        if (Singleton<TutorialManager>.Instance == null)
            return;
        TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;

        if (!tutorialManager.GetTutorialData().isMove || GetHp() <= 0)
            return;
        if (currentCooldown > 0)
            currentCooldown -= Time.deltaTime;
        else
        {
            if (Body == null || Singleton<Player>.Instance == null || UIFight.Instance != null)
                return;
            Player player = Singleton<Player>.Instance;
            if (player.GetHp() <= 0)
                return;
            Vector2 attackDirection = Vector2.zero;
            bool isResult = false;
            float focusRange = Random.Range(0, 100.0f);
            if (focusRange >= 100.0f - focusRate)
                isResult = true;
            switch (isResult)
            {
                case true:
                    attackDirection = player.transform.position - transform.position;
                    break;
                case false:
                    float failRange = Random.Range(0, 3.0f);
                    if (failRange >= 1.5f)
                        attackDirection = new Vector2(player.transform.position.x - transform.position.x,
                            player.transform.position.y - transform.position.y + GameConfig.enemyFocusFailRange);
                    else
                        attackDirection = new Vector2(player.transform.position.x - transform.position.x,
                            player.transform.position.y - transform.position.y - GameConfig.enemyFocusFailRange);
                    break;
            }

            Body.SetVelocity(attackDirection, GetSpeed());
            if (stopCoroutine != null)
                StopCoroutine(stopCoroutine);
            stopCoroutine = StartCoroutine(OnStop());
            currentCooldown = coolDown;
        }
    }

    public override void Stop()
    {
        if (Body != null)
            Body.SetFlying(false);
    }

    public override void DisplayHp()
    {
        if (UIGameplay.Instance == null || Singleton<StageManager>.Instance == null)
            return;
        UIGameplay uiGameplay = UIGameplay.Instance;
        StageManager stageManager = Singleton<StageManager>.Instance;
        int stage = stageManager.GetStage();
        uiGameplay.DisplayEnemyHp(GetHp(), stageManager.GetEnemyHp(stage));
    }

    public override void OnDropHitUp(int hit)
    {
        dropHit += hit;
    }

    public override void OnSetHit(int hit)
    {
        dropHit = hit;
    }

    public override bool IsDrop()
    {
        return dropHit >= GetDropHit();
    }

    private IEnumerator OnStop()
    {
        yield return new WaitForSeconds(GameConfig.enemyStopTime);
        Stop();
    }

    private void Init()
    {
        isDeath = false;
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        OnSetHit(0);
        if (Singleton<CharacterManager>.Instance == null || Singleton<StageManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        StageManager stageManager = Singleton<StageManager>.Instance;
        int index = stageManager.GetStage() + stageManager.GetWave();

        GameEnums.AI ai = characterManager.GetEnemyAI(index);
        coolDown = characterManager.GetEnemyCooldownAttack(ai);
        currentCooldown = coolDown;
        focusRate = characterManager.GetEnemyFocusRate(ai);

        SetHp(stageManager.GetEnemyHp(index));
        SetSpeed(characterManager.GetEnemySpeed(ai) * GameConfig.enemyBonusSpeed);
        SetPower(stageManager.GetEnemyAtk(index));
        SetDropHit(GameConfig.dropHit);

        transform.rotation = Quaternion.identity;
        SetWeapon();

        if (Body != null)
            Body.SetBodyWeaponPower(GetPower());
    }

    private void Clamp()
    {
        if (Body == null || GetHp() <= 0)
            return;
        Body.Clamp();
    }

    private void SetWeapon()
    {
        if (Singleton<WeaponManager>.Instance == null || Singleton<StageManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        StageManager stageManager = Singleton<StageManager>.Instance;
        int index = stageManager.GetStage() + stageManager.GetWave();
        GameObject weapon = weaponManager.SetWeapon(stageManager.GetEnemyWeapon(index), transform.position);
        weapon.transform.localScale = GameConfig.weaponIngameScale;
        weapon.transform.rotation = Quaternion.Euler(0, 0, 0);
        if (setWeaponCoroutine != null)
            StopCoroutine(setWeaponCoroutine);
        setWeaponCoroutine = StartCoroutine(OnSetWeapon(weapon));
    }

    private IEnumerator OnSetWeapon(GameObject weapon)
    {
        yield return new WaitForSeconds(GameConfig.initTime);
        if (gameObject.activeInHierarchy && Body != null)
        {
            Body.SetWeapon(weapon);
            Body.SetBodyKind(BODY_KIND.DYNAMIC);
            Body.SetJointBodyKind(BODY_KIND.DYNAMIC);
            Body.EnableClamp(true);
        }
    }
}