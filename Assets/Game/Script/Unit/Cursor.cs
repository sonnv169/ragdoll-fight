﻿using UnityEngine;

public class Cursor : ToggleBehaviour
{
    [SerializeField] private SpriteRenderer spr;

    protected override void OnAppear()
    {
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                Return();
                break;
        }
    }

    public void Return()
    {
        if (!GetComponent<ObjectPooling>())
            return;
        ObjectPooling objectPooling = GetComponent<ObjectPooling>();
        objectPooling.Return();
    }

    public void Init(Color color)
    {
        if (spr != null)
            spr.color = color;
    }
}