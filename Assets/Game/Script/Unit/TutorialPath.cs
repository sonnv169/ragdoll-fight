﻿using UnityEngine;

public class TutorialPath : MonoBehaviour
{
    [SerializeField] private TutorialData tutorial;
    [SerializeField] private bool isTutorialStatus;

    private void OnEnable()
    {
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    private void OnDisable()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                Return();
                break;
        }
    }

    private void SetTutorialDone()
    {
        if (Singleton<TutorialManager>.Instance == null)
            return;
        TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
        if (tutorial.isFight)
            tutorialManager.GetTutorialData().isFight = isTutorialStatus;
        else if (tutorial.isGift)
            tutorialManager.GetTutorialData().isGift = isTutorialStatus;
        else if (tutorial.isMove)
            tutorialManager.GetTutorialData().isMove = isTutorialStatus;
        else if (tutorial.isPlay)
            tutorialManager.GetTutorialData().isPlay = isTutorialStatus;
        tutorialManager.SaveTutorialData();
    }

    private void Return()
    {
        if (!gameObject.activeInHierarchy)
            return;
        SetTutorialDone();
        if (!GetComponent<ObjectPooling>())
            return;
        ObjectPooling objectPooling = GetComponent<ObjectPooling>();
        objectPooling.Return();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.parent == null || !other.transform.parent.GetComponent<BodyPath>())
            return;
        BodyPath bodyPath = other.transform.parent.GetComponent<BodyPath>();

        if (bodyPath.GetBody() == null || !bodyPath.GetBody().GetComponent<Player>())
            return;
        Return();
    }
}