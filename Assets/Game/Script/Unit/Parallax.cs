﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    private MeshRenderer mMeshRenderer;

    [SerializeField] private float scrollSpeed;

    #region property

    private MeshRenderer MeshRenderer
    {
        get
        {
            if (mMeshRenderer == null && GetComponent<MeshRenderer>())
                mMeshRenderer = GetComponent<MeshRenderer>();
            return mMeshRenderer;
        }
    }

    #endregion

    private void FixedUpdate()
    {
        OnScroll();
    }

    private void OnScroll()
    {
        if (MeshRenderer == null || Singleton<GameManager>.Instance == null ||
            Singleton<MainCamera>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        MainCamera mainCamera = Singleton<MainCamera>.Instance;
        Vector2 scrollPosition = MeshRenderer.material.mainTextureOffset;
        scrollPosition.x = mainCamera.transform.position.x * scrollSpeed * Time.deltaTime;
        MeshRenderer.material.mainTextureOffset = scrollPosition;
    }
}