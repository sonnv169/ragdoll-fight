﻿using System.Collections;
using UnityEngine;

public class Vfx : ToggleBehaviour
{
    private CHARACTER character;
    private Coroutine hideCoroutine;
    private Collider2D mCollider;
    private int damage;

    [SerializeField] private GameEnums.AUDIO_CLIP clip;
    [SerializeField] private Vector2 direction;
    [SerializeField] private float time;
    [SerializeField] private float speed;
    [SerializeField] private bool isLoop;
    [SerializeField] private bool isVibrateCollision;

    #region property

    private Collider2D Collider
    {
        get
        {
            if (mCollider == null && GetComponent<Collider2D>())
                mCollider = GetComponent<Collider2D>();
            return mCollider;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        SoundManager.Play(AUDIO_KIND.SOUND, clip, false);
        EnableCollider(true);
        hideCoroutine = StartCoroutine(OnHide());
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        if (gameObject.activeInHierarchy)
            transform.Translate(direction * speed * Time.deltaTime);
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                PenaltyReturn();
                break;
        }
    }

    private void EnableCollider(bool isStatus)
    {
        if (Collider != null)
            Collider.enabled = isStatus;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (isVibrateCollision && Singleton<MainCamera>.Instance != null)
        {
            MainCamera mainCamera = Singleton<MainCamera>.Instance;
            mainCamera.OnVibrate();
        }

        if (other.transform.parent != null && other.transform.parent.GetComponent<BodyPath>())
        {
            BodyPath path = other.transform.parent.GetComponent<BodyPath>();
            if (path.GetBody() == null)
                return;
            float damagePercent = path.GetDamagePercent();
            if (damagePercent > 0)
            {
                damage = damage + (int) (damage * path.GetCriticalRate());
                Eventon<WeaponHitEvent>.Trigger(new WeaponHitEvent()
                {
                    collisionPosition = path.transform.position
                });

                Player player = null;
                Enemy enemy = null;
                switch (character)
                {
                    case CHARACTER.PLAYER:
                        if (!path.GetBody().GetComponent<Enemy>())
                            return;
                        enemy = path.GetBody().GetComponent<Enemy>();
                        enemy.OnHpMinus(damage);

                        if (enemy.IsDrop())
                        {
                            enemy.OnSetHit(0);
                            path.GetBody().OnDropWeapon();
                        }

                        enemy.DisplayHp();
                        enemy.OnDropHitUp(1);

                        Eventon<WeaponHitEvent>.Trigger(new WeaponHitEvent()
                        {
                            collisionPosition = path.transform.position
                        });
                        break;

                    case CHARACTER.ENEMY:
                        if (!path.GetBody().GetComponent<Player>())
                            return;
                        player = path.GetBody().GetComponent<Player>();
                        player.OnHpMinus(damage);

                        if (player.IsDrop())
                        {
                            player.OnSetHit(0);
                            path.GetBody().OnDropWeapon();
                        }

                        player.DisplayHp();
                        player.OnDropHitUp(1);

                        Eventon<WeaponHitEvent>.Trigger(new WeaponHitEvent()
                        {
                            collisionPosition = path.transform.position
                        });
                        break;

                    case CHARACTER.TRAP:
                        if (path.GetBody().GetComponent<Player>())
                        {
                            player = path.GetBody().GetComponent<Player>();
                            player.OnHpMinus(damage);

                            if (player.IsDrop())
                            {
                                player.OnSetHit(0);
                                path.GetBody().OnDropWeapon();
                            }

                            player.DisplayHp();
                            player.OnDropHitUp(1);
                        }

                        if (path.GetBody().GetComponent<Enemy>())
                        {
                            enemy = path.GetBody().GetComponent<Enemy>();
                            enemy.OnHpMinus(damage);

                            if (enemy.IsDrop())
                            {
                                enemy.OnSetHit(0);
                                path.GetBody().OnDropWeapon();
                            }

                            enemy.DisplayHp();
                            enemy.OnDropHitUp(1);
                        }

                        Eventon<WeaponHitEvent>.Trigger(new WeaponHitEvent()
                        {
                            collisionPosition = path.transform.position
                        });

                        break;
                }


                path.OnIntactMinus(1);
                path.GetBlood();
            }
            else
                path.GetBlock();
        }

        EnableCollider(false);
    }

    private IEnumerator OnHide()
    {
        yield return new WaitForSeconds(time);
        if (hideCoroutine != null)
            StopCoroutine(hideCoroutine);
        Return();
    }

    private void Return()
    {
        if (isLoop)
            return;
        if (GetComponent<ObjectPooling>())
        {
            ObjectPooling objectPooling = GetComponent<ObjectPooling>();
            objectPooling.Return();
        }
        else
            gameObject.SetActive(false);
    }

    public void PenaltyReturn()
    {
        if (hideCoroutine != null)
            StopCoroutine(hideCoroutine);

        if (GetComponent<ObjectPooling>())
        {
            ObjectPooling objectPooling = GetComponent<ObjectPooling>();
            objectPooling.Return();
        }
        else
            gameObject.SetActive(false);
    }

    public void SetDamage(CHARACTER character, int value)
    {
        this.character = character;
        damage = value;
    }
}