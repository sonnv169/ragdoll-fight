﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class BodyPathResetEvent
{
    private bool isReset;
}

public class BodyPath : ToggleBehaviour
{
    private Rigidbody2D mRigidbody;
    private float hitDuration;
    private float criticalRate;
    private int intact;
    private int sortingOrder;

    [SerializeField] private Body body;
    [SerializeField] private GameEnums.VFX vfxBlood;
    [SerializeField] private float damagePercent;
    [SerializeField] private float[] criticalsRates;
    [SerializeField] private Animator[] animators;

    #region property

    private Rigidbody2D Rigidbody
    {
        get
        {
            if (mRigidbody == null && GetComponent<Rigidbody2D>())
                mRigidbody = GetComponent<Rigidbody2D>();
            return mRigidbody;
        }
    }

    private SpriteRenderer SpriteRender
    {
        get
        {
            SpriteRenderer spriteRenderer = null;
            if (transform.childCount > 0 && transform.GetChild(0).GetComponent<SpriteRenderer>())
                spriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
            return spriteRenderer;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Init();
        for (int i = 0; i < animators.Length; i++)
        {
            AnimatorManager.Play(animators[i], "anim_idle");
        }

        intact = GameConfig.bodyPathIntact;
    }

    protected override void OnCyclic()
    {
        if (hitDuration > 0)
            hitDuration -= Time.deltaTime;
    }

    protected override void OnDisappear()
    {
        //nothing
    }

    public void OnIntactMinus(int value)
    {
        intact -= value;
        if (intact <= 0)
            intact = 0;
        if (intact < criticalsRates.Length)
            criticalRate = criticalsRates[intact];
    }

    private void Init()
    {
        intact = criticalsRates.Length;
        if (intact < criticalsRates.Length)
            criticalRate = criticalsRates[intact];
        InitSortingOrder();
    }

    private void InitSortingOrder()
    {
        if (SpriteRender != null)
            sortingOrder = SpriteRender.sortingOrder;
    }

    public void SetSortingOrderOnUI(bool isStatus)
    {
        if (SpriteRender == null)
            return;
        switch (isStatus)
        {
            case true:
                SpriteRender.sortingOrder = sortingOrder + GameConfig.sortingOrderOnUI;
                break;

            case false:
                SpriteRender.sortingOrder = sortingOrder;
                break;
        }
    }

    public void GetBlood()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string path = "Prefab/Object/Vfx/" + vfxBlood;
        ObjectManager.Call<GameObject>(gameManager.transform, transform, path);

        for (int i = 0; i < animators.Length; i++)
        {
            AnimatorManager.Play(animators[i], "anim_get_hit");
        }

        hitDuration = GameConfig.hitDuration;
    }

    public void GetBlock()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string path = "Prefab/Object/Vfx/vfx_block_damage";
        ObjectManager.Call<GameObject>(gameManager.transform, transform, path);
    }

    public Body GetBody()
    {
        return body;
    }

    public float GetHitDuration()
    {
        return hitDuration;
    }

    public float GetDamagePercent()
    {
        return damagePercent;
    }

    public float GetCriticalRate()
    {
        return criticalRate;
    }

    public int GetIntact()
    {
        return intact;
    }
}