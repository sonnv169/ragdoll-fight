﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Trap : ToggleBehaviour
{
    private Coroutine destroyCoroutine;
    private int damage;
    private int currentSecToDestroy;

    [SerializeField] private GameEnums.TRAP trap;
    [SerializeField] private GameEnums.VFX destroyVfx;
    [SerializeField] private Text txtCountDown;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private int secToDestroy;
    [SerializeField] private bool isEnviroment;

    protected override void OnAppear()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        Rotate();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                if (destroyCoroutine != null)
                    StopCoroutine(destroyCoroutine);
                break;
        }
    }

    private void Init()
    {
        if (secToDestroy > 0)
        {
            currentSecToDestroy = secToDestroy;
            destroyCoroutine = StartCoroutine(OnMinusCurrentSecToDestroy(0));
        }

        DisplaySecondToDestroy();
        if (Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        damage = stageManager.GetTrapDamage(trap);
    }

    private void Rotate()
    {
        if (rotateSpeed > 0)
            transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
    }

    private void DisplaySecondToDestroy()
    {
        if (txtCountDown != null)
            txtCountDown.text = currentSecToDestroy.ToString();
    }

    private void Destroy()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        string path = "Prefab/Object/Vfx/" + destroyVfx;
        if (Resources.Load(path) != null)
        {
            GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, transform, path);
            if (!obj.GetComponent<Vfx>())
                return;
            Vfx vfxObj = obj.GetComponent<Vfx>();
            vfxObj.SetDamage(CHARACTER.TRAP, damage);
        }

        if (!gameObject.GetComponent<ObjectPooling>())
            return;
        ObjectPooling objectPooling = gameObject.GetComponent<ObjectPooling>();
        objectPooling.Return();
    }

    private IEnumerator OnMinusCurrentSecToDestroy(float time)
    {
        yield return new WaitForSeconds(time);
        if (destroyCoroutine != null)
            StopCoroutine(destroyCoroutine);
        if (currentSecToDestroy > 0)
        {
            currentSecToDestroy -= 1;
            DisplaySecondToDestroy();
            destroyCoroutine = StartCoroutine(OnMinusCurrentSecToDestroy(1));
        }
        else
            Destroy();
    }

    private void OnTrapHit(BodyPath path)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        if (isEnviroment || path == null || path.GetBody() == null)
            return;
        Body body = path.GetBody();

        damage = damage + (int) (damage * path.GetCriticalRate());
        if (body.GetComponent<Player>())
        {
            Player player = null;
            player = body.GetComponent<Player>();
            player.OnHpMinus(damage);

            if (player.IsDrop())
            {
                player.OnSetHit(0);
                body.OnDropWeapon();
            }

            player.DisplayHp();
            player.OnDropHitUp(1);
        }
        else if (body.GetComponent<Enemy>())
        {
            Enemy enemy = null;
            enemy = body.GetComponent<Enemy>();
            enemy.OnHpMinus(damage);

            if (enemy.IsDrop())
            {
                enemy.OnSetHit(0);
                body.OnDropWeapon();
            }

            enemy.DisplayHp();
            enemy.OnDropHitUp(1);
        }

        path.OnIntactMinus(1);
        path.GetBlood();

        GameEnums.AUDIO_CLIP clip = GameEnums.AUDIO_CLIP.None;
        int result = Random.Range(0, 2);
        string clipName = "sfx_man_hurt_" + result;
        Enum.TryParse(clipName, out clip);
        SoundManager.Play(AUDIO_KIND.SOUND, clip, false);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.GetComponent<BodyPath>())
            return;
        BodyPath bodyPath = other.gameObject.GetComponent<BodyPath>();
        OnTrapHit(bodyPath);
    }
}