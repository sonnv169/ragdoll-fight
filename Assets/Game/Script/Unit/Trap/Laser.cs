﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Laser : ToggleBehaviour
{
    private Coroutine renderCoroutine;
    private LineRenderer mLineRenderer;
    private float currentDelay;
    private int damage;
    private bool isRender;

    [SerializeField] private GameEnums.TRAP trap;
    [SerializeField] private GameEnums.AUDIO_CLIP clip;
    [SerializeField] private GameEnums.VFX collisionVfx;
    [SerializeField] private Transform[] points;
    [SerializeField] private Collider2D[] colliders;
    [SerializeField] private float renderDuration;
    [SerializeField] private float delayRender;

    #region property

    private LineRenderer LineRenderer
    {
        get
        {
            if (mLineRenderer == null && GetComponent<LineRenderer>())
                mLineRenderer = GetComponent<LineRenderer>();
            return mLineRenderer;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    private void Start()
    {
        Init();
        OnColliderRender();
    }

    protected override void OnCyclic()
    {
        OnRender();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                isRender = false;
                if (renderCoroutine != null)
                    StopCoroutine(renderCoroutine);
                break;
        }
    }

    private void Init()
    {
        EnableColliders(false);
        if (delayRender > 0)
            currentDelay = delayRender;
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i] != null)
                LineRenderer.SetPosition(i, points[i].position);
        }

        if (Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        damage = stageManager.GetTrapDamage(trap);
    }

    private IEnumerator OnResetRender(float time)
    {
        yield return new WaitForSeconds(time);
        currentDelay = delayRender;
        isRender = false;
    }

    private void EnableColliders(bool isStatus)
    {
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i] != null)
                colliders[i].enabled = isStatus;
        }
    }

    private void EnableLine(bool isStatus)
    {
        if (LineRenderer != null)
            LineRenderer.enabled = isStatus;
    }

    private void OnRender()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() == GAME_STATE.PLAY)
        {
            if (currentDelay > 0)
            {
                currentDelay -= Time.deltaTime;
                EnableColliders(false);
                EnableLine(false);
            }
            else
            {
                if (isRender)
                    return;
                EnableColliders(true);
                EnableLine(true);
                renderCoroutine = StartCoroutine(OnResetRender(renderDuration));
            }
        }
        else
        {
            EnableColliders(false);
            EnableLine(false);
        }
    }

    private void OnColliderRender()
    {
        if (Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        List<Vector2> pointPositions = new List<Vector2>();
        Stage stage = stageManager.GetStageObject();
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i] == null)
                continue;
            pointPositions.Add(new Vector2(0, points[i].position.y / stage.transform.localScale.y));
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;

        if (other.transform.parent == null || !other.transform.parent.GetComponent<BodyPath>() ||
            gameManager.GetState() != GAME_STATE.PLAY)
            return;
        BodyPath bodyPath = other.transform.parent.GetComponent<BodyPath>();
        if (bodyPath.GetBody() == null)
            return;
        Body body = bodyPath.GetBody();
        Player player = null;
        Enemy enemy = null;
        float damagePercent = bodyPath.GetDamagePercent();

        if (damagePercent > 0)
        {
            damage = damage + (int) (damage * bodyPath.GetCriticalRate());
            if (body.GetComponent<Player>())
            {
                player = body.GetComponent<Player>();
                player.OnHpMinus(damage);

                if (player.IsDrop())
                {
                    player.OnSetHit(0);
                    body.OnDropWeapon();
                }

                player.DisplayHp();
                player.OnDropHitUp(1);
            }
            else if (body.GetComponent<Enemy>())
            {
                enemy = body.GetComponent<Enemy>();
                enemy.OnHpMinus(damage);

                if (enemy.IsDrop())
                {
                    enemy.OnSetHit(0);
                    body.OnDropWeapon();
                }

                enemy.DisplayHp();
                enemy.OnDropHitUp(1);
            }

            bodyPath.OnIntactMinus(1);
            bodyPath.GetBlood();

            SoundManager.Play(AUDIO_KIND.SOUND, clip, false);

            string path = "Prefab/Object/Vfx/" + collisionVfx;
            if (Resources.Load(path) != null)
                ObjectManager.Call<GameObject>(gameManager.transform, transform, path);
        }
        else
            bodyPath.GetBlock();
    }
}