﻿using UnityEngine;

public class LootDropper : ToggleBehaviour
{
    private Vector3 movePosition;
    private float currentDelay;
    private int dropped;

    [SerializeField] private Vector2 startPositionClamp;
    [SerializeField] private Vector2 endPositionClamp;
    [SerializeField] private Vector3 droppedObjectScale;
    [SerializeField] private string droppedObjectName;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float dropDelay;

    protected override void OnAppear()
    {
        Init();
    }

    protected override void OnCyclic()
    {
        Move();
        CountDown();
    }

    protected override void OnDisappear()
    {
        //nothing
    }

    private void Init()
    {
        currentDelay = dropDelay;
        dropped = 0;
        FindMovePosition();
    }

    private void FindMovePosition()
    {
        float startDistance = 0;
        float endDistance = 0;
        startDistance = Vector2.Distance(transform.position, startPositionClamp);
        endDistance = Vector2.Distance(transform.position, endPositionClamp);

        if (startDistance < endDistance)
            movePosition = endPositionClamp;
        else
            movePosition = startPositionClamp;
    }

    private void Drop()
    {
        if (dropped > GameConfig.maxLootDrop || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string objPath = "Prefab/Object/Trap/" + droppedObjectName;
        string vfxPath = "Prefab/Object/Vfx/vfx_block_damage";
        if (Resources.Load(objPath) == null)
            return;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        GameObject lootObj = ObjectManager.Call<GameObject>(gameManager.transform, transform, objPath);
        ObjectManager.Call<GameObject>(gameManager.transform, transform, vfxPath);

        lootObj.transform.SetParent(transform, true);
        lootObj.transform.position = transform.position;
        lootObj.transform.localScale = new Vector3(1, 1, 1);
        lootObj.transform.localScale = droppedObjectScale;
        currentDelay = dropDelay;
        dropped += 1;
    }

    private void Move()
    {
        if (transform.position != movePosition)
            transform.position = Vector3.MoveTowards(transform.position, movePosition, moveSpeed * Time.deltaTime);
        else
            FindMovePosition();
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, startPositionClamp.x, endPositionClamp.x),
            Mathf.Clamp(transform.position.y, startPositionClamp.y, endPositionClamp.y), 0);
    }

    private void CountDown()
    {
        if (dropDelay <= 0)
            return;
        if (currentDelay > 0)
            currentDelay -= Time.deltaTime;
        else
            Drop();
    }
}