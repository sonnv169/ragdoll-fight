﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class BodyExploreEvent
{
    public Body body;
}

[System.Serializable]
public class JointClamp
{
    public Transform rect;
    public Transform joint;
}

public class Body : ToggleBehaviour
{
    private Weapon weapon;
    private Rigidbody2D mRigidbody;
    private Coroutine pausingCoroutine;
    private Coroutine dropingCoroutine;
    private Coroutine winCoroutine;
    private Coroutine tPoseCoroutine;
    private Coroutine deathCoroutine;
    private Animator mAnimator;
    private Vector2 savedDirection;
    private float savedSpeed;
    private bool isFlying;
    private bool isPausing;
    private bool isDroping;
    private bool isWin;

    [SerializeField] private JointClamp[] jointClamps;
    [SerializeField] private Weapon[] bodyWeapons;

    [SerializeField] private Transform insideWeapon;
    [SerializeField] private Transform[] motions;

    #region property

    private Rigidbody2D Rigidbody
    {
        get
        {
            if (mRigidbody == null && GetComponent<Rigidbody2D>())
                mRigidbody = GetComponent<Rigidbody2D>();
            return mRigidbody;
        }
    }

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
        Eventon<WeaponHitEvent>.EventStatic += OnWeaponHitEvent;
    }

    protected override void OnCyclic()
    {
        OnUpdate();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
        Eventon<WeaponHitEvent>.EventStatic -= OnWeaponHitEvent;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                Return();
                break;
        }
    }

    private void OnWeaponHitEvent(WeaponHitEvent param)
    {
        if (Rigidbody == null)
            return;
        int hp = 0;
        if (GetComponent<Player>())
        {
            Player player = GetComponent<Player>();
            hp = player.GetHp();
        }
        else if (GetComponent<Enemy>())
        {
            Enemy enemy = GetComponent<Enemy>();
            hp = enemy.GetHp();
        }

        if (hp <= 0)
            return;
        Vector2 direction = new Vector2(transform.position.x, transform.position.y) - param.collisionPosition;
        isPausing = true;
        pausingCoroutine = StartCoroutine(OnResume());
        if (Rigidbody.bodyType != RigidbodyType2D.Static)
            Rigidbody.velocity = direction.normalized * GameConfig.collisionPower;
    }

    private void Init()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        switch (gameManager.GetState())
        {
            case GAME_STATE.PLAY:
                Reborn();
                break;
        }
    }

    private void OnUpdate()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;

        switch (gameManager.GetState())
        {
            case GAME_STATE.PLAY:
                OnBalance();
                break;

            case GAME_STATE.WIN:
                OnBodyPlayerWin();
                break;
        }
    }

    private IEnumerator OnDeath(float time)
    {
        yield return new WaitForSeconds(time);
        Return();
    }

    private IEnumerator OnShowWinUI(float time)
    {
        yield return new WaitForSeconds(time);
        UIWin.Show();
    }

    private IEnumerator OnResume()
    {
        yield return new WaitForSeconds(GameConfig.hitDuration);
        isPausing = false;
    }

    private IEnumerator OnPicking()
    {
        yield return new WaitForSeconds(GameConfig.dropingTime);
        isDroping = false;
        weapon = null;
    }

    private IEnumerator OnDisableTpose(float time)
    {
        yield return new WaitForSeconds(time);
        if (tPoseCoroutine != null)
            StopCoroutine(tPoseCoroutine);
        AnimatorManager.Enable(Animator, false);

        Body2DEffector.Velocity(Rigidbody, 0.0f, 0.0f);
        Body2DEffector.ChangeType(Rigidbody, BODY_KIND.STATIC);

        for (int i = 0; i < jointClamps.Length; i++)
        {
            if (jointClamps[i].joint == null)
                continue;
            if (jointClamps[i].joint.GetComponent<Rigidbody2D>())
            {
                Rigidbody2D jointBody = jointClamps[i].joint.GetComponent<Rigidbody2D>();
                Body2DEffector.Velocity(jointBody, 0.0f, 0.0f);
                Body2DEffector.ChangeType(jointBody, BODY_KIND.STATIC);
            }
        }
    }

    private void OnBodyPlayerWin()
    {
        if (!GetComponent<Player>() || isWin || Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        Body2DEffector.Velocity(Rigidbody, 0.0f, 0.0f);
        SetBodyKind(BODY_KIND.STATIC);

        if (transform.position != stageManager.GetFinishLocation())
        {
            transform.position = Vector3.MoveTowards(transform.position, stageManager.GetFinishLocation(),
                GameConfig.winSpeed * Time.deltaTime);
        }
        else
        {
            SetJointBodyKind(BODY_KIND.STATIC);
            for (int i = 0; i < jointClamps.Length; i++)
            {
                if (!jointClamps[i].joint.GetComponent<Rigidbody2D>())
                    continue;
                Rigidbody2D body = jointClamps[i].joint.GetComponent<Rigidbody2D>();
                Body2DEffector.Velocity(body, 0.0f, 0.0f);
            }

            if (transform.rotation != Quaternion.identity)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity,
                    GameConfig.flyingSpeed);
            }
            else
            {
                isWin = true;
                EnableClamp(false);
                AnimatorManager.Enable(Animator, true);

                int number = Random.Range(0, 3);
                AnimatorManager.Play(Animator, "anim_win_" + number);
                float animDuration = AnimatorManager.GetLenght(Animator, "anim_win_" + number);
                winCoroutine = StartCoroutine(OnShowWinUI(animDuration + 1.5f));
            }
        }
    }

    private void OnBalance()
    {
        if (!isFlying)
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity,
                GameConfig.balanceSpeed * Time.deltaTime);
    }

    private void OnWeaponIdenty()
    {
        transform.rotation = Quaternion.identity;
        if (insideWeapon != null)
            insideWeapon.rotation = Quaternion.identity;
    }

    public void Scale(float scale)
    {
        for (int i = 0; i < motions.Length; i++)
        {
            if (motions[i] != null)
                motions[i].transform.localScale =
                    new Vector3(scale, motions[i].transform.localScale.y, motions[i].transform.localScale.z);
        }
    }

    public void OnBodyExplore()
    {
        Body2DEffector.Velocity(Rigidbody, 0.0f, 0.0f);
        Body2DEffector.ChangeType(Rigidbody, BODY_KIND.STATIC);
        isPausing = true;
        EnableClamp(false);

        for (int i = 0; i < jointClamps.Length; i++)
        {
            if (jointClamps[i].joint == null)
                continue;
            if (jointClamps[i].joint.GetComponent<Rigidbody2D>())
            {
                Rigidbody2D jointBody = jointClamps[i].joint.GetComponent<Rigidbody2D>();
                Body2DEffector.Velocity(jointBody, 0.0f, 0.0f);
                Vector2 direction = jointBody.transform.position -
                                    new Vector3(transform.position.x, transform.position.y + 5.5f, 0);
                Body2DEffector.Velocity(jointBody, direction.normalized, GameConfig.explorePower);
            }
        }

        Eventon<BodyExploreEvent>.Trigger(new BodyExploreEvent()
        {
            body = this
        });

        if (GetComponent<Enemy>())
            deathCoroutine = StartCoroutine(OnDeath(GameConfig.deathTime));
    }

    public void EnableClamp(bool isStatus)
    {
        for (int i = 0; i < jointClamps.Length; i++)
        {
            if (jointClamps[i].joint == null)
                continue;
            if (jointClamps[i].joint.GetComponent<HingeJoint2D>())
            {
                HingeJoint2D joint = jointClamps[i].joint.GetComponent<HingeJoint2D>();
                joint.enabled = isStatus;
            }

            if (jointClamps[i].joint.GetComponent<FixedJoint2D>())
            {
                FixedJoint2D joint = jointClamps[i].joint.GetComponent<FixedJoint2D>();
                joint.enabled = isStatus;
            }

            if (jointClamps[i].joint.childCount <= 0 || !jointClamps[i].joint.GetChild(0).GetComponent<Collider2D>())
                continue;
            Collider2D collider = jointClamps[i].joint.GetChild(0).GetComponent<Collider2D>();
            collider.enabled = isStatus;
        }

        if (GetComponent<HingeJoint2D>())
        {
            HingeJoint2D joint = GetComponent<HingeJoint2D>();
            joint.enabled = isStatus;
        }

        if (GetComponent<FixedJoint2D>())
        {
            FixedJoint2D joint = GetComponent<FixedJoint2D>();
            joint.enabled = isStatus;
        }

        if (insideWeapon != null)
        {
            if (insideWeapon.GetComponent<HingeJoint2D>())
            {
                HingeJoint2D joint = insideWeapon.GetComponent<HingeJoint2D>();
                joint.enabled = isStatus;
            }

            if (insideWeapon.GetComponent<FixedJoint2D>())
            {
                FixedJoint2D joint = insideWeapon.GetComponent<FixedJoint2D>();
                joint.enabled = isStatus;
            }
        }
    }

    public void Reborn()
    {
        if (Singleton<CharacterManager>.Instance == null || Singleton<StageManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        StageManager stageManager = Singleton<StageManager>.Instance;

        transform.position = GameConfig.infernal;

        isPausing = false;
        isFlying = false;
        isDroping = false;
        isWin = false;

        if (pausingCoroutine != null)
            StopCoroutine(pausingCoroutine);
        if (dropingCoroutine != null)
            StopCoroutine(dropingCoroutine);
        if (winCoroutine != null)
            StopCoroutine(winCoroutine);

        if (GetComponent<Player>())
        {
            int level = characterManager.GetHeroLevel();
            Player player = GetComponent<Player>();
            player.SetHp(characterManager.GetHeroHp(level));
        }
        else if (GetComponent<Enemy>())
        {
            int stage = stageManager.GetStage();
            Enemy enemy = GetComponent<Enemy>();
            enemy.SetHp(stageManager.GetEnemyHp(stage));
        }

        if (weapon != null)
            weapon.OnDrop();
        weapon = null;

        AnimatorManager.Enable(Animator, true);
        AnimatorManager.Play(Animator, "anim_tpose");
        float animLenght = AnimatorManager.GetLenght(Animator, "anim_tpose");
        tPoseCoroutine = StartCoroutine(OnDisableTpose(animLenght));

        OnWeaponIdenty();
    }

    public void Return()
    {
        isPausing = false;
        isFlying = false;
        isDroping = false;
        isWin = false;

        weapon = null;
        transform.position = GameConfig.infernal;
        if (pausingCoroutine != null)
            StopCoroutine(pausingCoroutine);
        if (dropingCoroutine != null)
            StopCoroutine(dropingCoroutine);
        if (winCoroutine != null)
            StopCoroutine(winCoroutine);
        if (tPoseCoroutine != null)
            StopCoroutine(tPoseCoroutine);
        if (deathCoroutine != null)
            StopCoroutine(deathCoroutine);

        if (GetComponent<Player>())
        {
            var player = GetComponent<Player>();
            Destroy(player);
        }

        transform.rotation = Quaternion.identity;
        OnWeaponIdenty();

        if (!GetComponent<ObjectPooling>())
            return;
        ObjectPooling objectPooling = GetComponent<ObjectPooling>();
        objectPooling.Return();
    }

    public void SetBodyKind(BODY_KIND kind)
    {
        Body2DEffector.Velocity(Rigidbody, 0.0f, 0.0f);
        Body2DEffector.ChangeType(Rigidbody, kind);
    }

    public void SetJointBodyKind(BODY_KIND kind)
    {
        for (int i = 0; i < jointClamps.Length; i++)
        {
            if (!jointClamps[i].joint.GetComponent<Rigidbody2D>())
                continue;
            Rigidbody2D body = jointClamps[i].joint.GetComponent<Rigidbody2D>();
            Body2DEffector.Velocity(body, 0.0f, 0.0f);
            Body2DEffector.ChangeType(body, kind);
        }
    }

    public void SetBodyWeaponPower(int power)
    {
        for (int i = 0; i < bodyWeapons.Length; i++)
        {
            if (bodyWeapons[i] == null)
                continue;
            bodyWeapons[i].InitBody(this);
            bodyWeapons[i].SetAtk(power);
        }
    }

    public void Clamp()
    {
        for (int i = 0; i < jointClamps.Length; i++)
        {
            if (jointClamps[i].rect == null || jointClamps[i].joint == null)
                continue;
            jointClamps[i].joint.position = jointClamps[i].rect.position;
        }
    }

    public void SetFlying(bool isStatus)
    {
        isFlying = isStatus;
        if (isStatus)
            return;
        for (int i = 0; i < jointClamps.Length; i++)
        {
            if (jointClamps[i].joint == null || !jointClamps[i].joint.GetComponent<Rigidbody2D>())
                continue;
            Rigidbody2D jointBody = jointClamps[i].joint.GetComponent<Rigidbody2D>();
            Body2DEffector.Velocity(jointBody, 0.0f, 0.0f);
        }

        Body2DEffector.Velocity(Rigidbody, savedDirection, savedSpeed * GameConfig.balanceSpeedAbs);
    }

    public void SetVelocity(Vector2 direction, float speed)
    {
        if (isPausing || GetHp() <= 0 || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() == GAME_STATE.WIN ||
            gameManager.GetState() == GAME_STATE.LOSE ||
            insideWeapon == null || !insideWeapon.GetComponent<Rigidbody2D>())
            return;
        Rigidbody2D insideWeaponBody = insideWeapon.GetComponent<Rigidbody2D>();
        Body2DEffector.Velocity(insideWeaponBody, direction.normalized, speed);
        savedDirection = direction.normalized;
        savedSpeed = speed;
        SetFlying(true);
    }

    public void SetWeapon(GameObject obj)
    {
        if (isDroping || obj == null || !obj.GetComponent<Weapon>())
            return;
        Weapon weapon = obj.GetComponent<Weapon>();
        Transform weaponRect = null;

        this.weapon = null;
        if (insideWeapon != null)
        {
            obj.transform.SetParent(insideWeapon.transform, true);
            obj.transform.position = insideWeapon.transform.position;
            this.weapon = weapon;
            weapon.Flip();

            if (insideWeapon.parent != null)
                insideWeapon.position = insideWeapon.parent.position;

            for (int i = 0; i < insideWeapon.childCount; i++)
            {
                insideWeapon.GetChild(i).position = insideWeapon.position;
                insideWeapon.GetChild(i).up = insideWeapon.GetChild(i).parent.up;
            }

            weaponRect = insideWeapon;
        }

        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager != null && gameManager.GetState() == GAME_STATE.PLAY)
        {
            weapon.SetBody(this);
            string path = "Prefab/Object/Vfx/vfx_weapon_equipt";
            if (Resources.Load(path) != null)
                ObjectManager.Call<GameObject>(gameManager.transform, weaponRect.transform, path);
        }

        SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_pick_up, false);
    }

    public void SetPausing(bool isStatus)
    {
        isPausing = isStatus;
    }

    public void SetSortingOrderOnUI(bool isStatus)
    {
        for (int i = 0; i < jointClamps.Length; i++)
        {
            if (jointClamps[i].joint == null || jointClamps[i].joint.childCount <= 0 ||
                !jointClamps[i].joint.GetComponent<BodyPath>())
                continue;
            BodyPath bodyPath = jointClamps[i].joint.GetComponent<BodyPath>();
            bodyPath.SetSortingOrderOnUI(isStatus);
        }
    }

    public void OnDropWeapon()
    {
        isDroping = true;
        if (weapon != null)
            weapon.OnDrop();
        weapon = null;
        dropingCoroutine = StartCoroutine(OnPicking());
    }

    public void OnDropWeaponAndReturn()
    {
        isDroping = false;
        if (weapon != null)
            weapon.OnDropAndReturn();
        weapon = null;
        if (dropingCoroutine != null)
            StopCoroutine(dropingCoroutine);
    }

    public void OnClearWeapon()
    {
        if (weapon != null)
            weapon.OnClear();
        weapon = null;
    }

    public Transform OnArm()
    {
        Transform target = null;
        Transform targetHead = null;

        if (GetComponent<Player>())
        {
            if (Singleton<CharacterManager>.Instance != null)
            {
                CharacterManager characterManager = Singleton<CharacterManager>.Instance;
                if (characterManager.GetTransforms().Count > 0)
                {
                    List<float> distances = new List<float>();
                    for (int i = 0; i < characterManager.GetTransforms().Count; i++)
                    {
                        Transform enemyTransform = characterManager.GetTransforms()[i];
                        float distance = Vector2.Distance(transform.position, enemyTransform.position);
                        if (!distances.Contains(distance))
                            distances.Add(distance);
                    }

                    for (int i = 0; i < distances.Count; i++)
                    {
                        if (distances[i] > distances.Max() || characterManager.GetTransforms()[i] == null)
                            continue;
                        Transform enemyTransform = characterManager.GetTransforms()[i];
                        if (!enemyTransform.GetComponent<Enemy>() || enemyTransform.GetComponent<Enemy>().GetHp() <= 0)
                            continue;
                        target = characterManager.GetTransforms()[i];
                    }
                }
            }
        }
        else if (GetComponent<Enemy>())
        {
            if (Singleton<Player>.Instance != null)
            {
                Player player = Singleton<Player>.Instance;
                if (player.GetHp() > 0)
                    target = player.transform;
            }
        }

        if (target != null && target.GetComponent<Body>())
        {
            Body targetBody = target.GetComponent<Body>();
            targetHead = targetBody.GetHead();
        }

        return targetHead;
    }

    public Rigidbody2D GetRigidbody()
    {
        return Rigidbody;
    }

    public Transform GetInsideWeapon()
    {
        return insideWeapon;
    }

    public Weapon GetEquiptedWeapon()
    {
        return weapon;
    }

    public Animator GetAnimator()
    {
        UnityEngine.Animator animator = null;
        if (this.Animator != null)
            animator = this.Animator;
        return animator;
    }

    public Transform GetHead()
    {
        return transform.GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0);
    }

    public bool IsDroping()
    {
        return isDroping;
    }

    public int GetHp()
    {
        int hp = 0;
        if (GetComponent<Player>())
        {
            Player player = GetComponent<Player>();
            hp = player.GetHp();
        }
        else if (GetComponent<Enemy>())
        {
            Enemy enemy = GetComponent<Enemy>();
            hp = enemy.GetHp();
        }

        return hp;
    }
}