﻿using UnityEngine;

public class Ground : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.GetComponent<BodyPath>())
            return;
        BodyPath bodyPath = other.gameObject.GetComponent<BodyPath>();
        if (bodyPath.GetBody() == null || bodyPath.GetBody().GetRigidbody() == null)
            return;
        Rigidbody2D body = bodyPath.GetBody().GetRigidbody();

        int hp = 0;
        if (bodyPath.GetBody().GetComponent<Player>())
        {
            Player player = bodyPath.GetBody().GetComponent<Player>();
            hp = player.GetHp();
        }
        else if (bodyPath.GetBody().GetComponent<Enemy>())
        {
            Enemy enemy = bodyPath.GetBody().GetComponent<Enemy>();
            hp = enemy.GetHp();
        }

        if (hp <= 0)
            return;
        Vector2 direction = new Vector2(body.transform.position.x, body.transform.position.y) -
                            other.contacts[0].point;

        if (body.bodyType != RigidbodyType2D.Static)
            body.velocity = direction.normalized * GameConfig.collisionPower;
    }
}