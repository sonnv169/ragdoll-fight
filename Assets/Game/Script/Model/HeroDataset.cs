﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HeroBase
{
    public int atk;
    public int hp;
    public int goldUpgrade;
}

[CreateAssetMenu(menuName = "Dataset/Hero", order = 0)]
public class HeroDataset : ScriptableObject
{
    [SerializeField] private List<HeroBase> heroBases = new List<HeroBase>();

    public void Init()
    {
        List<string> chars = new List<string>();
        chars = TxtPath.Get("HeroDataset");
        if (chars.Count <= 0)
            return;
        int column = 3;
        int number = (int) chars.Count / column;
        if (heroBases.Count >= number)
            return;

        heroBases.Clear();
        for (int i = 0; i < number; i++)
        {
            heroBases.Add(new HeroBase()
            {
                atk = 50,
                hp = 300,
                goldUpgrade = 1000
            });
        }

        for (int i = 0; i < heroBases.Count; i++)
        {
            List<string> attributes = new List<string>();
            for (int j = column - 3; j < column; j++)
            {
                attributes.Add(chars[j]);
            }

            if (attributes.Count > 0)
            {
                int.TryParse(attributes[0], out heroBases[i].atk);
                int.TryParse(attributes[1], out heroBases[i].hp);
                int.TryParse(attributes[2], out heroBases[i].goldUpgrade);
            }

            column += 3;
        }
    }

    public int GetAtk(int level)
    {
        int number = 0;
        if (level < heroBases.Count)
            number = heroBases[level].atk;
        return number;
    }

    public int GetHp(int level)
    {
        int number = 0;
        if (level < heroBases.Count)
            number = heroBases[level].hp;
        return number;
    }

    public int GetGoldUpgrade(int level)
    {
        int number = 0;
        if (level < heroBases.Count)
            number = heroBases[level].goldUpgrade;
        return number;
    }

    public int GetBases()
    {
        return heroBases.Count;
    }
}