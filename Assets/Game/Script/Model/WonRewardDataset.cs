﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WonRewardBase
{
    public int gold;
}

[CreateAssetMenu(menuName = "Dataset/Won Reward", order = 3)]
public class WonRewardDataset : ScriptableObject
{
    [SerializeField] private List<WonRewardBase> wonRewardBases = new List<WonRewardBase>();

    public void Init()
    {
        List<string> chars = new List<string>();
        chars = TxtPath.Get("WonRewardDataset");
        int column = 1;
        if (chars.Count <= 0)
            return;
        int number = (int) chars.Count / column;
        if (wonRewardBases.Count >= number)
            return;
        wonRewardBases.Clear();
        for (int i = 0; i < number; i++)
        {
            wonRewardBases.Add(new WonRewardBase()
            {
                gold = 100
            });
        }

        for (int i = 0; i < wonRewardBases.Count; i++)
        {
            List<string> attributes = new List<string>();
            for (int j = column - 1; j < column; j++)
            {
                attributes.Add(chars[j]);
            }

            if (attributes.Count > 0)
                int.TryParse(attributes[0], out wonRewardBases[i].gold);

            column += 1;
        }
    }

    public int GetBases()
    {
        return wonRewardBases.Count;
    }

    public int GetGold(int stage)
    {
        int number = 0;
        if (stage < wonRewardBases.Count)
            number = wonRewardBases[stage].gold;
        else
            number = wonRewardBases[wonRewardBases.Count - 1].gold;
        return number;
    }
}