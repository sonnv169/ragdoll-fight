﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class WeaponBase
{
    public GameEnums.WEAPON weaponID;
    public Sprite spr;
    public string name;
    public float countDown;
    public int bonusDamage;
    public int vip;
    public int coin;
}

[CreateAssetMenu(menuName = "Dataset/Weapon", order = 5)]
public class WeaponDataset : ScriptableObject
{
    [SerializeField] private List<WeaponBase> weaponBases = new List<WeaponBase>();

    public void Init()
    {
        List<string> chars = new List<string>();
        chars = TxtPath.Get("WeaponDataset");
        int column = 3;
        if (chars.Count <= 0)
            return;
        int number = Enum.GetNames(typeof(GameEnums.WEAPON)).ToList().Count;
        if (weaponBases.Count <= 0)
        {
            string[] id = Enum.GetNames(typeof(GameEnums.WEAPON)).ToArray();
            for (int i = 0; i < number; i++)
            {
                GameEnums.WEAPON weaponID;
                Enum.TryParse(id[i], out weaponID);
                weaponBases.Add(new WeaponBase()
                {
                    weaponID = weaponID,
                    name = weaponID.ToString(),
                    countDown = 0,
                    bonusDamage = 50,
                    vip = 0
                });
            }
        }
        
        for (int i = 1; i < weaponBases.Count; i++)
        {
            List<string> attributes = new List<string>();
            for (int j = column - 3; j < column; j++)
            {
                attributes.Add(chars[j]);
            }

            if (attributes.Count > 0)
            {
                float.TryParse(attributes[0], out weaponBases[i].countDown);
                int.TryParse(attributes[1], out weaponBases[i].bonusDamage);
                int.TryParse(attributes[2], out weaponBases[i].coin);
            }

            column += 3;
        }
    }

    public Sprite GetSprite(GameEnums.WEAPON weaponID)
    {
        Sprite spr = null;

        for (int i = 0; i < weaponBases.Count; i++)
        {
            if (weaponBases[i].weaponID == weaponID)
                spr = weaponBases[i].spr;
        }

        return spr;
    }

    public string GetName(GameEnums.WEAPON weaponID)
    {
        string name = null;

        for (int i = 0; i < weaponBases.Count; i++)
        {
            if (weaponBases[i].weaponID == weaponID)
                name = weaponBases[i].name;
        }

        return name;
    }

    public float GetCountDown(GameEnums.WEAPON weaponID)
    {
        float number = 0;

        for (int i = 0; i < weaponBases.Count; i++)
        {
            if (weaponBases[i].weaponID == weaponID)
                number = weaponBases[i].countDown;
        }

        return number + 1.0f;
    }

    public int GetVip(GameEnums.WEAPON weaponID)
    {
        int vip = 0;

        for (int i = 0; i < weaponBases.Count; i++)
        {
            if (weaponBases[i].weaponID == weaponID)
                vip = weaponBases[i].vip;
        }

        return vip;
    }

    public int GetBonusDamage(GameEnums.WEAPON weaponID)
    {
        int number = 0;

        for (int i = 0; i < weaponBases.Count; i++)
        {
            if (weaponBases[i].weaponID == weaponID)
                number = weaponBases[i].bonusDamage;
        }

        return number;
    }

    public int GetPrice(GameEnums.WEAPON weaponID)
    {
        int number = 0;

        for (int i = 0; i < weaponBases.Count; i++)
        {
            if (weaponBases[i].weaponID == weaponID)
                number = weaponBases[i].coin;
        }

        return number;
    }
}