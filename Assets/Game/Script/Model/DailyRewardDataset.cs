﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RewardBase
{
    public GameEnums.ITEM_GROUP itemGroup;
    public string name;
    public int amount;
}

[System.Serializable]
public class DailyRewardBase
{
    public List<RewardBase> rewardBases = new List<RewardBase>();
}

[CreateAssetMenu(menuName = "Dataset/Daily Reward", order = 6)]
public class DailyRewardDataset : ScriptableObject
{
    [SerializeField] private List<DailyRewardBase> dailyRewardBases = new List<DailyRewardBase>();

    public void Init()
    {
        int day = 28;

        if (dailyRewardBases.Count >= day)
            return;
        for (int i = 0; i < day; i++)
        {
            dailyRewardBases.Add(new DailyRewardBase());
        }
    }

    public List<RewardBase> GetRewardBases(int day)
    {
        List<RewardBase> rewardBases = new List<RewardBase>();
        if (day < dailyRewardBases.Count)
            rewardBases = dailyRewardBases[day].rewardBases;
        return rewardBases;
    }

    public int GetRewardDay()
    {
        int number = dailyRewardBases.Count;
        return number;
    }
}