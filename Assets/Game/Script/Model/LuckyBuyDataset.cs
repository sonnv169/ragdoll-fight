﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public enum LUCKY_BUY_GROUP
{
    GROUP_0,
    GROUP_1,
    GROUP_2
}

[System.Serializable]
public class LuckyBuySlot
{
    public GameEnums.ITEM_GROUP itemGroup;
    public string itemName;
}

[System.Serializable]
public class LuckyBuyGroup
{
    public LUCKY_BUY_GROUP luckyBuyGroup;
    public List<LuckyBuySlot> itemSlots = new List<LuckyBuySlot>();
}

[CreateAssetMenu(menuName = "Dataset/Lucky Buy", order = 9)]
public class LuckyBuyDataset : ScriptableObject
{
    [SerializeField] private List<LuckyBuyGroup> luckyBuyGroups = new List<LuckyBuyGroup>();

    private bool IsCorrect()
    {
        bool isResult = false;
        string[] groups = Enum.GetNames(typeof(LUCKY_BUY_GROUP)).ToArray();
        if (luckyBuyGroups.Count < groups.Length)
        {
            isResult = true;
        }
        else
        {
            for (int i = 0; i < luckyBuyGroups.Count; i++)
            {
                if (luckyBuyGroups[i].itemSlots.Count < GameConfig.luckyBuyMax)
                    isResult = true;
            }
        }

        return isResult;
    }

    public void Init()
    {
        string[] groups = Enum.GetNames(typeof(LUCKY_BUY_GROUP)).ToArray();
        List<string> chars = TxtPath.Get("LuckyBuyDataset");
        int column = 3;
        int number = GameConfig.luckyBuyMax;

        if (!IsCorrect())
            return;
        luckyBuyGroups.Clear();
        for (int i = 0; i < groups.Length; i++)
        {
            LUCKY_BUY_GROUP groupName = LUCKY_BUY_GROUP.GROUP_0;
            Enum.TryParse(groups[i], out groupName);
            LuckyBuyGroup luckyBuyGroup = new LuckyBuyGroup()
            {
                luckyBuyGroup = groupName
            };
            luckyBuyGroups.Add(luckyBuyGroup);
        }

        for (int i = 0; i < number; i++)
        {
            List<string> attributes = new List<string>();
            for (int j = column - 3; j < column; j++)
            {
                attributes.Add(chars[j]);
            }

            if (attributes.Count > 0)
            {
                LuckyBuySlot slot0 = new LuckyBuySlot()
                {
                    itemName = attributes[0]
                };

                LuckyBuySlot slot1 = new LuckyBuySlot()
                {
                    itemName = attributes[1]
                };

                LuckyBuySlot slot2 = new LuckyBuySlot()
                {
                    itemName = attributes[2]
                };
                luckyBuyGroups[0].itemSlots.Add(slot0);
                luckyBuyGroups[1].itemSlots.Add(slot1);
                luckyBuyGroups[2].itemSlots.Add(slot2);
            }

            column += 3;
        }
    }

    public GameEnums.ITEM_GROUP GetItemGroup(LUCKY_BUY_GROUP luckyBuyGroup, int index)
    {
        GameEnums.ITEM_GROUP itemGroup = GameEnums.ITEM_GROUP.General;
        for (int i = 0; i < luckyBuyGroups.Count; i++)
        {
            if (luckyBuyGroups[i].luckyBuyGroup != luckyBuyGroup || index > luckyBuyGroups[i].itemSlots.Count)
                continue;
            itemGroup = luckyBuyGroups[i].itemSlots[index].itemGroup;
        }

        return itemGroup;
    }

    public string GetItemName(LUCKY_BUY_GROUP luckyBuyGroup, int index)
    {
        string name = null;
        for (int i = 0; i < luckyBuyGroups.Count; i++)
        {
            if (luckyBuyGroups[i].luckyBuyGroup != luckyBuyGroup || index > luckyBuyGroups[i].itemSlots.Count)
                continue;
            name = luckyBuyGroups[i].itemSlots[index].itemName;
        }

        return name;
    }
}