﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class PlayerAvatar
{
    public GameEnums.HERO hero;
    public Sprite spr;
    public string name;
    public int vip;
    public int coin;
}

[System.Serializable]
public class EnemiAvatar
{
    public GameEnums.ENEMY enemy;
    public Sprite spr;
}

[System.Serializable]
public class GeneralItemAvatar
{
    public GameEnums.GENERAL item;
    public Sprite spr;
}

[CreateAssetMenu(menuName = "Dataset/Avatar", order = 7)]
public class AvatarDataset : ScriptableObject
{
    [SerializeField] private List<PlayerAvatar> playerAvatars = new List<PlayerAvatar>();
    [SerializeField] private List<EnemiAvatar> enemyAvatars = new List<EnemiAvatar>();
    [SerializeField] private List<GeneralItemAvatar> itemAvatars = new List<GeneralItemAvatar>();

    public void Init()
    {
        int heroNumber = Enum.GetNames(typeof(GameEnums.HERO)).Length;
        int enemyNumber = Enum.GetNames(typeof(GameEnums.ENEMY)).Length;
        int itemNumber = Enum.GetNames(typeof(GameEnums.GENERAL)).Length;

        string[] heroSkins = Enum.GetNames(typeof(GameEnums.HERO)).ToArray();
        string[] enemySkins = Enum.GetNames(typeof(GameEnums.ENEMY)).ToArray();
        string[] itemIcons = Enum.GetNames(typeof(GameEnums.GENERAL)).ToArray();

        List<string> chars = new List<string>();
        chars = TxtPath.Get("OutfitPriceDataset");
        int column = 1;

        if (chars.Count <= 0 || playerAvatars.Count >= heroNumber)
            return;
        for (int i = playerAvatars.Count; i < heroNumber; i++)
        {
            GameEnums.HERO hero;
            Enum.TryParse(heroSkins[i], out hero);

            PlayerAvatar avatar = new PlayerAvatar()
            {
                hero = hero,
                spr = null,
                name = "player",
                vip = 0,
                coin = 0
            };
            playerAvatars.Add(avatar);
        }

        for (int i = 0; i < playerAvatars.Count; i++)
        {
            List<string> attributes = new List<string>();
            for (int j = column - 1; j < column; j++)
            {
                attributes.Add(chars[j]);
            }

            if (attributes.Count > 0)
                int.TryParse(attributes[0], out playerAvatars[i].coin);

            column += 1;
        }

        if (enemyAvatars.Count < enemyNumber)
        {
            for (int i = enemyAvatars.Count; i < enemyNumber; i++)
            {
                GameEnums.ENEMY enemy;
                Enum.TryParse(enemySkins[i], out enemy);
                EnemiAvatar avatar = new EnemiAvatar()
                {
                    enemy = enemy,
                    spr = null
                };
                enemyAvatars.Add(avatar);
            }
        }

        if (itemAvatars.Count < itemNumber)
        {
            for (int i = itemAvatars.Count; i < itemNumber; i++)
            {
                GameEnums.GENERAL item;
                Enum.TryParse(itemIcons[i], out item);
                GeneralItemAvatar avatar = new GeneralItemAvatar()
                {
                    item = item,
                    spr = null
                };
                itemAvatars.Add(avatar);
            }
        }
    }

    public Sprite GetPlayerAvatar(GameEnums.HERO hero)
    {
        Sprite spr = null;
        for (int i = 0; i < playerAvatars.Count; i++)
        {
            if (playerAvatars[i].hero == hero)
                spr = playerAvatars[i].spr;
        }

        return spr;
    }

    public Sprite GetEnemyAvatar(GameEnums.ENEMY enemy)
    {
        Sprite spr = null;
        for (int i = 0; i < enemyAvatars.Count; i++)
        {
            if (enemyAvatars[i].enemy == enemy)
                spr = enemyAvatars[i].spr;
        }

        return spr;
    }

    public Sprite GetItemIcon(string itemName)
    {
        Sprite spr = null;
        for (int i = 0; i < itemAvatars.Count; i++)
        {
            GameEnums.GENERAL general = GameEnums.GENERAL.Gold;
            Enum.TryParse(itemName, out general);
            if (itemAvatars[i].item == general)
                spr = itemAvatars[i].spr;
        }

        return spr;
    }

    public string GetPlayerName(GameEnums.HERO hero)
    {
        string name = "player";

        for (int i = 0; i < playerAvatars.Count; i++)
        {
            if (playerAvatars[i].hero == hero)
                name = playerAvatars[i].name;
        }

        return name;
    }

    public int GetVip(GameEnums.HERO hero)
    {
        int vip = 0;

        for (int i = 0; i < playerAvatars.Count; i++)
        {
            if (playerAvatars[i].hero == hero)
                vip = playerAvatars[i].vip;
        }

        return vip;
    }

    public int GetPrice(GameEnums.HERO hero)
    {
        int number = 0;

        for (int i = 0; i < playerAvatars.Count; i++)
        {
            if (playerAvatars[i].hero == hero)
                number = playerAvatars[i].coin;
        }

        return number;
    }
}