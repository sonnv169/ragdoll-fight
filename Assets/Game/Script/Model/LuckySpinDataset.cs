﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum LUCKY_SPIN_GROUP
{
    GROUP_0,
    GROUP_1,
    GROUP_2,
    GROUP_3,
    GROUP_4,
    GROUP_5
}

[System.Serializable]
public class LuckySpinSlot
{
    public GameEnums.ITEM_GROUP itemGroup;
    public string itemName;
    public int amount;
}

[System.Serializable]
public class LuckySpinBase
{
    public LUCKY_SPIN_GROUP spinGroup;
    public List<LuckySpinSlot> spinSlots = new List<LuckySpinSlot>();
}

[CreateAssetMenu(menuName = "Dataset/Lucky Spin", order = 10)]
public class LuckySpinDataset : ScriptableObject
{
    [SerializeField] private LuckySpinBase[] bases;

    public LuckySpinBase GetLuckySpinBase(LUCKY_SPIN_GROUP luckySpinGroup)
    {
        LuckySpinBase spinBase = new LuckySpinBase();
        for (int i = 0; i < bases.Length; i++)
        {
            if (bases[i].spinGroup != luckySpinGroup)
                continue;
            spinBase = bases[i];
        }

        return spinBase;
    }

    public LUCKY_SPIN_GROUP GetLuckySpinGroup(int index)
    {
        LUCKY_SPIN_GROUP luckySpinGroup = LUCKY_SPIN_GROUP.GROUP_0;
        if (index < bases.Length)
            luckySpinGroup = bases[index].spinGroup;
        return luckySpinGroup;
    }

    public GameEnums.ITEM_GROUP GetItemGroup(LUCKY_SPIN_GROUP group, int index)
    {
        GameEnums.ITEM_GROUP itemGroup = GameEnums.ITEM_GROUP.General;
        for (int i = 0; i < bases.Length; i++)
        {
            if (bases[i].spinGroup != group)
                continue;
            itemGroup = bases[i].spinSlots[index].itemGroup;
        }

        return itemGroup;
    }

    public string GetItemName(LUCKY_SPIN_GROUP group, int index)
    {
        string name = null;
        for (int i = 0; i < bases.Length; i++)
        {
            if (bases[i].spinGroup != group)
                continue;
            name = bases[i].spinSlots[index].itemName;
        }

        return name;
    }

    public int GetBase()
    {
        return bases.Length;
    }

    public int GetBaseLenght(LUCKY_SPIN_GROUP group)
    {
        int number = 0;
        for (int i = 0; i < bases.Length; i++)
        {
            if (bases[i].spinGroup != group)
                continue;
            number = bases[i].spinSlots.Count;
        }

        return number;
    }

    public int GetItemAmount(LUCKY_SPIN_GROUP group, int index)
    {
        int number = 0;
        for (int i = 0; i < bases.Length; i++)
        {
            if (bases[i].spinGroup != group)
                continue;
            number = bases[i].spinSlots[index].amount;
        }

        return number;
    }
}