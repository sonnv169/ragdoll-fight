﻿using UnityEngine;

[System.Serializable]
public enum LUCKY_GIFT_GROUP
{
    GROUP_OUTFIT_AND_WEAPON,
    GROUP_GENERAL_1,
    GROUP_GENERAL_2
}

[System.Serializable]
public class LuckyGiftGroupData
{
    public GameEnums.GENERAL general;
    public int amount;
}

[System.Serializable]
public class LuckyGiftGroup
{
    public LUCKY_GIFT_GROUP giftGroup;
    public LuckyGiftGroupData[] groupDatas;
}

[CreateAssetMenu(menuName = "Dataset/Lucky Gift", order = 8)]
public class LuckyGiftDataset : ScriptableObject
{
    public LuckyGiftGroup[] groups;

    public GameEnums.GENERAL GetGeneral(LUCKY_GIFT_GROUP group, int index)
    {
        GameEnums.GENERAL general = GameEnums.GENERAL.Gold;
        for (int i = 0; i < groups.Length; i++)
        {
            if (groups[i].giftGroup != group)
                continue;
            general = groups[i].groupDatas[index].general;
        }

        return general;
    }

    public int GetLenght(LUCKY_GIFT_GROUP group)
    {
        int number = 0;
        for (int i = 0; i < groups.Length; i++)
        {
            if (groups[i].giftGroup == group)
                number = groups[i].groupDatas.Length;
        }

        return number;
    }

    public int GetAmount(LUCKY_GIFT_GROUP group, int index)
    {
        int number = 0;
        for (int i = 0; i < groups.Length; i++)
        {
            if (groups[i].giftGroup == group)
                number = groups[i].groupDatas[index].amount;
        }

        return number;
    }
}