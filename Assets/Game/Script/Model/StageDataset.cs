﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StageBase
{
    public int stage;
    public int enemyNumber;
    public int atk;
    public int hp;
    public GameEnums.ENEMY enemyID;
    public GameEnums.AI enemyAI;
    public GameEnums.WEAPON weapon;
}

[CreateAssetMenu(menuName = "Dataset/Stage", order = 1)]
public class StageDataset : ScriptableObject
{
    [SerializeField] private List<StageBase> stageBases = new List<StageBase>();

    public void Init()
    {
        List<string> chars = new List<string>();
        chars = TxtPath.Get("StageDataset");
        int column = 7;
        if (chars.Count <= 0)
            return;
        int number = (int) chars.Count / column;
        if (stageBases.Count >= number)
            return;
        stageBases.Clear();
        for (int i = 0; i < number; i++)
        {
            stageBases.Add(new StageBase()
            {
                stage = 1,
                enemyNumber = 1,
                atk = 10,
                hp = 400,
                enemyID = GameEnums.ENEMY.e0,
                enemyAI = GameEnums.AI.ai1,
                weapon = GameEnums.WEAPON.w1
            });
        }

        for (int i = 0; i < stageBases.Count; i++)
        {
            List<string> attributes = new List<string>();
            for (int j = column - 7; j < column; j++)
            {
                attributes.Add(chars[j]);
            }

            if (attributes.Count > 0)
            {
                int.TryParse(attributes[0], out stageBases[i].stage);
                int.TryParse(attributes[1], out stageBases[i].enemyNumber);
                int.TryParse(attributes[3], out stageBases[i].atk);
                int.TryParse(attributes[4], out stageBases[i].hp);
                Enum.TryParse(attributes[2], out stageBases[i].enemyID);
                Enum.TryParse(attributes[5], out stageBases[i].enemyAI);
                Enum.TryParse(attributes[6], out stageBases[i].weapon);
            }

            column += 7;
        }
    }

    public int GetEnemyNumber(int index)
    {
        int number = 0;
        if (index < stageBases.Count)
            number = stageBases[index].enemyNumber;
        if (number > GameConfig.maxEnemy)
            number = GameConfig.maxEnemy;
        return number;
    }

    public int GetAtk(int index)
    {
        int number = 0;
        if (index < stageBases.Count)
            number = stageBases[index].atk;
        return number;
    }

    public int GetHp(int index)
    {
        int number = 0;
        if (index < stageBases.Count)
            number = stageBases[index].hp;
        return number;
    }

    public int GetStageBase()
    {
        return stageBases.Count;
    }

    public GameEnums.ENEMY GetEnemyID(int index)
    {
        GameEnums.ENEMY enemyID = GameEnums.ENEMY.e0;
        if (index < stageBases.Count)
            enemyID = stageBases[index].enemyID;
        return enemyID;
    }

    public GameEnums.AI GetEnemyAI(int index)
    {
        GameEnums.AI enemyAI = GameEnums.AI.ai1;
        if (index < stageBases.Count)
            enemyAI = stageBases[index].enemyAI;
        return enemyAI;
    }

    public GameEnums.WEAPON GetWeapon(int index)
    {
        GameEnums.WEAPON weapon = GameEnums.WEAPON.w1;
        if (index < stageBases.Count)
            weapon = stageBases[index].weapon;
        return weapon;
    }
}