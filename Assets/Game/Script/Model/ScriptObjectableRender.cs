﻿using UnityEngine;

public class ScriptObjectableRender : MonoBehaviour
{
    [SerializeField] private HeroDataset heroDataset;
    [SerializeField] private StageDataset stageDataset;
    [SerializeField] private WonRewardDataset wonRewardDataset;
    [SerializeField] private WeaponDataset weaponDataset;
    [SerializeField] private TrapDataset trapDataset;
    [SerializeField] private DailyRewardDataset dailyRewardDataset;
    [SerializeField] private AvatarDataset avataDataset;
    [SerializeField] private LuckyBuyDataset luckyBuyDataset;

    private void OnEnable()
    {
//        InitHeroDataset();
//        InitStageDataset();
//        InitWonRewardDataset();
//        InitWeaponDataset();
//        InitTrapDataset();
//        InitDailyRewardDataset();
//        InitAvatarDataset();
//        InitLuckyBuyDataset();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InitWeaponDataset();
            InitTrapDataset();
        }
    }

    private void InitHeroDataset()
    {
        if (heroDataset != null)
            heroDataset.Init();
    }

    private void InitStageDataset()
    {
        if (stageDataset != null)
            stageDataset.Init();
    }

    private void InitWonRewardDataset()
    {
        if (wonRewardDataset != null)
            wonRewardDataset.Init();
    }

    private void InitWeaponDataset()
    {
        if (weaponDataset != null)
            weaponDataset.Init();
    }

    private void InitTrapDataset()
    {
        if (trapDataset != null)
            trapDataset.Init();
    }

    private void InitDailyRewardDataset()
    {
        if (dailyRewardDataset != null)
            dailyRewardDataset.Init();
    }

    private void InitAvatarDataset()
    {
        if (avataDataset != null)
            avataDataset.Init();
    }

    private void InitLuckyBuyDataset()
    {
        if (luckyBuyDataset != null)
            luckyBuyDataset.Init();
    }
}