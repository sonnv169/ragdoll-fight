﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class TrapBase
{
    public GameEnums.TRAP trapID;
    public string name;
    public int damage;
}

[CreateAssetMenu(menuName = "Dataset/Trap", order = 5)]
public class TrapDataset : ScriptableObject
{
    [SerializeField] private List<TrapBase> trapBases = new List<TrapBase>();

    public void Init()
    {
        List<string> chars = new List<string>();
        chars = TxtPath.Get("TrapDataset");
        int column = 1;
        if (chars.Count <= 0)
            return;
        int number = Enum.GetNames(typeof(GameEnums.TRAP)).ToList().Count;
        if (trapBases.Count <= 0)
        {
            for (int i = 0; i < number; i++)
            {
                GameEnums.TRAP trapID;
                Enum.TryParse(Enum.GetNames(typeof(GameEnums.TRAP)).ToList()[i], out trapID);
                trapBases.Add(new TrapBase()
                {
                    trapID = trapID,
                    name = trapID.ToString(),
                    damage = 10
                });
            }
        }

        for (int i = 0; i < trapBases.Count; i++)
        {
            List<string> attributes = new List<string>();
            for (int j = column - 1; j < column; j++)
                attributes.Add(chars[j]);

            if (attributes.Count > 0)
                int.TryParse(attributes[0], out trapBases[i].damage);

            column += 1;
        }
    }

    public string GetName(GameEnums.TRAP trapID)
    {
        string name = null;

        for (int i = 0; i < trapBases.Count; i++)
        {
            if (trapBases[i].trapID == trapID)
                name = trapBases[i].name;
        }

        return name;
    }

    public int GetDamage(GameEnums.TRAP trapID)
    {
        int damage = 0;

        for (int i = 0; i < trapBases.Count; i++)
        {
            if (trapBases[i].trapID == trapID)
                damage = trapBases[i].damage;
        }

        return damage;
    }
}