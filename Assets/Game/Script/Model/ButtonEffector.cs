﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonEffector : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private Vector3 scale;

    public float scaleBonus = 0.25f;
    public bool isSound = true;

    private void OnEnable()
    {
        scale = transform.localScale;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnScaleUp();
        if (isSound)
            SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_ui_click, false);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnReScale();
    }

    public void OnScaleUp()
    {
        bool isActive = true;
        if (GetComponent<Button>())
        {
            Button btn = GetComponent<Button>();
            isActive = btn.interactable;
        }

        if (!isActive)
            return;
        transform.localScale = new Vector3(
            scale.x + scaleBonus,
            scale.y + scaleBonus,
            scale.z + scaleBonus);
    }

    public void OnReScale()
    {
        transform.localScale = scale;
    }
}