﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyAIBase
{
    public GameEnums.AI enemyAI;
    public float speed;
    public float countDownAttack;
    public float focusPercent;
}

[CreateAssetMenu(menuName = "Dataset/Enemy AI", order = 2)]
public class EnemyAIDataset : ScriptableObject
{
    [SerializeField] private List<EnemyAIBase> enemyAiBases = new List<EnemyAIBase>();

    private int GetIndex(GameEnums.AI enemyAI)
    {
        int number = 0;
        for (int i = 0; i < enemyAiBases.Count; i++)
        {
            if (enemyAiBases[i].enemyAI == enemyAI)
                number = i;
        }

        return number;
    }

    public GameEnums.AI GetAI(int index)
    {
        GameEnums.AI ai = GameEnums.AI.ai1;
        if (index < enemyAiBases.Count)
            ai = enemyAiBases[index].enemyAI;
        return ai;
    }

    public int GetAIBase()
    {
        return enemyAiBases.Count;
    }

    public float GetSpeed(GameEnums.AI enemyAI)
    {
        return enemyAiBases[GetIndex(enemyAI)].speed;
    }

    public float GetCountDownAttack(GameEnums.AI enemyAI)
    {
        return enemyAiBases[GetIndex(enemyAI)].countDownAttack;
    }

    public float GetFocusPercent(GameEnums.AI enemyAI)
    {
        return enemyAiBases[GetIndex(enemyAI)].focusPercent;
    }
}