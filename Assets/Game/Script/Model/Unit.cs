﻿[System.Serializable]
public class UnitDeathEvent
{
    public Unit unit;
}

public abstract class Unit : ToggleBehaviour
{
    private float speed;
    private int hp;
    private int power;
    private int dropHit;

    public abstract void Flip();
    public abstract void Move();
    public abstract void Stop();
    public abstract void DisplayHp();
    public abstract void OnDropHitUp(int hit);
    public abstract void OnSetHit(int hit);
    public abstract bool IsDrop();

    protected void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    protected void SetPower(int power)
    {
        this.power = power;
    }

    protected void SetDropHit(int dropHit)
    {
        this.dropHit = dropHit;
    }

    protected float GetSpeed()
    {
        return this.speed;
    }

    protected int GetDropHit()
    {
        return this.dropHit;
    }

    public void OnHpMinus(int value)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.GetState() != GAME_STATE.PLAY)
            return;
        hp -= value;
        if (hp >= 1)
            return;
        hp = 0;
        Eventon<UnitDeathEvent>.Trigger(new UnitDeathEvent()
        {
            unit = this
        });
    }

    public void SetHp(int hp)
    {
        this.hp = hp;
    }

    public int GetHp()
    {
        return this.hp;
    }

    public int GetPower()
    {
        return this.power;
    }
}