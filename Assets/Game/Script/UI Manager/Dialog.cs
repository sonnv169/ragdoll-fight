﻿using UnityEngine;

public abstract class Dialog<T> : Dialog where T : Dialog<T>
{
    public static T Instance { get; private set; }

    protected virtual void Awake()
    {
        Instance = (T) this;
    }

    protected virtual void OnDestroy()
    {
        Instance = null;
    }

    protected static void Open()
    {
        if (Instance == null)
            UIManager.Instance.CreateDialog<T>();
        else
            Instance.gameObject.SetActive(true);

        UIManager.Instance.OpenDialog(Instance);

        if (Instance == null || !Instance.GetComponent<Canvas>())
            return;
        Canvas canvas = Instance.GetComponent<Canvas>();
        if (Instance.Equals(UIMenu.Instance))
            canvas.sortingOrder = 150;
        else if (Instance.Equals(UIWeapon.Instance) || Instance.Equals(UIOutfit.Instance))
            canvas.sortingOrder = 151;
        else
            canvas.sortingOrder = canvas.sortingOrder + 150;

        SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_popup_open, false);
    }

    protected static void Close()
    {
        if (Instance == null)
        {
#if UNITY_EDITOR
            Debug.LogErrorFormat("Trying to close dialog {0} but Instance is null", typeof(T));
#endif
            return;
        }

        UIManager.Instance.CloseDialog(Instance);
    }

    public override void OnBackPressed()
    {
        if (!Cancelable)
            return;
        if (Instance != null)
        {
            if (Instance.Equals(UIDailyGift.Instance))
                UIDailyGift.Instance.OnClose();
            else if (Instance.Equals(UIDisconnect.Instance))
            {
                Time.timeScale = GameConfig.originTime;
                UIDisconnect.Instance.OnClose();
            }
            else if (Instance.Equals(UILose.Instance))
                UILose.Instance.OnClose();
            else if (Instance.Equals(UILuckyBuy.Instance))
                UILuckyBuy.Instance.OnClose();
            else if (Instance.Equals(UILuckyGift.Instance))
                UILuckyGift.Instance.OnClose();
            else if (Instance.Equals(UILuckySpin.Instance))
                UILuckySpin.Instance.OnClose();
            else if (Instance.Equals(UIOutfit.Instance))
                UIOutfit.Instance.OnClose();
            else if (Instance.Equals(UIPause.Instance))
            {
                Time.timeScale = GameConfig.originTime;
                UIPause.Instance.OnClose();
            }
            else if (Instance.Equals(UIRevival.Instance))
                UIRevival.Instance.OnClose();
            else if (Instance.Equals(UISetting.Instance))
                UISetting.Instance.OnClose();
            else if (Instance.Equals(UIShop.Instance))
                UIShop.Instance.OnClose();
            else if (Instance.Equals(UISkinFree.Instance))
                UISkinFree.Instance.OnClose();
            else if (Instance.Equals(UIUnlocked.Instance))
                UIUnlocked.Instance.CheckingToClose();
            else if (Instance.Equals(UIWeapon.Instance))
                UIWeapon.Instance.OnClose();
            else if (Instance.Equals(UIWin.Instance))
                UIWin.Instance.OnClose();
            else if (Instance.Equals(UINoAd.Instance))
                UINoAd.Instance.OnClose();
            else if (Instance.Equals(UIRate.Instance))
                UIRate.Instance.OnClose();
        }

//        Close();
    }

    public override void OnDialogBecameVisible()
    {
    }
}

public abstract class Dialog : MonoBehaviour
{
    [Tooltip("Destroy the Game Object when dialog is closed (reduces memory usage)")]
    public bool DestroyWhenClosed = true;

    [Tooltip("Cancelable dialog when press back key")]
    public bool Cancelable = true;

    public abstract void OnDialogBecameVisible();
    public abstract void OnBackPressed();
}