﻿using System;
using System.Collections.Generic;
using UnityEngine;

public struct AdapterClickedEvent
{
    public bool isClick;
}

[System.Serializable]
public class CloseDialogEvent
{
    public Dialog dialog;
}

public class UIManager : MonoBehaviour
{
    private static UIManager mInstance;

    private readonly Stack<Menu> menuStack = new Stack<Menu>();
    private readonly List<Dialog> dialogList = new List<Dialog>();
    private Menu[] menus;

    private bool isVisible;
    private int resultTime;

    public Dialog[] dialogs;

    #region property

    public static UIManager Instance
    {
        get
        {
            if (mInstance == null)
                mInstance = FindObjectOfType<UIManager>();
            return mInstance;
        }
    }

    #endregion

    #region MENU

    public void CreateInstance<T>() where T : Menu
    {
        var prefab = GetPrefab<T>();

        Instantiate(prefab, transform);
    }

    private T GetPrefab<T>() where T : Menu
    {
        for (var i = 0; i < menus.Length; i++)
        {
            if (menus[i] != null && menus[i].GetType() == typeof(T))
            {
                return (T) menus[i];
            }
        }

        throw new MissingReferenceException("Prefab not found for type " + typeof(T));
    }

    public void OpenMenu(Menu instance)
    {
        // De-activate top menu
        var maxSortingOrder = 0;
        var topCanvas = instance.GetComponent<Canvas>();
        if (dialogList.Count > 0)
        {
            foreach (var dialog in dialogList)
            {
                if (dialog != null && dialog.GetComponent<Canvas>() != null)
                {
                    var sortingOrder = dialog.GetComponent<Canvas>().sortingOrder;
                    if (sortingOrder > maxSortingOrder)
                        maxSortingOrder = sortingOrder;
                }
            }
        }

        if (menuStack.Count > 0)
        {
            if (instance.DisableMenusUnderneath)
            {
                foreach (var menu in menuStack)
                {
                    menu.gameObject.SetActive(false);
                    if (menu.DisableMenusUnderneath)
                        break;
                }
            }

            var topMenuCanvas = menuStack.Peek().GetComponent<Canvas>();
            if (topMenuCanvas != null && topMenuCanvas.sortingOrder > maxSortingOrder)
                maxSortingOrder = topMenuCanvas.sortingOrder;
        }

        topCanvas.sortingOrder = maxSortingOrder + 1;
        if (menuStack.Count > 0)
        {
            var topMenu = menuStack.Peek();
            if (topMenu != null)
                topMenu.OnMenuBecameInvisible();
        }

        instance.OnMenuBecameVisible();
        if (!menuStack.Contains(instance))
            menuStack.Push(instance);
        Debug.Log("OpenMenu: " + instance.name);
    }

    public void CloseMenu(Menu menu)
    {
        if (menuStack.Count == 0)
        {
            Debug.LogErrorFormat(menu, "{0} cannot be closed because menu stack is empty", menu.GetType());
            return;
        }

        if (menuStack.Peek() != menu)
        {
            Debug.LogErrorFormat(menu, "{0} cannot be closed because it is not on top of stack", menu.GetType());
            return;
        }

        CloseTopMenu();
    }

    public void CloseTopMenu()
    {
        var instance = menuStack.Pop();

        if (instance.DestroyWhenClosed)
            Destroy(instance.gameObject);
        else
            instance.gameObject.SetActive(false);

        // Re-activate top menu
        // If a re-activated menu is an overlay we need to activate the menu under it

        Menu topMenu = null;
        if (dialogList.Count <= 0)
        {
            if (menuStack.Count > 0)
            {
                topMenu = menuStack.Peek();
                topMenu.OnMenuBecameVisible();
            }

            foreach (var menu in menuStack)
            {
                menu.gameObject.SetActive(true);
                if (menu.DisableMenusUnderneath)
                    break;
            }

            return;
        }

        Dialog topDialog = null;
        if (menuStack.Count > 0)
        {
            topMenu = menuStack.Peek();
        }

        var maxSortingOrder = (topMenu == null && dialogList[0] != null && dialogList[0].GetComponent<Canvas>() != null)
            ? dialogList[0].GetComponent<Canvas>().sortingOrder
            : topMenu.GetComponent<Canvas>().sortingOrder;
        foreach (var dialog in dialogList)
        {
            if (dialog == null || dialog.GetComponent<Canvas>() == null)
                continue;
            if (maxSortingOrder < dialog.GetComponent<Canvas>().sortingOrder)
            {
                topDialog = dialog;
                topMenu = null;
            }
        }

        if (topDialog != null)
        {
            topDialog.gameObject.SetActive(true);
            topDialog.OnDialogBecameVisible();
        }
        else if (topMenu != null)
        {
            foreach (var menu in menuStack)
            {
                menu.gameObject.SetActive(true);
                if (menu.DisableMenusUnderneath)
                    break;
            }

            topMenu.OnMenuBecameVisible();
        }
    }

    private void Update()
    {
        // On Android the back button is sent as Esc
        if (!Input.GetKeyUp(KeyCode.Escape))
        {
            return;
        }

        Dialog topDialog = null;
        Menu topMenu = null;
        if (menuStack.Count > 0)
        {
            topMenu = menuStack.Peek();
        }

        if (dialogList.Count <= 0)
        {
            if (topMenu != null)
                topMenu.OnBackPressed();
            return;
        }

        var maxSortingOrder = (topMenu == null && dialogList[0] != null && dialogList[0].GetComponent<Canvas>() != null)
            ? dialogList[0].GetComponent<Canvas>().sortingOrder
            : topMenu.GetComponent<Canvas>().sortingOrder;
        foreach (var dialog in dialogList)
        {
            if (dialog == null || dialog.GetComponent<Canvas>() == null)
                continue;
            if (maxSortingOrder < dialog.GetComponent<Canvas>().sortingOrder)
            {
                topDialog = dialog;
                topMenu = null;
            }
        }

        if (topMenu != null)
            topMenu.OnBackPressed();
        if (topDialog != null)
            topDialog.OnBackPressed();
    }

    #endregion

    #region DIALOG

    public void CreateDialog<T>() where T : Dialog
    {
        var prefab = GetDialogPrefab<T>();

        Instantiate(prefab, transform);
    }

    private T GetDialogPrefab<T>() where T : Dialog
    {
        for (var i = 0; i < dialogs.Length; i++)
        {
            if (dialogs[i] != null && dialogs[i].GetType() == typeof(T))
            {
                return (T) dialogs[i];
            }
        }

        throw new MissingReferenceException("Prefab not found for type " + typeof(T));
    }

    public void OpenDialog(Dialog instance)
    {
        // De-activate top menu
        var maxSortingOrder = 0;
        var topCanvas = instance.GetComponent<Canvas>();
//        dialogs.Remove(instance);
        if (dialogList.Count > 0)
        {
            foreach (var dialog in dialogList)
            {
                if (dialog == null || dialog.GetComponent<Canvas>() == null)
                {
                    continue;
                }

                var sortingOrder = dialog.GetComponent<Canvas>().sortingOrder;
                if (sortingOrder > maxSortingOrder)
                    maxSortingOrder = sortingOrder;
            }
        }

        if (menuStack.Count > 0)
        {
            var topMenuCanvas = menuStack.Peek().GetComponent<Canvas>();
            if (topMenuCanvas != null && topMenuCanvas.sortingOrder > maxSortingOrder)
                maxSortingOrder = topMenuCanvas.sortingOrder;
        }

        topCanvas.sortingOrder = maxSortingOrder + 1;
        if (!dialogList.Contains(instance))
            dialogList.Add(instance);
        if (menuStack.Count > 0)
        {
            var topMenu = menuStack.Peek();
            if (topMenu != null)
                topMenu.OnMenuBecameInvisible();
        }

        instance.OnDialogBecameVisible();
    }

    public void CloseDialog(Dialog instance)
    {
        Eventon<CloseDialogEvent>.Trigger(new CloseDialogEvent()
        {
            dialog = instance
        });

        if (dialogList.Count == 0)
        {
//            Debug.LogErrorFormat(instance, "{0} cannot be closed because dialog list is empty", instance.GetType());
            return;
        }

        for (var i = 0; i < dialogList.Count; i++)
        {
            try
            {
                if (instance != dialogList[i])
                {
                    continue;
                }

                dialogList.Remove(dialogList[i]);
                if (instance.DestroyWhenClosed)
                    Destroy(instance.gameObject);
                else
                    instance.gameObject.SetActive(false);

                //Re-active top dialog
                Dialog topDialog = null;
                Menu topMenu = null;
                if (menuStack.Count > 0)
                {
                    topMenu = menuStack.Peek();
                }

                if (topMenu == null && dialogList.Count == 0)
                    return;
                var maxSortingOrder =
                    (topMenu == null && dialogList[0] != null && dialogList[0].GetComponent<Canvas>() != null)
                        ? dialogList[0].GetComponent<Canvas>().sortingOrder
                        : topMenu.GetComponent<Canvas>().sortingOrder;
                foreach (var dialog in dialogList)
                {
                    if (instance == dialog || dialog == null || dialog.GetComponent<Canvas>() == null)
                        continue;
                    if (maxSortingOrder < dialog.GetComponent<Canvas>().sortingOrder)
                    {
                        topDialog = dialog;
                        topMenu = null;
                    }
                }

                if (topDialog != null && topDialog.gameObject.activeSelf)
                {
//                    topDialog.gameObject.SetActive(true);
                    topDialog.OnDialogBecameVisible();
                }
                else if (topMenu != null)
                {
                    foreach (var menu in menuStack)
                    {
                        menu.gameObject.SetActive(true);
                        if (menu.DisableMenusUnderneath)
                            break;
                    }

                    topMenu.OnMenuBecameVisible();
                }

                break;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }

    #endregion

    private void OnEnable()
    {
        if (!isVisible)
        {
            UIMenu.Show();
            resultTime = 0;
            isVisible = true;
        }
    }

    public void UpResultTime()
    {
        if (Singleton<TutorialManager>.Instance == null)
            return;
        TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
        if (!tutorialManager.GetTutorialData().isMove)
            return;
        resultTime += 1;
        if (resultTime >= GameConfig.timeToInterstitial)
        {
            if (Singleton<GameManager>.Instance == null)
                return;
            GameManager gameManager = Singleton<GameManager>.Instance;
            if (gameManager.IsRemoveAds())
                return;
            IronSource.Agent.showInterstitial();
            resultTime = 0;

#if UNITY_EDITOR
            Debug.Log("Show inters!");
#endif

            Singleton<GameManager>.Instance.OnLogAd(AdsType.Inter, "end_match", "back_ui_home");
        }
    }
}