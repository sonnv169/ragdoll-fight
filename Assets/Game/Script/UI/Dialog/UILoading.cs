﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public enum LOADING
{
    MENU,
    PLAY,
    RETRY
}

public class UILoading : Dialog<UILoading>
{
    private Animator mAnimator;
    private static LOADING loading;

    [SerializeField] private float motion;
    [SerializeField] private Image imgFade;
    [SerializeField] private Image imgName;
    [SerializeField] private Sprite sprMenuFade;
    [SerializeField] private Sprite sprPlayFade;
    [SerializeField] private Sprite sprMenuName;
    [SerializeField] private Sprite sprPlayName;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    private static void Hide()
    {
        Close();
    }

    public static void Show(LOADING loading)
    {
        Open();
        Time.timeScale = GameConfig.originTime;
        UILoading.loading = loading;
    }

    private void OnEnable()
    {
        SetSprite();
        StartCoroutine(DelayFadeOut());
    }

    private IEnumerator DelayFadeOut()
    {
        yield return new WaitForSeconds(motion);
        AnimatorManager.Play(Animator, "anim_fade_out");
        float time = AnimatorManager.GetLenght(Animator, "anim_fade_out");
        StartCoroutine(DelayClose(time + 0.5f));
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        OnLoad();
        Hide();
    }

    private void SetSprite()
    {
        switch (loading)
        {
            case LOADING.MENU:
                if (imgFade != null)
                    imgFade.sprite = sprMenuFade;
                if (imgName != null)
                    imgName.sprite = sprMenuName;
                break;

            case LOADING.PLAY:
            case LOADING.RETRY:
                if (imgFade != null)
                    imgFade.sprite = sprPlayFade;
                if (imgName != null)
                    imgName.sprite = sprPlayName;
                break;
        }

        if (imgName != null)
            imgName.SetNativeSize();
    }

    private void OnLoad()
    {
        SoundManager.Stop(AUDIO_KIND.MUSIC);
        if (Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        switch (loading)
        {
            case LOADING.MENU:
                UIMenu.Show();
                break;

            case LOADING.PLAY:
                UIGameplay.Show();
                stageManager.CreatState(stageManager.GetStage());
                break;

            case LOADING.RETRY:
                stageManager.CreatState(stageManager.GetStage());

                IronSource.Agent.displayBanner();
                break;
        }
    }
}