﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UISetting : Dialog<UISetting>
{
    private Animator mAnimator;

    [SerializeField] private Button btnClose;
    [SerializeField] private Slider sldMusic;
    [SerializeField] private Slider sldSound;
    [SerializeField] private Text txtMusicPercent;
    [SerializeField] private Text txtSoundPercent;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_setting_show");

        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);

        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        if (sldMusic != null)
        {
            sldMusic.value = audioManager.GetMusic().volume;
            DisplayMusicPercent(audioManager.GetMusic().volume);
            sldMusic.onValueChanged.AddListener(delegate { SetMusicVolume(sldMusic.value); });
        }

        if (sldSound != null)
        {
            sldSound.value = audioManager.GetSound().volume;
            DisplaySoundPercent(audioManager.GetSound().volume);
            sldSound.onValueChanged.AddListener(delegate { SetSoundVolume(sldSound.value); });
        }
    }

    private void DisplayMusicPercent(float value)
    {
        if (txtMusicPercent == null)
            return;
        txtMusicPercent.text = (int) (value * 100 / 1) + "%";
    }

    private void DisplaySoundPercent(float value)
    {
        if (txtSoundPercent == null)
            return;
        txtSoundPercent.text = (int) (value * 100 / 1) + "%";
    }

    private void SetMusicVolume(float value)
    {
        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        audioManager.SetMusicVolume(value);
        DisplayMusicPercent(audioManager.GetMusic().volume);
    }

    private void SetSoundVolume(float value)
    {
        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        audioManager.SetSoundVolume(value);
        DisplaySoundPercent(audioManager.GetSound().volume);
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Hide();
    }

    public void OnClose()
    {
        UIMenu.Instance.EnableBlocker(false);
        AnimatorManager.Play(Animator, "anim_setting_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_setting_close");
        StartCoroutine(DelayClose(lenght + 0.15f));

        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        audioManager.Save();
    }
}