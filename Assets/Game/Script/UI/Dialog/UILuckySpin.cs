﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[System.Serializable]
public class SpinResultEvent
{
    public int result;
}

public class UILuckySpin : Dialog<UILuckySpin>
{
    private Animator mAnimator;
    private float currenSpeed;
    private float targetAngle;
    private float currentAngle;
    private int result;
    private bool isSpin;

    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnFeeSpin;
    [SerializeField] private Button btnFreeSpin;
    [SerializeField] private Text txtSpinPrice;
    [SerializeField] private Transform spinContainer;
    [SerializeField] private float angle;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float minRotationSpeed;
    [SerializeField] private float[] slotRates;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        Init();
    }

    private void Update()
    {
        OnRotation();
    }

    private void Init()
    {
        currentAngle = 1;
        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);

        if (btnFeeSpin != null)
            btnFeeSpin.onClick.AddListener(() =>
            {
                if (Singleton<CurrencyManager>.Instance == null)
                    return;
                CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
                if (currencyManager.GetCurrency(CURRENCY.COIN) >= GameConfig.spinFee)
                {
                    currencyManager.Pay(CURRENCY.COIN, GameConfig.spinFee);
                    OnSpin();
                }
                else
                    UIShop.Show();
            });

        if (btnFreeSpin != null)
            btnFreeSpin.onClick.AddListener(() =>
            {
                btnFreeSpin.interactable = false;
                AdReward.Show();
                Eventon<AD_STATE>.EventDynamic += state =>
                {
                    if (state == AD_STATE.FINISHED)
                    {
                        OnSpin();
                        
                        Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "get_free_spin", "ui_free_spin");
                    }
                    else
                        btnFreeSpin.interactable = true;
                };
                if (!AdController.IsConnecting())
                    btnFreeSpin.interactable = true;
            });

        string spinFee = GameConfig.spinFee.ToString();
        if (GameConfig.spinFee >= 1000)
            spinFee = $"{GameConfig.spinFee:#,#.}";
        if (txtSpinPrice != null)
            txtSpinPrice.text = "-" + spinFee;
    }

    private void EnableFeatureButton(bool isStatus)
    {
        EnableFeeSpinButton(isStatus);
        EnableFreeSpinButton(isStatus);
    }

    private void EnableFeeSpinButton(bool isStatus)
    {
        if (btnFeeSpin != null)
            btnFeeSpin.interactable = isStatus;
    }

    private void EnableFreeSpinButton(bool isStatus)
    {
        if (btnFreeSpin != null)
            btnFreeSpin.interactable = isStatus;
    }

    private int GetResultSlot()
    {
        List<float> rates = new List<float>();
        int number = 0;

        for (int i = 0; i < slotRates.Length; i++)
        {
            float rate = Random.Range(0, slotRates[i]);
            if (!rates.Contains(rate))
                rates.Add(rate);
        }

        if (rates.Count > 0)
        {
            for (int i = 0; i < rates.Count; i++)
            {
                if (rates[i] < rates.Max())
                    continue;
                number = i;
            }
        }

        return number;
    }

    private float GetAngle(int newResult)
    {
        int step = newResult;
        float angleResult = (step * angle) + (slotRates.Length * 360);
        return angleResult;
    }

    private void OnSpin()
    {
        EnableFeatureButton(false);
        isSpin = true;
        int newResult = GetResultSlot();
        targetAngle += GetAngle(newResult);
        result = newResult;
        currenSpeed = rotationSpeed;
    }

    private void OnRotation()
    {
        if (!isSpin || spinContainer == null || Time.timeScale <= 0)
            return;
        if (currentAngle < targetAngle)
        {
            float angelPercent = (currentAngle * 100) / targetAngle;
            float inversePercent = 100.0f - angelPercent;
            currenSpeed = (rotationSpeed * inversePercent) / 100;
            if (currenSpeed <= minRotationSpeed)
                currenSpeed = minRotationSpeed;
            currentAngle += currenSpeed;
            spinContainer.rotation = Quaternion.Euler(0, 0, currentAngle);
        }
        else
        {
            targetAngle = spinContainer.transform.rotation.z;
            currentAngle = targetAngle;
            isSpin = false;

            Eventon<SpinResultEvent>.Trigger(new SpinResultEvent()
            {
                result = result
            });

            EnableFeatureButton(true);
        }
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Hide();
    }

    public void OnClose()
    {
        AnimatorManager.Play(Animator, "anim_skin_free_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_skin_free_close");
        StartCoroutine(DelayClose(lenght + 0.15f));
        if (Singleton<SaleFeatureManager>.Instance == null)
            return;
        SaleFeatureManager saleFeatureManager = Singleton<SaleFeatureManager>.Instance;
        string[] groups = Enum.GetNames(typeof(LUCKY_SPIN_GROUP)).ToArray();
        for (int i = 0; i < groups.Length; i++)
        {
            LUCKY_SPIN_GROUP spinGroup = LUCKY_SPIN_GROUP.GROUP_0;
            Enum.TryParse(groups[i], out spinGroup);
            saleFeatureManager.UpLuckySpinRound(spinGroup);
        }
    }
}