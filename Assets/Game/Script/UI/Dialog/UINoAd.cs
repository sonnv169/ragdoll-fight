﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UINoAd : Dialog<UINoAd>
{
    private Animator mAnimator;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_pausing");
        Time.timeScale = 0;

        if (UIMenu.Instance != null)
            UIMenu.Instance.EnableBlocker(false);
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Hide();
    }

    public void OnClose()
    {
        Eventon<AD_STATE>.Trigger(AD_STATE.FAILED);
       
        Time.timeScale = GameConfig.originTime;
        AnimatorManager.Play(Animator, "anim_resume");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_resume");
        StartCoroutine(DelayClose(lenght + 0.5f));
    }
}