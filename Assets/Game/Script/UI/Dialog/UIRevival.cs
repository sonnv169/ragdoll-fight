﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIRevival : Dialog<UIRevival>
{
    private Animator mAnimator;
    private float revivalTime;

    [SerializeField] private Button btnRevival;
    [SerializeField] private Button btnClose;
    [SerializeField] private Image imgTimer;
    [SerializeField] private Text txtTimer;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_lose_show");

        if (Singleton<Player>.Instance != null)
        {
            Player player = Singleton<Player>.Instance;
            player.GetBody().Reborn();
        }

        if (btnRevival != null)
            btnRevival.onClick.AddListener(() =>
            {
                AdReward.Show();
                if (AdController.IsConnecting())
                    btnRevival.interactable = false;
                Eventon<AD_STATE>.EventDynamic += state =>
                {
                    if (state != AD_STATE.FINISHED)
                        btnRevival.interactable = true;
                    else
                    {
                        if (Singleton<GameManager>.Instance == null ||
                            Singleton<Player>.Instance == null)
                            return;
                        GameManager gameManager = Singleton<GameManager>.Instance;
                        Player player = Singleton<Player>.Instance;
                        player.Reborn();
                        gameManager.SetState(GAME_STATE.PLAY);
                        
                        Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "get_revival", "ui_revival");
                        
                        Hide();
                    }
                };
            });

        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);

        revivalTime = GameConfig.revivalTime;
    }

    private void Update()
    {
        if (revivalTime > 0)
        {
            revivalTime -= Time.deltaTime;
            if (imgTimer != null)
                imgTimer.fillAmount = revivalTime / GameConfig.revivalTime;
            if (txtTimer != null)
                txtTimer.text = ((int) revivalTime).ToString();
        }
        else
        {
            revivalTime = 0;
            if (UILose.Instance == null)
            {
                UILose.Show();
                Hide();
            }
        }
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        if (UILose.Instance == null)
            UILose.Show();
        Hide();
    }

    public void OnClose()
    {
        Time.timeScale = GameConfig.originTime;
        AnimatorManager.Play(Animator, "anim_lose_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_lose_close");
        StartCoroutine(DelayClose(lenght + 0.15f));

        SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_you_lose, false);
    }
}