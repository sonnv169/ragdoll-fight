﻿using UnityEngine;
using UnityEngine.UI;

public class UIGameplay : Dialog<UIGameplay>
{
    private Transform enemy;
    private int tutorialStep;

    [SerializeField] private IcoBooster[] icosBooster;
    [SerializeField] private Text txtPlayerHp;
    [SerializeField] private Text txtEnemyHp;
    [SerializeField] private Image imgPlayerHp;
    [SerializeField] private Image imgEnemyHp;
    [SerializeField] private Image icoPlayerAvatar;
    [SerializeField] private Image[] icoEnemyAvatars;
    [SerializeField] private Image[] imgEnemyTurns;
    [SerializeField] private RectTransform lblEnemy;
    [SerializeField] private RectTransform avatarEnemysRoot;
    [SerializeField] private Vector2[] lblEnemyRects;
    [SerializeField] private Vector2[] avatarEnemyRects;
    [SerializeField] private GameObject[] tutorialPanels;
    [SerializeField] private GameObject boosterTutorialPanel;
    [SerializeField] private GameObject btnSetting;
    [SerializeField] private GameObject[] btnBoosters;

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
        Eventon<BoosterEvent>.EventStatic += OnBoosterEvent;
        Eventon<CloseDialogEvent>.EventStatic += OnCloseDialogEvent;
        Eventon<TutorialData>.EventStatic += OnTutorialData;
    }

    private void OnDisable()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
        Eventon<BoosterEvent>.EventStatic -= OnBoosterEvent;
        Eventon<CloseDialogEvent>.EventStatic -= OnCloseDialogEvent;
        Eventon<TutorialData>.EventStatic -= OnTutorialData;
    }

    private void OnTutorialData(TutorialData param)
    {
        if (Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        if (stageManager.GetStage() > 0 || !param.isMove)
            return;
        EnableLblEnemy(true);
    }

    private void OnCloseDialogEvent(CloseDialogEvent param)
    {
        if (param.dialog != UILoading.Instance)
            return;
        CheckingTutorialOpen();
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                OnReset();
                break;
        }
    }

    private void OnBoosterEvent(BoosterEvent param)
    {
        for (int i = 0; i < icosBooster.Length; i++)
        {
            if (icosBooster[i] == null || param.booster != icosBooster[i].GetBooster() || !param.isCasting)
                continue;
            icosBooster[i].gameObject.SetActive(true);
            icosBooster[i].SetCooldown();
        }
    }

    private void DisableTutorialPanel(int index)
    {
        if (index < tutorialPanels.Length && tutorialPanels[index] != null)
            tutorialPanels[index].SetActive(false);
    }

    private void DisableTutorialPanels()
    {
        for (int i = 0; i < tutorialPanels.Length; i++)
        {
            DisableTutorialPanel(i);
        }

        EnableBoosterTutorialPanel(false);
    }

    private void EnableMoveTutorialPanel()
    {
        if (tutorialStep >= tutorialPanels.Length)
            return;
        if (tutorialPanels[tutorialStep] != null)
            tutorialPanels[tutorialStep].SetActive(true);
    }

    private void EnableBoosterTutorialPanel(bool isStatus)
    {
        if (boosterTutorialPanel != null)
            boosterTutorialPanel.SetActive(isStatus);
    }

    private void CheckingTutorialOpen()
    {
        if (Singleton<TutorialManager>.Instance == null)
            return;
        TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
        if (!tutorialManager.GetTutorialData().isMove)
        {
            Time.timeScale = 0;
            EnableMoveTutorialPanel();
        }
        else
        {
            if (tutorialManager.GetTutorialData().isBooster)
                return;
            Time.timeScale = 0;
            EnableBoosterTutorialPanel(true);
        }
    }

    private void OnReset()
    {
        for (int i = 0; i < icosBooster.Length; i++)
        {
            if (icosBooster[i] != null)
                icosBooster[i].gameObject.SetActive(false);
        }

        EnableLblEnemy(false);
    }

    private void Init()
    {
        OnReset();
        DisableTutorialPanels();

        if (Singleton<GameManager>.Instance == null || Singleton<TutorialManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;

        if (gameManager.IsTest() || !tutorialManager.GetTutorialData().isMove)
        {
            for (int i = 0; i < btnBoosters.Length; i++)
            {
                if (btnBoosters[i] != null)
                    btnBoosters[i].SetActive(false);
            }
        }

        if (tutorialManager.GetTutorialData().isMove)
        {
            if (btnSetting != null)
                btnSetting.gameObject.SetActive(true);
        }
        else
        {
            EnableLblEnemy(false);
            if (btnSetting != null)
                btnSetting.gameObject.SetActive(false);
        }
    }

    public void ShowUIPause()
    {
        UIPause.Show();
        SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_ui_click, false);

        IronSource.Agent.hideBanner();
        IronSource.Agent.showInterstitial();
        
        Singleton<GameManager>.Instance.OnLogAd(AdsType.Inter, "pause_game", "ui_pause");
    }

    public void EnableLblEnemy(bool isStatus)
    {
        if (lblEnemy != null)
            lblEnemy.gameObject.SetActive(isStatus);
    }

    public void CheckingTutorialClose()
    {
        DisableTutorialPanel(tutorialStep);
        tutorialStep += 1;
        if (tutorialStep < tutorialPanels.Length)
            EnableMoveTutorialPanel();
        else
        {
            Time.timeScale = GameConfig.originTime;
            if (Singleton<TutorialManager>.Instance == null)
                return;
            TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
            tutorialManager.GetTutorialData().isPlay = true;
            tutorialManager.SaveTutorialData();
        }
    }

    public void CloseBoosterTutorial()
    {
        Time.timeScale = GameConfig.originTime;
        EnableBoosterTutorialPanel(false);
        if (Singleton<TutorialManager>.Instance == null)
            return;
        TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
        tutorialManager.GetTutorialData().isBooster = true;
        tutorialManager.SaveTutorialData();
    }

    public void SetPlayerAvatar()
    {
        if (icoPlayerAvatar == null || Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        Sprite spr = characterManager.GetPlayerAvatar(characterManager.GetHero());
        icoPlayerAvatar.sprite = spr;
        DisplayPlayerAvatar(Color.white);
        icoPlayerAvatar.SetNativeSize();
    }

    public void DisplayPlayerAvatar(Color color)
    {
        if (icoPlayerAvatar != null)
            icoPlayerAvatar.color = color;
    }

    public void InitEnemyAvatar(int target)
    {
        if (Singleton<CharacterManager>.Instance == null || Singleton<StageManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        StageManager stageManager = Singleton<StageManager>.Instance;

        for (int i = 0; i < icoEnemyAvatars.Length; i++)
        {
            GameEnums.ENEMY enemyId = stageManager.GetEnemyID(i);
            Sprite spr = characterManager.GetEnemyAvatar(enemyId);
            if (icoEnemyAvatars[i] == null)
                continue;
            icoEnemyAvatars[i].color = Color.white;
            icoEnemyAvatars[i].sprite = spr;
            icoEnemyAvatars[i].SetNativeSize();
        }

        for (int i = 0; i < imgEnemyTurns.Length; i++)
        {
            if (imgEnemyTurns[i] == null)
                continue;
            imgEnemyTurns[i].gameObject.SetActive(false);
            if (i <= target - 1)
                imgEnemyTurns[i].gameObject.SetActive(true);
        }
    }

    public void SortLblEnemy()
    {
        if (Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        int target = stageManager.GetTarget();

        if (target >= lblEnemyRects.Length || target == 0)
            return;
        if (lblEnemy != null)
            lblEnemy.anchoredPosition = lblEnemyRects[target - 1];

        if (avatarEnemysRoot != null)
            avatarEnemysRoot.anchoredPosition = avatarEnemyRects[target - 1];
    }

    public void SortEnemyAvatar()
    {
        if (Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        int wave = stageManager.GetWave();

        for (int i = 0; i < imgEnemyTurns.Length; i++)
        {
            if (imgEnemyTurns[i] == null)
                continue;
            if (i < wave)
                imgEnemyTurns[i].gameObject.SetActive(false);
        }
    }

    public void DisplayPlayerHp(float current, float full)
    {
        if (imgPlayerHp != null)
            imgPlayerHp.fillAmount = current / full;
        if (txtPlayerHp != null)
            txtPlayerHp.text = (int) (current * 100 / full) + "%";
    }

    public void DisplayEnemyHp(float current, float full)
    {
        if (imgEnemyHp != null)
            imgEnemyHp.fillAmount = current / full;
        if (txtEnemyHp != null)
            txtEnemyHp.text = (int) (current * 100 / full) + "%";
    }

    public void DisplayCriticalDamage(Vector3 position, int damage)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string path = "Prefab/Object/Vfx/vfx_critical_text";

        if (Resources.Load(path) == null)
            return;
        GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, gameManager.transform, path);
        obj.transform.SetParent(transform, true);
        obj.transform.position = position;
        if (!obj.GetComponent<Text>())
            return;
        Text txt = obj.GetComponent<Text>();
        txt.text = "CRIT " + damage;
    }

    public void DisplayDamage(CHARACTER character, Vector3 position, int damage)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string path = null;
        switch (character)
        {
            case CHARACTER.PLAYER:
                path = "Prefab/Object/Vfx/vfx_take_hit_text";
                break;

            case CHARACTER.ENEMY:
                path = "Prefab/Object/Vfx/vfx_get_hit_text";
                break;
        }

        if (Resources.Load(path) == null)
            return;
        GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, gameManager.transform, path);
        obj.transform.SetParent(transform, true);
        obj.transform.position = position;
        if (!obj.GetComponent<Text>())
            return;
        Text txt = obj.GetComponent<Text>();
        txt.text = damage.ToString();
    }
}