﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu : Dialog<UIMenu>
{
    private enum MENU_MAP
    {
        NONE,
        FIGHT,
        REMOVE_ADS,
        WEAPON,
        DAILY_GIFT,
        OUTFIT
    }

    private Coroutine tutorialCoroutine;

    [SerializeField] private Text txtStage;
    [SerializeField] private Transform modelRoot;
    [SerializeField] private Transform pageGrid;
    [SerializeField] private GameObject btnRemoveAds;
    [SerializeField] private GameObject btnDailyGift;
    [SerializeField] private GameObject vfxWeaponReward;
    [SerializeField] private GameObject vfxOutfitReward;
    [SerializeField] private GameObject vfxDailyGift;
    [SerializeField] private GameObject blocker;
    [SerializeField] private GameObject fightTutorialPanel;
    [SerializeField] private GameObject upgradeTutorialPanel;
    [SerializeField] private int pageElement;

    private static void Hide()
    {
        Close();
    }

    public static void Show()
    {
        Open();
    }

    private void Start()
    {
        SoundManager.Play(AUDIO_KIND.MUSIC, GameEnums.AUDIO_CLIP.sbg_home_menu, true);
        StartCoroutine(DisableInputBlocking(0.5f));
        EnableBlocker(true);

        if (btnRemoveAds != null)
        {
            if (Singleton<GameManager>.Instance == null)
                return;
            GameManager gameManager = Singleton<GameManager>.Instance;
            if (gameManager.IsRemoveAds())
                btnRemoveAds.gameObject.SetActive(false);
        }

        ShowFeature();
        InitModel();
        InitPage();

        EnableVfxWeaponReward();
        EnableVfxOutfitReward();
        EnableVfxDailyGift();

        Eventon<WeaponRewardData>.EventStatic += OnWeaponReward;
        Eventon<OutfitRewardData>.EventStatic += OnOutfitReward;
        Eventon<StoreData>.EventStatic += OnStoreData;
        Eventon<TutorialData>.EventStatic += OnTutorialData;

        if (Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        if (rewardManager.GetGiftedDay() >= rewardManager.GetGiftDay() && btnDailyGift != null)
            btnDailyGift.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        Eventon<WeaponRewardData>.EventStatic -= OnWeaponReward;
        Eventon<OutfitRewardData>.EventStatic -= OnOutfitReward;
        Eventon<StoreData>.EventStatic -= OnStoreData;
        Eventon<TutorialData>.EventStatic -= OnTutorialData;
    }

    private void OnTutorialData(TutorialData param)
    {
        if (param.isFight)
        {
            if (fightTutorialPanel != null)
                fightTutorialPanel.SetActive(!param.isFight);
        }
        else if (param.isUpgrade)
        {
            if (upgradeTutorialPanel != null)
                upgradeTutorialPanel.SetActive(!param.isUpgrade);
        }
    }

    private void OnStoreData(StoreData param)
    {
        if (param.isRemoveAds && btnRemoveAds != null)
            btnRemoveAds.gameObject.SetActive(false);
    }

    private void OnOutfitReward(OutfitRewardData param)
    {
        EnableVfxOutfitReward();
    }

    private void OnWeaponReward(WeaponRewardData param)
    {
        EnableVfxWeaponReward();
    }

    private IEnumerator EnableFightTutorialPanel(float time)
    {
        yield return new WaitForSeconds(time);
        if (fightTutorialPanel != null)
            fightTutorialPanel.SetActive(true);
        EnableBlocker(false);
    }

    private IEnumerator EnableUpgradeTutorialPanel(float time)
    {
        yield return new WaitForSeconds(time);
        if (upgradeTutorialPanel != null)
            upgradeTutorialPanel.SetActive(true);
        EnableBlocker(false);
    }

    private void CheckingTutorial()
    {
        EnableBlocker(false);
        if (Singleton<TutorialManager>.Instance == null)
            return;
        TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
        if (tutorialCoroutine != null)
            StopCoroutine(tutorialCoroutine);
        if (!tutorialManager.GetTutorialData().isFight)
            tutorialCoroutine = StartCoroutine(EnableFightTutorialPanel(0.5f));
        else
            tutorialCoroutine = StartCoroutine(EnableUpgradeTutorialPanel(0.5f));
    }

    private void ShowFeature()
    {
        if (fightTutorialPanel != null)
            fightTutorialPanel.SetActive(false);
        if (upgradeTutorialPanel != null)
            upgradeTutorialPanel.SetActive(false);
        if (Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        if (rewardManager.GetGiftedDay() < rewardManager.GetLoginDay() + 1)
        {
            if (Singleton<TutorialManager>.Instance == null)
                return;
            TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
            if (tutorialManager.GetTutorialData().isFight && tutorialManager.GetTutorialData().isUpgrade)
                StartCoroutine(DelayShowGiftDay(0.5f));
            else
                CheckingTutorial();
        }
        else
        {
            if (Singleton<StageManager>.Instance == null || Singleton<GameManager>.Instance == null)
                return;
            StageManager stageManager = Singleton<StageManager>.Instance;
            GameManager gameManager = Singleton<GameManager>.Instance;
            if (stageManager.GetStage() % GameConfig.stageToRating == 0 && !gameManager.IsRated())
                UIRate.Show();
            else
                EnableBlocker(false);
        }
    }

    private IEnumerator DelayShowGiftDay(float time)
    {
        yield return new WaitForSeconds(time);
        if (Singleton<GameManager>.Instance != null)
        {
            GameManager gameManager = Singleton<GameManager>.Instance;
            if (!gameManager.IsTest())
                UIDailyGift.Show();
        }

        EnableBlocker(false);
    }

    private void EnableVfxWeaponReward()
    {
        if (vfxWeaponReward == null || Singleton<RewardManager>.Instance == null ||
            Singleton<WeaponManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        vfxWeaponReward.SetActive(true);
        if (!rewardManager.IsWeaponReward() || weaponManager.IsFullUnlock())
            vfxWeaponReward.SetActive(false);
    }

    private void EnableVfxOutfitReward()
    {
        if (vfxOutfitReward == null || Singleton<RewardManager>.Instance == null ||
            Singleton<CharacterManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        vfxOutfitReward.SetActive(true);
        if (!rewardManager.IsOutfitReward() || characterManager.IsFullUnlock())
            vfxOutfitReward.SetActive(false);
    }

    private void InitModel()
    {
        if (modelRoot == null || modelRoot.childCount > 0 || Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        GameObject model = characterManager.SetCharacter(CHARACTER.PLAYER, modelRoot.position);
        model.transform.SetParent(modelRoot, true);
        model.transform.localScale = GameConfig.modelUIScale;

        if (!model.GetComponent<Body>())
            return;
        Body body = model.GetComponent<Body>();
        body.SetBodyKind(BODY_KIND.STATIC);
        body.SetJointBodyKind(BODY_KIND.STATIC);

        if (body.GetAnimator() != null)
        {
            UnityEngine.Animator animator = body.GetAnimator();
            AnimatorManager.Enable(animator, true);
            AnimatorManager.Play(animator, "anim_model_0");
        }

        if (!model.GetComponent<Player>())
        {
            model.AddComponent<Player>();
            Player player = model.GetComponent<Player>();
            player.SetWeapon();
        }
    }

    private void ReturnModel()
    {
        if (modelRoot == null || modelRoot.childCount <= 0)
            return;
        for (int i = 0; i < modelRoot.childCount; i++)
        {
            if (!modelRoot.GetChild(i).GetComponent<Body>())
                continue;
            Body body = modelRoot.GetChild(i).GetComponent<Body>();
            body.EnableClamp(false);
            if (body.GetAnimator() != null)
            {
                UnityEngine.Animator animator = body.GetAnimator();
                AnimatorManager.Enable(animator, true);
                AnimatorManager.Play(animator, "anim_tpose");
            }

            body.Return();
        }
    }

    private void InitPage()
    {
        if (Singleton<StageManager>.Instance == null || Singleton<GameManager>.Instance == null || pageGrid == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        GameManager gameManager = Singleton<GameManager>.Instance;
        int page = (int) (stageManager.GetStage() / pageElement);
        string path = "Prefab/UI/Common/pageDot";

        for (int i = 0; i < pageElement; i++)
        {
            GameObject dot = ObjectManager.Call<GameObject>(gameManager.transform, pageGrid, path);
            dot.transform.SetParent(pageGrid, true);
            dot.transform.localScale = new Vector3(1, 1, 1);
            if (!dot.GetComponent<PageDot>())
                continue;
            PageDot dotComponent = dot.GetComponent<PageDot>();
            dotComponent.OnDisplay(page * pageElement + i);
        }

        if (txtStage != null)
            txtStage.text = "Stage " + (stageManager.GetStage() + 1);
    }

    private IEnumerator DisableInputBlocking(float time)
    {
        yield return new WaitForSeconds(time);
        EnableBlocker(false);
    }

    private IEnumerator DelayClose(float time, MENU_MAP menuMap)
    {
        yield return new WaitForSeconds(time);
        switch (menuMap)
        {
            case MENU_MAP.FIGHT:
                Eventon<GAME_STATE>.Trigger(GAME_STATE.CLEAR);
                UILoading.Show(LOADING.PLAY);
                if (tutorialCoroutine != null)
                    StopCoroutine(tutorialCoroutine);
                ReturnModel();
                Hide();
                break;
            case MENU_MAP.REMOVE_ADS:
                break;
            case MENU_MAP.WEAPON:
                break;
            case MENU_MAP.DAILY_GIFT:
                break;
            case MENU_MAP.OUTFIT:
                break;
        }
    }

    public void RemoveAd()
    {
//        if (Singleton<PurchasingManager>.Instance == null)
//            return;
//        PurchasingManager purchasingManager = Singleton<PurchasingManager>.Instance;
//        purchasingManager.RemoveAds();
    }

    public void ShowWeaponUI()
    {
        UIWeapon.Show();
        EnableBlocker(true);
    }

    public void ShowDailyGiftUI()
    {
        UIDailyGift.Show();
        EnableBlocker(true);
    }

    public void ShowOutfitUI()
    {
        UIOutfit.Show();
        EnableBlocker(true);
    }

    public void OnFight()
    {
        if (fightTutorialPanel != null)
            fightTutorialPanel.SetActive(false);
        StartCoroutine(DelayClose(0.55f, MENU_MAP.FIGHT));
        EnableBlocker(true);

        if (Singleton<TutorialManager>.Instance != null)
        {
            TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
            if (tutorialManager.GetTutorialData().isFight)
                return;
            tutorialManager.GetTutorialData().isFight = true;
            tutorialManager.SaveTutorialData();
        }
    }

    public void ShowSetingUI()
    {
        UISetting.Show();
        EnableBlocker(true);
    }

    public void EnableBlocker(bool isStatus)
    {
        if (blocker != null)
            blocker.SetActive(isStatus);
    }

    public void EnableVfxDailyGift()
    {
        if (vfxDailyGift == null || Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        vfxDailyGift.SetActive(true);
        if (!rewardManager.IsGift())
            vfxDailyGift.SetActive(false);
    }

    public void RetakeModel(GameObject model)
    {
        if (model == null || modelRoot == null)
            return;
        model.transform.position = modelRoot.position;
        model.transform.SetParent(modelRoot, true);
        model.transform.position = model.transform.parent.position;
        model.transform.localScale = GameConfig.modelUIScale;
    }

    public void DisableUpgradeTutorialPanel()
    {
        if (upgradeTutorialPanel != null)
            upgradeTutorialPanel.SetActive(false);
    }

    public void LoadBanner()
    {
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
    }

    public GameObject GetModel()
    {
        GameObject obj = null;
        if (modelRoot != null)
            obj = modelRoot.GetChild(0).gameObject;
        return obj;
    }
}