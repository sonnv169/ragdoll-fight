﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UILose : Dialog<UILose>
{
    private Animator mAnimator;

    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnSkipLevel;
    [SerializeField] private Button btnRetry;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
        SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_ui_defeat, false);

        Singleton<GameManager>.Instance.OnLogLevel(new PassLevelStatus()
        {
            isStatus = false
        });
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_lose_show");

        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);

        if (btnSkipLevel != null)
            btnSkipLevel.onClick.AddListener(() =>
            {
                AdReward.Show();
                if (AdController.IsConnecting())
                    btnSkipLevel.interactable = false;
                Eventon<AD_STATE>.EventDynamic += state =>
                {
                    if (state == AD_STATE.FINISHED)
                    {
                        if (Singleton<StageManager>.Instance == null)
                            return;
                        StageManager stageManager = Singleton<StageManager>.Instance;
                        stageManager.UnlockStage();

                        Eventon<GAME_STATE>.Trigger(GAME_STATE.CLEAR);
                        UILoading.Show(LOADING.RETRY);
                        Hide();
                    }
                    else
                        btnSkipLevel.interactable = true;
                };
            });

        if (btnRetry != null)
            btnRetry.onClick.AddListener(() =>
            {
                Eventon<GAME_STATE>.Trigger(GAME_STATE.CLEAR);
                Eventon<ReTrackingEvent>.Trigger(new ReTrackingEvent()
                {
                    isRetracking = true
                });
                UILoading.Show(LOADING.RETRY);

                IronSource.Agent.hideBanner();
                IronSource.Agent.showInterstitial();

                Singleton<GameManager>.Instance.OnLogAd(AdsType.Inter, "retry_game", "ui_lose");

                Hide();
            });
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);

        //test
        Eventon<GAME_STATE>.Trigger(GAME_STATE.CLEAR);
        UILoading.Show(LOADING.RETRY);

        Hide();
    }

    public void OnClose()
    {
        UIManager.Instance.UpResultTime();
        Time.timeScale = GameConfig.originTime;
        AnimatorManager.Play(Animator, "anim_lose_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_lose_close");
        StartCoroutine(DelayClose(lenght + 0.15f));
    }
}