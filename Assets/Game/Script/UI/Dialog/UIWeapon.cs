﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWeapon : Dialog<UIWeapon>
{
    private List<WeaponAdapter> adapters = new List<WeaponAdapter>();
    private Animator mAnimator;

    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnShop;
    [SerializeField] private Transform modelRoot;
    [SerializeField] private Transform adapterGrid;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_weapon_show");

        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);

        if (btnShop != null)
            btnShop.onClick.AddListener(() =>
            {
                AnimatorManager.Play(Animator, "anim_weapon_close");
                float lenght = AnimatorManager.GetLenght(Animator, "anim_weapon_close");
                StartCoroutine(DelayClose(lenght));
                StartCoroutine(DelayOpenShop(lenght));
            });

        TakeModel();
        InitAdapter();
    }

    private void TakeModel()
    {
        if (modelRoot == null || UIMenu.Instance == null)
            return;
        UIMenu uiMenu = UIMenu.Instance;
        GameObject obj = uiMenu.GetModel();
        obj.transform.SetParent(modelRoot.transform, true);
        obj.transform.position = obj.transform.parent.position;
        obj.transform.localScale = GameConfig.modelUIScale;
    }

    private void Sort()
    {
        if (adapterGrid == null || adapters.Count <= 0 || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        for (int i = 0; i < adapters.Count; i++)
        {
            if (adapters[i] != null)
                adapters[i].transform.SetParent(gameManager.transform, true);
        }

        for (int i = 0; i < GameConfig.vipLevel; i++)
        {
            for (int j = 0; j < adapters.Count; j++)
            {
                if (adapters[j].GetVip() != i)
                    continue;
                adapters[j].transform.SetParent(adapterGrid, true);
                adapters[j].transform.localScale = new Vector3(1, 1, 1);
            }
        }

        if (adapterGrid.childCount > 0)
            adapterGrid.GetChild(0).gameObject.SetActive(false);
    }

    private void InitAdapter()
    {
        if (adapterGrid == null || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string path = "Prefab/UI/Common/weaponAdapter";
        int number = Enum.GetNames(typeof(GameEnums.WEAPON)).Length;

        if (Resources.Load(path) == null)
            return;
        for (int i = 0; i < number; i++)
        {
            GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, adapterGrid.transform, path);
            obj.transform.SetParent(adapterGrid.transform, true);
            obj.transform.localScale = new Vector3(1, 1, 1);
            if (!obj.GetComponent<WeaponAdapter>())
                continue;
            WeaponAdapter weaponAdapter = obj.GetComponent<WeaponAdapter>();
            weaponAdapter.Init();
            if (!adapters.Contains(weaponAdapter))
                adapters.Add(weaponAdapter);
        }

        if (adapterGrid.childCount > 0)
            adapterGrid.GetChild(0).gameObject.SetActive(false);

        Sort();
        OnAdapterActive();
    }

    private void OnAdapterActive()
    {
        if (Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        for (int i = 0; i < adapters.Count; i++)
        {
            if (adapters[i] == null)
                continue;
            GameEnums.WEAPON weapon = adapters[i].GetWeapon();
            adapters[i].OnActive(false);
            if (weaponManager.IsUnlocked(weapon))
                adapters[i].OnActive(true);
        }
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        if (modelRoot != null && modelRoot.childCount > 0 && UIMenu.Instance != null)
        {
            UIMenu uiMenu = UIMenu.Instance;
            GameObject obj = modelRoot.GetChild(0).gameObject;
            uiMenu.RetakeModel(obj);
        }

        Eventon<Dialog>.Trigger(this);

        Hide();
    }

    private IEnumerator DelayOpenShop(float time)
    {
        yield return new WaitForSeconds(time);
        UIShop.Show();
    }

    public void SetWeapon()
    {
        if (modelRoot == null || modelRoot.childCount <= 0 || !modelRoot.GetChild(0).GetComponent<Player>())
            return;
        Player player = modelRoot.GetChild(0).GetComponent<Player>();
        player.ClearWeapon();
        player.SetWeapon();
    }

    public void Reload()
    {
        OnAdapterActive();
        SetWeapon();
    }

    public void OnClose()
    {
        UIMenu.Instance.EnableBlocker(false);
        AnimatorManager.Play(Animator, "anim_weapon_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_weapon_close");
        StartCoroutine(DelayClose(lenght + 0.15f));
    }
}