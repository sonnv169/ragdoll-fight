﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class UnlockedSlot
{
    public GameEnums.ITEM_GROUP itemGroup;
    public string itemName;
    public int amount;
}

public class UIUnlocked : Dialog<UIUnlocked>
{
    private List<UnlockedSlot> unlockedSlots = new List<UnlockedSlot>();

    [SerializeField] private Button btnClose;
    [SerializeField] private Image imgPreview;
    [SerializeField] private Text txtAmount;
    [SerializeField] private GameObject vfxUnlocked;

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        if (btnClose != null)
            btnClose.onClick.AddListener(CheckingToClose);
        EnableUnlockedVfx(false);
    }

    private void DisplayPreview(Sprite spr)
    {
        if (imgPreview == null)
            return;
        imgPreview.sprite = spr;
        imgPreview.SetNativeSize();
        imgPreview.transform.localScale = new Vector3(
            imgPreview.transform.localScale.x, imgPreview.transform.localScale.y, imgPreview.transform.localScale.z);
    }

    private void DisplayAmount(int amount)
    {
        string content = amount.ToString();
        if (amount <= 0)
            content = null;
        else if (amount >= 1000)
            content = $"{amount:#,#.}";
        if (txtAmount != null)
            txtAmount.text = content;
    }

    private void EnableUnlockedVfx(bool isStatus)
    {
        if (vfxUnlocked != null)
            vfxUnlocked.gameObject.SetActive(isStatus);
    }

    private void DisplaySlot()
    {
        EnableUnlockedVfx(true);

        if (unlockedSlots.Count <= 0 || Singleton<WeaponManager>.Instance == null ||
            Singleton<CharacterManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        Sprite spr = null;
        int amount = 0;

        switch (unlockedSlots[0].itemGroup)
        {
            case GameEnums.ITEM_GROUP.General:
                spr = characterManager.GetItemIcon(unlockedSlots[0].itemName);
                amount = unlockedSlots[0].amount;
                break;
            case GameEnums.ITEM_GROUP.Weapon:
                GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                Enum.TryParse(unlockedSlots[0].itemName, out weapon);
                spr = weaponManager.GetSprite(weapon);
                break;
            case GameEnums.ITEM_GROUP.Outfit:
                GameEnums.HERO hero = GameEnums.HERO.h0;
                Enum.TryParse(unlockedSlots[0].itemName, out hero);
                spr = characterManager.GetPlayerAvatar(hero);
                break;
        }

        DisplayPreview(spr);
        DisplayAmount(amount);
    }

    public void InitSlot(GameEnums.ITEM_GROUP itemGroup, string itemName, int amount)
    {
        UnlockedSlot slot = new UnlockedSlot()
        {
            itemGroup = itemGroup,
            itemName = itemName,
            amount = amount
        };

        unlockedSlots.Add(slot);
        DisplaySlot();
    }

    public void CheckingToClose()
    {
        unlockedSlots.Remove(unlockedSlots[0]);
        if (unlockedSlots.Count > 0)
            DisplaySlot();
        else
        {
            if (UIDailyGift.Instance != null)
                UIDailyGift.Instance.OnClose();
            Hide();
        }
    }
}