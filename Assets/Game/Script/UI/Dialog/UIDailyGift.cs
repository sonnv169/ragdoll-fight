﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class GiftPageEvent
{
    public int page;
}

public class UIDailyGift : Dialog<UIDailyGift>
{
    private Animator mAnimator;
    private Coroutine tutorialCoroutine;
    private int page;

    [SerializeField] private Button btnBack;
    [SerializeField] private Button btnNextPage;
    [SerializeField] private Button btnBackPage;
    [SerializeField] private Button btnShop;
    [SerializeField] private BtnDailyGift[] btnDailyGift;
    [SerializeField] private GameObject tutorialPanel;


    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_daily_gift_show");

        CheckingTutorial();
        InitPage();
        if (btnBack != null)
            btnBack.onClick.AddListener(OnClose);

        if (btnNextPage != null)
            btnNextPage.onClick.AddListener(() =>
            {
                if (Singleton<RewardManager>.Instance == null)
                    return;
                RewardManager rewardManager = Singleton<RewardManager>.Instance;
                if (page >= (int) rewardManager.GetGiftDay() / GameConfig.giftOnWeek - 1)
                    return;
                page += 1;
                OnGiftRender();
            });

        if (btnBackPage != null)
            btnBackPage.onClick.AddListener(() =>
            {
                if (page <= 0)
                    return;
                page -= 1;
                OnGiftRender();
            });

        if (btnShop != null)
            btnShop.onClick.AddListener(() =>
            {
                AnimatorManager.Play(Animator, "anim_daily_gift_close");
                float lenght = AnimatorManager.GetLenght(Animator, "anim_daily_gift_close");
                StartCoroutine(DelayClose(lenght));
                StartCoroutine(DelayOpenShop(lenght));
            });

        Eventon<TutorialData>.EventStatic += OnTutorialData;
    }

    private void OnDisable()
    {
        Eventon<TutorialData>.EventStatic -= OnTutorialData;
    }

    private void OnTutorialData(TutorialData param)
    {
        if (tutorialPanel != null)
            tutorialPanel.SetActive(!param.isGift);
    }

    private IEnumerator EnableTutorialPanel(float time)
    {
        yield return new WaitForSeconds(time);
        if (Singleton<TutorialManager>.Instance != null)
        {
            TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
            tutorialManager.GetTutorialData().isGift = true;
            tutorialManager.SaveTutorialData();
            if (tutorialPanel != null)
                tutorialPanel.SetActive(true);
        }
    }

    private void CheckingTutorial()
    {
        if (tutorialPanel != null)
            tutorialPanel.SetActive(false);
        if (Singleton<TutorialManager>.Instance == null)
            return;
        TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
        float time = AnimatorManager.GetLenght(Animator, "anim_daily_gift_show");
        if (!tutorialManager.GetTutorialData().isGift)
            tutorialCoroutine = StartCoroutine(EnableTutorialPanel(time));
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Eventon<CurrencyEvent>.Trigger(new CurrencyEvent()
        {
            isUpdate = true
        });
        Hide();
    }

    private IEnumerator DelayOpenShop(float time)
    {
        yield return new WaitForSeconds(time);
        UIShop.Show();
    }

    private void InitPage()
    {
        if (Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        int rewardedDay = rewardManager.GetLoginDay();
        page = (int) (rewardedDay / GameConfig.giftOnWeek) - 1;
        if (page <= 0)
            page = 0;
        EnableButtonMovePage();
        OnGiftRender();
    }

    private void EnableButtonNextPage(bool isStatus)
    {
        if (btnNextPage != null)
            btnNextPage.gameObject.SetActive(isStatus);
    }

    private void EnableButtonBackPage(bool isStatus)
    {
        if (btnBackPage != null)
            btnBackPage.gameObject.SetActive(isStatus);
    }

    private void EnableButtonMovePage()
    {
        if (Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;

        if (page <= 0)
        {
            EnableButtonNextPage(true);
            EnableButtonBackPage(false);
        }
        else
        {
            if (page < (int) rewardManager.GetGiftDay() / GameConfig.giftOnWeek - 1)
            {
                EnableButtonNextPage(true);
                EnableButtonBackPage(true);
            }
            else
            {
                EnableButtonNextPage(false);
                EnableButtonBackPage(true);
            }
        }
    }

    private void OnGiftRender()
    {
        EnableButtonMovePage();

        Eventon<GiftPageEvent>.Trigger(new GiftPageEvent()
        {
            page = this.page
        });

        for (int i = 0; i < btnDailyGift.Length; i++)
        {
            if (btnDailyGift[i] == null)
                continue;
            btnDailyGift[i].Init((GameConfig.giftOnWeek * page) + i);
        }
    }

    public void OnClose()
    {
        UIMenu.Instance.EnableBlocker(false);
        if (tutorialPanel != null)
            tutorialPanel.SetActive(false);
        if (tutorialCoroutine != null)
            StopCoroutine(tutorialCoroutine);
        AnimatorManager.Play(Animator, "anim_daily_gift_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_daily_gift_close");
        StartCoroutine(DelayClose(lenght + 0.5f));
    }
}