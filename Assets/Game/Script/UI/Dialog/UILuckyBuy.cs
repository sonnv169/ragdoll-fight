﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class SaleEvent
{
    public bool isSaled;
}

public class UILuckyBuy : Dialog<UILuckyBuy>
{
    private Animator mAnimator;
    private int boughtTurn;
    [SerializeField] private int hidenTurn;

    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnSale;
    [SerializeField] private Text txtSale;
    [SerializeField] private int buyMaxTurn;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        Init();
    }

    private void Init()
    {
        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);

        if (btnSale != null)
            btnSale.onClick.AddListener(() =>
            {
                btnSale.interactable = false;
                AdReward.Show();
                Eventon<AD_STATE>.EventStatic += state =>
                {
                    if (state == AD_STATE.FINISHED)
                    {
                        EnableBtnSale(false);

                        Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "get_sale_off", "ui_lucky_buy");

                        Eventon<SaleEvent>.Trigger(new SaleEvent()
                        {
                            isSaled = true
                        });
                    }
                    else
                        btnSale.interactable = true;
                };

                if (!AdController.IsConnecting())
                    btnSale.interactable = true;
            });

        if (txtSale != null)
            txtSale.text = "Sale -" + GameConfig.salePercent + "%";
    }

    private void EnableBtnSale(bool isStatus)
    {
        if (btnSale != null)
            btnSale.gameObject.SetActive(isStatus);
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Hide();
    }

    public void UpBoughtTurn()
    {
        boughtTurn += 1;
        if (boughtTurn >= buyMaxTurn && btnSale != null)
            btnSale.gameObject.SetActive(false);
    }

    public void OnClose()
    {
        AnimatorManager.Play(Animator, "anim_skin_free_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_skin_free_close");
        StartCoroutine(DelayClose(lenght + 0.15f));
    }

    public void EnableSale(bool isEnable)
    {
        if (btnSale != null)
            btnSale.gameObject.SetActive(isEnable);
    }
}