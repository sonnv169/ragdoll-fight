﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIRate : Dialog<UIRate>
{
    private Animator mAnimator;

    [SerializeField] private Button btnClose;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Hide();
    }

    public void OnClose()
    {
        UIMenu.Instance.EnableBlocker(false);
        AnimatorManager.Play(Animator, "anim_setting_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_setting_close");
        StartCoroutine(DelayClose(lenght + 0.15f));
    }
}