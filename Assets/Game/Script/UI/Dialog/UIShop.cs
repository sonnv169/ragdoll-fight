﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIShop : Dialog<UIShop>
{
    private Animator mAnimator;
    private CURRENCY buyCurrency;
    private Transform buyTransform;
    private string buyContent;

    [SerializeField] private Button btnBack;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_shop_show");

        if (btnBack != null)
            btnBack.onClick.AddListener(OnClose);
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Eventon<CurrencyEvent>.Trigger(new CurrencyEvent()
        {
            isUpdate = true
        });
        Hide();
    }

    public void SetBuyTransform(CURRENCY currency, Transform param, int value)
    {
        buyCurrency = currency;
        buyTransform = param;
        buyContent = $"{value:#,#.}";
    }

    public void SetBuyVfx()
    {
        if (buyTransform == null || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string path = null;
        switch (buyCurrency)
        {
            case CURRENCY.COIN:
                path = "Prefab/Object/Vfx/vfx_currency_collected";
                break;
            case CURRENCY.HP_REGEN:
                path = "Prefab/Object/Vfx/vfx_hp_regen_collected";
                break;
            case CURRENCY.DOUBLE_POWER:
                path = "Prefab/Object/Vfx/vfx_double_power_collected";
                break;
            case CURRENCY.SHIELD:
                path = "Prefab/Object/Vfx/vfx_shield_collected";
                break;
        }

        GameObject obj =
            ObjectManager.Call<GameObject>(gameManager.transform, buyTransform, path, false);
        obj.transform.SetParent(transform, true);
        obj.transform.position = new Vector2(buyTransform.position.x - 100, buyTransform.position.y);
        obj.transform.localScale = new Vector3(1, 1, 1);
        if (!obj.GetComponent<Text>())
            return;
        Text txt = obj.GetComponent<Text>();
        txt.text = "+" + buyContent;
        switch (buyCurrency)
        {
            case CURRENCY.COIN:
                txt.color = Color.yellow;
                break;
            case CURRENCY.HP_REGEN:
            case CURRENCY.DOUBLE_POWER:
            case CURRENCY.SHIELD:
                txt.color = Color.white;
                break;
        }

        obj.SetActive(true);
    }

    public void OnClose()
    {
        if (UIMenu.Instance != null)
            UIMenu.Instance.EnableBlocker(false);
        AnimatorManager.Play(Animator, "anim_shop_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_shop_close");
        StartCoroutine(DelayClose(lenght + 0.5f));
    }
}