﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIStarting : MonoBehaviour
{
    [SerializeField] private string sceneName;

    private void Start()
    {
        StartCoroutine(OnChangeScene());
    }

    private IEnumerator OnChangeScene()
    {
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene(sceneName);
    }
}