﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class UIOutfit : Dialog<UIOutfit>
{
    private List<OutfitAdapter> adapters = new List<OutfitAdapter>();
    private Animator mAnimator;
    private Coroutine setWeaponCoroutine;

    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnShop;
    [SerializeField] private Transform modelRoot;
    [SerializeField] private Transform adapterGrid;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_weapon_show");

        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);

        if (btnShop != null)
            btnShop.onClick.AddListener(() =>
            {
                AnimatorManager.Play(Animator, "anim_weapon_close");
                float lenght = AnimatorManager.GetLenght(Animator, "anim_weapon_close");
                StartCoroutine(DelayClose(lenght));
                StartCoroutine(DelayOpenShop(lenght));
            });

        TakeModel();
        InitAdapter();
    }

    private void TakeModel()
    {
        if (modelRoot == null || UIMenu.Instance == null)
            return;
        UIMenu uiMenu = UIMenu.Instance;
        GameObject obj = uiMenu.GetModel();
        obj.transform.SetParent(modelRoot.transform, true);
        obj.transform.position = obj.transform.parent.position;
        obj.transform.localScale = GameConfig.modelUIScale;
    }

    private void Sort()
    {
        if (adapterGrid == null || adapters.Count <= 0 || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        for (int i = 0; i < adapters.Count; i++)
        {
            if (adapters[i] != null)
                adapters[i].transform.SetParent(gameManager.transform, true);
        }

        for (int i = 0; i < GameConfig.vipLevel; i++)
        {
            for (int j = 0; j < adapters.Count; j++)
            {
                if (adapters[j].GetVip() != i)
                    continue;
                adapters[j].transform.SetParent(adapterGrid, true);
                adapters[j].transform.localScale = new Vector3(1, 1, 1);
            }
        }

        if (adapterGrid.childCount > 0)
            adapterGrid.GetChild(0).gameObject.SetActive(false);
    }

    private void InitAdapter()
    {
        if (adapterGrid == null || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string path = "Prefab/UI/Common/outfitAdapter";
        int number = Enum.GetNames(typeof(GameEnums.HERO)).Length;

        if (Resources.Load(path) == null)
            return;
        for (int i = 0; i < number; i++)
        {
            GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, adapterGrid.transform, path);
            obj.transform.SetParent(adapterGrid.transform, true);
            obj.transform.localScale = new Vector3(1, 1, 1);
            if (!obj.GetComponent<OutfitAdapter>())
                continue;
            OutfitAdapter outfitAdapter = obj.GetComponent<OutfitAdapter>();
            outfitAdapter.Init();
            if (!adapters.Contains(outfitAdapter))
                adapters.Add(outfitAdapter);
        }

        Sort();
        OnAdapterActive();
    }

    private void OnAdapterActive()
    {
        if (Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        for (int i = 0; i < adapters.Count; i++)
        {
            if (adapters[i] == null)
                continue;
            GameEnums.HERO hero = adapters[i].GetHero();
            adapters[i].OnActive(false);
            if (characterManager.IsUnlocked(hero))
                adapters[i].OnActive(true);
        }
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        if (modelRoot != null && modelRoot.childCount > 0 && UIMenu.Instance != null)
        {
            UIMenu uiMenu = UIMenu.Instance;
            GameObject obj = modelRoot.GetChild(0).gameObject;
            uiMenu.RetakeModel(obj);

            if (setWeaponCoroutine != null)
                StopCoroutine(setWeaponCoroutine);
        }

        Eventon<Dialog>.Trigger(this);

        Hide();
    }

    private IEnumerator DelayOpenShop(float time)
    {
        yield return new WaitForSeconds(time);
        UIShop.Show();
    }

    private IEnumerator DelayInitWeapon()
    {
        yield return new WaitForSeconds(GameConfig.initTime);
        if (modelRoot != null && modelRoot.childCount > 0)
        {
            GameObject model = modelRoot.GetChild(0).gameObject;
            if (model.GetComponent<Player>())
            {
                Player player = model.GetComponent<Player>();
                player.SetWeapon();
            }
        }
    }

    public void SetHero()
    {
        if (modelRoot == null || modelRoot.childCount <= 0 || Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        for (int i = 0; i < modelRoot.childCount; i++)
        {
            if (modelRoot.GetChild(i).GetComponent<Player>())
            {
                Player player = modelRoot.GetChild(i).GetComponent<Player>();
                player.ReturnWeapon();
            }

            if (!modelRoot.GetChild(i).GetComponent<Body>())
                continue;
            Body modelBody = modelRoot.GetChild(i).GetComponent<Body>();
            modelBody.Return();
        }

        GameObject model = characterManager.SetCharacter(CHARACTER.PLAYER, modelRoot.position);
        model.transform.SetParent(modelRoot, true);
        model.transform.localScale = GameConfig.modelUIScale;

        if (!model.GetComponent<Player>())
            model.AddComponent<Player>();

        if (!model.GetComponent<Body>())
            return;
        Body body = model.GetComponent<Body>();
        body.SetBodyKind(BODY_KIND.STATIC);
        body.SetJointBodyKind(BODY_KIND.STATIC);

        if (body.GetAnimator() != null)
        {
            UnityEngine.Animator animator = body.GetAnimator();
            AnimatorManager.Enable(animator, true);
            AnimatorManager.Play(animator, "anim_model_0");
        }

        setWeaponCoroutine = StartCoroutine(DelayInitWeapon());
    }

    public void Reload()
    {
        OnAdapterActive();
        SetHero();
    }

    public void OnClose()
    {
        UIMenu.Instance.EnableBlocker(false);
        AnimatorManager.Play(Animator, "anim_weapon_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_weapon_close");
        StartCoroutine(DelayClose(lenght + 0.15f));
    }
}