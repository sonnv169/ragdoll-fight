﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class UISkinFree : Dialog<UISkinFree>
{
    private GameEnums.HERO resultHero;
    private Animator mAnimator;

    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnGetFree;
    [SerializeField] private Transform modelRoot;
    [SerializeField] private Transform skinVfx;
    [SerializeField] private float vfxSpeed;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    private static void Hide()
    {
        Close();
    }

    public static void Show()
    {
        Open();
    }

    private void Start()
    {
        InitModel();
        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);

        if (btnGetFree != null)
            btnGetFree.onClick.AddListener(() =>
            {
                btnGetFree.interactable = false;
                AdReward.Show();
                Eventon<AD_STATE>.EventDynamic += state =>
                {
                    if (state == AD_STATE.FINISHED)
                    {
                        if (Singleton<CharacterManager>.Instance == null)
                            return;
                        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
                        characterManager.Unlock(resultHero);
                        ReturnModel();
                        UIUnlocked.Show();
                        UIUnlocked.Instance.InitSlot(GameEnums.ITEM_GROUP.Outfit, resultHero.ToString(), 0);

                        Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "get_skin", "ui_free_skin");

                        Hide();
                    }
                    else
                        btnGetFree.interactable = true;
                };

                if (!AdController.IsConnecting())
                    btnGetFree.interactable = true;
            });
    }

    private void Update()
    {
        if (skinVfx != null)
            skinVfx.Rotate(Vector3.forward * vfxSpeed * Time.deltaTime);
    }

    private void InitModel()
    {
        if (modelRoot == null || modelRoot.childCount > 0 || Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        string[] heros = Enum.GetNames(typeof(GameEnums.HERO)).ToArray();
        List<GameEnums.HERO> notUnlockeds = new List<GameEnums.HERO>();
        for (int i = 1; i < heros.Length; i++)
        {
            GameEnums.HERO hero = GameEnums.HERO.h0;
            Enum.TryParse(heros[i], out hero);
            if (!characterManager.IsUnlocked(hero))
                notUnlockeds.Add(hero);
        }

        int result = Random.Range(0, notUnlockeds.Count);

        if (notUnlockeds.Count <= 0)
            return;
        resultHero = notUnlockeds[result];
        GameObject model = characterManager.SetOtherPlayer(resultHero, modelRoot.position);

        model.transform.SetParent(modelRoot, true);
        model.transform.localScale = GameConfig.modelUIScale;

        if (!model.GetComponent<Body>())
            return;
        Body body = model.GetComponent<Body>();
        body.SetBodyKind(BODY_KIND.STATIC);
        body.SetJointBodyKind(BODY_KIND.STATIC);

        if (body.GetAnimator() != null)
        {
            UnityEngine.Animator animator = body.GetAnimator();
            AnimatorManager.Enable(animator, true);
            AnimatorManager.Play(animator, "anim_model_3");
        }

        body.SetSortingOrderOnUI(true);
    }

    private void ReturnModel()
    {
        if (modelRoot == null || modelRoot.childCount <= 0)
            return;
        for (int i = 0; i < modelRoot.childCount; i++)
        {
            if (!modelRoot.GetChild(i).GetComponent<Body>())
                continue;
            Body body = modelRoot.GetChild(i).GetComponent<Body>();
            if (body.GetAnimator() != null)
            {
                UnityEngine.Animator animator = body.GetAnimator();
                AnimatorManager.Enable(animator, true);
                AnimatorManager.Play(animator, "anim_tpose");
            }

            body.SetSortingOrderOnUI(false);
            body.Return();
        }
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Hide();
    }

    public void OnClose()
    {
        AnimatorManager.Play(Animator, "anim_skin_free_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_skin_free_close");
        StartCoroutine(DelayClose(lenght + 0.15f));
    }
}