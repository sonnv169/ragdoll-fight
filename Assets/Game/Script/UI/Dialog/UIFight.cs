﻿using System.Collections;
using System.ComponentModel;
using UnityEngine;

public class UIFight : Dialog<UIFight>
{
    private Animator mAnimator;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_fight_in");
        StartCoroutine(DelayPlayAnimation("anim_fight_out", 0.5f));
        StartCoroutine(DelayClose(AnimatorManager.GetLenght(Animator, "anim_fight_out") + 0.5f));
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Hide();
    }

    private IEnumerator DelayPlayAnimation(string anim, float time)
    {
        yield return new WaitForSeconds(time);
        AnimatorManager.Play(Animator, anim);
    }
}