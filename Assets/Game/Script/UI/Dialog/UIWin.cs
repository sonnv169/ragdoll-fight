﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIWin : Dialog<UIWin>
{
    private Animator mAnimator;

    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnGetMore;
    [SerializeField] private Text txtWonReward;
    [SerializeField] private Text txtGetMoreReward;
    [SerializeField] private GameObject vfxShow;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
        SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_ui_victory, false);

        Singleton<GameManager>.Instance.OnLogLevel(new PassLevelStatus()
        {
            isStatus = true
        });
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_win_show");
        ShowSaleFeature();

        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);

        if (btnGetMore != null)
            btnGetMore.onClick.AddListener(() =>
            {
                AdReward.Show();
                if (AdController.IsConnecting())
                    btnGetMore.interactable = false;
                Eventon<AD_STATE>.EventDynamic += state =>
                {
                    if (state == AD_STATE.FINISHED)
                    {
                        if (Singleton<CurrencyManager>.Instance == null || Singleton<GameManager>.Instance == null)
                            return;
                        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
                        GameManager gameManager = Singleton<GameManager>.Instance;
                        currencyManager.Collect(CURRENCY.COIN, GameConfig.getMoreReward);

                        string path = "Prefab/Object/Vfx/vfx_currency_collected";
                        GameObject obj =
                            ObjectManager.Call<GameObject>(gameManager.transform, btnGetMore.transform, path);
                        obj.transform.SetParent(transform, true);
                        obj.transform.localScale = new Vector3(1, 1, 1);
                        if (!obj.GetComponent<Text>())
                            return;
                        Text txt = obj.GetComponent<Text>();
                        txt.text = "+" + GameConfig.getMoreReward;
                        txt.color = Color.yellow;

                        for (int i = 0; i < btnGetMore.transform.childCount; i++)
                        {
                            Transform getMoreChild = btnGetMore.transform.GetChild(i);
                            if (getMoreChild.GetComponent<Text>())
                            {
                                Text txtChild = getMoreChild.GetComponent<Text>();
                                txtChild.color = Color.gray;
                            }

                            if (getMoreChild.GetComponent<Image>())
                            {
                                Image imgChild = getMoreChild.GetComponent<Image>();
                                imgChild.color = Color.gray;
                            }

                            for (int j = 0; j < getMoreChild.childCount; j++)
                            {
                                Transform childTransform = getMoreChild.transform.GetChild(i);
                                if (childTransform.GetComponent<Text>())
                                {
                                    Text txtChild = childTransform.GetComponent<Text>();
                                    txtChild.color = Color.gray;
                                }

                                if (childTransform.GetComponent<Image>())
                                {
                                    Image imgChild = childTransform.GetComponent<Image>();
                                    imgChild.color = Color.gray;
                                }
                            }
                        }

                        Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "get_1500_coin", "ui_win");
                    }
                    else
                        btnGetMore.interactable = true;
                };
            });

        if (Singleton<StageManager>.Instance != null && txtWonReward != null &&
            Singleton<CurrencyManager>.Instance != null)
        {
            StageManager stageManager = Singleton<StageManager>.Instance;
            CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
            int stage = stageManager.GetStage();
            int coin = stageManager.GetWonReward(stage);
            string content = coin.ToString();
            if (coin >= 1000)
                content = $"{coin:#,#.}";
            txtWonReward.text = "+" + content;
            currencyManager.Collect(CURRENCY.COIN, coin);
        }

        if (txtGetMoreReward != null)
            txtGetMoreReward.text = GameConfig.getMoreReward.ToString();
    }

    private void ShowSaleFeature()
    {
        if (Singleton<SaleFeatureManager>.Instance == null)
            return;
        SaleFeatureManager saleFeatureManager = Singleton<SaleFeatureManager>.Instance;
        saleFeatureManager.ShowSaleFeature();
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Eventon<GAME_STATE>.Trigger(GAME_STATE.CLEAR);
        UILoading.Show(LOADING.MENU);
        UIGameplay.Hide();
        Hide();
    }

    public void OnClose()
    {
        UIManager.Instance.UpResultTime();
        Time.timeScale = GameConfig.originTime;
        AnimatorManager.Play(Animator, "anim_win_close");
        if (vfxShow != null)
            vfxShow.SetActive(true);
        float lenght = AnimatorManager.GetLenght(Animator, "anim_win_close");
        StartCoroutine(DelayClose(lenght + 0.15f));
    }
}