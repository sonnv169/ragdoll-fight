﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class UILuckyGift : Dialog<UILuckyGift>
{
    private List<LUCKY_GIFT_GROUP> luckyGiftGroups = new List<LUCKY_GIFT_GROUP>();
    private GameEnums.ITEM_GROUP itemGroup;
    private GameEnums.WEAPON weaponGift;
    private GameEnums.HERO heroGift;
    private Animator mAnimator;
    [SerializeField] private int opened;

    [SerializeField] private Button btnClose;
    [SerializeField] private Image imgPreview;
    [SerializeField] private LuckyGiftAdapter[] luckyGiftAdapters;

    [SerializeField] private float weaponAngle;
    [SerializeField] private float outfitAngle;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    public static void Show()
    {
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    private void OnEnable()
    {
        Init();
        if (btnClose != null)
            btnClose.onClick.AddListener(OnClose);
    }

    private void Init()
    {
        opened = 0;
        luckyGiftGroups.Clear();
        weaponGift = GameEnums.WEAPON.NONE;
        heroGift = GameEnums.HERO.h0;

        InitResult();
    }

    private void InitResult()
    {
        if (Singleton<CharacterManager>.Instance == null || Singleton<WeaponManager>.Instance == null ||
            Singleton<SaleFeatureManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        SaleFeatureManager saleFeatureManager = Singleton<SaleFeatureManager>.Instance;
        luckyGiftGroups = saleFeatureManager.GetLuckyGiftGroups();
        GameEnums.ITEM_GROUP item = GameEnums.ITEM_GROUP.General;
        Sprite spr = null;
        int result = 0;
        if (characterManager.IsFullUnlock())
            item = GameEnums.ITEM_GROUP.Weapon;
        else
        {
            if (weaponManager.IsFullUnlock())
                item = GameEnums.ITEM_GROUP.Outfit;
            else
            {
                float randomResult = Random.Range(0, 3.0f);
                if (randomResult > 0)
                    item = GameEnums.ITEM_GROUP.Weapon;
                else
                    item = GameEnums.ITEM_GROUP.Outfit;
            }
        }

        switch (item)
        {
            case GameEnums.ITEM_GROUP.Weapon:
                List<GameEnums.WEAPON> weaponResults = new List<GameEnums.WEAPON>();
                string[] weapons = Enum.GetNames(typeof(GameEnums.WEAPON)).ToArray();
                for (int i = 0; i < weapons.Length; i++)
                {
                    GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                    Enum.TryParse(weapons[i], out weapon);
                    if (weaponManager.IsUnlocked(weapon))
                        continue;
                    weaponResults.Add(weapon);
                }

                if (weaponResults.Count <= 0)
                    return;
                itemGroup = GameEnums.ITEM_GROUP.Weapon;
                result = Random.Range(0, weaponResults.Count);
                weaponGift = weaponResults[result];
                spr = weaponManager.GetSprite(weaponGift);

                break;
            case GameEnums.ITEM_GROUP.Outfit:
                List<GameEnums.HERO> outfitResults = new List<GameEnums.HERO>();
                string[] outfits = Enum.GetNames(typeof(GameEnums.HERO)).ToArray();
                for (int i = 0; i < outfits.Length; i++)
                {
                    GameEnums.HERO hero = GameEnums.HERO.h0;
                    Enum.TryParse(outfits[i], out hero);
                    if (characterManager.IsUnlocked(hero))
                        continue;
                    outfitResults.Add(hero);
                }

                if (outfitResults.Count <= 0)
                    return;
                itemGroup = GameEnums.ITEM_GROUP.Outfit;
                result = Random.Range(0, outfitResults.Count);
                heroGift = outfitResults[result];
                spr = characterManager.GetPlayerAvatar(heroGift);

                break;
        }

        for (int i = 0; i < luckyGiftGroups.Count; i++)
        {
            if (luckyGiftAdapters[i] != null)
                luckyGiftAdapters[i].InitLuckyGiftGroup(luckyGiftGroups[i]);
        }

        if (imgPreview == null)
            return;
        imgPreview.sprite = spr;
        imgPreview.SetNativeSize();
        imgPreview.transform.localScale = new Vector3(
            imgPreview.transform.localScale.x, imgPreview.transform.localScale.y, imgPreview.transform.localScale.z);

        switch (itemGroup)
        {
            case GameEnums.ITEM_GROUP.Weapon:
                imgPreview.transform.rotation = Quaternion.Euler(0, 0, weaponAngle);
                break;

            case GameEnums.ITEM_GROUP.Outfit:
                imgPreview.transform.rotation = Quaternion.Euler(0, 0, outfitAngle);
                break;
        }
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Hide();
    }

    public void OnClose()
    {
        AnimatorManager.Play(Animator, "anim_skin_free_close");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_skin_free_close");
        StartCoroutine(DelayClose(lenght + 0.15f));
    }

    public void OpenAdapter()
    {
        opened += 1;
        if (opened >= luckyGiftAdapters.Length)
            opened = luckyGiftAdapters.Length;
    }

    public GameEnums.ITEM_GROUP GetItemGroup()
    {
        return itemGroup;
    }

    public GameEnums.WEAPON GetWeaponGift()
    {
        return weaponGift;
    }

    public GameEnums.HERO GetOutfitGift()
    {
        return heroGift;
    }

    public int GetOpenedAdapter()
    {
        return opened;
    }
}