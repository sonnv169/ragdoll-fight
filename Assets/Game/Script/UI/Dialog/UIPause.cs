﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIPause : Dialog<UIPause>
{
    private Animator mAnimator;

    [SerializeField] private Button btnHome;
    [SerializeField] private Button btnRetry;
    [SerializeField] private Button btnPlay;
    [SerializeField] private Slider sldMusic;
    [SerializeField] private Slider sldSound;
    [SerializeField] private Text txtMusicPercent;
    [SerializeField] private Text txtSoundPercent;

    #region property

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    private static void Hide()
    {
        Close();
    }

    public static void Show()
    {
        Open();
    }

    private void OnEnable()
    {
        AnimatorManager.Play(Animator, "anim_pausing");
        Time.timeScale = 0;

        if (btnHome != null)
            btnHome.onClick.AddListener(() =>
            {
                Eventon<GAME_STATE>.Trigger(GAME_STATE.CLEAR);

                UILoading.Show(LOADING.MENU);
                UIGameplay.Hide();

                IronSource.Agent.displayBanner();

                Hide();
            });

        if (btnRetry != null)
            btnRetry.onClick.AddListener(() =>
            {
                Eventon<GAME_STATE>.Trigger(GAME_STATE.CLEAR);
                UILoading.Show(LOADING.RETRY);

                IronSource.Agent.hideBanner();
                IronSource.Agent.showInterstitial();

                Singleton<GameManager>.Instance.OnLogAd(AdsType.Inter, "retry_game", "ui_pause");

                Hide();
            });

        if (btnPlay != null)
            btnPlay.onClick.AddListener(() =>
            {
                IronSource.Agent.displayBanner();
                OnClose();
            });

        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        if (sldMusic != null)
        {
            sldMusic.value = audioManager.GetMusic().volume;
            DisplayMusicPercent(audioManager.GetMusic().volume);
            sldMusic.onValueChanged.AddListener(delegate { SetMusicVolume(sldMusic.value); });
        }

        if (sldSound != null)
        {
            sldSound.value = audioManager.GetSound().volume;
            DisplaySoundPercent(audioManager.GetSound().volume);
            sldSound.onValueChanged.AddListener(delegate { SetSoundVolume(sldSound.value); });
        }
    }

    private void DisplayMusicPercent(float value)
    {
        if (txtMusicPercent == null)
            return;
        txtMusicPercent.text = (int) (value * 100 / 1) + "%";
    }

    private void DisplaySoundPercent(float value)
    {
        if (txtSoundPercent == null)
            return;
        txtSoundPercent.text = (int) (value * 100 / 1) + "%";
    }

    private void SetMusicVolume(float value)
    {
        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        audioManager.SetMusicVolume(value);
        DisplayMusicPercent(audioManager.GetMusic().volume);
    }

    private void SetSoundVolume(float value)
    {
        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        audioManager.SetSoundVolume(value);
        DisplaySoundPercent(audioManager.GetSound().volume);
    }

    private IEnumerator DelayClose(float time)
    {
        yield return new WaitForSeconds(time);
        Hide();
    }

    public void OnClose()
    {
        Time.timeScale = GameConfig.originTime;
        AnimatorManager.Play(Animator, "anim_resume");
        float lenght = AnimatorManager.GetLenght(Animator, "anim_resume");
        StartCoroutine(DelayClose(lenght + 0.5f));

        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        audioManager.Save();
    }
}