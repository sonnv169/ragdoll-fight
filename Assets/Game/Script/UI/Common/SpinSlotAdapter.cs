﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SpinSlotAdapter : ToggleBehaviour
{
    private GameEnums.ITEM_GROUP itemGroup;
    private string itemName;
    private int amount;
    private int index;

    [SerializeField] private LUCKY_SPIN_GROUP luckySpinGroup;
    [SerializeField] private Image imgPreview;
    [SerializeField] private Text txtAmount;

    protected override void OnAppear()
    {
        Init();
        Eventon<SpinResultEvent>.EventStatic += OnSpinResultEvent;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<SpinResultEvent>.EventStatic -= OnSpinResultEvent;
    }

    private void OnSpinResultEvent(SpinResultEvent param)
    {
        if (param.result != index || Singleton<WeaponManager>.Instance == null ||
            Singleton<CharacterManager>.Instance == null || Singleton<CurrencyManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;

        switch (itemGroup)
        {
            case GameEnums.ITEM_GROUP.General:
                GameEnums.GENERAL general = GameEnums.GENERAL.Gold;
                Enum.TryParse(itemName, out general);
                switch (general)
                {
                    case GameEnums.GENERAL.Gold:
                        currencyManager.Collect(CURRENCY.COIN, amount);
                        break;
                    case GameEnums.GENERAL.Hp:
                        currencyManager.Collect(CURRENCY.HP_REGEN, amount);
                        break;
                    case GameEnums.GENERAL.PowerX2:
                        currencyManager.Collect(CURRENCY.DOUBLE_POWER, amount);
                        break;
                    case GameEnums.GENERAL.Shield:
                        currencyManager.Collect(CURRENCY.SHIELD, amount);
                        break;
                }

                break;
            case GameEnums.ITEM_GROUP.Weapon:
                GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                Enum.TryParse(itemName, out weapon);
                weaponManager.Unlock(weapon);
                break;
            case GameEnums.ITEM_GROUP.Outfit:
                GameEnums.HERO hero = GameEnums.HERO.h0;
                Enum.TryParse(itemName, out hero);
                characterManager.Unlock(hero);
                break;
        }

        UIUnlocked.Show();
        UIUnlocked.Instance.InitSlot(itemGroup, itemName, amount);
    }

    private void Init()
    {
        SetIndex();
        InitContent();
    }

    private void SetIndex()
    {
        string[] groups = Enum.GetNames(typeof(LUCKY_SPIN_GROUP)).ToArray();
        for (int i = 0; i < groups.Length; i++)
        {
            LUCKY_SPIN_GROUP group = LUCKY_SPIN_GROUP.GROUP_0;
            Enum.TryParse(groups[i], out group);
            if (luckySpinGroup == group)
                index = i;
        }
    }

    private void DisplayPreview(Sprite spr)
    {
        if (imgPreview == null)
            return;
        imgPreview.sprite = spr;
        imgPreview.SetNativeSize();
        switch (itemGroup)
        {
            case GameEnums.ITEM_GROUP.General:
                imgPreview.transform.localScale = new Vector3(1, 1, 1);
                break;

            case GameEnums.ITEM_GROUP.Weapon:
            case GameEnums.ITEM_GROUP.Outfit:
                imgPreview.transform.localScale = new Vector3(
                    imgPreview.transform.localScale.x, imgPreview.transform.localScale.y,
                    imgPreview.transform.localScale.z);
                imgPreview.transform.localScale = new Vector3(0.85f, 0.85f, 0.85f);
                break;
        }
    }

    private void DisplayAmount(int number)
    {
        if (txtAmount == null)
            return;
        string content = number.ToString();
        switch (itemGroup)
        {
            case GameEnums.ITEM_GROUP.General:
                if (number >= 1000)
                    content = $"{number:#,#.}";
                break;

            case GameEnums.ITEM_GROUP.Weapon:
            case GameEnums.ITEM_GROUP.Outfit:
                content = null;
                break;
        }

        txtAmount.text = content;
    }

    private void InitContent()
    {
        if (Singleton<SaleFeatureManager>.Instance == null || Singleton<WeaponManager>.Instance == null ||
            Singleton<CharacterManager>.Instance == null)
            return;
        SaleFeatureManager saleFeatureManager = Singleton<SaleFeatureManager>.Instance;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        saleFeatureManager.OnLuckySpinCheckingRound();

        itemGroup = saleFeatureManager.GetLuckySpinItemGroup(luckySpinGroup);
        itemName = saleFeatureManager.GetSpinSlotItem(luckySpinGroup);
        amount = saleFeatureManager.GetLuckySpinAmount(luckySpinGroup);
        Sprite spr = null;

        switch (itemGroup)
        {
            case GameEnums.ITEM_GROUP.General:
                GameEnums.GENERAL general = GameEnums.GENERAL.Gold;
                Enum.TryParse(itemName, out general);
                spr = characterManager.GetItemIcon(itemName);
                break;

            case GameEnums.ITEM_GROUP.Weapon:
                GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                Enum.TryParse(itemName, out weapon);
                spr = weaponManager.GetSprite(weapon);
                break;

            case GameEnums.ITEM_GROUP.Outfit:
                GameEnums.HERO hero = GameEnums.HERO.h0;
                Enum.TryParse(itemName, out hero);
                spr = characterManager.GetPlayerAvatar(hero);
                break;
        }

        DisplayPreview(spr);
        DisplayAmount(amount);
    }
}