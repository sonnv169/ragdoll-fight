﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class IcoBooster : ToggleBehaviour
{
    private float cooldown;

    [SerializeField] private GameEnums.BOOSTER booster;
    [SerializeField] private Image ico;

    protected override void OnAppear()
    {
        //nothing
    }

    protected override void OnCyclic()
    {
        OnCooldown();
    }

    protected override void OnDisappear()
    {
        //nothing
    }

    private void Display()
    {
        if (ico == null)
            return;
        float max = 0;
        switch (booster)
        {
            case GameEnums.BOOSTER.HP_REGEN:
                max = GameConfig.hpDuration;
                break;
            case GameEnums.BOOSTER.SHIELD:
                max = GameConfig.shieldDuration;
                break;
            case GameEnums.BOOSTER.DOUBLE_POWER:
                max = GameConfig.doublePowerDuration;
                break;
            case GameEnums.BOOSTER.WEAPON:
                max = GameConfig.weaponDuration;
                break;
        }

        ico.fillAmount = cooldown / max;
    }

    private void OnCooldown()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        switch (gameManager.GetState())
        {
            case GAME_STATE.PLAY:
                Display();
                if (cooldown > 0)
                    cooldown -= Time.deltaTime;
                else
                {
                    Eventon<BoosterEvent>.Trigger(new BoosterEvent()
                    {
                        booster = booster,
                        isCasting = false
                    });

                    cooldown = 0;
                    gameObject.SetActive(false);
                }

                break;
        }
    }

    public void SetCooldown()
    {
        cooldown = 0;
        switch (booster)
        {
            case GameEnums.BOOSTER.HP_REGEN:
                cooldown = GameConfig.hpDuration;
                break;
            case GameEnums.BOOSTER.SHIELD:
                cooldown = GameConfig.shieldDuration;
                break;
            case GameEnums.BOOSTER.DOUBLE_POWER:
                cooldown = GameConfig.doublePowerDuration;
                break;
            case GameEnums.BOOSTER.WEAPON:
                cooldown = GameConfig.weaponDuration;
                break;
        }
    }

    public GameEnums.BOOSTER GetBooster()
    {
        return booster;
    }
}