﻿using UnityEngine;
using UnityEngine.UI;

public class PurchaseAdapter : MonoBehaviour
{
    private Button mButton;
    private int index;
    private int value;

    [SerializeField] private Text txtValue;
    [SerializeField] private Text txtBonus;
    [SerializeField] private Text txtFreeTime;
    [SerializeField] private bool isFree;

    #region property

    private Button Button
    {
        get
        {
            if (mButton == null && GetComponent<Button>())
                mButton = GetComponent<Button>();
            return mButton;
        }
    }

    #endregion

    private void OnEnable()
    {
        if (transform.parent != null)
        {
            for (int i = 0; i < transform.parent.childCount; i++)
            {
                if (!transform.parent.GetChild(i).Equals(transform))
                    continue;
                index = i;
            }
        }

        Display();
        if (Button != null)
            Button.onClick.AddListener(OnClick);
    }

    private void Display()
    {
        DisplayFreeTime();
        if (Singleton<PurchasingManager>.Instance == null)
            return;
        PurchasingManager purchasingManager = Singleton<PurchasingManager>.Instance;
        if (isFree)
            value = GameConfig.getFreeReward;
        else
            value = purchasingManager.products[index - 1].value;

        string content = null;
        if (value < 1000)
            content = value.ToString();
        else
            content = $"{value:#,#.}";

        if (txtValue != null)
            txtValue.text = content;
        if (txtBonus != null)
            txtBonus.text = "Bonus \n" + purchasingManager.products[index - 1].bonusPercent + "%";
    }

    private void DisplayFreeTime()
    {
        if (txtFreeTime == null || Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        txtFreeTime.text = gameManager.GetFreeTimeGeted().ToString();
    }

    private void OnClick()
    {
        if (!AdController.IsConnecting() || Singleton<CurrencyManager>.Instance == null ||
            Singleton<GameManager>.Instance == null || Singleton<PurchasingManager>.Instance == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        GameManager gameManager = Singleton<GameManager>.Instance;
        PurchasingManager purchasingManager = Singleton<PurchasingManager>.Instance;

        UIShop.Instance.SetBuyTransform(CURRENCY.COIN, transform, value);
        if (isFree)
        {
            if (gameManager.GetFreeTime() <= 0)
                return;
            AdReward.Show();
            if (AdController.IsConnecting() && Button != null)
                Button.interactable = false;
            Eventon<AD_STATE>.EventDynamic += state =>
            {
                if (state == AD_STATE.FINISHED)
                {
                    currencyManager.Collect(CURRENCY.COIN, value);
                    gameManager.MinusGetFreeTime(1);
                    DisplayFreeTime();

                    UIShop.Instance.SetBuyVfx();

                    if (gameManager.GetFreeTime() > 0)
                    {
                        if (Button != null)
                            Button.interactable = true;
                    }
                }
                else
                {
                    if (Button != null)
                        Button.interactable = true;
                }
            };
        }
        else
        {
            Eventon<IAP_STATE>.EventDynamic += state =>
            {
                if (state != IAP_STATE.DONE)
                    return;
                int totalReward = purchasingManager.products[index - 1].value +
                                  (int) (purchasingManager.products[index - 1].value *
                                         purchasingManager.products[index - 1].bonusPercent) / 100;
                currencyManager.Collect(CURRENCY.COIN, totalReward);

                if (purchasingManager.products[index - 1].isRemoveAds)
                    gameManager.EnableAds(false);
                UIShop.Instance.SetBuyVfx();
            };
            purchasingManager.Buy(index - 1);
        }
    }
}