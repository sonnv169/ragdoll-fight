﻿using System.Collections;
using UnityEngine;

public class MainCamera : ToggleBehaviour
{
    private Coroutine zoomCoroutine;
    private Coroutine scaleTimeCoroutine;
    private Coroutine resetTargetCoroutine;
    private Coroutine resetKilledTargetCoroutine;

    private Camera mCamera;
    private Animator mAnimator;
    private Transform followTarget;
    private Transform savedTarget;
    private bool isZoom;

    [SerializeField] private Vector2 clamp;

    #region property

    private Camera Camera
    {
        get
        {
            if (mCamera == null && GetComponent<Camera>())
                mCamera = GetComponent<Camera>();
            return mCamera;
        }
    }

    private Animator Animator
    {
        get
        {
            if (mAnimator == null && GetComponent<Animator>())
                mAnimator = GetComponent<Animator>();
            return mAnimator;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
        Eventon<WeaponHitEvent>.EventStatic += OnWeaponHitEvent;
        Eventon<UnitDeathEvent>.EventStatic += OnUnitDeathEvent;
    }

    protected override void OnCyclic()
    {
        Follow();
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
        Eventon<WeaponHitEvent>.EventStatic -= OnWeaponHitEvent;
        Eventon<UnitDeathEvent>.EventStatic -= OnUnitDeathEvent;
    }

    private void OnUnitDeathEvent(UnitDeathEvent param)
    {
        OnZoomIn();

        followTarget = param.unit.transform;
        resetKilledTargetCoroutine = StartCoroutine(OnResetKillTarget(1.5f));

        if (Singleton<Player>.Instance == null)
            return;
        Player player = Singleton<Player>.Instance;
        savedTarget = player.transform;
    }

    private void OnWeaponHitEvent(WeaponHitEvent param)
    {
        if (Animator == null)
            return;
        OnZoomIn();
        Time.timeScale = GameConfig.slowTime;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        if (scaleTimeCoroutine != null)
            StopCoroutine(scaleTimeCoroutine);
        scaleTimeCoroutine = StartCoroutine(ScaleTime(GameConfig.slowDuration, GameConfig.originTime));

        if (zoomCoroutine != null)
            StopCoroutine(zoomCoroutine);
        zoomCoroutine = StartCoroutine(EnableZoom(GameConfig.zoomTime, false));
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                Init();
                break;
        }
    }

    private void Init()
    {
        followTarget = null;
        savedTarget = null;
        isZoom = false;
        transform.position = new Vector3(0, 0, -10);
        if (zoomCoroutine != null)
            StopCoroutine(zoomCoroutine);
        if (scaleTimeCoroutine != null)
            StopCoroutine(scaleTimeCoroutine);
        if (resetTargetCoroutine != null)
            StopCoroutine(resetTargetCoroutine);
        if (resetKilledTargetCoroutine != null)
            StopCoroutine(resetKilledTargetCoroutine);
    }

    private IEnumerator EnableZoom(float time, bool isStatus)
    {
        yield return new WaitForSeconds(time);
        isZoom = isStatus;
        switch (isZoom)
        {
            case true:
                AnimatorManager.Play(Animator, "anim_zoom_in");
                break;

            case false:
                AnimatorManager.Play(Animator, "anim_zoom_out");
                break;
        }
    }

    private IEnumerator ScaleTime(float time, float scale)
    {
        yield return new WaitForSeconds(time);
        Time.timeScale = scale;
        if (scaleTimeCoroutine != null)
            StopCoroutine(scaleTimeCoroutine);
    }

    private void Follow()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;

        if (Camera == null || followTarget == null)
            transform.position = new Vector3(0, 0, -10);
        else
        {
            switch (gameManager.GetState())
            {
                case GAME_STATE.PLAY:
                case GAME_STATE.WIN:
                    transform.position = new Vector3(followTarget.position.x, followTarget.position.y, -10);
                    transform.position = new Vector3(
                        Mathf.Clamp(transform.position.x, -clamp.x, clamp.x),
                        Mathf.Clamp(transform.position.y, -clamp.y, clamp.y), -10);
                    break;
            }
        }
    }

    private void OnZoomOut()
    {
        isZoom = false;
        AnimatorManager.Play(Animator, "anim_zoom_out");
    }

    private IEnumerator OnResetTarget()
    {
        yield return new WaitForSeconds(0.5f);
        OnZoomOut();
        if (savedTarget != null)
        {
            followTarget = savedTarget;
            savedTarget = null;
        }
    }

    private IEnumerator OnResetKillTarget(float time)
    {
        yield return new WaitForSeconds(time);
        OnZoomOut();
        if (savedTarget != null)
        {
            followTarget = savedTarget;
            savedTarget = null;
        }
    }

    public void SetTarget(Transform target)
    {
        savedTarget = this.followTarget;
        this.followTarget = target;
    }

    public void ReturnTarget()
    {
        if (resetTargetCoroutine != null)
            StopCoroutine(resetTargetCoroutine);
        resetTargetCoroutine = StartCoroutine(OnResetTarget());
    }

    public void OnVibrate()
    {
        AnimatorManager.Play(Animator, "anim_vibrate");
    }

    public void OnZoomIn()
    {
        if (isZoom)
            return;
        isZoom = true;
        AnimatorManager.Play(Animator, "anim_zoom_in");
    }
}