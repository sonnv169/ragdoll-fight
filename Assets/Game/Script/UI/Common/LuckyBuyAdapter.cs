﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LuckyBuyAdapter : MonoBehaviour
{
    private GameEnums.ITEM_GROUP itemGroup;
    private string itemName;
    private int originPrice;

    [SerializeField] private LUCKY_BUY_GROUP luckyBuyGroup;
    [SerializeField] private Button btnAction;
    [SerializeField] private Image imgPreview;
    [SerializeField] private Image imgUnlocked;
    [SerializeField] private Text txtAdOpen;
    [SerializeField] private Text txtName;
    [SerializeField] private Text txtSalePrice;
    [SerializeField] private Text txtOriginPrice;

    private void OnEnable()
    {
        Init();
        Eventon<SaleEvent>.EventStatic += OnSaleEvent;
    }

    private void OnDisable()
    {
        Eventon<SaleEvent>.EventStatic -= OnSaleEvent;
    }

    private void OnSaleEvent(SaleEvent param)
    {
        if (!param.isSaled)
            return;
        EnableTxtSalePrice(true);
        originPrice = (int) (originPrice * GameConfig.salePercent) / 100;
        OnTxtOriginPriceDisplay(originPrice);
    }

    private void Init()
    {
        gameObject.SetActive(true);
        EnableImgUnlocked(false);

        if (Singleton<SaleFeatureManager>.Instance == null || Singleton<WeaponManager>.Instance == null ||
            Singleton<CharacterManager>.Instance == null)
            return;
        SaleFeatureManager saleFeatureManager = Singleton<SaleFeatureManager>.Instance;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        Sprite spr = null;
        string name = null;
        int round = saleFeatureManager.GetLuckyBuyRound();
        itemGroup = saleFeatureManager.GetLuckyBuyItemGroup(luckyBuyGroup, round);
        itemName = saleFeatureManager.GetLuckyBuyItem(luckyBuyGroup, round);

        switch (itemGroup)
        {
            case GameEnums.ITEM_GROUP.Weapon:
                GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                Enum.TryParse(itemName, out weapon);
                originPrice = weaponManager.GetPrice(weapon);
                spr = weaponManager.GetSprite(weapon);
                name = weaponManager.GetName(weapon);

                if (weaponManager.IsUnlocked(weapon))
                {
                    gameObject.SetActive(false);
                }

                break;

            case GameEnums.ITEM_GROUP.Outfit:
                GameEnums.HERO hero = GameEnums.HERO.h0;
                Enum.TryParse(itemName, out hero);
                originPrice = characterManager.GetPrice(hero);
                spr = characterManager.GetPlayerAvatar(hero);
                name = characterManager.GetPlayerName(hero);

                if (characterManager.IsUnlocked(hero))
                    gameObject.SetActive(false);

                break;
        }

        DisplayPreview(spr);
        EnableBtnAction(true);
        EnableTxtSalePrice(false);
        OnTxtOriginPriceDisplay(originPrice);
        OnTxtSalePriceDisplay(originPrice);

        if (txtName != null)
            txtName.text = name;

        if (btnAction != null)
            btnAction.onClick.AddListener(OnAction);
    }


    private void OnUnlock()
    {
        EnableImgUnlocked(true);
        EnableBtnAction(false);

        if (UILuckyBuy.Instance != null && txtAdOpen == null)
        {
            UILuckyBuy uiLuckyBuy = UILuckyBuy.Instance;
            uiLuckyBuy.UpBoughtTurn();
        }

        if (Singleton<WeaponManager>.Instance == null || Singleton<CharacterManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        switch (itemGroup)
        {
            case GameEnums.ITEM_GROUP.Weapon:
                GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                Enum.TryParse(itemName, out weapon);
                weaponManager.Unlock(weapon);
                break;

            case GameEnums.ITEM_GROUP.Outfit:
                GameEnums.HERO hero = GameEnums.HERO.h0;
                Enum.TryParse(itemName, out hero);
                characterManager.Unlock(hero);
                break;
        }

        UIUnlocked.Show();
        UIUnlocked uiUnlocked = UIUnlocked.Instance;
        uiUnlocked.InitSlot(itemGroup, itemName, 0);
    }

    private void OnAction()
    {
        if (txtAdOpen != null)
        {
            AdReward.Show();
            Eventon<AD_STATE>.EventDynamic += state =>
            {
                if (state != AD_STATE.FINISHED)
                    return;

                Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "unlock_item", "ui_lucky_buy");

                OnUnlock();
            };
        }
        else
        {
            if (Singleton<CurrencyManager>.Instance == null)
                return;
            CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
            int coin = currencyManager.GetCurrency(CURRENCY.COIN);
            if (coin >= originPrice)
            {
                currencyManager.Pay(CURRENCY.COIN, originPrice);
                OnUnlock();

                UILuckyBuy.Instance.EnableSale(false);
            }
            else
                UIShop.Show();
        }
    }

    private void DisplayPreview(Sprite spr)
    {
        if (imgPreview == null)
            return;
        imgPreview.sprite = spr;
        imgPreview.SetNativeSize();
        imgPreview.transform.localScale = new Vector3(imgPreview.transform.localScale.x,
            imgPreview.transform.localScale.y, imgPreview.transform.localScale.z);
    }

    private void EnableImgUnlocked(bool isStatus)
    {
        if (imgUnlocked != null)
            imgUnlocked.gameObject.SetActive(isStatus);
    }

    private void OnTxtOriginPriceDisplay(int number)
    {
        string content = number.ToString();
        if (number >= 1000)
            content = $"{number:#,#.}";
        if (txtOriginPrice != null)
            txtOriginPrice.text = content;
    }

    private void OnTxtSalePriceDisplay(int number)
    {
        string content = number.ToString();
        if (number >= 1000)
            content = $"{number:#,#.}";
        if (txtSalePrice != null)
            txtSalePrice.text = content;
    }

    private void EnableTxtName(bool isStatus)
    {
        if (txtName != null)
            txtName.gameObject.SetActive(isStatus);
    }

    private void EnableTxtSalePrice(bool isStatus)
    {
        if (txtSalePrice != null)
            txtSalePrice.gameObject.SetActive(isStatus);
    }

    private void EnableBtnAction(bool isStatus)
    {
        if (btnAction != null)
            btnAction.gameObject.SetActive(isStatus);
    }
}