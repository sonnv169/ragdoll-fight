﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BtnBooster : ToggleBehaviour
{
    private Button mButton;
    private Coroutine cooldownCoroutine;
    private float cooldown;

    [SerializeField] private GameEnums.BOOSTER booster;
    [SerializeField] private Image icoBooster;
    [SerializeField] private Image icoAds;
    [SerializeField] private Image icoWeapon;
    [SerializeField] private Text txtNumber;
    [SerializeField] private Text txtCooldown;

    #region property

    private Button Button
    {
        get
        {
            if (mButton == null && GetComponent<Button>())
                mButton = GetComponent<Button>();
            return mButton;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Init();
        Eventon<GAME_STATE>.EventStatic += OnGameState;
        Eventon<CurrencyData>.EventStatic += OnCurrencyData;
        Eventon<BoosterEvent>.EventStatic += OnBoosterEvent;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
        Eventon<CurrencyData>.EventStatic -= OnCurrencyData;
        Eventon<BoosterEvent>.EventStatic -= OnBoosterEvent;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                OnReset();
                break;
        }
    }

    private void OnCurrencyData(CurrencyData param)
    {
        DisplayNumber();
    }

    private void OnBoosterEvent(BoosterEvent param)
    {
        if (param.booster != booster)
            return;
        switch (param.isCasting)
        {
            case true:
                if (Button != null)
                    Button.interactable = false;
                if (icoBooster != null)
                    icoBooster.color = Color.grey;
                SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_item_active, false);
                break;

            case false:
                cooldown = GameConfig.boostedTime;
                DisplayCooldown(cooldown.ToString());
                cooldownCoroutine = StartCoroutine(OnCooldown(1.0f));
                break;
        }
    }

    private void Init()
    {
        OnReset();
        DisplayNumber();
        if (Button != null)
            Button.onClick.AddListener(() =>
            {
                if (Singleton<CurrencyManager>.Instance == null)
                    return;
                CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
                currencyManager.UseBooster(booster);
            });
    }

    private void OnReset()
    {
        cooldown = 0;
        RandomWeapon();
        DisplayCooldown(null);
        if (Button != null)
            Button.interactable = true;
        if (icoBooster != null)
            icoBooster.color = Color.white;
        if (cooldownCoroutine != null)
            StopCoroutine(cooldownCoroutine);
    }

    private void RandomWeapon()
    {
        if (icoWeapon == null || Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        string[] weaponNames = Enum.GetNames(typeof(GameEnums.WEAPON)).ToArray();
        List<GameEnums.WEAPON> readys = new List<GameEnums.WEAPON>();
        for (int i = 0; i < weaponNames.Length; i++)
        {
            GameEnums.WEAPON temp = GameEnums.WEAPON.NONE;
            Enum.TryParse(weaponNames[i], out temp);
            if (temp == GameEnums.WEAPON.NONE || weaponManager.GetEquipted() == temp)
                continue;
            readys.Add(temp);
        }

        int result = Random.Range(0, readys.Count);
        GameEnums.WEAPON weapon = readys[result];
        weaponManager.SetWeaponRandom(weapon);
        if (icoWeapon != null)
        {
            icoWeapon.sprite = weaponManager.GetSprite(weapon);
            icoWeapon.SetNativeSize();
            icoWeapon.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    private IEnumerator OnCooldown(float time)
    {
        yield return new WaitForSeconds(time);
        if (cooldown > 0)
        {
            cooldown -= 1.0f;
            DisplayCooldown(cooldown.ToString());
            cooldownCoroutine = StartCoroutine(OnCooldown(1.0f));
        }
        else
            OnReset();
    }

    private void DisplayNumber()
    {
        if (Singleton<CurrencyManager>.Instance == null || txtNumber == null || icoAds == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        int number = 0;

        switch (booster)
        {
            case GameEnums.BOOSTER.HP_REGEN:
                number = currencyManager.GetCurrency(CURRENCY.HP_REGEN);
                break;
            case GameEnums.BOOSTER.SHIELD:
                number = currencyManager.GetCurrency(CURRENCY.SHIELD);
                break;
            case GameEnums.BOOSTER.DOUBLE_POWER:
                number = currencyManager.GetCurrency(CURRENCY.DOUBLE_POWER);
                break;
        }

        if (number > 0)
        {
            icoAds.color = Color.clear;
            txtNumber.text = number.ToString();
        }
        else
        {
            icoAds.color = Color.white;
            txtNumber.text = null;
        }
    }

    private void DisplayCooldown(string content)
    {
        if (txtCooldown != null)
            txtCooldown.text = content;
    }
}