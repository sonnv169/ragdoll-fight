﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BtnWeaponReward : ToggleBehaviour
{
    private Button mButton;

    [SerializeField] private Text txtTime;
    [SerializeField] private Image ico;
    [SerializeField] private Sprite sprReward;
    [SerializeField] private Sprite sprDelay;
    [SerializeField] private GameObject vfxReward;

    #region property

    private Button Button
    {
        get
        {
            if (mButton == null && GetComponent<Button>())
                mButton = GetComponent<Button>();
            return mButton;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Init();
        Eventon<WeaponRewardData>.EventStatic += OnReload;
        Eventon<WeaponRewardData>.EventStatic += OnWeaponRewardData;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<WeaponRewardData>.EventStatic -= OnReload;
        Eventon<WeaponRewardData>.EventStatic -= OnWeaponRewardData;
    }

    private void OnWeaponRewardData(WeaponRewardData param)
    {
        CheckingRewardEnable();
    }

    private void OnReload(WeaponRewardData param)
    {
        OnDisplayTime();
    }

    private void Init()
    {
        if (Button != null)
            Button.onClick.AddListener(() =>
            {
                Button.interactable = false;
                AdReward.Show();
                Eventon<AD_STATE>.EventDynamic += state =>
                {
                    if (state == AD_STATE.FINISHED)
                    {
                        OnReward();

                        Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "get_weapon", "ui_weapon");
                    }
                    else
                        Button.interactable = true;
                };
                if (!AdController.IsConnecting())
                    Button.interactable = true;
            });
        OnActive();
        OnDisplayTime();
        CheckingRewardEnable();
    }

    private void OnActive()
    {
        if (Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        string[] weapons = Enum.GetNames(typeof(GameEnums.WEAPON)).ToArray();
        if (weaponManager.GetUnlockeds().Count >= weapons.Length)
            gameObject.SetActive(false);
    }

    private void CheckingRewardEnable()
    {
        if (Button == null || Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        if (rewardManager.GetWeaponRewardDelay() > 0)
            Button.interactable = false;
        else
            Button.interactable = true;
    }

    private void OnDisplayTime()
    {
        if (txtTime == null || Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        string content = null;
        Sprite spr = sprReward;

        if (!rewardManager.IsWeaponReward())
        {
            int delaySecond = rewardManager.GetWeaponRewardDelay();
            int second = (int) delaySecond % 60;
            int minute = (int) delaySecond / 60;
            int hour = (int) minute / 60;

            string secondContent = null;
            string minuteContent = null;
            string hourContent = null;

            if (second < 10)
                secondContent = "0" + second;
            else
                secondContent = second.ToString();

            if (minute < 10)
                minuteContent = "0" + minute;
            else
                minuteContent = minute.ToString();

            if (hour < 10)
                hourContent = "0" + hour;
            else
                hourContent = hour.ToString();

            content = hourContent + ":" + minuteContent + ":" + secondContent;
            spr = sprDelay;

            if (vfxReward != null)
                vfxReward.SetActive(false);
        }
        else
        {
            if (vfxReward != null)
                vfxReward.SetActive(true);
        }

        txtTime.text = content;
        if (ico != null)
            ico.sprite = spr;
    }

    private void OnReward()
    {
        if (Singleton<WeaponManager>.Instance == null || Singleton<RewardManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;

        if (!rewardManager.IsWeaponReward())
            return;
        string[] weapons = Enum.GetNames(typeof(GameEnums.WEAPON)).ToArray();
        List<GameEnums.WEAPON> readys = new List<GameEnums.WEAPON>();
        GameEnums.WEAPON reward = GameEnums.WEAPON.NONE;

        for (int i = 0; i < weapons.Length; i++)
        {
            GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
            Enum.TryParse(weapons[i], out weapon);
            if (weapon != GameEnums.WEAPON.NONE && !weaponManager.IsUnlocked(weapon))
                readys.Add(weapon);
        }

        if (readys.Count <= 0)
            return;
        int result = Random.Range(0, readys.Count);
        reward = readys[result];
        weaponManager.Unlock(reward);
        rewardManager.SetWeaponRewardDelay();
        if (UIWeapon.Instance != null)
            UIWeapon.Instance.Reload();

        UIUnlocked.Show();
        UIUnlocked.Instance.InitSlot(GameEnums.ITEM_GROUP.Weapon, reward.ToString(), 1);

        if (weaponManager.IsFullUnlock())
            gameObject.SetActive(false);
    }
}