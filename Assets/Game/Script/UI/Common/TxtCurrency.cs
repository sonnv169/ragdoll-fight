﻿using UnityEngine;
using UnityEngine.UI;

public class TxtCurrency : ToggleBehaviour
{
    private Text mText;
    private int curren;

    [SerializeField] private CURRENCY currency;
    [SerializeField] private int updateStep;

    #region property

    private Text Text
    {
        get
        {
            if (mText == null && GetComponent<Text>())
                mText = GetComponent<Text>();
            return mText;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Eventon<CurrencyEvent>.EventStatic += OnCurrencyEvent;
    }

    private void Start()
    {
        Init();
    }

    protected override void OnCyclic()
    {
        OnUpdate();
    }

    protected override void OnDisappear()
    {
        Eventon<CurrencyEvent>.EventStatic -= OnCurrencyEvent;
    }

    private void OnCurrencyEvent(CurrencyEvent param)
    {
        if (!param.isUpdate || Singleton<CurrencyManager>.Instance == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        curren = currencyManager.GetCurrency(currency);
    }

    private void Init()
    {
        if (Singleton<CurrencyManager>.Instance == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        curren = currencyManager.GetCurrency(currency);
    }

    private void OnUpdate()
    {
        if (Singleton<CurrencyManager>.Instance == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        int target = currencyManager.GetCurrency(currency);
        int step = 1;

        if (curren < target)
        {
            step = (int) (target - curren) / updateStep;
            if (step <= 1)
                step = 1;
            curren += step;
            if (curren >= target)
                curren = target;
        }
        else if (curren > target)
        {
            step = (int) (curren - target) / updateStep;
            if (step <= 1)
                step = 1;
            curren -= step;
            if (curren <= target)
                curren = target;
        }

        if (Text == null)
            return;
        string content = null;
        if (curren < 1000)
            content = curren.ToString();
        else
            content = $"{curren:#,#.}";

        Text.text = content;
    }
}