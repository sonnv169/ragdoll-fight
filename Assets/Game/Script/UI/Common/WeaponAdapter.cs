﻿using System.Collections;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WeaponAdapter : ToggleBehaviour
{
    private Coroutine blockCorountine;
    private GameEnums.WEAPON weapon;
    private Button mButton;
    private int vip;

    [SerializeField] private Image ico;
    [SerializeField] private Image img;
    [SerializeField] private Text txtName;
    [SerializeField] private Text txtAtk;
    [SerializeField] private GameObject vfxEquipted;
    [SerializeField] private Sprite[] sprVips;

    #region property

    private Button Button
    {
        get
        {
            if (mButton == null && GetComponent<Button>())
                mButton = GetComponent<Button>();
            return mButton;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        EnableVfxEquipted();
        if (Button != null)
            Button.onClick.AddListener(Equipt);
        Eventon<Dialog>.EventStatic += OnDialog;
        Eventon<WeaponData>.EventStatic += OnWeaponData;
        Eventon<AdapterClickedEvent>.EventStatic += OnAdapterClickedEvent;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<Dialog>.EventStatic -= OnDialog;
        Eventon<WeaponData>.EventStatic -= OnWeaponData;
        Eventon<AdapterClickedEvent>.EventStatic -= OnAdapterClickedEvent;
    }

    private void OnAdapterClickedEvent(AdapterClickedEvent param)
    {
        if (!param.isClick || Button == null)
            return;
        Button.interactable = false;
        blockCorountine = StartCoroutine(OnEndBlock());
    }

    private IEnumerator OnEndBlock()
    {
        yield return new WaitForSeconds(0.5f);
        Button.interactable = true;
    }

    private void OnWeaponData(WeaponData param)
    {
        EnableVfxEquipted();
    }

    private void OnDialog(Dialog param)
    {
        if (param.Equals(UIWeapon.Instance))
            Return();
    }

    private void EnableVfxEquipted()
    {
        if (vfxEquipted == null || Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        vfxEquipted.SetActive(false);
        if (weaponManager.GetEquipted() == weapon)
            vfxEquipted.SetActive(true);
    }

    private void Equipt()
    {
        Eventon<AdapterClickedEvent>.Trigger(new AdapterClickedEvent()
        {
            isClick = true
        });

        SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_ui_equipt, false);

        if (Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        if (weaponManager.GetEquipted() == weapon)
            return;
        weaponManager.Equipt(weapon);
        if (UIWeapon.Instance != null)
            UIWeapon.Instance.SetWeapon();
    }

    private void Return()
    {
        if (blockCorountine != null)
            StopCoroutine(blockCorountine);
        if (!GetComponent<ObjectPooling>())
            return;
        ObjectPooling objectPooling = GetComponent<ObjectPooling>();
        objectPooling.Return();
    }

    public void Init()
    {
        string[] weapons = Enum.GetNames(typeof(GameEnums.WEAPON)).ToArray();
        int index = 0;
        if (transform.parent != null)
        {
            for (int i = 0; i < transform.parent.childCount; i++)
            {
                if (transform.parent.GetChild(i).Equals(transform))
                    index = i;
            }
        }

        Enum.TryParse(weapons[index], out weapon);
        EnableVfxEquipted();

        if (Singleton<WeaponManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        vip = weaponManager.GetVip(weapon);
        if (ico != null)
        {
            ico.sprite = weaponManager.GetSprite(weapon);
            ico.SetNativeSize();
        }

        if (img != null)
        {
            if (vip < sprVips.Length)
                img.sprite = sprVips[vip];
        }

        if (txtName != null)
            txtName.text = weaponManager.GetName(weapon);

        if (txtAtk != null)
            txtAtk.text = weaponManager.GetBonusDamage(weapon).ToString();
    }

    public void OnActive(bool isStatus)
    {
        if (Button != null)
            Button.interactable = isStatus;
        if (ico == null || img == null || txtName == null)
            return;
        switch (isStatus)
        {
            case true:
                ico.color = Color.white;
                img.color = Color.white;
                txtName.color = Color.white;
                break;

            case false:
                ico.color = Color.gray;
                img.color = Color.gray;
                txtName.color = Color.gray;
                break;
        }
    }

    public GameEnums.WEAPON GetWeapon()
    {
        return weapon;
    }

    public int GetVip()
    {
        return vip;
    }
}