﻿using UnityEngine;
using UnityEngine.UI;

public class StickDrager : ToggleBehaviour
{
    private Vector2 direction;

    [SerializeField] private Transform root;
    [SerializeField] private Image stick;
    [SerializeField] private Image handle;
    [SerializeField] private Image looker;
    [SerializeField] private Image stickCLamp;
    [SerializeField] private LayerMask clampLayer;

    protected override void OnAppear()
    {
        Eventon<GAME_STATE>.EventStatic += OnGameState;
    }

    protected override void OnCyclic()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;

        if (gameManager.GetState() != GAME_STATE.PLAY || stick == null || handle == null || looker == null ||
            stickCLamp == null)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 inputPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 movePosition = new Vector3(inputPosition.x, inputPosition.y, 0);
            transform.position = movePosition;
            stick.transform.position = movePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            Vector2 inputPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 movePosition = new Vector3(inputPosition.x, inputPosition.y, 0);
            bool isClamp = Physics2D.OverlapCircle(handle.transform.position, 0.01f, clampLayer);
            handle.transform.position = movePosition;
            direction = handle.transform.position - transform.position;
            direction.Normalize();
            looker.gameObject.SetActive(true);
            looker.transform.up = new Vector2(direction.x, direction.y);

            if (!isClamp)
            {
                handle.transform.position = stickCLamp.transform.position;

                if (Singleton<Player>.Instance == null)
                    return;
                Player player = Singleton<Player>.Instance;
                player.Move();
            }

            stick.transform.position = new Vector3(stick.transform.position.x, stick.transform.position.y, 0);
            handle.transform.position = new Vector3(handle.transform.position.x, handle.transform.position.y, 0);
            looker.transform.position = new Vector3(looker.transform.position.x, looker.transform.position.y, 0);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Reset();
            looker.transform.rotation = Quaternion.identity;
            looker.gameObject.SetActive(false);

            if (Singleton<Player>.Instance == null)
                return;
            Player player = Singleton<Player>.Instance;
            player.Stop();
        }
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                Reset();
                break;
        }
    }

    private void Reset()
    {
        direction = Vector2.zero;
        if (root != null)
            transform.position = root.position;

        if (stick != null)
        {
            stick.transform.position = Vector3.zero;
            stick.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }

        if (handle != null)
        {
            handle.transform.position = Vector3.zero;
            handle.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }

        if (looker != null)
        {
            looker.transform.rotation = Quaternion.identity;
            looker.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }
    }

    public Vector3 GetDirection()
    {
        return direction;
    }
}