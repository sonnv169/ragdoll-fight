﻿using UnityEngine;
using UnityEngine.UI;

public class SldPower : ToggleBehaviour
{
    private Image mImage;

    [SerializeField] private Text txtPower;

    #region property

    private Image Image
    {
        get
        {
            if (mImage == null && GetComponent<Image>())
                mImage = GetComponent<Image>();
            return mImage;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Eventon<HeroData>.EventStatic += OnReload;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<HeroData>.EventStatic -= OnReload;
    }

    private void Start()
    {
        OnDisplay();
    }

    private void OnReload(HeroData param)
    {
        OnDisplay();
    }

    private void OnDisplay()
    {
        if (Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        int currenLevel = characterManager.GetHeroLevel();
        int maxLevel = characterManager.GetHeroBases();

        if (Image != null)
            Image.fillAmount = (float) characterManager.GetHeroAtk(currenLevel) /
                               (float) characterManager.GetHeroAtk(maxLevel - 1);

        if (txtPower != null)
            txtPower.text = characterManager.GetHeroAtk(currenLevel).ToString();
    }
}