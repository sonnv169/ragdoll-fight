﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class LuckyAdapterOpenEvent
{
    public int number;
}

public class LuckyGiftAdapter : ToggleBehaviour
{
    private LUCKY_GIFT_GROUP luckyGiftGroup;
    private GameEnums.ITEM_GROUP itemGroup;
    private GameEnums.GENERAL general;
    private int amount;
    private int price;

    [SerializeField] private Button btnOpen;
    [SerializeField] private Image imgPreview;
    [SerializeField] private Image imgUnlocked;
    [SerializeField] private Text txtTitle;
    [SerializeField] private Text txtFreeOpen;
    [SerializeField] private Text txtAdOpen;
    [SerializeField] private Text txtPrice;
    [SerializeField] private Text txtAmount;

    protected override void OnAppear()
    {
        Init();
        Eventon<LuckyAdapterOpenEvent>.EventStatic += OnLuckyAdapterOpenEvent;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<LuckyAdapterOpenEvent>.EventStatic -= OnLuckyAdapterOpenEvent;
    }

    private void OnLuckyAdapterOpenEvent(LuckyAdapterOpenEvent param)
    {
        switch (param.number)
        {
            case 1:
                EnableTxtAdOpen(true);
                EnableTxtFreeOpen(false);
                break;

            case 2:
                DisplayPrice(price);
                EnableTxtPrice(true);
                EnableImgPreview(true);
                EnableTxtTitle(false);
                EnableTxtAdOpen(false);
                EnableTxtAmount(false);

                switch (luckyGiftGroup)
                {
                    case LUCKY_GIFT_GROUP.GROUP_GENERAL_1:
                    case LUCKY_GIFT_GROUP.GROUP_GENERAL_2:
                        DisplayPrice(price);
                        EnableTxtPrice(true);
                        EnableTxtAmount(true);
                        if (general != GameEnums.GENERAL.Gold)
                            return;
                        if (txtPrice != null)
                            txtPrice.text = "Claim";
                        break;
                }

                break;

            case 3:
                break;
        }
    }

    private void Init()
    {
        EnableBtnOpen(true);
        EnableTxtTitle(true);
        EnableTxtFreeOpen(true);
        EnableImgPreview(false);
        EnableImgUnlocked(false);
        EnableTxtAdOpen(false);
        EnableTxtPrice(false);
        EnableTxtAmount(false);

        if (btnOpen != null)
            btnOpen.onClick.AddListener(() =>
            {
                if (UILuckyGift.Instance == null)
                    return;
                UILuckyGift uiLuckyGift = UILuckyGift.Instance;
                uiLuckyGift.OpenAdapter();
                Eventon<LuckyAdapterOpenEvent>.Trigger(new LuckyAdapterOpenEvent()
                {
                    number = uiLuckyGift.GetOpenedAdapter()
                });

                switch (uiLuckyGift.GetOpenedAdapter())
                {
                    case 1:
                        EnableImgPreview(true);
                        EnableTxtTitle(false);
                        EnableTxtFreeOpen(false);

                        if (luckyGiftGroup == LUCKY_GIFT_GROUP.GROUP_OUTFIT_AND_WEAPON)
                            UnlockWeaponAndOutfit();
                        else
                        {
                            EnableTxtAmount(true);
                            GetGeneralGift();
                        }

                        EnableBtnOpen(false);
                        break;

                    case 2:
                        btnOpen.interactable = false;
                        AdReward.Show();
                        Eventon<AD_STATE>.EventDynamic += state =>
                        {
                            if (state == AD_STATE.FINISHED)
                            {
                                EnableImgPreview(true);
                                EnableTxtTitle(false);
                                EnableTxtFreeOpen(false);

                                if (luckyGiftGroup == LUCKY_GIFT_GROUP.GROUP_OUTFIT_AND_WEAPON)
                                    UnlockWeaponAndOutfit();
                                else
                                {
                                    EnableTxtAmount(true);
                                    GetGeneralGift();
                                }

                                EnableBtnOpen(false);

                                Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "get_gift_item",
                                    "ui_lucky_gift");
                            }
                            else
                                btnOpen.interactable = true;
                        };

                        if (!AdController.IsConnecting())
                            btnOpen.interactable = true;
                        break;

                    case 3:
                        if (Singleton<CurrencyManager>.Instance == null)
                            return;
                        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
                        if (currencyManager.GetCurrency(CURRENCY.COIN) >= price)
                        {
                            currencyManager.Pay(CURRENCY.COIN, price);
                            switch (luckyGiftGroup)
                            {
                                case LUCKY_GIFT_GROUP.GROUP_OUTFIT_AND_WEAPON:
                                    UnlockWeaponAndOutfit();
                                    break;
                                case LUCKY_GIFT_GROUP.GROUP_GENERAL_1:
                                case LUCKY_GIFT_GROUP.GROUP_GENERAL_2:
                                    EnableTxtAmount(true);
                                    GetGeneralGift();
                                    break;
                            }

                            EnableBtnOpen(false);
                        }
                        else
                            UIShop.Show();

                        break;
                }
            });
    }

    private void GetGeneralGift()
    {
        EnableImgUnlocked(true);
        if (Singleton<CurrencyManager>.Instance == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        if (itemGroup != GameEnums.ITEM_GROUP.General)
            return;

        switch (general)
        {
            case GameEnums.GENERAL.Gold:
                currencyManager.Collect(CURRENCY.COIN, amount);
                break;

            case GameEnums.GENERAL.Hp:
                currencyManager.Collect(CURRENCY.HP_REGEN, amount);
                break;

            case GameEnums.GENERAL.PowerX2:
                currencyManager.Collect(CURRENCY.DOUBLE_POWER, amount);
                break;

            case GameEnums.GENERAL.Shield:
                currencyManager.Collect(CURRENCY.SHIELD, amount);
                break;
        }

        UIUnlocked.Show();
        UIUnlocked uiUnlocked = UIUnlocked.Instance;
        uiUnlocked.InitSlot(itemGroup, general.ToString(), amount);
    }

    private void UnlockWeaponAndOutfit()
    {
        EnableImgUnlocked(true);
        if (Singleton<UILuckyGift>.Instance == null || Singleton<WeaponManager>.Instance == null ||
            Singleton<CharacterManager>.Instance == null)
            return;
        UILuckyGift uiLuckyGift = UILuckyGift.Instance;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        string itemName = null;
        switch (itemGroup)
        {
            case GameEnums.ITEM_GROUP.Outfit:
                characterManager.Unlock(uiLuckyGift.GetOutfitGift());
                itemName = uiLuckyGift.GetOutfitGift().ToString();
                break;

            case GameEnums.ITEM_GROUP.Weapon:
                weaponManager.Unlock(uiLuckyGift.GetWeaponGift());
                itemName = uiLuckyGift.GetWeaponGift().ToString();
                break;
        }

        UIUnlocked.Show();
        UIUnlocked uiUnlocked = UIUnlocked.Instance;
        uiUnlocked.InitSlot(itemGroup, itemName, amount);
    }

    private void InitConent(LUCKY_GIFT_GROUP luckyGiftGroup)
    {
        if (Singleton<SaleFeatureManager>.Instance == null ||
            Singleton<CharacterManager>.Instance == null ||
            Singleton<WeaponManager>.Instance == null)
            return;
        SaleFeatureManager saleFeatureManager = Singleton<SaleFeatureManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        Sprite spr = null;
        int lenght = 0;
        switch (luckyGiftGroup)
        {
            case LUCKY_GIFT_GROUP.GROUP_OUTFIT_AND_WEAPON:
                if (UILuckyGift.Instance != null)
                {
                    UILuckyGift uiLuckyGift = UILuckyGift.Instance;
                    itemGroup = uiLuckyGift.GetItemGroup();
                    GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                    GameEnums.HERO hero = GameEnums.HERO.h0;
                    switch (itemGroup)
                    {
                        case GameEnums.ITEM_GROUP.Weapon:
                            weapon = uiLuckyGift.GetWeaponGift();
                            spr = weaponManager.GetSprite(weapon);
                            price = weaponManager.GetPrice(weapon);
                            break;
                        case GameEnums.ITEM_GROUP.Outfit:
                            hero = uiLuckyGift.GetOutfitGift();
                            spr = characterManager.GetPlayerAvatar(hero);
                            price = characterManager.GetPrice(hero);
                            break;
                    }
                }

                break;
            case LUCKY_GIFT_GROUP.GROUP_GENERAL_1:
            case LUCKY_GIFT_GROUP.GROUP_GENERAL_2:
                itemGroup = GameEnums.ITEM_GROUP.General;
                lenght = saleFeatureManager.GetLuckyGiftLenght(luckyGiftGroup);
                int index = Random.Range(0, lenght);
                general = saleFeatureManager.GetLuckyGiftGeneral(luckyGiftGroup, index);
                amount = saleFeatureManager.GetLuckyGiftAmount(luckyGiftGroup, index);
                switch (general)
                {
                    case GameEnums.GENERAL.Hp:
                        price = GameConfig.hpRegenPrice;
                        break;
                    case GameEnums.GENERAL.PowerX2:
                        price = GameConfig.doublePowerPrice;
                        break;
                    case GameEnums.GENERAL.Shield:
                        price = GameConfig.shieldPrice;
                        break;
                }

                spr = characterManager.GetItemIcon(general.ToString());
                break;
        }

        SetPreviewSprite(spr);
        DisplayPrice(price);
        DisplayAmount(amount);
    }

    private void EnableBtnOpen(bool isStatus)
    {
        if (btnOpen != null)
            btnOpen.gameObject.SetActive(isStatus);
    }

    private void EnableImgPreview(bool isStatus)
    {
        if (imgPreview != null)
            imgPreview.gameObject.SetActive(isStatus);
    }

    private void EnableImgUnlocked(bool isStatus)
    {
        if (imgUnlocked != null)
            imgUnlocked.gameObject.SetActive(isStatus);
    }

    private void SetPreviewSprite(Sprite sprite)
    {
        if (imgPreview == null)
            return;
        imgPreview.sprite = sprite;
        imgPreview.SetNativeSize();
        imgPreview.transform.localScale = new Vector3(imgPreview.transform.localScale.x,
            imgPreview.transform.localScale.y, imgPreview.transform.localScale.z);
    }

    private void EnableTxtTitle(bool isStatus)
    {
        if (txtTitle != null)
            txtTitle.gameObject.SetActive(isStatus);
    }

    private void EnableTxtFreeOpen(bool isStatus)
    {
        if (txtFreeOpen != null)
            txtFreeOpen.gameObject.SetActive(isStatus);
    }

    private void EnableTxtAdOpen(bool isStatus)
    {
        if (txtAdOpen != null)
            txtAdOpen.gameObject.SetActive(isStatus);
    }

    private void EnableTxtPrice(bool isStatus)
    {
        if (txtPrice != null)
            txtPrice.gameObject.SetActive(isStatus);
    }

    private void DisplayPrice(int number)
    {
        if (txtPrice == null)
            return;
        string content = number.ToString();
        if (number >= 1000)
            content = $"{number:#,#.}";
        txtPrice.text = content;
    }

    private void EnableTxtAmount(bool isStatus)
    {
        if (txtAmount != null)
            txtAmount.gameObject.SetActive(isStatus);
    }

    private void DisplayAmount(int number)
    {
        if (txtAmount == null)
            return;
        string content = number.ToString();
        if (number >= 1000)
            content = $"{number:#,#.}";
        txtAmount.text = content;
    }

    public void InitLuckyGiftGroup(LUCKY_GIFT_GROUP luckyGiftGroup)
    {
        this.luckyGiftGroup = luckyGiftGroup;
        InitConent(luckyGiftGroup);
    }
}