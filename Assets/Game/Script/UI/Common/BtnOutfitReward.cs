﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BtnOutfitReward : ToggleBehaviour
{
    private Button mButton;

    [SerializeField] private Text txtTime;
    [SerializeField] private Image ico;
    [SerializeField] private Sprite sprReward;
    [SerializeField] private Sprite sprDelay;
    [SerializeField] private GameObject vfxReward;

    #region property

    private Button Button
    {
        get
        {
            if (mButton == null && GetComponent<Button>())
                mButton = GetComponent<Button>();
            return mButton;
        }
    }

    #endregion

    protected override void OnAppear()
    {
        Init();
        Eventon<OutfitRewardData>.EventStatic += OnReload;
        Eventon<OutfitRewardData>.EventStatic += OnOutfitRewardData;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<OutfitRewardData>.EventStatic -= OnReload;
        Eventon<OutfitRewardData>.EventStatic -= OnOutfitRewardData;
    }

    private void OnOutfitRewardData(OutfitRewardData param)
    {
        CheckingRewardEnable();
    }

    private void OnReload(OutfitRewardData param)
    {
        OnDisplayTime();
    }

    private void Init()
    {
        if (Button != null)
            Button.onClick.AddListener(() =>
            {
                Button.interactable = false;
                AdReward.Show();
                Eventon<AD_STATE>.EventDynamic += state =>
                {
                    if (state == AD_STATE.FINISHED)
                    {
                        OnReward();

                        Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "get_outfit", "ui_outfit");
                    }
                    else
                        Button.interactable = true;
                };

                if (!AdController.IsConnecting())
                    Button.interactable = true;
            });
        OnActive();
        OnDisplayTime();
        CheckingRewardEnable();
    }

    private void OnActive()
    {
        if (Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        string[] heroes = Enum.GetNames(typeof(GameEnums.HERO)).ToArray();
        if (characterManager.GetUnlockeds().Count >= heroes.Length)
            gameObject.SetActive(false);
    }

    private void CheckingRewardEnable()
    {
        if (Button == null || Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        if (rewardManager.GetOutfitRewardDelay() > 0)
            Button.interactable = false;
        else
            Button.interactable = true;
    }

    private void OnDisplayTime()
    {
        if (txtTime == null || Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        string content = null;
        Sprite spr = sprReward;

        if (!rewardManager.IsOutfitReward())
        {
            int delaySecond = rewardManager.GetOutfitRewardDelay();
            int second = (int) delaySecond % 60;
            int minute = (int) delaySecond / 60;
            int hour = (int) minute / 60;

            string secondContent = null;
            string minuteContent = null;
            string hourContent = null;

            if (second < 10)
                secondContent = "0" + second;
            else
                secondContent = second.ToString();

            if (minute < 10)
                minuteContent = "0" + minute;
            else
                minuteContent = minute.ToString();

            if (hour < 10)
                hourContent = "0" + hour;
            else
                hourContent = hour.ToString();

            content = hourContent + ":" + minuteContent + ":" + secondContent;
            spr = sprDelay;

            if (vfxReward != null)
                vfxReward.SetActive(false);
        }
        else
        {
            if (vfxReward != null)
                vfxReward.SetActive(true);
        }

        txtTime.text = content;
        if (ico != null)
            ico.sprite = spr;
    }

    private void OnReward()
    {
        if (Singleton<CharacterManager>.Instance == null || Singleton<RewardManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;

        if (!rewardManager.IsOutfitReward())
            return;

        string[] heroes = Enum.GetNames(typeof(GameEnums.HERO)).ToArray();
        List<GameEnums.HERO> readys = new List<GameEnums.HERO>();
        GameEnums.HERO reward = GameEnums.HERO.h0;

        for (int i = 0; i < heroes.Length; i++)
        {
            GameEnums.HERO hero = GameEnums.HERO.h0;
            Enum.TryParse(heroes[i], out hero);
            if (hero != GameEnums.HERO.h0 && !characterManager.IsUnlocked(hero))
                readys.Add(hero);
        }

        if (readys.Count <= 0)
            return;
        int result = Random.Range(0, readys.Count);
        reward = readys[result];
        characterManager.Unlock(reward);
        rewardManager.SetOutfitRewardDelay();
        if (UIOutfit.Instance != null)
            UIOutfit.Instance.Reload();

        UIUnlocked.Show();
        UIUnlocked.Instance.InitSlot(GameEnums.ITEM_GROUP.Outfit, reward.ToString(), 1);

        if (characterManager.IsFullUnlock())
            gameObject.SetActive(false);
    }
}