﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyGiftAdapter : ToggleBehaviour
{
    private GameEnums.ITEM_GROUP itemGroup;
    private string item;
    private int amount;
    private int baseIndex;
    private int indexInParent;

    [SerializeField] private Text txtValue;
    [SerializeField] private Image imgReward;
    [SerializeField] private Image imgGifted;

    protected override void OnAppear()
    {
        Eventon<GiftPageEvent>.EventStatic += OnGiftPageEvent;
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<GiftPageEvent>.EventStatic -= OnGiftPageEvent;
    }

    private void OnGiftPageEvent(GiftPageEvent param)
    {
        if (!GetComponent<ObjectPooling>())
            return;
        ObjectPooling objectPooling = GetComponent<ObjectPooling>();

        itemGroup = GameEnums.ITEM_GROUP.General;
        item = null;
        amount = 0;

        if (txtValue != null)
            txtValue.text = null;
        if (imgReward != null)
            imgReward.sprite = null;
        if (imgGifted != null)
            imgGifted.color = Color.clear;
        objectPooling.Return();
    }

    public void Init(int baseIndex, int indexInParent)
    {
        this.baseIndex = baseIndex;
        this.indexInParent = indexInParent;

        if (Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        List<RewardBase> rewardBases = rewardManager.GetRewardBases(this.baseIndex);
        if (rewardBases.Count <= 0)
            return;
        RewardBase rewardBase = rewardBases[this.indexInParent];
        itemGroup = rewardBase.itemGroup;
        item = rewardBase.name;
        amount = rewardBase.amount;

        if (txtValue != null && imgReward != null && Singleton<CharacterManager>.Instance != null &&
            Singleton<WeaponManager>.Instance != null)
        {
            CharacterManager characterManager = Singleton<CharacterManager>.Instance;
            WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
            imgReward.transform.rotation = Quaternion.identity;
            imgReward.color = Color.white;
            txtValue.color = Color.white;
            Sprite spr = null;
            string content = null;
            switch (itemGroup)
            {
                case GameEnums.ITEM_GROUP.General:
                    spr = characterManager.GetItemIcon(rewardBase.name);
                    content = rewardBase.amount.ToString();
                    if (rewardBase.amount >= 1000)
                        content = $"{rewardBase.amount:#,#.}";

                    break;
                case GameEnums.ITEM_GROUP.Outfit:
                    GameEnums.HERO hero = GameEnums.HERO.h0;
                    Enum.TryParse(rewardBase.name, out hero);
                    spr = characterManager.GetPlayerAvatar(hero);
                    break;
                case GameEnums.ITEM_GROUP.Weapon:
                    GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                    Enum.TryParse(rewardBase.name, out weapon);
                    spr = weaponManager.GetSprite(weapon);
                    imgReward.transform.rotation = Quaternion.Euler(0, 0, 45);
                    break;
            }

            txtValue.text = content;
            imgReward.sprite = spr;
            imgReward.SetNativeSize();
            imgReward.transform.localScale = new Vector3(1, 1, 1);

            if (imgGifted != null)
            {
                imgGifted.color = Color.clear;
                if (rewardManager.IsDayGeted(baseIndex))
                    imgGifted.color = Color.white;
            }

            if (baseIndex <= rewardManager.GetLoginDay())
                return;
            imgReward.color = Color.gray;
            txtValue.color = Color.gray;
        }
    }

    public void GetGift()
    {
        if (Singleton<CurrencyManager>.Instance == null || Singleton<CharacterManager>.Instance == null ||
            Singleton<WeaponManager>.Instance == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;

        switch (itemGroup)
        {
            case GameEnums.ITEM_GROUP.General:
                GameEnums.GENERAL general = GameEnums.GENERAL.Gold;
                Enum.TryParse(item, out general);
                switch (general)
                {
                    case GameEnums.GENERAL.Gold:
                        currencyManager.Collect(CURRENCY.COIN, amount);
                        break;

                    case GameEnums.GENERAL.Hp:
                        currencyManager.Collect(CURRENCY.HP_REGEN, amount);
                        break;

                    case GameEnums.GENERAL.PowerX2:
                        currencyManager.Collect(CURRENCY.DOUBLE_POWER, amount);
                        break;

                    case GameEnums.GENERAL.Shield:
                        currencyManager.Collect(CURRENCY.SHIELD, amount);
                        break;
                }

                break;
            case GameEnums.ITEM_GROUP.Outfit:
                GameEnums.HERO hero = GameEnums.HERO.h0;
                Enum.TryParse(item, out hero);
                characterManager.Unlock(hero);
                break;
            case GameEnums.ITEM_GROUP.Weapon:
                GameEnums.WEAPON weapon = GameEnums.WEAPON.w1;
                Enum.TryParse(item, out weapon);
                weaponManager.Unlock(weapon);
                break;
        }

        if (imgGifted != null)
            imgGifted.color = Color.white;

        if (imgReward != null)
            imgReward.color = Color.gray;

        UIUnlocked.Show();
        UIUnlocked uiUnlocked = UIUnlocked.Instance;
        uiUnlocked.InitSlot(itemGroup, item, amount);
    }
}