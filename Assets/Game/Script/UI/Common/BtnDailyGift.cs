﻿using UnityEngine;
using UnityEngine.UI;

public class BtnDailyGift : MonoBehaviour
{
    private Button mButton;
    private int baseIndex;

    [SerializeField] private Transform container;
    [SerializeField] private Text txtDay;
    [SerializeField] private GameObject todayVfx;

    #region property

    private Button Button
    {
        get
        {
            if (mButton == null && GetComponent<Button>())
                mButton = GetComponent<Button>();
            return mButton;
        }
    }

    #endregion

    private void OnEnable()
    {
        if (Button != null)
            Button.onClick.AddListener(GetGift);
    }

    private void GetGift()
    {
        if (container == null || Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;

        for (int i = 0; i < container.childCount; i++)
        {
            if (!container.GetChild(i).GetComponent<DailyGiftAdapter>())
                continue;
            DailyGiftAdapter dailyGiftAdapter = container.GetChild(i).GetComponent<DailyGiftAdapter>();
            dailyGiftAdapter.GetGift();
        }

        if (Button != null)
            Button.interactable = false;
        if (todayVfx != null)
            todayVfx.SetActive(false);

        rewardManager.SaveDailyGiftData(baseIndex);

        if (UIMenu.Instance == null)
            return;
        UIMenu uiMenu = UIMenu.Instance;
        uiMenu.EnableVfxDailyGift();
    }

    private void GiftChecking(int day)
    {
        if (Button == null || txtDay == null || todayVfx == null)
            return;
        Button.interactable = true;
        Button.image.color = Color.white;
        txtDay.text = "Day " + (day + 1);
        txtDay.color = Color.white;

        if (Singleton<RewardManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        if (rewardManager.IsDayGeted(day) || day > rewardManager.GetLoginDay())
        {
            Button.interactable = false;
            todayVfx.SetActive(false);

            if (day > rewardManager.GetLoginDay())
            {
                Button.image.color = Color.grey;
                txtDay.color = Color.grey;
            }
        }
        else
            todayVfx.SetActive(true);
    }

    public void Init(int baseIndex)
    {
        this.baseIndex = baseIndex;
        GiftChecking(baseIndex);
        if (container == null || Singleton<RewardManager>.Instance == null || Singleton<GameManager>.Instance == null)
            return;
        RewardManager rewardManager = Singleton<RewardManager>.Instance;
        GameManager gameManager = Singleton<GameManager>.Instance;
        string path = "Prefab/UI/Common/dailyGiftAdapter";
        if (Resources.Load(path) == null)
            return;
        for (int i = 0; i < rewardManager.GetRewardBases(baseIndex).Count; i++)
        {
            GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, container, path);
            obj.transform.SetParent(container, true);
            obj.transform.localScale = new Vector3(1, 1, 1);
            obj.transform.position = obj.transform.parent.position;

            if (!obj.GetComponent<DailyGiftAdapter>())
                return;
            DailyGiftAdapter dailyGiftAdapter = obj.GetComponent<DailyGiftAdapter>();
            dailyGiftAdapter.Init(baseIndex, i);
        }
    }
}