﻿using UnityEngine;
using UnityEngine.UI;

public class TxtPlayerLevel : MonoBehaviour
{
    private Text mText;

    #region property

    private Text Text
    {
        get
        {
            if (mText == null && GetComponent<Text>())
                mText = GetComponent<Text>();
            return mText;
        }
    }

    #endregion

    private void OnEnable()
    {
        Eventon<HeroData>.EventStatic += OnHeroData;
    }

    private void Start()
    {
        OnDisplay();
    }

    private void OnDisable()
    {
        Eventon<HeroData>.EventStatic -= OnHeroData;
    }

    private void OnHeroData(HeroData param)
    {
        OnDisplay();
    }

    private void OnDisplay()
    {
        if (Text == null || Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        int currenLevel = characterManager.GetHeroLevel() + 1;
        int maxLevel = characterManager.GetHeroBases();
        Text.text = "Lv." + currenLevel + "/" + maxLevel;
    }
}