﻿using UnityEngine;
using UnityEngine.UI;

public class PageDot : MonoBehaviour
{
    private Image mImage;

    [SerializeField] private Text txtNumber;
    [SerializeField] private Image imgSelected;
    [SerializeField] private Sprite sprUnlocked;
    [SerializeField] private Sprite sprLocking;

    #region property

    private Image Image
    {
        get
        {
            if (mImage == null && GetComponent<Image>())
                mImage = GetComponent<Image>();
            return mImage;
        }
    }

    #endregion

    public void OnDisplay(int number)
    {
        if (txtNumber != null)
            txtNumber.text = null;
        if (imgSelected == null || Image == null || Singleton<StageManager>.Instance == null)
            return;
        StageManager stageManager = Singleton<StageManager>.Instance;
        if (sprUnlocked != null)
            Image.sprite = sprUnlocked;
        imgSelected.gameObject.SetActive(false);
        if (number == stageManager.GetStage())
        {
            if (txtNumber != null)
                txtNumber.text = (number + 1).ToString();
            if (number == stageManager.GetStage())
                imgSelected.gameObject.SetActive(true);
        }
        else if (number > stageManager.GetStage())
        {
            if (sprLocking != null)
                Image.sprite = sprLocking;
        }
    }
}