﻿using UnityEngine;
using UnityEngine.UI;

public class BoosterAdapter : MonoBehaviour
{
    [SerializeField] private GameEnums.BOOSTER booster;
    [SerializeField] private Text txtNumber;
    [SerializeField] private Text txtPrice;
    [SerializeField] private Button btnBuy;
    [SerializeField] private Button btnReward;

    private void OnEnable()
    {
        Display();

        if (btnBuy != null)
            btnBuy.onClick.AddListener(Buy);
        if (btnReward != null)
            btnReward.onClick.AddListener(Reward);

        Eventon<CurrencyData>.EventStatic += OnCurrencyData;
    }

    private void OnDisable()
    {
        Eventon<CurrencyData>.EventStatic -= OnCurrencyData;
    }

    private void OnCurrencyData(CurrencyData param)
    {
        Display();
    }

    private void Display()
    {
        if (Singleton<CurrencyManager>.Instance == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        int number = 0;
        int price = 0;
        switch (booster)
        {
            case GameEnums.BOOSTER.HP_REGEN:
                number = currencyManager.GetCurrency(CURRENCY.HP_REGEN);
                price = GameConfig.hpRegenPrice;
                break;
            case GameEnums.BOOSTER.DOUBLE_POWER:
                number = currencyManager.GetCurrency(CURRENCY.DOUBLE_POWER);
                price = GameConfig.doublePowerPrice;
                break;
            case GameEnums.BOOSTER.SHIELD:
                number = currencyManager.GetCurrency(CURRENCY.SHIELD);
                price = GameConfig.shieldPrice;
                break;
        }

        if (txtNumber != null)
            txtNumber.text = number.ToString();

        string priceContent = $"{price:#,#.}";
        if (txtPrice != null)
            txtPrice.text = priceContent;
    }

    private void Buy()
    {
        if (btnBuy == null || Singleton<CurrencyManager>.Instance == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        int curren = currencyManager.GetCurrency(CURRENCY.COIN);
        int price = 0;
        switch (booster)
        {
            case GameEnums.BOOSTER.HP_REGEN:
                price = GameConfig.hpRegenPrice;
                break;
            case GameEnums.BOOSTER.DOUBLE_POWER:
                price = GameConfig.doublePowerPrice;
                break;
            case GameEnums.BOOSTER.SHIELD:
                price = GameConfig.shieldPrice;
                break;
        }

        if (curren < price)
            return;
        currencyManager.Pay(CURRENCY.COIN, price);
        switch (booster)
        {
            case GameEnums.BOOSTER.HP_REGEN:
                currencyManager.Collect(CURRENCY.HP_REGEN, 1);
                UIShop.Instance.SetBuyTransform(CURRENCY.HP_REGEN, btnBuy.transform, 1);
                break;
            case GameEnums.BOOSTER.DOUBLE_POWER:
                currencyManager.Collect(CURRENCY.DOUBLE_POWER, 1);
                UIShop.Instance.SetBuyTransform(CURRENCY.DOUBLE_POWER, btnBuy.transform, 1);
                break;
            case GameEnums.BOOSTER.SHIELD:
                currencyManager.Collect(CURRENCY.SHIELD, 1);
                UIShop.Instance.SetBuyTransform(CURRENCY.SHIELD, btnBuy.transform, 1);
                break;
        }

        UIShop.Instance.SetBuyVfx();
    }

    private void Reward()
    {
        if (btnReward == null || Singleton<CurrencyManager>.Instance == null)
            return;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;
        AdReward.Show();
        if (AdController.IsConnecting())
            btnReward.interactable = false;
        Eventon<AD_STATE>.EventDynamic += state =>
        {
            btnReward.interactable = true;
            if (state != AD_STATE.FINISHED)
                return;
            switch (booster)
            {
                case GameEnums.BOOSTER.HP_REGEN:
                    currencyManager.Collect(CURRENCY.HP_REGEN, 1);
                    UIShop.Instance.SetBuyTransform(CURRENCY.HP_REGEN, btnReward.transform, 1);
                    break;
                case GameEnums.BOOSTER.DOUBLE_POWER:
                    currencyManager.Collect(CURRENCY.DOUBLE_POWER, 1);
                    UIShop.Instance.SetBuyTransform(CURRENCY.DOUBLE_POWER, btnReward.transform, 1);
                    break;
                case GameEnums.BOOSTER.SHIELD:
                    currencyManager.Collect(CURRENCY.SHIELD, 1);
                    UIShop.Instance.SetBuyTransform(CURRENCY.SHIELD, btnReward.transform, 1);
                    break;
            }

            UIShop.Instance.SetBuyVfx();
        };
    }
}