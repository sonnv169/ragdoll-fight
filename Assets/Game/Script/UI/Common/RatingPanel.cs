﻿using UnityEngine;
using UnityEngine.UI;

public class RatingPanel : MonoBehaviour
{
    [SerializeField] private Text txtTitle;
    [SerializeField] private Transform gridStar;

    private void OnEnable()
    {
        OnInit();
    }

    private void OnInit()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        bool isRate = gameManager.IsRated();
        int rated = gameManager.GetRated();

        if (isRate && txtTitle != null)
            txtTitle.text = "Thanks for your feedback!";

        if (gridStar == null)
            return;
        for (int i = 0; i < gridStar.childCount; i++)
        {
            if (!gridStar.GetChild(i).GetComponent<Button>())
                continue;
            Button btn = gridStar.GetChild(i).GetComponent<Button>();
            if (i > rated)
                btn.image.color = Color.gray;
            else
            {
                btn.image.color = Color.white;
                btn.interactable = false;
            }
        }
    }

    public void PushIndex(int index)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        bool isRate = gameManager.IsRated();
        if (isRate)
            return;
        if (txtTitle != null)
            txtTitle.text = "Thanks for your feedback!";
        for (int i = 0; i < gridStar.childCount; i++)
        {
            if (!gridStar.GetChild(i).GetComponent<Button>())
                continue;
            Button btn = gridStar.GetChild(i).GetComponent<Button>();
            btn.interactable = false;
            if (i > index)
                btn.image.color = Color.gray;
            else
                btn.image.color = Color.white;
        }

        if (index >= GameConfig.minRateToStore - 1)
            Application.OpenURL(GameConfig.storeLink);

        gameManager.EnableRating(false, index);
        if (UIRate.Instance != null)
            UIRate.Instance.OnClose();
    }
}