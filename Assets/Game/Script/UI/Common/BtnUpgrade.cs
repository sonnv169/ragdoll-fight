﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BtnUpgrade : MonoBehaviour
{
    private Button mButton;

    [SerializeField] private Text txtUpgradePrice;
    [SerializeField] private GameObject[] vfxUpgrades;

    #region property

    private Button Button
    {
        get
        {
            if (mButton == null && GetComponent<Button>())
                mButton = GetComponent<Button>();
            return mButton;
        }
    }

    #endregion

    private void Start()
    {
        EnableVfxUpgrades(false);

        if (Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        int currenLevel = characterManager.GetHeroLevel();
        int nextLevel = 0;
        int upgradePrice = 0;
        string priceContent = null;

        if (characterManager.IsMaxLevel())
            gameObject.SetActive(false);
        else
        {
            nextLevel = currenLevel + 1;
            upgradePrice = characterManager.GetHeroGoldUpgrade(nextLevel);
            priceContent = $"{upgradePrice:#,#.}";
        }

        if (txtUpgradePrice != null)
            txtUpgradePrice.text = priceContent;
    }

    private void EnableVfxUpgrades(bool isStatus)
    {
        for (int i = 0; i < vfxUpgrades.Length; i++)
        {
            if (vfxUpgrades[i] != null)
                vfxUpgrades[i].SetActive(isStatus);
        }
    }

    private IEnumerator DelayDisableInputBlocker()
    {
        yield return new WaitForSeconds(1.0f);
        if (UIMenu.Instance != null)
            UIMenu.Instance.EnableBlocker(false);
    }

    public void OnUpgrade()
    {
        if (Singleton<CharacterManager>.Instance == null || Singleton<CurrencyManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        CurrencyManager currencyManager = Singleton<CurrencyManager>.Instance;

        if (characterManager.IsMaxLevel())
            return;
        int currenLevel = characterManager.GetHeroLevel();
        int coin = currencyManager.GetCurrency(CURRENCY.COIN);
        int nextLevel = currenLevel + 1;

        if (nextLevel > characterManager.GetHeroBases())
            nextLevel = characterManager.GetHeroBases();
        int upgradePrice = characterManager.GetHeroGoldUpgrade(nextLevel);

        if (coin >= upgradePrice)
        {
            currencyManager.Pay(CURRENCY.COIN, upgradePrice);
            characterManager.Uplevel(1);
            EnableVfxUpgrades(true);

            if (UIMenu.Instance != null)
            {
                UIMenu.Instance.EnableBlocker(true);
                StartCoroutine(DelayDisableInputBlocker());
            }

            if (txtUpgradePrice != null)
            {
                string priceContent = null;
                currenLevel = characterManager.GetHeroLevel();
                if (characterManager.IsMaxLevel())
                    gameObject.SetActive(false);
                else
                {
                    nextLevel = currenLevel + 1;
                    if (nextLevel > characterManager.GetHeroBases())
                        nextLevel = characterManager.GetHeroBases();
                    upgradePrice = characterManager.GetHeroGoldUpgrade(nextLevel);
                    priceContent = $"{upgradePrice:#,#.}";
                }

                txtUpgradePrice.text = priceContent;
            }

            if (UIMenu.Instance != null)
                UIMenu.Instance.DisableUpgradeTutorialPanel();

            if (Singleton<TutorialManager>.Instance != null)
            {
                TutorialManager tutorialManager = Singleton<TutorialManager>.Instance;
                tutorialManager.GetTutorialData().isUpgrade = true;
                tutorialManager.SaveTutorialData();
            }

            SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_ui_upgrade, false);
        }
        else
        {
            UIShop.Show();
            if (UIMenu.Instance != null)
                UIMenu.Instance.EnableBlocker(true);
        }
    }
}