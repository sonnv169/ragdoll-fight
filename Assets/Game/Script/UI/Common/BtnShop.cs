﻿using UnityEngine;
using UnityEngine.UI;

public class BtnShop : MonoBehaviour
{
    private Button mButton;

    #region property

    private Button Button
    {
        get
        {
            if (mButton == null && GetComponent<Button>())
                mButton = GetComponent<Button>();
            return mButton;
        }
    }

    #endregion

    private void OnEnable()
    {
        if (Button != null)
            Button.onClick.AddListener(() =>
            {
                UIShop.Show();
                UIMenu.Instance.EnableBlocker(true);
            });
    }
}