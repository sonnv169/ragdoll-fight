﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[System.Serializable]
public enum AUDIO_KIND
{
    SOUND,
    MUSIC,
    ALL
}

[System.Serializable]
public class AudioData
{
    public bool isSound;
    public bool isMusic;
    public float sound;
    public float music;
}

[System.Serializable]
public class LoopAudioClip
{
    public AUDIO_KIND loopKind;
    public AudioClip loopClip;
    public Coroutine loopCoroutine;
}

public class AudioManager : MonoBehaviour
{
    private static AudioManager mInstance;
    private Coroutine replayCoroutine;
    private AudioData audioData;
    private List<LoopAudioClip> loopAudioClips = new List<LoopAudioClip>();
    private bool isVisible;

    public AudioSource sound;
    public AudioSource music;

    #region property

    public static AudioManager Instance
    {
        get
        {
            if (mInstance == null)
                mInstance = FindObjectOfType<AudioManager>();
            return mInstance;
        }
    }

    #endregion

    private void OnEnable()
    {
        if (!isVisible)
        {
            Init();
            isVisible = true;
        }
    }

    private bool IsAudioSourceNull(AudioSource audio)
    {
        return audio == null;
    }

    private bool IsAudioClipNull(AudioClip clip)
    {
        return clip == null;
    }

    private bool IsLoopAudioClipNull(AUDIO_KIND kind, AudioClip clip)
    {
        bool isResult = false;
        if (loopAudioClips.Count > 0)
        {
            for (int i = 0; i < loopAudioClips.Count; i++)
            {
                if (!loopAudioClips[i].loopKind.Equals(kind) ||
                    !loopAudioClips[i].loopClip.Equals(clip))
                    continue;
                isResult = true;
            }
        }

        return isResult;
    }

    private void Load()
    {
        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "audio_data.xml"))
            audioData = XmlPath.GetByPlatform<AudioData>("audio_data");

        if (!IsAudioSourceNull(music))
        {
            music.mute = !audioData.isMusic;
            music.volume = audioData.music;
        }

        if (!IsAudioSourceNull(sound))
        {
            sound.mute = !audioData.isSound;
            sound.volume = audioData.sound;
        }
    }

    private void Init()
    {
        if (sound != null)
            sound.mute = true;
        if (music != null)
            music.mute = true;
        audioData = new AudioData()
        {
            isSound = true,
            isMusic = true,
            sound = 1,
            music = 1
        };
        Load();
    }

    public void Save()
    {
        XmlPath.SetByPlatform("audio_data", audioData);
        Eventon<AudioData>.Trigger(audioData);
    }

    private void StopAllLoopAudioClip(AUDIO_KIND kind)
    {
        if (loopAudioClips.Count <= 0)
            return;
        for (int i = 0; i < loopAudioClips.Count; i++)
        {
            if (!loopAudioClips[i].loopKind.Equals(kind) ||
                loopAudioClips[i].loopCoroutine == null)
                continue;
            StopCoroutine(loopAudioClips[i].loopCoroutine);
            loopAudioClips.Remove(loopAudioClips[i]);
        }
    }

    private IEnumerator LoopPlay(AUDIO_KIND kind, AudioClip clip, float time)
    {
        yield return new WaitForSecondsRealtime(time);
        switch (kind)
        {
            case AUDIO_KIND.SOUND:
                if (!IsAudioSourceNull(sound) &&
                    !IsAudioClipNull(clip))
                    sound.PlayOneShot(clip);
                break;

            case AUDIO_KIND.MUSIC:
                if (!IsAudioSourceNull(music) &&
                    !IsAudioClipNull(clip))
                    music.PlayOneShot(clip);
                break;

            case AUDIO_KIND.ALL:
                if (!IsAudioSourceNull(music) &&
                    !IsAudioSourceNull(sound) &&
                    !IsAudioClipNull(clip))
                {
                    sound.PlayOneShot(clip);
                    music.PlayOneShot(clip);
                }

                break;
        }

        AddLoopAudioClip(kind, clip, time);
    }

    private void AddLoopAudioClip(AUDIO_KIND kind, AudioClip clip, float time)
    {
        if (IsLoopAudioClipNull(kind, clip))
        {
            LoopAudioClip loopBase = new LoopAudioClip()
            {
                loopKind = kind,
                loopClip = clip,
                loopCoroutine = StartCoroutine(LoopPlay(kind, clip, time))
            };
            loopAudioClips.Add(loopBase);
        }
        else
        {
            for (int i = 0; i < loopAudioClips.Count; i++)
            {
                if (!loopAudioClips[i].loopKind.Equals(kind) ||
                    !loopAudioClips[i].loopClip.Equals(clip))
                    continue;
                loopAudioClips[i].loopCoroutine = StartCoroutine(LoopPlay(kind, clip, time));
            }
        }
    }

    private IEnumerator OnReplay(AUDIO_KIND kind, AudioClip clip)
    {
        yield return new WaitForSeconds(clip.length);
        switch (kind)
        {
            case AUDIO_KIND.SOUND:
                if (sound != null)
                {
                    sound.clip = clip;
                    sound.PlayOneShot(clip);
                }

                break;

            case AUDIO_KIND.MUSIC:
                if (music != null)
                {
                    music.clip = clip;
                    music.PlayOneShot(clip);
                }

                break;
        }

        replayCoroutine = StartCoroutine(OnReplay(kind, clip));
    }

    public void Play(AUDIO_KIND kind, AudioClip clip, bool isLoop)
    {
        switch (kind)
        {
            case AUDIO_KIND.SOUND:
                if (IsAudioSourceNull(sound) ||
                    IsAudioClipNull(clip))
                    return;
                sound.PlayOneShot(clip);
                break;

            case AUDIO_KIND.MUSIC:
                if (IsAudioSourceNull(music) ||
                    IsAudioClipNull(clip))
                    return;
                music.clip = clip;
                music.PlayOneShot(clip);
                break;

            case AUDIO_KIND.ALL:
                if (IsAudioSourceNull(music) ||
                    IsAudioSourceNull(sound) ||
                    IsAudioClipNull(clip))
                    return;
                sound.PlayOneShot(clip);
                music.PlayOneShot(clip);
                break;
        }

        if (isLoop)
            replayCoroutine = StartCoroutine(OnReplay(kind, clip));
    }

    public void Stop(AUDIO_KIND kind)
    {
        switch (kind)
        {
            case AUDIO_KIND.SOUND:
                if (IsAudioSourceNull(sound))
                    return;
                sound.Stop();
                StopAllLoopAudioClip(AUDIO_KIND.SOUND);
                break;

            case AUDIO_KIND.MUSIC:
                if (IsAudioSourceNull(music))
                    return;
                music.Stop();
                StopAllLoopAudioClip(AUDIO_KIND.MUSIC);
                break;

            case AUDIO_KIND.ALL:
                if (IsAudioSourceNull(music) ||
                    IsAudioSourceNull(sound))
                    return;
                music.Stop();
                sound.Stop();
                StopAllLoopAudioClip(AUDIO_KIND.SOUND);
                StopAllLoopAudioClip(AUDIO_KIND.MUSIC);
                break;
        }

        if (replayCoroutine != null)
            StopCoroutine(replayCoroutine);
    }

    public void OnAudioInverse(AUDIO_KIND kind)
    {
        switch (kind)
        {
            case AUDIO_KIND.SOUND:
                if (IsAudioSourceNull(sound))
                    return;
                audioData.isSound = !audioData.isSound;
                break;

            case AUDIO_KIND.MUSIC:
                if (IsAudioSourceNull(music))
                    return;
                audioData.isMusic = !audioData.isMusic;
                break;

            case AUDIO_KIND.ALL:
                if (IsAudioSourceNull(music) ||
                    IsAudioSourceNull(sound))
                    return;
                audioData.isSound = !audioData.isSound;
                audioData.isMusic = !audioData.isMusic;
                break;
        }

        Save();
    }

    public void SetMusicVolume(float value)
    {
        if (music == null)
            return;
        music.volume = value;
        if (audioData != null)
            audioData.music = value;
        Save();
    }

    public void SetSoundVolume(float value)
    {
        if (sound == null)
            return;
        sound.volume = value;
        if (audioData != null)
            audioData.sound = value;
        Save();
    }

    public AudioData GetAudiosData()
    {
        AudioData cloneData = new AudioData()
        {
            isSound = true,
            isMusic = true
        };
        if (audioData != null)
            cloneData = audioData;
        return cloneData;
    }

    public AudioSource GetSound()
    {
        return sound;
    }

    public AudioSource GetMusic()
    {
        return music;
    }
}