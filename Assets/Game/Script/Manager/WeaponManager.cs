﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class WeaponData
{
    public GameEnums.WEAPON equipted;
    public List<GameEnums.WEAPON> unlockeds = new List<GameEnums.WEAPON>();
}

public class WeaponManager : ToggleBehaviour
{
    private WeaponData weaponData;
    private GameEnums.WEAPON weaponRandom;
    private bool isVisible;

    [SerializeField] private WeaponDataset weaponDataset;

    protected override void OnAppear()
    {
        if (!isVisible)
        {
            Init();
            isVisible = true;
        }
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        //nothing
    }

    private void Init()
    {
        weaponRandom = GameEnums.WEAPON.w1;
        Load();
    }

    private void Load()
    {
        weaponData = new WeaponData()
        {
            equipted = GameEnums.WEAPON.w1
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "weapon_data.xml"))
            weaponData = XmlPath.GetByPlatform<WeaponData>("weapon_data");

        if (Singleton<GameManager>.Instance != null)
        {
            GameManager gameManager = Singleton<GameManager>.Instance;
            switch (gameManager.IsTest())
            {
                case true:
                    string[] weapons = Enum.GetNames(typeof(GameEnums.WEAPON)).ToArray();
                    for (int i = 0; i < weapons.Length; i++)
                    {
                        GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                        Enum.TryParse(weapons[i], out weapon);
                        weaponData.unlockeds.Add(weapon);
                    }

                    break;

                case false:
                    weaponData.unlockeds.Add(GameEnums.WEAPON.w1);
                    break;
            }
        }
    }

    private void Save()
    {
        XmlPath.SetByPlatform("weapon_data", weaponData);
        Eventon<WeaponData>.Trigger(weaponData);
    }

    public bool IsUnlocked(GameEnums.WEAPON weapon)
    {
        return weaponData != null && weaponData.unlockeds.Contains(weapon);
    }

    public void Equipt(GameEnums.WEAPON weapon)
    {
        if (!IsUnlocked(weapon))
            return;
        weaponData.equipted = weapon;
        Save();

        SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_ui_equipt, false);
    }

    public void Unlock(GameEnums.WEAPON weapon)
    {
        if (IsUnlocked(weapon))
            return;
        weaponData.equipted = weapon;
        weaponData.unlockeds.Add(weapon);
        Save();
    }

    public void SetWeaponRandom(GameEnums.WEAPON weapon)
    {
        weaponRandom = weapon;
    }

    public GameObject SetWeapon(GameEnums.WEAPON weapon, Vector3 position)
    {
        GameObject obj = null;
        string path = "Prefab/Object/Weapon/" + weapon;
        if (Resources.Load(path) == null)
            path = "Prefab/Object/Weapon/w1";
        obj = ObjectManager.Call<GameObject>(transform, transform, path);
        obj.transform.position = position;
        return obj;
    }

    public GameEnums.WEAPON GetEquipted()
    {
        GameEnums.WEAPON weapon = GameEnums.WEAPON.w1;
        if (weaponData != null)
            weapon = weaponData.equipted;
        return weapon;
    }

    public List<GameEnums.WEAPON> GetUnlockeds()
    {
        List<GameEnums.WEAPON> weapons = new List<GameEnums.WEAPON>();

        if (weaponData != null)
        {
            for (int i = 0; i < weaponData.unlockeds.Count; i++)
            {
                weapons.Add(weaponData.unlockeds[i]);
            }
        }

        return weapons;
    }

    public GameEnums.WEAPON GetWeaponRandom()
    {
        return weaponRandom;
    }

    public int GetVip(GameEnums.WEAPON weapon)
    {
        int vip = 0;
        if (weaponDataset != null)
            vip = weaponDataset.GetVip(weapon);
        return vip;
    }

    public Sprite GetSprite(GameEnums.WEAPON weapon)
    {
        Sprite spr = null;
        if (weaponDataset != null)
            spr = weaponDataset.GetSprite(weapon);
        return spr;
    }

    public string GetName(GameEnums.WEAPON weapon)
    {
        string name = "weapon";
        if (weaponDataset != null)
            name = weaponDataset.GetName(weapon);
        return name;
    }

    public float GetCooldown(GameEnums.WEAPON weapon)
    {
        float number = 0;
        if (weaponDataset != null)
            number = weaponDataset.GetCountDown(weapon);
        return number;
    }

    public int GetBonusDamage(GameEnums.WEAPON weapon)
    {
        int number = 0;
        if (weaponDataset != null)
            number = weaponDataset.GetBonusDamage(weapon);
        return number;
    }

    public int GetPrice(GameEnums.WEAPON weapon)
    {
        int number = 0;
        if (weaponDataset != null)
            number = weaponDataset.GetPrice(weapon);
        return number;
    }

    public bool IsFullUnlock()
    {
        string[] weapons = Enum.GetNames(typeof(GameEnums.WEAPON)).ToArray();
        return weaponData != null && weaponData.unlockeds.Count >= weapons.Length;
    }
}