﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[System.Serializable]
public enum CHARACTER
{
    NONE,
    PLAYER,
    ENEMY,
    TRAP
}

[System.Serializable]
public class HeroData
{
    public int level;
    public GameEnums.HERO equipted;
    public List<GameEnums.HERO> unlockeds = new List<GameEnums.HERO>();
}

public class CharacterManager : ToggleBehaviour
{
    private List<Transform> transforms = new List<Transform>();
    private HeroData heroData;
    private bool isVisible;

    [SerializeField] private HeroDataset heroDataset;
    [SerializeField] private EnemyAIDataset enemyAiDataset;
    [SerializeField] private AvatarDataset avatarDataset;

    protected override void OnAppear()
    {
        if (!isVisible)
        {
            Load();
            Eventon<GAME_STATE>.EventStatic += OnGameState;
            Eventon<UnitDeathEvent>.EventStatic += OnUnitDeathEvent;
            isVisible = true;
        }
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
        Eventon<UnitDeathEvent>.EventStatic -= OnUnitDeathEvent;
    }

    private void OnUnitDeathEvent(UnitDeathEvent param)
    {
        for (int i = 0; i < transforms.Count; i++)
        {
            if (transforms.Contains(param.unit.transform))
                transforms.Remove(param.unit.transform);
        }
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                transforms.Clear();
                break;
        }
    }

    private void Load()
    {
        heroData = new HeroData()
        {
            level = 0,
            equipted = GameEnums.HERO.h1
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "hero_data.xml"))
            heroData = XmlPath.GetByPlatform<HeroData>("hero_data");

        if (Singleton<GameManager>.Instance != null)
        {
            GameManager gameManager = Singleton<GameManager>.Instance;
            switch (gameManager.IsTest())
            {
                case true:
                    string[] heros = Enum.GetNames(typeof(GameEnums.HERO)).ToArray();
                    string temp = "h";
                    for (int i = 0; i < heros.Length; i++)
                    {
                        GameEnums.HERO hero = GameEnums.HERO.h1;
                        Enum.TryParse(temp + i, out hero);
                        heroData.unlockeds.Add(hero);
                    }

                    break;

                case false:
                    heroData.unlockeds.Add(GameEnums.HERO.h1);
                    break;
            }
        }
    }

    private void Save()
    {
        XmlPath.SetByPlatform("hero_data", heroData);
        Eventon<HeroData>.Trigger(heroData);
    }

    public bool IsUnlocked(GameEnums.HERO hero)
    {
        return heroData != null && heroData.unlockeds.Contains(hero);
    }

    public void Uplevel(int value)
    {
        if (heroData == null)
            return;
        heroData.level += value;
        Save();
    }

    public void Equipt(GameEnums.HERO hero)
    {
        if (!IsUnlocked(hero))
            return;
        heroData.equipted = hero;
        Save();

        SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_ui_equipt, false);
    }

    public void Unlock(GameEnums.HERO hero)
    {
        if (IsUnlocked(hero))
            return;
        heroData.unlockeds.Add(hero);
        Equipt(hero);
    }

    public GameObject SetCharacter(CHARACTER character, Vector3 position)
    {
        GameObject obj = null;
        string path = null;
        switch (character)
        {
            case CHARACTER.PLAYER:
                path = "Prefab/Object/Hero/" + heroData.equipted;
                break;

            case CHARACTER.ENEMY:
                if (Singleton<StageManager>.Instance != null)
                {
                    StageManager stageManager = Singleton<StageManager>.Instance;
                    path = "Prefab/Object/Enemy/" + stageManager.GetEnemyID(stageManager.GetWave());
                    if (Resources.Load(path) == null)
                        path = "Prefab/Object/Enemy/e1";
                }

                break;
        }

        obj = ObjectManager.Call<GameObject>(transform, transform, path);
        obj.transform.localScale = GameConfig.modelIngameScale;
        obj.transform.position = position;

        switch (character)
        {
            case CHARACTER.PLAYER:
                break;
            case CHARACTER.ENEMY:
                transforms.Add(obj.transform);
                break;
        }

        return obj;
    }

    public GameObject SetOtherPlayer(GameEnums.HERO hero, Vector3 position)
    {
        GameObject obj = null;
        string path = "Prefab/Object/Hero/" + hero;

        if (Resources.Load(path) != null)
        {
            obj = ObjectManager.Call<GameObject>(transform, transform, path);
            obj.transform.localScale = GameConfig.modelIngameScale;
            obj.transform.position = position;
        }

        return obj;
    }

    public List<Transform> GetTransforms()
    {
        return transforms;
    }

    public GameEnums.HERO GetHero()
    {
        GameEnums.HERO hero = GameEnums.HERO.h1;
        if (heroData != null)
            hero = heroData.equipted;
        return hero;
    }

    public Sprite GetItemIcon(string itemName)
    {
        Sprite spr = null;
        if (avatarDataset != null)
            spr = avatarDataset.GetItemIcon(itemName);

        return spr;
    }

    public int GetVip(GameEnums.HERO hero)
    {
        int vip = 0;
        if (avatarDataset != null)
            vip = avatarDataset.GetVip(hero);
        return vip;
    }

    public GameEnums.AI GetEnemyAI(int stage)
    {
        GameEnums.AI ai = GameEnums.AI.ai1;
        if (enemyAiDataset != null && stage < enemyAiDataset.GetAIBase())
            ai = enemyAiDataset.GetAI(stage);
        return ai;
    }

    public Sprite GetPlayerAvatar(GameEnums.HERO hero)
    {
        Sprite spr = null;
        if (avatarDataset != null)
            spr = avatarDataset.GetPlayerAvatar(hero);
        return spr;
    }

    public Sprite GetEnemyAvatar(GameEnums.ENEMY enemy)
    {
        Sprite spr = null;
        if (avatarDataset != null)
            spr = avatarDataset.GetEnemyAvatar(enemy);
        return spr;
    }

    public string GetPlayerName(GameEnums.HERO hero)
    {
        string player = "player";
        if (avatarDataset != null)
            player = avatarDataset.GetPlayerName(hero);
        return player;
    }

    public float GetEnemyCooldownAttack(GameEnums.AI ai)
    {
        float cooldown = 0;
        if (enemyAiDataset != null)
            cooldown = enemyAiDataset.GetCountDownAttack(ai);
        return cooldown;
    }

    public float GetEnemyFocusRate(GameEnums.AI ai)
    {
        float rate = 0;
        if (enemyAiDataset != null)
            rate = enemyAiDataset.GetFocusPercent(ai);
        return rate;
    }

    public float GetEnemySpeed(GameEnums.AI ai)
    {
        float speed = 0;
        if (enemyAiDataset != null)
            speed = enemyAiDataset.GetSpeed(ai);
        return speed;
    }

    public List<GameEnums.HERO> GetUnlockeds()
    {
        List<GameEnums.HERO> heroes = new List<GameEnums.HERO>();

        if (heroData != null)
        {
            for (int i = 0; i < heroData.unlockeds.Count; i++)
            {
                heroes.Add(heroData.unlockeds[i]);
            }
        }

        return heroes;
    }

    public int GetTransform()
    {
        return transforms.Count;
    }

    public int GetHeroLevel()
    {
        return heroData.level;
    }

    public int GetHeroAtk(int level)
    {
        int number = 0;
        if (heroDataset != null)
            number = heroDataset.GetAtk(level);
        return number;
    }

    public int GetHeroHp(int level)
    {
        int number = 0;
        if (heroDataset != null)
            number = heroDataset.GetHp(level);
        return number;
    }

    public int GetHeroGoldUpgrade(int level)
    {
        int number = 0;
        if (heroDataset != null)
            number = heroDataset.GetGoldUpgrade(level);
        return number;
    }

    public int GetHeroBases()
    {
        int number = 0;
        if (heroDataset != null)
            number = heroDataset.GetBases();
        return number;
    }

    public int GetPrice(GameEnums.HERO hero)
    {
        int number = 0;
        if (avatarDataset != null)
            number = avatarDataset.GetPrice(hero);
        return number;
    }

    public bool IsMaxLevel()
    {
        return heroData != null && heroDataset != null && heroData.level >= heroDataset.GetBases() - 1;
    }

    public bool IsFullUnlock()
    {
        string[] heros = Enum.GetNames(typeof(GameEnums.HERO)).ToArray();
        return heroData != null && heroData.unlockeds.Count >= heros.Length;
    }
}