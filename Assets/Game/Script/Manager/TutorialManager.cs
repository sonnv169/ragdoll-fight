﻿using System.IO;
using UnityEngine;

[System.Serializable]
public class TutorialData
{
    public bool isGift;
    public bool isFight;
    public bool isPlay;
    public bool isMove;
    public bool isUpgrade;
    public bool isBooster;
}

public class TutorialManager : MonoBehaviour
{
    private TutorialData tutorialData;
    private bool isVisible;

    private void OnEnable()
    {
        if (!isVisible)
        {
            LoadTutorialData();
            Eventon<TutorialData>.EventStatic += OnTutorialData;
            isVisible = true;
        }
    }

    private void OnDisable()
    {
        Eventon<TutorialData>.EventStatic -= OnTutorialData;
    }

    private void OnTutorialData(TutorialData param)
    {
        if (UIMenu.Instance != null || !param.isPlay || tutorialData.isMove ||
            Singleton<GameManager>.Instance == null ||
            Singleton<Player>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        Player player = Singleton<Player>.Instance;
        string path = "Prefab/Object/Vfx/vfx_move_tutorial";
        if (Resources.Load(path) == null)
            return;
        GameObject obj = ObjectManager.Call<GameObject>(gameManager.transform, transform, path);
        obj.transform.position = new Vector2(player.transform.position.x,
            player.transform.position.y + GameConfig.characterStartRange);
    }

    private void LoadTutorialData()
    {
        tutorialData = new TutorialData();
        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "tutorial_data.xml"))
            tutorialData = XmlPath.GetByPlatform<TutorialData>("tutorial_data");

        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (!gameManager.IsTest())
            return;
        tutorialData.isGift = true;
        tutorialData.isFight = true;
        tutorialData.isPlay = true;
        tutorialData.isMove = true;
        tutorialData.isUpgrade = true;
        tutorialData.isBooster = true;
    }

    public void SaveTutorialData()
    {
        XmlPath.SetByPlatform("tutorial_data", tutorialData);
        Eventon<TutorialData>.Trigger(tutorialData);
    }

    public TutorialData GetTutorialData()
    {
        return tutorialData;
    }
}