﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public enum SALE_FEATURE
{
    SKIN_FREE,
    LUCKY_GIFT,
//    LUCKY_SPIN,
    LUCKY_BUY
}

[System.Serializable]
public class SaleFeatureData
{
    public SALE_FEATURE feature;
}

[System.Serializable]
public class LuckyGiftData
{
    public int round;
}

[System.Serializable]
public class LuckyGiftRound
{
    public LUCKY_GIFT_GROUP[] groups;
}

[System.Serializable]
public class LuckyBuyData
{
    public int round;
}

[System.Serializable]
public class LuckySpinGroup
{
    public LUCKY_SPIN_GROUP luckySpinGroup;
    public int round;
}

[System.Serializable]
public class LuckySpinData
{
    public List<LuckySpinGroup> groups = new List<LuckySpinGroup>();
}

public class SaleFeatureManager : MonoBehaviour
{
    private SaleFeatureData saleFeatureData;
    private LuckyGiftData luckyGiftData;
    private LuckyBuyData luckyBuyData;
    private LuckySpinData luckySpinData;
    private bool isVisible;

    [SerializeField] private LuckyGiftDataset luckyGiftDataset;
    [SerializeField] private LuckyGiftRound[] luckyGiftRounds;
    [SerializeField] private LuckyBuyDataset luckyBuyDataset;
    [SerializeField] private LuckySpinDataset luckySpinDataset;

    private void OnEnable()
    {
        if (!isVisible)
        {
            LoadSaleFeatureData();
            LoadLuckyGiftData();
            LoadLuckyBuyData();
            LoadLuckySpinData();
            isVisible = true;
        }
    }

    private void LoadSaleFeatureData()
    {
        saleFeatureData = new SaleFeatureData()
        {
            feature = SALE_FEATURE.SKIN_FREE
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "sale_feature_data.xml"))
            saleFeatureData = XmlPath.GetByPlatform<SaleFeatureData>("sale_feature_data");
    }

    private void SaveSaleFeatureData()
    {
        XmlPath.SetByPlatform("sale_feature_data", saleFeatureData);
        Eventon<SaleFeatureData>.Trigger(saleFeatureData);
    }

    private void LoadLuckyGiftData()
    {
        luckyGiftData = new LuckyGiftData()
        {
            round = 0
        };

        if (!File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "lucky_gift_data.xml"))
            return;
        luckyGiftData = XmlPath.GetByPlatform<LuckyGiftData>("lucky_gift_data");
    }

    private void SaveLuckyGiftData()
    {
        XmlPath.SetByPlatform("lucky_gift_data", luckyGiftData);
        Eventon<LuckyGiftData>.Trigger(luckyGiftData);
    }

    private void LoadLuckyBuyData()
    {
        luckyBuyData = new LuckyBuyData()
        {
            round = 0
        };

        if (!File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "lucky_buy_data.xml"))
            return;
        luckyBuyData = XmlPath.GetByPlatform<LuckyBuyData>("lucky_buy_data");
    }

    private void SaveLuckyBuyData()
    {
        XmlPath.SetByPlatform("lucky_buy_data", luckyBuyData);
        Eventon<LuckyBuyData>.Trigger(luckyBuyData);
    }

    private void LoadLuckySpinData()
    {
        string[] groups = Enum.GetNames(typeof(LUCKY_SPIN_GROUP)).ToArray();
        luckySpinData = new LuckySpinData();
        for (int i = 0; i < groups.Length; i++)
        {
            LUCKY_SPIN_GROUP spinGroup = LUCKY_SPIN_GROUP.GROUP_0;
            Enum.TryParse(groups[i], out spinGroup);
            LuckySpinGroup luckySpinGroup = new LuckySpinGroup()
            {
                luckySpinGroup = spinGroup,
                round = 0
            };
            luckySpinData.groups.Add(luckySpinGroup);
        }

        if (!File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "lucky_spin_data.xml"))
            return;
        luckySpinData = XmlPath.GetByPlatform<LuckySpinData>("lucky_spin_data");
    }

    private void SaveLuckySpinData()
    {
        XmlPath.SetByPlatform("lucky_spin_data", luckySpinData);
        Eventon<LuckySpinData>.Trigger(luckySpinData);
    }

    private IEnumerator DelayShowSaleFeature(SALE_FEATURE feature, float time)
    {
        yield return new WaitForSeconds(time);
        switch (feature)
        {
            case SALE_FEATURE.SKIN_FREE:
                UISkinFree.Show();
                break;
            case SALE_FEATURE.LUCKY_GIFT:
                UILuckyGift.Show();
                if (luckyGiftData != null)
                {
                    luckyGiftData.round += 1;
                    if (luckyGiftData.round >= luckyGiftRounds.Length)
                        luckyGiftData.round = 1;
                }

                SaveLuckyGiftData();

                break;
//            case SALE_FEATURE.LUCKY_SPIN:
//                UILuckySpin.Show();
//                break;
            case SALE_FEATURE.LUCKY_BUY:
                if (luckyBuyData != null)
                {
                    string[] luckyBuyGroups = Enum.GetNames(typeof(LUCKY_BUY_GROUP)).ToArray();
                    int enableSlot = luckyBuyGroups.Length;
                    for (int i = 0; i < luckyBuyGroups.Length; i++)
                    {
                        LUCKY_BUY_GROUP group = LUCKY_BUY_GROUP.GROUP_0;
                        Enum.TryParse(luckyBuyGroups[i], out group);
                        GameEnums.ITEM_GROUP itemGroup = GetLuckyBuyItemGroup(group, luckyBuyData.round);
                        string itemName = GetLuckyBuyItem(group, luckyBuyData.round);
                        switch (itemGroup)
                        {
                            case GameEnums.ITEM_GROUP.Weapon:
                                GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                                Enum.TryParse(itemName, out weapon);
                                if (Singleton<WeaponManager>.Instance != null &&
                                    Singleton<WeaponManager>.Instance.IsUnlocked(weapon))
                                    enableSlot -= 1;
                                break;
                            case GameEnums.ITEM_GROUP.Outfit:
                                GameEnums.HERO hero = GameEnums.HERO.h0;
                                Enum.TryParse(itemName, out hero);
                                if (Singleton<CharacterManager>.Instance != null &&
                                    Singleton<CharacterManager>.Instance.IsUnlocked(hero))
                                    enableSlot -= 1;
                                break;
                        }
                    }

                    if (enableSlot > 0)
                    {
                        UILuckyBuy.Show();
                        UpLuckyBuyRound();
                    }
                }

                break;
        }

        UpSaleFeatureRound(feature);
    }

    private void UpSaleFeatureRound(SALE_FEATURE feature)
    {
        if (saleFeatureData == null)
            return;
        int index = 0;
        string[] features = Enum.GetNames(typeof(SALE_FEATURE)).ToArray();
        SALE_FEATURE tempFeature = SALE_FEATURE.SKIN_FREE;
        for (int i = 0; i < features.Length; i++)
        {
            Enum.TryParse(features[i], out tempFeature);
            if (tempFeature != feature)
                continue;
            index = i + 1;
            if (index >= features.Length)
                index = 0;
        }

        Enum.TryParse(features[index], out tempFeature);
        saleFeatureData.feature = tempFeature;
        SaveSaleFeatureData();
    }

    private void UpLuckyBuyRound()
    {
        if (luckyBuyData == null)
            return;
        luckyBuyData.round += 1;
        if (luckyBuyData.round >= GameConfig.luckyBuyMax)
            luckyBuyData.round = 0;
        SaveLuckyBuyData();
    }

    public void ShowSaleFeature()
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (gameManager.IsTest())
            return;

        if (saleFeatureData == null || Singleton<CharacterManager>.Instance == null ||
            Singleton<WeaponManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        SALE_FEATURE feature = saleFeatureData.feature;
        bool isShow = true;
        switch (feature)
        {
            case SALE_FEATURE.SKIN_FREE:
                feature = SALE_FEATURE.SKIN_FREE;
                break;
            case SALE_FEATURE.LUCKY_GIFT:
                feature = SALE_FEATURE.LUCKY_GIFT;
                break;
//            case SALE_FEATURE.LUCKY_SPIN:
//                feature = SALE_FEATURE.LUCKY_SPIN;
//                break;
            case SALE_FEATURE.LUCKY_BUY:
                feature = SALE_FEATURE.LUCKY_BUY;
                break;
        }

        if (characterManager.IsFullUnlock() || weaponManager.IsFullUnlock())
            isShow = false;

        if (isShow)
            StartCoroutine(DelayShowSaleFeature(feature, 1.0f));
    }

    public void UpLuckySpinRound(LUCKY_SPIN_GROUP luckySpinGroup)
    {
        if (luckySpinData == null || luckySpinDataset == null)
            return;
        int maxRound = luckySpinDataset.GetBaseLenght(luckySpinGroup);
        for (int i = 0; i < luckySpinData.groups.Count; i++)
        {
            if (luckySpinData.groups[i].luckySpinGroup != luckySpinGroup)
                continue;
            luckySpinData.groups[i].round += 1;
            if (luckySpinData.groups[i].round < maxRound)
                continue;
            luckySpinData.groups[i].round = 0;
            SaveLuckySpinData();
        }
    }

    public void OnLuckySpinCheckingRound()
    {
        if (luckySpinDataset == null || Singleton<WeaponManager>.Instance == null ||
            Singleton<CharacterManager>.Instance == null)
            return;
        WeaponManager weaponManager = Singleton<WeaponManager>.Instance;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;

        for (int i = 0; i < luckySpinDataset.GetBase(); i++)
        {
            LUCKY_SPIN_GROUP spinGroup =
                luckySpinDataset.GetLuckySpinBase(luckySpinDataset.GetLuckySpinGroup(i)).spinGroup;
            int slot = luckySpinDataset.GetLuckySpinBase(luckySpinDataset.GetLuckySpinGroup(i)).spinSlots.Count;
            for (int j = 0; j < slot; j++)
            {
                GameEnums.ITEM_GROUP itemGroup = luckySpinDataset.GetItemGroup(spinGroup, j);
                string itemName = luckySpinDataset.GetItemName(spinGroup, j);
                switch (itemGroup)
                {
                    case GameEnums.ITEM_GROUP.Weapon:
                        GameEnums.WEAPON weapon = GameEnums.WEAPON.NONE;
                        Enum.TryParse(itemName, out weapon);
                        if (weaponManager.IsUnlocked(weapon))
                            UpLuckySpinRound(spinGroup);
                        break;

                    case GameEnums.ITEM_GROUP.Outfit:
                        GameEnums.HERO hero = GameEnums.HERO.h0;
                        Enum.TryParse(itemName, out hero);
                        if (characterManager.IsUnlocked(hero))
                            UpLuckySpinRound(spinGroup);
                        break;
                }
            }
        }
    }

    public List<LUCKY_GIFT_GROUP> GetLuckyGiftGroups()
    {
        List<LUCKY_GIFT_GROUP> groups = new List<LUCKY_GIFT_GROUP>();
        int round = 0;

        if (luckyGiftData != null)
            round = luckyGiftData.round;

        for (int i = 0; i < luckyGiftRounds[round].groups.Length; i++)
        {
            groups.Add(luckyGiftRounds[round].groups[i]);
        }

        return groups;
    }

    public GameEnums.GENERAL GetLuckyGiftGeneral(LUCKY_GIFT_GROUP group, int index)
    {
        GameEnums.GENERAL general = GameEnums.GENERAL.Gold;
        if (luckyGiftDataset != null)
            general = luckyGiftDataset.GetGeneral(group, index);
        return general;
    }

    public GameEnums.ITEM_GROUP GetLuckyBuyItemGroup(LUCKY_BUY_GROUP luckyBuyGroup, int index)
    {
        GameEnums.ITEM_GROUP itemGroup = GameEnums.ITEM_GROUP.General;
        if (luckyBuyDataset != null)
            itemGroup = luckyBuyDataset.GetItemGroup(luckyBuyGroup, index);
        return itemGroup;
    }

    public GameEnums.ITEM_GROUP GetLuckySpinItemGroup(LUCKY_SPIN_GROUP luckySpinGroup)
    {
        GameEnums.ITEM_GROUP itemGroup = GameEnums.ITEM_GROUP.General;
        if (luckySpinData != null && luckySpinDataset != null)
        {
            int round = 0;
            for (int i = 0; i < luckySpinData.groups.Count; i++)
            {
                if (luckySpinData.groups[i].luckySpinGroup == luckySpinGroup)
                {
                    round = luckySpinData.groups[i].round;
                    itemGroup = luckySpinDataset.GetItemGroup(luckySpinGroup, round);
                }
            }
        }

        return itemGroup;
    }

    public string GetLuckyBuyItem(LUCKY_BUY_GROUP group, int index)
    {
        string name = null;
        if (luckyBuyDataset != null)
            name = luckyBuyDataset.GetItemName(group, index);
        return name;
    }

    public string GetSpinSlotItem(LUCKY_SPIN_GROUP luckySpinGroup)
    {
        string name = null;
        if (luckySpinData != null && luckySpinDataset != null)
        {
            int round = 0;
            for (int i = 0; i < luckySpinData.groups.Count; i++)
            {
                if (luckySpinData.groups[i].luckySpinGroup == luckySpinGroup)
                {
                    round = luckySpinData.groups[i].round;
                    name = luckySpinDataset.GetItemName(luckySpinGroup, round);
                }
            }
        }

        return name;
    }

    public int GetLuckyGiftLenght(LUCKY_GIFT_GROUP group)
    {
        int number = 0;
        if (luckyGiftDataset != null)
            number = luckyGiftDataset.GetLenght(group);
        return number;
    }

    public int GetLuckyGiftAmount(LUCKY_GIFT_GROUP group, int index)
    {
        int number = 0;
        if (luckyGiftDataset != null)
            number = luckyGiftDataset.GetAmount(group, index);
        return number;
    }

    public int GetLuckyBuyRound()
    {
        int round = 0;
        if (luckyBuyData != null)
            round = luckyBuyData.round;
        return round;
    }

    public int GetLuckySpinAmount(LUCKY_SPIN_GROUP luckySpinGroup)
    {
        int number = 0;
        if (luckySpinData != null && luckySpinDataset != null)
        {
            int round = 0;
            for (int i = 0; i < luckySpinData.groups.Count; i++)
            {
                if (luckySpinData.groups[i].luckySpinGroup == luckySpinGroup)
                {
                    round = luckySpinData.groups[i].round;
                    number = luckySpinDataset.GetItemAmount(luckySpinGroup, round);
                }
            }
        }

        return number;
    }
}