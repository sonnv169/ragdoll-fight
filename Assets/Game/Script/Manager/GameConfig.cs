﻿using UnityEngine;

public class GameConfig : MonoBehaviour
{
    public static Vector3 infernal = new Vector3(0, -1000, 0);
    public static Vector3 modelUIScale = new Vector3(80, 80, 80);
    public static Vector3 modelIngameScale = new Vector3(1, 1, 1);
    public static Vector3 weaponIngameScale = new Vector3(1.5f, 1.5f, 1.5f);
    public static Vector3 cursorPosition = new Vector3(0, 2.5f, 0);

    public static string storeLink =
        "https://play.google.com/store/apps/details?id=com.os.falcon.cyforce.infinity.stickman.fight";

    public static float zoomTime = 1.5f;
    public static float initTime = 0.01f;
    public static float deathTime = 2.75f;
    public static float dropingTime = 1.0f;
    public static float boostedTime = 2.0f;
    public static float revivalDelay = 2.5f;
    public static float revivalTime = 10.0f;

    public static float originTime = 1.25f;
    public static float slowTime = 0.5f;
    public static float slowDuration = 0.5f;

    public static float characterStartRange = 15.5f;
    public static float flyingSpeed = 30.5f;
    public static float winSpeed = 15.5f;
    public static float balanceSpeed = 7.5f;
    public static float balanceSpeedAbs = 5.5f;
    public static float xTrackingRange = 25.0f;
    public static float yTrackingRange = 35.0f;

    public static float enemyBonusSpeed = 50.5f;
    public static float enemyFocusFailRange = 0.05f;
    public static float enemyStopTime = 1.5f;
    public static float enemyInitTime = 2.25f;

    public static float collisionPower = 35.5f;
    public static float hitDuration = 1.55f;
    public static float explorePower = 15.5f;

    public static float hpDuration = 0.5f;
    public static float shieldDuration = 5.0f;
    public static float doublePowerDuration = 5.0f;
    public static float weaponDuration = 0.5f;

    public static float backStackRange = 0.5f;
    public static float backStackClamp = 5.25f;
    public static float jointDuration = 1.5f;
    public static float hookSpeed = 25.5f;

    public static int faceScale = 1;
    public static int dropHit = 10;
    public static int bodyPathIntact = 2;
    public static int enemyBonusHp = 100;
    public static int sortingOrderOnUI = 750;

    public static int getMoreReward = 1500;
    public static int getFreeReward = 1500;
    public static int getFreeTime = 10;

    public static int hpRegenPrice = 500;
    public static int doublePowerPrice = 500;
    public static int shieldPrice = 500;

    public static int vipLevel = 4;
    public static int rewardDelay = 60;
    public static int maxEnemy = 3;
    public static int maxLootDrop = 4;
    public static int giftOnWeek = 7;

    public static int luckyBuyMax = 17;
    public static int salePercent = 70;
    public static int spinFee = 1000;

    public static int timeToInterstitial = 1;
    public static int stageToRating = 3;
    public static int minRateToStore = 5;
}