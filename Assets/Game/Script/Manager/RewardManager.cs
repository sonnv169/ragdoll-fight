﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class WeaponRewardData
{
    public TimeOfYear time;
    public int delaySecond;
}

[System.Serializable]
public class OutfitRewardData
{
    public TimeOfYear time;
    public int delaySecond;
}

[System.Serializable]
public class DailyGiftData
{
    public List<bool> getedDays = new List<bool>();
    public TimeOfYear time;
    public int loginDay;
}

public class RewardManager : ToggleBehaviour
{
    private WeaponRewardData weaponRewardData;
    private OutfitRewardData outfitRewardData;
    private DailyGiftData dailyGiftData;
    private bool isVisible;

    [SerializeField] private DailyRewardDataset dailyRewardDataset;

    protected override void OnAppear()
    {
        if (!isVisible)
        {
            InitDailyGiftData();
            LoadWeaponRewardData();
            LoadOutfitRewardData();
            isVisible = true;
        }
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        //nothing
    }

    private bool IsNextDay(TimeOfYear time)
    {
        return DateTime.Today.Year > time.year ||
               DateTime.Today.Year == time.year && DateTime.Today.Month > time.month ||
               DateTime.Today.Year == time.year && DateTime.Today.Month == time.month && DateTime.Today.Day > time.day;
    }

    private IEnumerator OnDownWeaponRewardDelay()
    {
        yield return new WaitForSeconds(1);
        if (weaponRewardData != null && weaponRewardData.delaySecond > 0)
        {
            weaponRewardData.delaySecond -= 1;
            SaveWeaponRewardData();
            StartCoroutine(OnDownWeaponRewardDelay());
        }
    }

    private IEnumerator OnDownOutfitRewardDelay()
    {
        yield return new WaitForSeconds(1);
        if (outfitRewardData != null && outfitRewardData.delaySecond > 0)
        {
            outfitRewardData.delaySecond -= 1;
            SaveOutfitRewardData();
            StartCoroutine(OnDownOutfitRewardDelay());
        }
    }

    private void LoadWeaponRewardData()
    {
        weaponRewardData = new WeaponRewardData()
        {
            time = new TimeOfYear()
            {
                day = DateTime.Today.Day,
                month = DateTime.Today.Month,
                year = DateTime.Today.Year
            },
            delaySecond = 0
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "weapon_reward_data.xml"))
            weaponRewardData = XmlPath.GetByPlatform<WeaponRewardData>("weapon_reward_data");

        if (!IsNextDay(weaponRewardData.time))
            StartCoroutine(OnDownWeaponRewardDelay());
    }

    private void LoadOutfitRewardData()
    {
        outfitRewardData = new OutfitRewardData()
        {
            time = new TimeOfYear()
            {
                day = DateTime.Today.Day,
                month = DateTime.Today.Month,
                year = DateTime.Today.Year
            },
            delaySecond = 0
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "outfit_reward_data.xml"))
            outfitRewardData = XmlPath.GetByPlatform<OutfitRewardData>("outfit_reward_data");

        if (!IsNextDay(outfitRewardData.time))
            StartCoroutine(OnDownOutfitRewardDelay());
    }

    private void InitDailyGiftData()
    {
        dailyGiftData = new DailyGiftData();
        for (int i = 0; i < GetGiftDay(); i++)
        {
            dailyGiftData.getedDays.Add(false);
        }

        dailyGiftData.time = new TimeOfYear()
        {
            day = DateTime.Today.Day,
            month = DateTime.Today.Month,
            year = DateTime.Today.Year
        };
        dailyGiftData.loginDay = 0;

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "daily_gift_data.xml"))
            dailyGiftData = XmlPath.GetByPlatform<DailyGiftData>("daily_gift_data");

        if (!IsNextDay(dailyGiftData.time))
            return;
        dailyGiftData.time = new TimeOfYear()
        {
            day = DateTime.Today.Day,
            month = DateTime.Today.Month,
            year = DateTime.Today.Year
        };
        dailyGiftData.loginDay += 1;
    }

    private void SaveWeaponRewardData()
    {
        XmlPath.SetByPlatform("weapon_reward_data", weaponRewardData);
        Eventon<WeaponRewardData>.Trigger(weaponRewardData);
    }

    private void SaveOutfitRewardData()
    {
        XmlPath.SetByPlatform("outfit_reward_data", outfitRewardData);
        Eventon<OutfitRewardData>.Trigger(outfitRewardData);
    }

    public void SaveDailyGiftData(int day)
    {
        dailyGiftData.time = new TimeOfYear()
        {
            day = DateTime.Today.Day,
            month = DateTime.Today.Month,
            year = DateTime.Today.Year
        };
        dailyGiftData.getedDays[day] = true;

        XmlPath.SetByPlatform("daily_gift_data", dailyGiftData);
        Eventon<DailyGiftData>.Trigger(dailyGiftData);
    }

    public bool IsWeaponReward()
    {
        return weaponRewardData != null && weaponRewardData.delaySecond <= 0 ||
               weaponRewardData != null && IsNextDay(weaponRewardData.time);
    }

    public bool IsOutfitReward()
    {
        return outfitRewardData != null && outfitRewardData.delaySecond <= 0 ||
               outfitRewardData != null && IsNextDay(outfitRewardData.time);
    }

    public void SetWeaponRewardDelay()
    {
        weaponRewardData = new WeaponRewardData()
        {
            time = new TimeOfYear()
            {
                day = DateTime.Today.Day,
                month = DateTime.Today.Month,
                year = DateTime.Today.Year
            },
            delaySecond = GameConfig.rewardDelay
        };
        SaveWeaponRewardData();
        StartCoroutine(OnDownWeaponRewardDelay());
    }

    public void SetOutfitRewardDelay()
    {
        outfitRewardData = new OutfitRewardData()
        {
            time = new TimeOfYear()
            {
                day = DateTime.Today.Day,
                month = DateTime.Today.Month,
                year = DateTime.Today.Year
            },
            delaySecond = GameConfig.rewardDelay
        };
        SaveOutfitRewardData();
        StartCoroutine(OnDownOutfitRewardDelay());
    }

    public List<RewardBase> GetRewardBases(int day)
    {
        List<RewardBase> rewardBases = new List<RewardBase>();
        if (dailyRewardDataset != null && day < dailyRewardDataset.GetRewardDay())
            rewardBases = dailyRewardDataset.GetRewardBases(day);
        return rewardBases;
    }

    public int GetWeaponRewardDelay()
    {
        int number = 0;
        if (weaponRewardData != null)
            number = weaponRewardData.delaySecond;
        return number;
    }

    public int GetOutfitRewardDelay()
    {
        int number = 0;
        if (outfitRewardData != null)
            number = outfitRewardData.delaySecond;
        return number;
    }

    public int GetGiftDay()
    {
        int number = 0;
        if (dailyRewardDataset != null)
            number = dailyRewardDataset.GetRewardDay();
        return number;
    }

    public int GetGiftedDay()
    {
        int number = 0;
        if (dailyGiftData != null)
        {
            for (int i = 0; i < dailyGiftData.getedDays.Count; i++)
            {
                if (dailyGiftData.getedDays[i])
                    number += 1;
            }
        }

        return number;
    }

    public int GetLoginDay()
    {
        int number = 0;
        if (dailyGiftData != null)
            number = dailyGiftData.loginDay;
        return number;
    }

    public bool IsDayGeted(int day)
    {
        return dailyGiftData != null && day < dailyGiftData.getedDays.Count && dailyGiftData.getedDays[day];
    }

    public bool IsGift()
    {
        bool isResult = false;
        if (dailyGiftData != null)
            for (int i = 0; i < dailyGiftData.loginDay; i++)
            {
                if (!dailyGiftData.getedDays[i])
                    isResult = true;
            }

        return isResult;
    }
}