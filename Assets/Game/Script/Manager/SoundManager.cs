﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static List<AudioClip> audioClips = new List<AudioClip>();

    private void OnEnable()
    {
        Init();
    }

    private void Init()
    {
        string[] audios = Enum.GetNames(typeof(GameEnums.AUDIO_CLIP)).ToArray();
        for (int i = 0; i < audios.Length; i++)
        {
            string path = "Sound/" + audios[i];
            if (Resources.Load(path) == null)
                continue;
            AudioClip clip = Resources.Load(path) as AudioClip;
            if (!audioClips.Contains(clip))
                audioClips.Add(clip);
        }
    }

    public static void Play(AUDIO_KIND kind, GameEnums.AUDIO_CLIP audioClip, bool isLoop)
    {
        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        AudioClip clip = null;
        for (int i = 0; i < audioClips.Count; i++)
        {
            if (audioClips[i].name == audioClip.ToString())
                clip = audioClips[i];
        }

        if (clip != null)
            audioManager.Play(kind, clip, isLoop);
    }

    public static void Stop(AUDIO_KIND kind)
    {
        if (Singleton<AudioManager>.Instance == null)
            return;
        AudioManager audioManager = Singleton<AudioManager>.Instance;
        audioManager.Stop(kind);
    }
}