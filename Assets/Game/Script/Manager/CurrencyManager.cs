using System.IO;
using UnityEngine;

public enum CURRENCY
{
    COIN,
    HP_REGEN,
    SHIELD,
    DOUBLE_POWER
}

[System.Serializable]
public class CurrencyData
{
    public int coin;
    public int hpRegen;
    public int sheild;
    public int doublePower;
}

[System.Serializable]
public class CurrencyEvent
{
    public bool isUpdate;
}

public class CurrencyManager : MonoBehaviour
{
    private CurrencyData currencyData;
    private bool isVisible;

    private void OnEnable()
    {
        if (!isVisible)
        {
            Load();
            isVisible = true;
        }
    }

    private void Load()
    {
        currencyData = new CurrencyData()
        {
            coin = 2000,
            hpRegen = 1,
            sheild = 1,
            doublePower = 1
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "currency_data.xml"))
            currencyData = XmlPath.GetByPlatform<CurrencyData>("currency_data");

        if (Singleton<GameManager>.Instance != null)
        {
            GameManager gameManager = Singleton<GameManager>.Instance;
            if (gameManager.IsTest())
                currencyData.coin = 9999999;
        }
    }

    private void Save()
    {
        XmlPath.SetByPlatform("currency_data", currencyData);
        Eventon<CurrencyData>.Trigger(currencyData);
    }

    public void Collect(CURRENCY currency, int value)
    {
        switch (currency)
        {
            case CURRENCY.COIN:
                currencyData.coin += value;
                SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_gold_collection, false);
                break;
            case CURRENCY.HP_REGEN:
                currencyData.hpRegen += value;
                break;
            case CURRENCY.SHIELD:
                currencyData.sheild += value;
                break;
            case CURRENCY.DOUBLE_POWER:
                currencyData.doublePower += value;
                break;
        }

        Save();
    }

    public void Pay(CURRENCY currency, int value)
    {
        switch (currency)
        {
            case CURRENCY.COIN:
                currencyData.coin -= value;
                if (currencyData.coin <= 0)
                    currencyData.coin = 0;
                break;
            case CURRENCY.HP_REGEN:
                currencyData.hpRegen -= value;
                if (currencyData.hpRegen <= 0)
                    currencyData.hpRegen = 0;
                break;
            case CURRENCY.SHIELD:
                currencyData.sheild -= value;
                if (currencyData.sheild <= 0)
                    currencyData.sheild = 0;
                break;
            case CURRENCY.DOUBLE_POWER:
                currencyData.doublePower -= value;
                if (currencyData.doublePower <= 0)
                    currencyData.doublePower = 0;
                break;
        }

        Save();
    }

    public void UseBooster(GameEnums.BOOSTER booster)
    {
        if (Singleton<Player>.Instance == null)
            return;
        Player player = Singleton<Player>.Instance;
        int number = 0;

        switch (booster)
        {
            case GameEnums.BOOSTER.HP_REGEN:
                number = GetCurrency(CURRENCY.HP_REGEN);
                break;
            case GameEnums.BOOSTER.SHIELD:
                number = GetCurrency(CURRENCY.SHIELD);
                break;
            case GameEnums.BOOSTER.DOUBLE_POWER:
                number = GetCurrency(CURRENCY.DOUBLE_POWER);
                break;
        }

        if (number > 0)
        {
            switch (booster)
            {
                case GameEnums.BOOSTER.HP_REGEN:
                    player.SetHpRegen();
                    Pay(CURRENCY.HP_REGEN, 1);
                    break;
                case GameEnums.BOOSTER.SHIELD:
                    player.SetShield();
                    Pay(CURRENCY.SHIELD, 1);
                    break;
                case GameEnums.BOOSTER.DOUBLE_POWER:
                    player.SetDoublePower();
                    Pay(CURRENCY.DOUBLE_POWER, 1);
                    break;
            }

            Eventon<BoosterEvent>.Trigger(new BoosterEvent()
            {
                booster = booster,
                isCasting = true
            });
        }
        else
        {
            AdReward.Show();
            Eventon<AD_STATE>.EventDynamic += state =>
            {
                if (state != AD_STATE.FINISHED)
                    return;
                string boosterName = null;
                switch (booster)
                {
                    case GameEnums.BOOSTER.HP_REGEN:
                        player.SetHpRegen();
                        boosterName = "hp";
                        break;
                    case GameEnums.BOOSTER.SHIELD:
                        player.SetShield();
                        boosterName = "shield";
                        break;
                    case GameEnums.BOOSTER.DOUBLE_POWER:
                        player.SetDoublePower();
                        boosterName = "x2damage";
                        break;
                    case GameEnums.BOOSTER.WEAPON:
                        player.SetWeaponRandom();
                        boosterName = "get_weapon";
                        break;
                }

                Singleton<GameManager>.Instance.OnLogAd(AdsType.Reward, "get" + boosterName, "btn_booster");

                Eventon<BoosterEvent>.Trigger(new BoosterEvent()
                {
                    booster = booster,
                    isCasting = true
                });
            };
        }
    }

    public int GetCurrency(CURRENCY currency)
    {
        int number = 0;

        switch (currency)
        {
            case CURRENCY.COIN:
                number = currencyData.coin;
                break;
            case CURRENCY.HP_REGEN:
                number = currencyData.hpRegen;
                break;
            case CURRENCY.SHIELD:
                number = currencyData.sheild;
                break;
            case CURRENCY.DOUBLE_POWER:
                number = currencyData.doublePower;
                break;
        }

        return number;
    }
}