﻿using System.IO;
using System.Collections;
using UnityEngine;

[System.Serializable]
public enum GAME_STATE
{
    NONE,
    PLAY,
    WIN,
    LOSE,
    CLEAR
}

[System.Serializable]
public class GetFreeData
{
    public int time;
}

[System.Serializable]
public class StoreData
{
    public bool isRemoveAds;
    public bool isRated;
    public int rated;
}

[System.Serializable]
public struct PassLevelStatus
{
    public bool isStatus;
}

[System.Serializable]
public enum AdsType
{
    None,
    Banner,
    Inter,
    Reward
}

public class GameManager : ToggleBehaviour
{
    private GAME_STATE gameState;
    private GetFreeData getFreeData;
    private StoreData storeData;
    private bool isVisible;
    private int timeToLoad = 3;
    private int loadedTime;

    [SerializeField] private bool isTest;

    protected override void OnAppear()
    {
        DontDestroyOnLoad(this);

        if (!isVisible)
        {
            Init();
            Eventon<GAME_STATE>.EventStatic += OnGameState;
            isVisible = true;
        }
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
    }

    private void OnGameState(GAME_STATE param)
    {
        if (GetState() == param)
            return;
        SetState(param);
        switch (param)
        {
            case GAME_STATE.WIN:
                SoundManager.Play(AUDIO_KIND.SOUND, GameEnums.AUDIO_CLIP.sfx_you_win, false);
                break;
            case GAME_STATE.LOSE:
                StartCoroutine(DelayLose(GameConfig.revivalDelay));
                break;
        }
    }

    private IEnumerator DelayLose(float time)
    {
        yield return new WaitForSeconds(time);
        UIRevival.Show();
    }

    private void LoadGetFreeData()
    {
        getFreeData = new GetFreeData()
        {
            time = GameConfig.getFreeTime
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "getfree_data.xml"))
            getFreeData = XmlPath.GetByPlatform<GetFreeData>("getfree_data");
    }

    private void SaveGetFreeData()
    {
        XmlPath.SetByPlatform("getfree_data", getFreeData);
        Eventon<GetFreeData>.Trigger(getFreeData);
    }

    private void LoadStoreData()
    {
        storeData = new StoreData()
        {
            isRemoveAds = false,
            isRated = false,
            rated = -1
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "store_data.xml"))
            storeData = XmlPath.GetByPlatform<StoreData>("store_data");

        if (isTest)
        {
            storeData.isRemoveAds = true;
            storeData.isRated = true;
        }
    }

    private void SaveStoreData()
    {
        XmlPath.SetByPlatform("store_data", storeData);
        Eventon<StoreData>.Trigger(storeData);
    }

    private IEnumerator OnLoadBanner()
    {
        yield return new WaitForSeconds(2.0f);
        if (loadedTime < timeToLoad)
        {
#if UNITY_EDITOR
            Debug.Log("Show banner!");
#endif
            IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
            loadedTime += 1;

            StartCoroutine(OnLoadBanner());
        }
    }

    private void Init()
    {
        Application.targetFrameRate = 60;
        Time.timeScale = GameConfig.originTime;

        LoadGetFreeData();
        LoadStoreData();

        if (!IsRemoveAds())
        {
#if UNITY_EDITOR
            Debug.Log("Show banner!");
#endif
            IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
            StartCoroutine(OnLoadBanner());
        }
    }

    public void SetState(GAME_STATE state)
    {
        gameState = state;
    }

    public void MinusGetFreeTime(int value)
    {
        if (getFreeData == null)
            return;
        getFreeData.time -= value;
        SaveGetFreeData();
    }

    public void EnableAds(bool isSatus)
    {
        if (storeData == null)
            return;
        storeData.isRemoveAds = !isSatus;

        if (!isSatus)
            IronSource.Agent.destroyBanner();

        SaveStoreData();
    }

    public void EnableRating(bool isStatus, int rate)
    {
        if (storeData == null)
            return;
        storeData.isRated = !isStatus;
        storeData.rated = rate;
        SaveStoreData();
    }

    public void OnLogLevel(PassLevelStatus status)
    {
        int level = Singleton<StageManager>.Instance.GetStage() + 1;
    }

    public void OnLogAd(AdsType adsType, string adName, string adWhere)
    {
        int level = Singleton<StageManager>.Instance.GetStage() + 1;
    }

    public GAME_STATE GetState()
    {
        return gameState;
    }

    public int GetFreeTime()
    {
        int number = 0;
        if (getFreeData != null)
            number = getFreeData.time;
        return number;
    }

    public int GetRated()
    {
        int number = 0;
        if (storeData != null)
            number = storeData.rated;
        return number;
    }

    public bool IsRemoveAds()
    {
        bool isResult = false;
        if (storeData != null)
            isResult = storeData.isRemoveAds;
        return isResult;
    }

    public bool IsRated()
    {
        bool isResult = false;
        if (storeData != null)
            isResult = storeData.isRated;
        return isResult;
    }

    public int GetFreeTimeGeted()
    {
        int number = 0;
        if (getFreeData != null)
            number = getFreeData.time;
        return number;
    }

    public bool IsTest()
    {
        return isTest;
    }
}