﻿using System;
using System.IO;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class StageData
{
    public int stage;
}

public class StageManager : ToggleBehaviour
{
    private Coroutine initEnemyCoroutine;
    private StageData stageData;
    private Stage stage;
    private int target;
    private int wave;
    private bool isVisible;

    [SerializeField] private StageDataset stageDataset;
    [SerializeField] private WonRewardDataset wonRewardDataset;
    [SerializeField] private TrapDataset trapDataset;

    protected override void OnAppear()
    {
        if (!isVisible)
        {
            Load();
            Eventon<GAME_STATE>.EventStatic += OnGameState;
            Eventon<TutorialData>.EventStatic += OnTutorialData;
            isVisible = true;
        }
    }

    protected override void OnCyclic()
    {
        //nothing
    }

    protected override void OnDisappear()
    {
        Eventon<GAME_STATE>.EventStatic -= OnGameState;
        Eventon<TutorialData>.EventStatic -= OnTutorialData;
    }

    private void OnTutorialData(TutorialData param)
    {
        if (Singleton<GameManager>.Instance == null)
            return;
        GameManager gameManager = Singleton<GameManager>.Instance;
        if (!param.isMove || gameManager.GetState() != GAME_STATE.PLAY)
            return;
        SetEnemy();
        UIFight.Show();
    }

    private void OnGameState(GAME_STATE param)
    {
        switch (param)
        {
            case GAME_STATE.NONE:
            case GAME_STATE.CLEAR:
                if (initEnemyCoroutine != null)
                    StopCoroutine(initEnemyCoroutine);
                target = 0;
                wave = 0;
                break;

            case GAME_STATE.WIN:
                UnlockStage();
                break;
        }
    }

    private void Load()
    {
        stageData = new StageData()
        {
            stage = 0
        };

        if (File.Exists(PlatformPath.GetPath(PlatformPath.GetPlatform()) + "stage_data.xml"))
            stageData = XmlPath.GetByPlatform<StageData>("stage_data");
    }

    private void Save()
    {
        XmlPath.SetByPlatform("stage_data", stageData);
        Eventon<StageData>.Trigger(stageData);
    }

    private void SetEnemy()
    {
        if (Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;

        GameObject obj = characterManager.SetCharacter(CHARACTER.ENEMY, transform.position);
        obj.transform.position = new Vector2(transform.position.x + GameConfig.characterStartRange,
            transform.position.y);

        if (UIGameplay.Instance == null)
            return;
        UIGameplay uiGameplay = UIGameplay.Instance;
        int hp = GetEnemyHp(GetStage());
        uiGameplay.EnableLblEnemy(true);
        uiGameplay.DisplayEnemyHp(hp, hp);
        uiGameplay.SortLblEnemy();
        uiGameplay.SortEnemyAvatar();

        wave += 1;
    }

    private bool IsWin()
    {
        return target <= 0;
    }

    private bool IsOutStageRange(int index)
    {
        return stageDataset != null && index > stageDataset.GetStageBase();
    }

    private int GetEnemyNumber(int index)
    {
        int number = 0;
        if (!IsOutStageRange(index))
            number = stageDataset.GetEnemyNumber(index);
        return number;
    }

    private IEnumerator DelaySetEnemy()
    {
        yield return new WaitForSeconds(GameConfig.enemyInitTime);
        SetEnemy();
    }

    private void SetPlayer()
    {
        if (Singleton<CharacterManager>.Instance == null)
            return;
        CharacterManager characterManager = Singleton<CharacterManager>.Instance;
        GameObject obj = characterManager.SetCharacter(CHARACTER.PLAYER, transform.position);
        obj.transform.position = new Vector2(transform.position.x - GameConfig.characterStartRange,
            transform.position.y);
        if (!obj.GetComponent<Player>())
            obj.AddComponent<Player>();
        Player player = obj.GetComponent<Player>();
        player.Init();
        UIGameplay.Instance.SetPlayerAvatar();
    }

    public void UnlockStage()
    {
        if (stageData == null || stageData.stage >= stageDataset.GetStageBase())
            return;
        stageData.stage += 1;
        Save();
    }

    public void DownTarget(int value)
    {
        target -= value;
        if (UIGameplay.Instance != null)
        {
            UIGameplay uiGameplay = UIGameplay.Instance;
            if (target <= 0)
                uiGameplay.EnableLblEnemy(false);
        }

        if (!IsWin())
            initEnemyCoroutine = StartCoroutine(DelaySetEnemy());
        else
            Eventon<GAME_STATE>.Trigger(GAME_STATE.WIN);
    }

    public void CreatState(int index)
    {
        Eventon<GAME_STATE>.Trigger(GAME_STATE.PLAY);

        target += GetEnemyNumber(index);
        UIGameplay.Instance.InitEnemyAvatar(target);
        SetPlayer();

        int stageResult = 0;
        if (stageData.stage > 0)
            stageResult = Random.Range(1, 9);

        if (Singleton<TutorialManager>.Instance != null &&
            Singleton<TutorialManager>.Instance.GetTutorialData().isBooster)
        {
            SetEnemy();
            UIFight.Show();
        }

        string path = "Prefab/Object/Stage/stage" + stageResult;
        GameObject obj = ObjectManager.Call<GameObject>(transform, transform, path);
        if (obj.GetComponent<Stage>())
            stage = obj.GetComponent<Stage>();

        int result = Random.Range(0, 2);
        string clipName = "sbg_ingame_" + result;
        GameEnums.AUDIO_CLIP clip = GameEnums.AUDIO_CLIP.None;
        Enum.TryParse(clipName, out clip);
        SoundManager.Play(AUDIO_KIND.MUSIC, clip, true);
    }

    public GameEnums.ENEMY GetEnemyID(int index)
    {
        GameEnums.ENEMY enemy = GameEnums.ENEMY.e0;
        if (!IsOutStageRange(GetStage() + index))
            enemy = stageDataset.GetEnemyID(GetStage() + index);
        return enemy;
    }

    public GameEnums.WEAPON GetEnemyWeapon(int index)
    {
        GameEnums.WEAPON weapon = GameEnums.WEAPON.w1;
        if (!IsOutStageRange(index))
            weapon = stageDataset.GetWeapon(index);
        return weapon;
    }

    public Stage GetStageObject()
    {
        return stage;
    }

    public Vector3 GetFinishLocation()
    {
        Vector3 location = Vector3.zero;
        if (stage != null)
            location = stage.GetFinishLocation();
        return location;
    }

    public string GetTrapName(GameEnums.TRAP trap)
    {
        string name = null;
        if (trapDataset != null)
            name = trapDataset.GetName(trap);
        return name;
    }

    public int GetWave()
    {
        return wave;
    }

    public int GetStage()
    {
        return stageData.stage;
    }

    public int GetEnemyAtk(int index)
    {
        int number = 0;
        if (!IsOutStageRange(index))
            number = stageDataset.GetAtk(index);
        return number;
    }

    public int GetEnemyHp(int index)
    {
        int number = 0;
        if (!IsOutStageRange(index))
            number = stageDataset.GetHp(index) + GameConfig.enemyBonusHp;
        return number;
    }

    public int GetWonReward(int index)
    {
        int number = wonRewardDataset.GetGold(index);
        return number;
    }

    public int GetTarget()
    {
        return target;
    }

    public int GetTrapDamage(GameEnums.TRAP trap)
    {
        int number = 0;
        if (trapDataset != null)
            number = trapDataset.GetDamage(trap);
        return number;
    }
}